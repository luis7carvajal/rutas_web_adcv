class Productpaseado < ActiveRecord::Base
  self.primary_key = "Id"
  #self.table_name = 'productpaseado' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)

  def self.reporte_inventario_sobrante(params)
    query = select("R.Ruta, FORMAT(O.Fecha,'dd-MM-yyyy') Fecha, O.DiaO, P.Clave, P.Producto Descripcion, ROUND(ProductoPaseado.Stock / PP.PzaXCja, 0) Cajas, (ProductoPaseado.Stock % PP.PzaXCja) Piezas")
            .joins("INNER JOIN Productos P ON P.Clave = ProductoPaseado.CodProd")
            .joins("INNER JOIN ProductosXPzas PP ON PP.Producto = ProductoPaseado.CodProd")
            .joins("LEFT JOIN DiasO O ON O.RutaId = ProductoPaseado.RutaId AND O.DiaO = ProductoPaseado.DiaO AND O.IdEmpresa = ProductoPaseado.IdEmpresa")
            .joins("LEFT JOIN Rutas R ON R.IdRutas = ProductoPaseado.RutaId")
            .where("ProductoPaseado.Stock > 0")
            .where("ProductoPaseado.IdEmpresa = ?", params[:search6])
            .where("ProductoPaseado.RutaId = ?", params[:search])
            .where("O.DiaO = ?", params[:diaO])
    unless params[:Clave_param].blank?
      query = query.where("P.Clave LIKE ?", "%#{params[:Clave_param]}%")
    end
    unless params[:Descripcion_param].blank?
      query = query.where("P.Producto LIKE ?", "%#{params[:Descripcion_param]}%")
    end
    query
  end


end
