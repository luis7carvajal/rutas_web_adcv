class Usuarios::SessionsController < Devise::SessionsController
# before_action :configure_sign_in_params, only: [:create]
prepend_before_filter :verify_user, only: [:destroy]#esto para ver si se arregla el error del token al cerrar sesion
  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
  def busqueda_sucursales #Actualiza las sucursales concorde a las empresas en el acceso
    @sucursales = Empresa.where("empresamadre_id = ?", params[:empresa])
    respond_to do |format|
      format.js
    end
  end

  def error_empresa

  end

  def new
      @empresas = Empresamadre.all
        @sucursales = Empresa.where("empresamadre_id = ?", @empresas.first.id)
        super
  end



  def create
    if usuario_signed_in? and current_usuario.try(:empresa_id) === params[:sucursal] || current_usuario.try(:usuario_sucursales).where(IdEmpresa: params[:sucursal]).any?
      super
      permiso_cambio_sucursal
    else
      destroy
    end
  end

  def permiso_cambio_sucursal
    @Descripcion_Modulo = "Catálogo de usuarios"
    @puede_editar = current_usuario.try(:perfiles).find_by_Descripcion(@Descripcion_Modulo).try(:Modi)
    if @puede_editar == true
      current_usuario.update(empresa_id: params[:sucursal])
    end
  end

  def update
    super
  end

  private
  def sign_up_params
    allow = [:email, :usuario, :password, :password_confirmation, :nombre, :idempresa]
    params.require(resource_name).permit(allow)
  end

#esto para ver si se arregla el error del token al cerrar sesion
  ## This method intercepts SessionsController#destroy action
## If a signed in user tries to sign out, it allows the user to sign out
## If a signed out user tries to sign out again, it redirects them to sign in page
def verify_user
  ## redirect to appropriate path
  redirect_to new_usuario_session_path, notice: 'You have already signed out. Please sign in again.' and return unless usuario_signed_in?
end

end
