class PedidosliberadosController < ApplicationController
  before_action :set_pedidoliberado, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  # GET /pedidosliberados
  # GET /pedidosliberados.json

  def index
    elementos_de_busqueda

    @new_pedido_liberado = Pedidoliberado.new

    @new_backorder = Backord.new

    @search_liberados = Pedidoliberado.proceso_pedidos_lib.search(search_params)
    # make name the default sort column
    @search_liberados.sorts = 'Pza desc' if @search_liberados.sorts.empty?
    @pedidosliberados = @search_liberados.result().page(params[:pedidosliberados]).per(15)

    @search_backorder = Backord.proceso_pedidos_liberados_backorder.search(search_params)
    # make name the default sort column
    @search_backorder.sorts = 'Folio_BO desc' if @search_backorder.sorts.empty?
    @backorder = @search_backorder.result().page(params[:backorder]).per(15)




  end

  def elementos_de_busqueda
    @pathDeBusqueda = cobranza_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable

    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])


    @searchClienteB = Cliente.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchClienteB.sorts = 'IdCli' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])
  end

  def busqueda_stored
   @Ruta = params[:search]
   @Fecha = params[:fecha_entrega]
   @Cte = params[:search2]

#   results = ActiveRecord::Base.connection.exec_query %Q{SPAD_LiberarPedidos('#{params}')}
#   ActiveRecord::Base.clear_active_connections!
#   results

    respond_to do |format|
    format.js
    end
  end

  # GET /pedidosliberados/1
  # GET /pedidosliberados/1.json
  def show
  end

  # GET /pedidosliberados/new
  def new
    @pedidoliberado = Pedidoliberado.new
  end

  # GET /pedidosliberados/1/edit
  def edit
  end

  # POST /pedidosliberados
  # POST /pedidosliberados.json
  def create
    @pedidoliberado = Pedidoliberado.new(pedidoliberado_params)

    respond_to do |format|
      if @pedidoliberado.save
        format.html { redirect_to @pedidoliberado, notice: 'Pedidoliberado was successfully created.' }
        format.json { render :show, status: :created, location: @pedidoliberado }
        format.js {flash.now[:notice] = 'El pedido ha sido creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @pedidoliberado.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el pedido.'} #ajax
      end
    end
  end

  # PATCH/PUT /pedidosliberados/1
  # PATCH/PUT /pedidosliberados/1.json
  def update
    respond_to do |format|
      if @pedidoliberado.update(pedidoliberado_params)

        if @pedidoliberado.STATUS == 2
          @pedidoliberado.update(FECHA_LIBERACION: Time.now)
        end

        if @pedidoliberado.CANCELADA != TRUE
          #para que se haga uso del query en el ajax
          @pedidoliberado_ESTADOS = Pedidoliberado.proceso_pedidos_lib.where(:ID => @pedidoliberado.id).first
        else

          if Backord.exists?(PEDIDO: @pedidoliberado.PEDIDO, RUTA: @pedidoliberado.RUTA, IDEMPRESA: @pedidoliberado.IDEMPRESA)
            Backord.find_by(PEDIDO: @pedidoliberado.PEDIDO, RUTA: @pedidoliberado.RUTA, IDEMPRESA: @pedidoliberado.IDEMPRESA).update(CANCELADA: FALSE)

          else
            Backord.create(PEDIDO: @pedidoliberado.PEDIDO, RUTA: @pedidoliberado.RUTA, CODCLIENTE: @pedidoliberado.COD_CLIENTE, FOLIO_BO: @pedidoliberado.FOLIO_BO, FECHA_PEDIDO: @pedidoliberado.FECHA_PEDIDO, FECHA_LIBERACION: @pedidoliberado.FECHA_LIBERACION, FECHA_ENTREGA: @pedidoliberado.FECHA_ENTREGA, TIPO: @pedidoliberado.TIPO, STATUS: 1, SUBTOTAL: @pedidoliberado.SUBTOTAL, IVA: @pedidoliberado.IVA,
                            IEPS: @pedidoliberado.IEPS, TOTAL: @pedidoliberado.TOTAL, KG: @pedidoliberado.KG, CANCELADA: 0, IDEMPRESA: @pedidoliberado.IDEMPRESA)

            params[:id_pedidoliberado] = @pedidoliberado.ID

            @detallepedidoliberado = Detallepedidolib.losDetalles_de_un_PedidoLiberado(params)

            @detallepedidoliberado.each do |detallepedidolib|
               Detallebo.create(PEDIDO: detallepedidolib.PEDIDO, FOLIO_BO: detallepedidolib.FOLIO_BO, RUTA: detallepedidolib.RUTA, ARTICULO: detallepedidolib.ARTICULO, PZA: detallepedidolib.PZA, TIPO: detallepedidolib.TIPO, KG: detallepedidolib.KG, PRECIO: detallepedidolib.PRECIO, IMPORTE: detallepedidolib.IMPORTE, IVA: detallepedidolib.IVA, IEPS: detallepedidolib.IEPS, DESCUENTO: detallepedidolib.DESCUENTO,
                                        BAN_PROMO: detallepedidolib.BAN_PROMO, STATUS: detallepedidolib.STATUS, IDEMPRESA: detallepedidolib.IDEMPRESA)
            end
          end
          @backord = Backord.proceso_pedidos_liberados_backorder.where(:PEDIDO => @pedidoliberado.PEDIDO, :RUTA => @pedidoliberado.RUTA, :IDEMPRESA => @pedidoliberado.IDEMPRESA).first

        end

        format.html { redirect_to @pedidoliberado, notice: 'Pedidoliberado was successfully updated.' }
        format.json { render :show, status: :ok, location: @pedidoliberado }
        format.js {flash.now[:notice] = 'El pedido se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @pedidoliberado.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el pedido.'} #ajax
      end
    end
  end

  # DELETE /pedidosliberados/1
  # DELETE /pedidosliberados/1.json
  def destroy
    @pedidoliberado.destroy
    respond_to do |format|
      format.html { redirect_to pedidosliberados_url, notice: 'Pedidoliberado was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El envase se ha creado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedidoliberado
      @pedidoliberado = Pedidoliberado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pedidoliberado_params
      params.require(:pedidoliberado).permit(:PEDIDO, :RUTA, :COD_CLIENTE, :FOLIO_BO, :FECHA_PEDIDO, :FECHA_LIBERACION, :FECHA_ENTREGA, :TIPO, :STATUS, :CANCELADA, :INCIDENCIA, :SUBTOTAL, :IVA, :IEPS, :TOTAL, :KG, :IDVENDEDOR, :ID_AYUDANTE1, :ID_AYUDANTE2, :IDEMPRESA, :RutaEnt)
    end
end
