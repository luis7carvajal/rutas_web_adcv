class Relprogrupo < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :producto, class_name:"Producto", foreign_key: "ProductoId"
  belongs_to :catgrupo, class_name:"Catgrupo", foreign_key: "IdGrupo"
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :productosquetiene, -> (id_param) { where(IdGrupo: id_param) }
end
