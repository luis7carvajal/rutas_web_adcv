class Configruta < ActiveRecord::Base
  self.primary_key = "Id"
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  before_save :valores_por_default

  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"


  def valores_por_default
    self.VelCom ||= 19200
    self.Puerto ||= 21
  end

end
