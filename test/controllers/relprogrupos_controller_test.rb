require 'test_helper'

class RelprogruposControllerTest < ActionController::TestCase
  setup do
    @relprogrupo = relprogrupos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:relprogrupos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create relprogrupo" do
    assert_difference('Relprogrupo.count') do
      post :create, relprogrupo: { IdEmpresa: @relprogrupo.IdEmpresa, IdGrupo: @relprogrupo.IdGrupo, ProductoId: @relprogrupo.ProductoId }
    end

    assert_redirected_to relprogrupo_path(assigns(:relprogrupo))
  end

  test "should show relprogrupo" do
    get :show, id: @relprogrupo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @relprogrupo
    assert_response :success
  end

  test "should update relprogrupo" do
    patch :update, id: @relprogrupo, relprogrupo: { IdEmpresa: @relprogrupo.IdEmpresa, IdGrupo: @relprogrupo.IdGrupo, ProductoId: @relprogrupo.ProductoId }
    assert_redirected_to relprogrupo_path(assigns(:relprogrupo))
  end

  test "should destroy relprogrupo" do
    assert_difference('Relprogrupo.count', -1) do
      delete :destroy, id: @relprogrupo
    end

    assert_redirected_to relprogrupos_path
  end
end
