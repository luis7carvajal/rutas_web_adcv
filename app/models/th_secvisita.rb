class Th_secvisita < ActiveRecord::Base
  self.primary_key = 'Id'


  def self.cantidad_visitas_programadas_dashboard(params)
    query = select('(Secuencia)')
         .where('(Fecha between ? and ?) AND (IdEmpresa = ?)', (Time.current.beginning_of_day-1.day), (Time.current.end_of_day), (params[:search0]))

  end


  def self.clientes_no_visitados_R_Bitacora(params)
    query = select("TH_SecVisitas.Id, Clientes.IdCli, Clientes.Nombre, Clientes.NombreCorto, Clientes.Direccion, Clientes.Colonia")
           .distinct
           .joins('JOIN Clientes ON TH_SecVisitas.CodCli=Clientes.IdCli')
           .where("(TH_SecVisitas.RutaId = :rutaId or :rutaId = '') AND  TH_SecVisitas.CodCli NOT IN (SELECT Codigo FROM BitacoraTiempos WHERE RutaId= :rutaId
           AND HI BETWEEN :fechainicial AND :fechafinal) AND  Fecha = :fechainicial AND (TH_SecVisitas.IdEmpresa = :idempresa)",{rutaId: params[:search], fechainicial: (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
           .where("(TH_SecVisitas.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
           .order('Clientes.IdCli')

  end

  def self.clientes_no_visitados_R_Bitacora_count(params)
    query = select("TH_SecVisitas.Id")
           .distinct
           .joins('JOIN Clientes ON TH_SecVisitas.CodCli=Clientes.IdCli')
           .where("(TH_SecVisitas.RutaId = :rutaId or :rutaId = '') AND TH_SecVisitas.CodCli NOT IN (SELECT Codigo FROM BitacoraTiempos WHERE RutaId= :rutaId
           AND DiaO = :diao AND HI BETWEEN :fechainicial AND :fechafinal) And Fecha = :fechainicial AND (TH_SecVisitas.IdEmpresa = :idempresa)",{rutaId: params[:search], fechainicial: (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6], diao: params[:diaO]})
           .where("(TH_SecVisitas.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
           .order('Clientes.IdCli')
    query
  end


  def self.reporte_bitacoratiempos_visitas_counter(params)
    query = where("(RutaId = :rutaId or :rutaId = '') AND (IdEmpresa = :idempresa) AND (IdVendedor = :vendedor_id or :vendedor_id = '') And (Fecha = :fecha)",{rutaId: params[:search], idempresa: params[:search6], vendedor_id: params[:vendedor_id], fecha: params[:fechaDiaO].try(:to_date)})
  end

end
