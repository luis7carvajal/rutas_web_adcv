require 'test_helper'

class BackorderControllerTest < ActionController::TestCase
  setup do
    @backord = backorder(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:backorder)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create backord" do
    assert_difference('Backord.count') do
      post :create, backord: { CANCELADA: @backord.CANCELADA, CODCLIENTE: @backord.CODCLIENTE, FECHA_BO: @backord.FECHA_BO, FECHA_ENTREGA: @backord.FECHA_ENTREGA, FECHA_LIBERACION: @backord.FECHA_LIBERACION, FECHA_PEDIDO: @backord.FECHA_PEDIDO, FOLIO_BO: @backord.FOLIO_BO, ID: @backord.ID, IDEMPRESA: @backord.IDEMPRESA, IEPS: @backord.IEPS, IVA: @backord.IVA, KG: @backord.KG, Motivo: @backord.Motivo, PEDIDO: @backord.PEDIDO, RUTA: @backord.RUTA, STATUS: @backord.STATUS, SUBTOTAL: @backord.SUBTOTAL, TIPO: @backord.TIPO, TOTAL: @backord.TOTAL }
    end

    assert_redirected_to backord_path(assigns(:backord))
  end

  test "should show backord" do
    get :show, id: @backord
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @backord
    assert_response :success
  end

  test "should update backord" do
    patch :update, id: @backord, backord: { CANCELADA: @backord.CANCELADA, CODCLIENTE: @backord.CODCLIENTE, FECHA_BO: @backord.FECHA_BO, FECHA_ENTREGA: @backord.FECHA_ENTREGA, FECHA_LIBERACION: @backord.FECHA_LIBERACION, FECHA_PEDIDO: @backord.FECHA_PEDIDO, FOLIO_BO: @backord.FOLIO_BO, ID: @backord.ID, IDEMPRESA: @backord.IDEMPRESA, IEPS: @backord.IEPS, IVA: @backord.IVA, KG: @backord.KG, Motivo: @backord.Motivo, PEDIDO: @backord.PEDIDO, RUTA: @backord.RUTA, STATUS: @backord.STATUS, SUBTOTAL: @backord.SUBTOTAL, TIPO: @backord.TIPO, TOTAL: @backord.TOTAL }
    assert_redirected_to backord_path(assigns(:backord))
  end

  test "should destroy backord" do
    assert_difference('Backord.count', -1) do
      delete :destroy, id: @backord
    end

    assert_redirected_to backorder_path
  end
end
