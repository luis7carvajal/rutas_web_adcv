require 'test_helper'

class CtiketControllerTest < ActionController::TestCase
  setup do
    @tiket = ctiket(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ctiket)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tiket" do
    assert_difference('Tiket.count') do
      post :create, tiket: { IdEmpresa: @tiket.IdEmpresa, LOGO: @tiket.LOGO, Linea1: @tiket.Linea1, Linea2: @tiket.Linea2, Linea3: @tiket.Linea3, Linea4: @tiket.Linea4, MLiq: @tiket.MLiq, Mensaje: @tiket.Mensaje, Tdv: @tiket.Tdv }
    end

    assert_redirected_to tiket_path(assigns(:tiket))
  end

  test "should show tiket" do
    get :show, id: @tiket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tiket
    assert_response :success
  end

  test "should update tiket" do
    patch :update, id: @tiket, tiket: { IdEmpresa: @tiket.IdEmpresa, LOGO: @tiket.LOGO, Linea1: @tiket.Linea1, Linea2: @tiket.Linea2, Linea3: @tiket.Linea3, Linea4: @tiket.Linea4, MLiq: @tiket.MLiq, Mensaje: @tiket.Mensaje, Tdv: @tiket.Tdv }
    assert_redirected_to tiket_path(assigns(:tiket))
  end

  test "should destroy tiket" do
    assert_difference('Tiket.count', -1) do
      delete :destroy, id: @tiket
    end

    assert_redirected_to ctiket_path
  end
end
