class CatunidadmedidaController < ApplicationController
  before_action :set_catunidadmed, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de unidad de medidas"
  end

  # GET /catunidadmedida
  # GET /catunidadmedida.json
  def index

    @search = Catunidadmed.activos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @catunidadmedida = @search.result().page(params[:page])

    @catunidadmed = Catunidadmed.new

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catunidadmed.to_csv}
      format.xls
    end
  end

  def inactivos

    @search = Catunidadmed.inactivos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @catunidadmedida = @search.result().page(params[:page])


    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catunidadmed.to_csv}
      format.xls
    end
  end

  def catunidadmed_check
    @catunidadmedx = Catunidadmed.find_by_Clave(params[:catunidadmed][:Clave])
    respond_to do |format|
    format.json { render :json => !@catunidadmedx }
    end
  end

  # GET /catunidadmedida/1
  # GET /catunidadmedida/1.json
  def show
  end

  # GET /catunidadmedida/new
  def new
    @catunidadmed = Catunidadmed.new
  end

  # GET /catunidadmedida/1/edit
  def edit
  end

  # POST /catunidadmedida
  # POST /catunidadmedida.json
  def create
    if @puede_crear != true
      return
    end
    @catunidadmed = Catunidadmed.new(catunidadmed_params)
    respond_to do |format|
      if @catunidadmed.save
        format.html { redirect_to @catunidadmed, notice: 'Catunidadmed was successfully created.' }
        format.json { render :show, status: :created, location: @catunidadmed }
        format.js {flash.now[:notice] = 'La unidad se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @catunidadmed.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la unidad.'} #ajax
      end
    end
  end

  # PATCH/PUT /catunidadmedida/1
  # PATCH/PUT /catunidadmedida/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @catunidadmed.update(catunidadmed_params)
        format.html { redirect_to @catunidadmed, notice: 'Catunidadmed was successfully updated.' }
        format.json { render :show, status: :ok, location: @catunidadmed }
        format.js {flash.now[:notice] = 'La unidad se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @catunidadmed.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la unidad.'} #ajax
      end
    end
  end

  # DELETE /catunidadmedida/1
  # DELETE /catunidadmedida/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to catunidadmedisa_url, notice: 'unidad was successfully destroyed.' }
      format.json { head :no_content }
      if @catunidadmed.Status == true
        @catunidadmed.update(Status:false)
        format.js {flash.now[:notice] = 'La unidad se ha borrado de forma exitosa.'} #ajax
      else
        @catunidadmed.update(Status:true)
        format.js {flash.now[:notice] = 'La unidad se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catunidadmed
      @catunidadmed = Catunidadmed.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def catunidadmed_params
      params.require(:catunidadmed).permit(:Clave, :UnidadMedida, :Abreviatura, :Status, :IdEmpresa)
    end
end
