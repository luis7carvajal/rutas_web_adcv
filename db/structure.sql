/*
Navicat SQL Server Data Transfer

Source Server         : OVH
Source Server Version : 140000
Source Host           : vps157135.vps.ovh.ca:1433
Source Database       : SCTP
Source Schema         : dbo

Target Server Type    : SQL Server
Target Server Version : 140000
File Encoding         : 65001

Date: 2018-02-28 14:00:21
*/


-- ----------------------------
-- Table structure for Activos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Activos]
GO
CREATE TABLE [dbo].[Activos] (
[IdActivos] int NOT NULL IDENTITY(1,1) ,
[Descripcion] nvarchar(255) NULL ,
[Modelo] nvarchar(255) NULL ,
[Fecha] datetime NULL ,
[Serie] nvarchar(50) NULL ,
[CB] nvarchar(50) NULL ,
[Status] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL ,
[cover_file_name] nvarchar(4000) NULL ,
[cover_content_type] nvarchar(4000) NULL ,
[cover_file_size] int NULL ,
[cover_updated_at] datetime NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Activos]', RESEED, 83)
GO

-- ----------------------------
-- Table structure for Ayudantes
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Ayudantes]
GO
CREATE TABLE [dbo].[Ayudantes] (
[Nombre] varchar(MAX) NOT NULL ,
[Direccion] varchar(MAX) NULL ,
[Telefono] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL ,
[Status] bit NULL ,
[NumLicencia] varchar(255) NULL ,
[VenceLic] datetime NULL ,
[Clave] varchar(50) NULL ,
[id_ayudante] int NOT NULL IDENTITY(1,1) 
)


GO

-- ----------------------------
-- Table structure for BACKORDER
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[BACKORDER]
GO
CREATE TABLE [dbo].[BACKORDER] (
[ID] int NOT NULL IDENTITY(1,1) ,
[PEDIDO] int NULL ,
[RUTA] int NULL ,
[CODCLIENTE] varchar(50) NULL ,
[FOLIO_BO] varchar(50) NULL ,
[FECHA_PEDIDO] datetime NULL ,
[FECHA_BO] datetime NULL ,
[FECHA_ENTREGA] datetime NULL ,
[FECHA_LIBERACION] datetime NULL ,
[TIPO] varchar(50) NULL ,
[STATUS] int NULL ,
[CANCELADA] bit NULL ,
[SUBTOTAL] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[TOTAL] money NULL ,
[KG] numeric(18,2) NULL ,
[IDEMPRESA] varchar(50) NULL ,
[Motivo] varchar(MAX) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[BACKORDER]', RESEED, 278)
GO

-- ----------------------------
-- Table structure for bi_project
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[bi_project]
GO
CREATE TABLE [dbo].[bi_project] (
[id] int NOT NULL IDENTITY(1,1) ,
[priority] varchar(255) NULL ,
[status] varchar(255) NULL ,
[assigned] date NULL ,
[corrected] date NULL ,
[closed] date NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[bi_project]', RESEED, 481)
GO

-- ----------------------------
-- Table structure for bi_salary_survey
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[bi_salary_survey]
GO
CREATE TABLE [dbo].[bi_salary_survey] (
[id] int NOT NULL IDENTITY(1,1) ,
[salary_local] int NULL ,
[currency] varchar(255) NULL ,
[salary_usd] decimal(15,2) NULL ,
[job_type] varchar(255) NULL ,
[work_hours_day] varchar(255) NULL ,
[years_experience] int NULL ,
[country] varchar(255) NULL ,
[continent] varchar(255) NULL ,
[code_hours] int NULL ,
[ppp_factor] decimal(15,2) NULL ,
[salary_usd_ppp] decimal(15,2) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[bi_salary_survey]', RESEED, 1883)
GO

-- ----------------------------
-- Table structure for bi_sales
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[bi_sales]
GO
CREATE TABLE [dbo].[bi_sales] (
[id] int NOT NULL IDENTITY(1,1) ,
[salesdate] date NULL ,
[region] varchar(50) NOT NULL DEFAULT '' ,
[category] varchar(15) NOT NULL DEFAULT '' ,
[product] varchar(40) NOT NULL DEFAULT '' ,
[customer] varchar(50) NULL ,
[qtysold] int NOT NULL ,
[pricein] float(53) NOT NULL DEFAULT ((0.00)) ,
[priceout] decimal(16,2) NULL ,
[cost] decimal(26,2) NULL ,
[sales] decimal(26,2) NULL ,
[profit] decimal(17,2) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[bi_sales]', RESEED, 874)
GO

-- ----------------------------
-- Table structure for BitacoraCuotas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[BitacoraCuotas]
GO
CREATE TABLE [dbo].[BitacoraCuotas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CuoId] int NULL ,
[Fecha] datetime NULL ,
[RutaId] int NULL ,
[Valor] numeric(18,2) NULL 
)


GO

-- ----------------------------
-- Table structure for BitacoraTiempos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[BitacoraTiempos]
GO
CREATE TABLE [dbo].[BitacoraTiempos] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Codigo] varchar(50) NULL ,
[Descripcion] varchar(255) NULL ,
[HI] datetime NULL ,
[HF] datetime NULL ,
[HT] varchar(20) NULL ,
[TS] varchar(20) NULL ,
[Visita] bit NULL ,
[Programado] bit NULL ,
[DiaO] int NULL ,
[RutaId] int NULL ,
[Cerrado] bit NULL ,
[IdV] int NULL ,
[Tip] nvarchar(10) NULL ,
[latitude] float(53) NULL ,
[longitude] float(53) NULL ,
[pila] tinyint NULL ,
[IdEmpresa] varchar(50) NULL ,
[IdVendedor] int NULL ,
[Id_Ayudante1] int NULL ,
[Id_Ayudante2] int NULL ,
[IdVehiculo] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[BitacoraTiempos]', RESEED, 26176)
GO

-- ----------------------------
-- Table structure for Cancelaciones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Cancelaciones]
GO
CREATE TABLE [dbo].[Cancelaciones] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodigoDKL] nvarchar(50) NULL ,
[Docto] numeric(18) NULL ,
[Tipo] nvarchar(50) NULL ,
[DiaO] int NULL ,
[Llave] nvarchar(50) NULL ,
[RutaId] int NULL 
)


GO

-- ----------------------------
-- Table structure for CargaRuta
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CargaRuta]
GO
CREATE TABLE [dbo].[CargaRuta] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Articulo] varchar(50) NULL ,
[Fecha] datetime NULL ,
[RutaId] int NULL ,
[Tipo] bit NULL ,
[Stock] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for CatBancos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CatBancos]
GO
CREATE TABLE [dbo].[CatBancos] (
[IdBanco] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] bit NULL ,
[IdEmpresa] varchar(50) NULL ,
[CLABE] nvarchar(30) NULL ,
[NCuenta] nvarchar(30) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[CatBancos]', RESEED, 14)
GO

-- ----------------------------
-- Table structure for CatGrupos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CatGrupos]
GO
CREATE TABLE [dbo].[CatGrupos] (
[IdGrupo] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] bit NULL ,
[TipoGrupo] char(1) NOT NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[CatGrupos]', RESEED, 194)
GO

-- ----------------------------
-- Table structure for CatMarcas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CatMarcas]
GO
CREATE TABLE [dbo].[CatMarcas] (
[IdMarca] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] bit NULL ,
[TipoMarca] char(1) NOT NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[CatMarcas]', RESEED, 50)
GO

-- ----------------------------
-- Table structure for CatUnidadMedida
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CatUnidadMedida]
GO
CREATE TABLE [dbo].[CatUnidadMedida] (
[IdUnidad] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(50) NOT NULL ,
[UnidadMedida] varchar(MAX) NULL ,
[Abreviatura] varchar(50) NULL ,
[Status] bit NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[CatUnidadMedida]', RESEED, 23)
GO

-- ----------------------------
-- Table structure for ClasClientes
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ClasClientes]
GO
CREATE TABLE [dbo].[ClasClientes] (
[IdClasC] int NOT NULL IDENTITY(1,1) ,
[Descripcion] nvarchar(MAX) NULL ,
[NivelNum] int NULL ,
[idsegmento] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ClasClientes]', RESEED, 565)
GO

-- ----------------------------
-- Table structure for ClasifComp
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ClasifComp]
GO
CREATE TABLE [dbo].[ClasifComp] (
[Id_Clasif] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] char(1) NULL 
)


GO

-- ----------------------------
-- Table structure for ClasProductos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ClasProductos]
GO
CREATE TABLE [dbo].[ClasProductos] (
[IdClasP] int NOT NULL IDENTITY(1,1) ,
[Descripcion] nvarchar(MAX) NULL ,
[NivelNum] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ClasRutas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ClasRutas]
GO
CREATE TABLE [dbo].[ClasRutas] (
[IdCRutas] int NOT NULL IDENTITY(1,1) ,
[Descripcion] varchar(MAX) NULL ,
[NivelNum] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ClasRutas]', RESEED, 15)
GO

-- ----------------------------
-- Table structure for Clientes
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Clientes]
GO
CREATE TABLE [dbo].[Clientes] (
[IdCli] varchar(50) NOT NULL ,
[Nombre] varchar(255) NULL ,
[NombreCorto] varchar(100) NULL ,
[Direccion] varchar(255) NULL ,
[Referencia] nvarchar(510) NULL ,
[Telefono] nvarchar(50) NULL ,
[CP] char(5) NULL ,
[Credito] bit NULL ,
[LimiteCredito] money NULL ,
[Status] bit NULL ,
[DiasCreedito] int NULL ,
[Colonia] varchar(100) NULL ,
[Tel2] nvarchar(50) NULL ,
[Email] nvarchar(50) NOT NULL ,
[VisitaObligada] bit NULL ,
[FirmaObligada] bit NULL ,
[MotivoBajaId] int NULL ,
[Saldo] money NULL ,
[Horario] nvarchar(50) NULL ,
[idclasc] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Longitud] float(53) NULL ,
[Latitud] float(53) NULL ,
[VigenciaComodato] int NULL ,
[CorreoE] varchar(250) NULL ,
[Ciudad] varchar(MAX) NULL ,
[Estado] varchar(MAX) NULL ,
[RFC] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for Clientes_copy
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Clientes_copy]
GO
CREATE TABLE [dbo].[Clientes_copy] (
[IdCli] varchar(50) NOT NULL ,
[Nombre] varchar(255) NULL ,
[NombreCorto] varchar(100) NULL ,
[Direccion] varchar(255) NULL ,
[Referencia] nvarchar(510) NULL ,
[Telefono] nvarchar(50) NULL ,
[CP] char(5) NULL ,
[Credito] bit NULL ,
[LimiteCredito] money NULL ,
[Status] bit NULL ,
[DiasCreedito] int NULL ,
[Colonia] varchar(100) NULL ,
[Tel2] nvarchar(50) NULL ,
[Email] nvarchar(50) NOT NULL ,
[VisitaObligada] bit NULL ,
[FirmaObligada] bit NULL ,
[MotivoBajaId] int NULL ,
[Saldo] money NULL ,
[Horario] nvarchar(50) NULL ,
[idclasc] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Longitud] float(53) NULL ,
[Latitud] float(53) NULL ,
[VigenciaComodato] int NULL ,
[CorreoE] varchar(250) NULL 
)


GO

-- ----------------------------
-- Table structure for Clientes2
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Clientes2]
GO
CREATE TABLE [dbo].[Clientes2] (
[IdCli] varchar(50) NOT NULL ,
[Nombre] varchar(255) NULL ,
[NombreCorto] varchar(100) NULL ,
[Direccion] varchar(255) NULL ,
[Referencia] nvarchar(510) NULL ,
[Telefono] nvarchar(50) NULL ,
[CP] char(5) NULL ,
[Credito] bit NULL ,
[LimiteCredito] money NULL ,
[Status] bit NULL ,
[DiasCreedito] int NULL ,
[Colonia] varchar(100) NULL ,
[Tel2] nvarchar(50) NULL ,
[Email] nvarchar(50) NOT NULL ,
[VisitaObligada] bit NULL ,
[FirmaObligada] bit NULL ,
[MotivoBajaId] int NULL ,
[Saldo] money NULL ,
[Horario] nvarchar(50) NULL ,
[idclasc] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Longitud] float(53) NULL ,
[Latitud] float(53) NULL 
)


GO

-- ----------------------------
-- Table structure for Cobranza
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Cobranza]
GO
CREATE TABLE [dbo].[Cobranza] (
[id] int NOT NULL IDENTITY(1,1) ,
[Cliente] varchar(50) NULL ,
[Documento] nvarchar(50) NULL ,
[Saldo] money NULL ,
[Status] int NULL ,
[RutaId] int NULL ,
[UltPago] nvarchar(50) NULL ,
[FechaReg] datetime NULL ,
[FechaVence] datetime NULL ,
[FolioInterno] int NULL ,
[TipoDoc] nvarchar(50) NULL ,
[DiaO] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Cobranza]', RESEED, 103705)
GO

-- ----------------------------
-- Table structure for CodesOp
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CodesOp]
GO
CREATE TABLE [dbo].[CodesOp] (
[Codi] nvarchar(50) NOT NULL ,
[Operacion] nvarchar(MAX) NULL ,
[Tipo] bit NULL ,
[IdEmpresa] varchar(50) NULL ,
[EsRecarga] bit NULL ,
[Gasto] bit NULL 
)


GO

-- ----------------------------
-- Table structure for Comisiones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Comisiones]
GO
CREATE TABLE [dbo].[Comisiones] (
[IdRow] int NOT NULL IDENTITY(1,1) ,
[Comision] nvarchar(50) NULL ,
[Activa] bit NULL ,
[Tipo] numeric(18) NULL ,
[Factor] nvarchar(50) NULL ,
[Niveles] numeric(18) NULL ,
[Clave] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ComponentesArt
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ComponentesArt]
GO
CREATE TABLE [dbo].[ComponentesArt] (
[Id] numeric(18) NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[Articulo] varchar(50) NULL ,
[Complemento] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Id_Clasif] int NULL 
)


GO

-- ----------------------------
-- Table structure for ConfigRutasP
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ConfigRutasP]
GO
CREATE TABLE [dbo].[ConfigRutasP] (
[Id] int NOT NULL IDENTITY(1,1) ,
[RutaId] int NULL ,
[ModelPrinter] nvarchar(50) NULL ,
[VelCom] nvarchar(50) NULL ,
[COM] nvarchar(50) NULL ,
[Server] varchar(255) NULL ,
[Puerto] int NULL ,
[ServerGPS] nvarchar(50) NULL ,
[GPS] bit NULL ,
[PuertoG] nvarchar(50) NULL ,
[PagoContado] bit NULL ,
[CteNvo] bit NULL ,
[CveCteNvo] bit NULL ,
[IdEmpresa] varchar(50) NULL ,
[SugerirCant] bit NULL ,
[PromoEq] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ConfigRutasP]', RESEED, 318)
GO

-- ----------------------------
-- Table structure for Configuracion
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Configuracion]
GO
CREATE TABLE [dbo].[Configuracion] (
[Clave] int NOT NULL IDENTITY(1,1) ,
[EliminarClasRutas] bit NULL ,
[EliminarClasClientes] bit NULL ,
[EliminarClasProductos] bit NULL ,
[PrimerLineaTickets] nvarchar(MAX) NULL ,
[SegundaLinea] nvarchar(MAX) NULL ,
[TercerLinea] nvarchar(MAX) NULL ,
[CuartaLinea] nvarchar(MAX) NULL ,
[MensajeFinal] nvarchar(MAX) NULL ,
[HabilitarTdV] bit NULL ,
[MensajeTdV] nvarchar(MAX) NULL ,
[Pedidos] int NULL ,
[Ventas] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ConfiguracionGral
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ConfiguracionGral]
GO
CREATE TABLE [dbo].[ConfiguracionGral] (
[ID_Config] varchar(5) NOT NULL ,
[Nombre] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Valor] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for Consolidado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Consolidado]
GO
CREATE TABLE [dbo].[Consolidado] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Ruta] int NOT NULL ,
[Fol_Con] varchar(20) NOT NULL ,
[Pedido] int NOT NULL ,
[Fecha] datetime NULL ,
[Id_Empresa] varchar(50) NOT NULL 
)


GO

-- ----------------------------
-- Table structure for ContadorNC
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ContadorNC]
GO
CREATE TABLE [dbo].[ContadorNC] (
[Idrow] int NOT NULL IDENTITY(1,1) ,
[Ruta] nvarchar(100) NULL ,
[Contador] nvarchar(500) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for Continuidad
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Continuidad]
GO
CREATE TABLE [dbo].[Continuidad] (
[RutaID] int NOT NULL ,
[DiaO] int NULL ,
[FolVta] int NULL ,
[FolPed] int NULL ,
[FolDevol] int NULL ,
[FolCob] int NULL ,
[UDiaO] int NULL ,
[CteNvo] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL ,
[FolGto] int NULL 
)


GO

-- ----------------------------
-- Table structure for CP
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CP]
GO
CREATE TABLE [dbo].[CP] (
[CP] int NULL ,
[Colonia] varchar(100) NULL ,
[Municipio] varchar(75) NULL ,
[Estado] varchar(50) NULL ,
[Ciudad] varchar(75) NULL 
)


GO

-- ----------------------------
-- Table structure for CteImagen
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CteImagen]
GO
CREATE TABLE [dbo].[CteImagen] (
[IdCli] varchar(50) NULL ,
[Foto_file_name] nvarchar(4000) NULL ,
[Foto_content_type] nvarchar(4000) NULL ,
[Foto_file_size] int NULL ,
[Foto_updated_at] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for CTiket
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CTiket]
GO
CREATE TABLE [dbo].[CTiket] (
[ID] int NOT NULL IDENTITY(1,1) ,
[Linea1] varchar(255) NULL ,
[Linea2] varchar(255) NULL ,
[Linea3] varchar(255) NULL ,
[Linea4] varchar(255) NULL ,
[Mensaje] varchar(255) NULL ,
[Tdv] bit NULL ,
[LOGO] image NULL ,
[MLiq] bit NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[CTiket]', RESEED, 27)
GO

-- ----------------------------
-- Table structure for Cuotas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Cuotas]
GO
CREATE TABLE [dbo].[Cuotas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Clave] nvarchar(50) NULL ,
[Descripcion] nvarchar(MAX) NULL ,
[UniMed] nvarchar(50) NULL ,
[Cantidad] nvarchar(50) NULL ,
[FechaI] datetime NULL ,
[FechaF] datetime NULL ,
[Producto] varchar(50) NULL ,
[Tipo] bit NULL ,
[Activa] bit NULL ,
[NivelNum] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Cuotas]', RESEED, 7)
GO

-- ----------------------------
-- Table structure for CuotasVenta
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[CuotasVenta]
GO
CREATE TABLE [dbo].[CuotasVenta] (
[year] int NOT NULL ,
[Ene] real NULL ,
[Feb] real NULL ,
[Mar] real NULL ,
[Abr] real NULL ,
[May] real NULL ,
[Jun] real NULL ,
[Jul] real NULL ,
[Ago] real NULL ,
[Sep] real NULL ,
[Oct] real NULL ,
[Nov] real NULL ,
[Dic] real NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for DETALLE_BO
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DETALLE_BO]
GO
CREATE TABLE [dbo].[DETALLE_BO] (
[ID] int NOT NULL IDENTITY(1,1) ,
[PEDIDO] int NULL ,
[FOLIO_BO] varchar(50) NULL ,
[RUTA] int NULL ,
[ARTICULO] varchar(50) NULL ,
[PZA] numeric(18,2) NULL ,
[PZA_ENTREGADAS] numeric(18,2) NULL ,
[TIPO] int NULL ,
[KG] numeric(18,2) NULL ,
[PRECIO] money NULL ,
[IMPORTE] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[DESCUENTO] money NULL ,
[BAN_PROMO] bit NULL ,
[STATUS] int NULL ,
[IDEMPRESA] varchar(50) NULL ,
[CANCELADA] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DETALLE_BO]', RESEED, 905)
GO

-- ----------------------------
-- Table structure for Detalle_Consolidado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Detalle_Consolidado]
GO
CREATE TABLE [dbo].[Detalle_Consolidado] (
[Fol_Con] varchar(20) NOT NULL ,
[Articulo] varchar(50) NOT NULL ,
[Cantidad] decimal(18) NULL ,
[Id_Empresa] varchar(50) NOT NULL 
)


GO

-- ----------------------------
-- Table structure for DetalleCob
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleCob]
GO
CREATE TABLE [dbo].[DetalleCob] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdCobranza] int NULL ,
[Abono] money NULL ,
[Fecha] datetime NULL ,
[RutaId] int NULL ,
[SaldoAnt] money NULL ,
[Saldo] money NULL ,
[FormaP] int NULL ,
[DiaO] int NULL ,
[Documento] nvarchar(50) NULL ,
[Cliente] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL ,
[Cancelada] bit NULL ,
[ClaveBco] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleCob]', RESEED, 104855)
GO

-- ----------------------------
-- Table structure for DetalleCombo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleCombo]
GO
CREATE TABLE [dbo].[DetalleCombo] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Articulo] varchar(50) NULL ,
[ComboId] int NULL ,
[Cantidad] numeric(18) NULL ,
[TipMed] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for DetalleDevol
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleDevol]
GO
CREATE TABLE [dbo].[DetalleDevol] (
[Id] int NOT NULL IDENTITY(1,1) ,
[SKU] nvarchar(50) NULL ,
[Pza] int NULL ,
[KG] numeric(18,2) NULL ,
[Precio] money NULL ,
[Importe] money NULL ,
[EDO] bit NULL ,
[Motivo] nvarchar(MAX) NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[Devol] int NULL ,
[Docto] int NULL ,
[RutaId] int NULL ,
[Tipo] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleDevol]', RESEED, 46)
GO

-- ----------------------------
-- Table structure for DetalleLD
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleLD]
GO
CREATE TABLE [dbo].[DetalleLD] (
[id] int NOT NULL IDENTITY(1,1) ,
[ListaId] int NULL ,
[Articulo] varchar(50) NULL ,
[Factor] decimal(18,4) NULL ,
[Minimo] decimal(18,4) NULL ,
[Maximo] decimal(18,4) NULL ,
[Tipo] char(1) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleLD]', RESEED, 141)
GO

-- ----------------------------
-- Table structure for DetalleLP
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleLP]
GO
CREATE TABLE [dbo].[DetalleLP] (
[id] int NOT NULL IDENTITY(1,1) ,
[ListaId] int NULL ,
[Articulo] varchar(50) NULL ,
[PrecioMin] nvarchar(50) NULL ,
[PrecioMax] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleLP]', RESEED, 7457)
GO

-- ----------------------------
-- Table structure for DetalleLProMaster
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleLProMaster]
GO
CREATE TABLE [dbo].[DetalleLProMaster] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdLm] int NULL ,
[IdPromo] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleLProMaster]', RESEED, 357)
GO

-- ----------------------------
-- Table structure for DETALLEPEDIDOLIBERADO
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DETALLEPEDIDOLIBERADO]
GO
CREATE TABLE [dbo].[DETALLEPEDIDOLIBERADO] (
[ID] int NOT NULL IDENTITY(1,1) ,
[PEDIDO] int NULL ,
[FOLIO_BO] varchar(50) NULL ,
[RUTA] int NULL ,
[ARTICULO] varchar(50) NULL ,
[PZA] numeric(18,2) NULL ,
[PZA_LIBERADAS] numeric(18,2) NULL ,
[TIPO] int NULL ,
[KG] numeric(18,2) NULL ,
[PRECIO] money NULL ,
[IMPORTE] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[DESCUENTO] money NULL ,
[BAN_PROMO] bit NULL ,
[STATUS] int NULL ,
[CANCELADA] bit NULL ,
[INCIDENCIA] int NULL ,
[IDEMPRESA] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DETALLEPEDIDOLIBERADO]', RESEED, 15258)
GO

-- ----------------------------
-- Table structure for DetallePedidos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetallePedidos]
GO
CREATE TABLE [dbo].[DetallePedidos] (
[Id] int NOT NULL IDENTITY(1,1) ,
[SKU] varchar(50) NULL ,
[Pza] int NULL ,
[KG] numeric(18,2) NULL ,
[Precio] money NULL ,
[Importe] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[Docto] int NULL ,
[DesctoPor] numeric(18,2) NULL ,
[DesctoV] numeric(18,2) NULL ,
[RutaId] int NULL ,
[Vendedo] int NULL ,
[Descripcion] varchar(255) NULL ,
[Tipo] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Total] money NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetallePedidos]', RESEED, 4522)
GO

-- ----------------------------
-- Table structure for DetallePromo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetallePromo]
GO
CREATE TABLE [dbo].[DetallePromo] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Articulo] varchar(50) NULL ,
[PromoId] int NULL ,
[Cantidad] numeric(18) NULL ,
[Tipo] bit NULL ,
[TipoProm] nvarchar(50) NULL ,
[Monto] nvarchar(50) NULL ,
[Volumen] nchar(10) NULL ,
[TipMed] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Nivel] int NULL ,
[Grupo] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetallePromo]', RESEED, 671)
GO

-- ----------------------------
-- Table structure for DetalleSalidas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleSalidas]
GO
CREATE TABLE [dbo].[DetalleSalidas] (
[IDSalida] int NULL ,
[Articulo] varchar(50) NULL ,
[Cantidad] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for DetalleVet
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DetalleVet]
GO
CREATE TABLE [dbo].[DetalleVet] (
[Articulo] varchar(50) NULL ,
[Descripcion] nvarchar(MAX) NULL ,
[Precio] money NULL ,
[Pza] int NULL ,
[Kg] numeric(18,2) NULL ,
[DescPorc] nvarchar(50) NULL ,
[DescMon] numeric(18,2) NULL ,
[Tipo] int NULL ,
[Docto] int NULL ,
[Importe] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[RutaId] int NULL ,
[ID] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DetalleVet]', RESEED, 859584)
GO

-- ----------------------------
-- Table structure for DevEnvases
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DevEnvases]
GO
CREATE TABLE [dbo].[DevEnvases] (
[RutaId] int NULL ,
[DiaO] int NULL ,
[CodCli] varchar(50) NULL ,
[Docto] int NULL ,
[Articulo] varchar(50) NULL ,
[Cantidad] int NULL ,
[Devuelto] int NULL ,
[Tipo] nvarchar(100) NULL ,
[Envase] varchar(50) NULL ,
[Status] char(1) NULL ,
[ID] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DevEnvases]', RESEED, 363516)
GO

-- ----------------------------
-- Table structure for Devoluciones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Devoluciones]
GO
CREATE TABLE [dbo].[Devoluciones] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodCliente] varchar(50) NULL ,
[Devol] int NULL ,
[Fecha] datetime NULL ,
[Status] numeric(18) NULL ,
[Ruta] int NULL ,
[Vendedor] int NULL ,
[Items] int NULL ,
[KG] numeric(18,2) NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[Subtotal] money NULL ,
[Total] money NULL ,
[EnLetras] varchar(255) NULL ,
[DiaO] int NULL ,
[Docto] int NULL ,
[Cancelada] bit NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Devoluciones]', RESEED, 39425)
GO

-- ----------------------------
-- Table structure for Dias
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Dias]
GO
CREATE TABLE [dbo].[Dias] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Dia] nvarchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Dias]', RESEED, 7)
GO

-- ----------------------------
-- Table structure for DiasO
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DiasO]
GO
CREATE TABLE [dbo].[DiasO] (
[Id] int NOT NULL IDENTITY(1,1) ,
[DiaO] int NOT NULL ,
[Fecha] date NULL ,
[RutaId] int NOT NULL ,
[VProg] numeric(18) NULL ,
[Ve] numeric(18) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[DiasO]', RESEED, 77803)
GO

-- ----------------------------
-- Table structure for DiasOperativos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[DiasOperativos]
GO
CREATE TABLE [dbo].[DiasOperativos] (
[Clave] int NOT NULL IDENTITY(1,1) ,
[Dia] int NULL ,
[RutaId] int NULL ,
[Ventas] int NULL ,
[VentasCre] int NULL ,
[Devoluciones] int NULL ,
[Pedidos] int NULL 
)


GO

-- ----------------------------
-- Table structure for Empresas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Empresas]
GO
CREATE TABLE [dbo].[Empresas] (
[IdEmpresa] varchar(50) NOT NULL ,
[Direccion] nvarchar(4000) NULL ,
[Telefono] nvarchar(4000) NULL ,
[Contacto] nvarchar(4000) NULL ,
[Email] nvarchar(4000) NULL ,
[NoExterior] nvarchar(4000) NULL ,
[NoInterior] nvarchar(4000) NULL ,
[CP] nvarchar(4000) NULL ,
[empresamadre_id] int NULL ,
[Sucursal] nvarchar(4000) NULL ,
[NombreComercial] nvarchar(4000) NULL 
)


GO

-- ----------------------------
-- Table structure for empresasmadre
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[empresasmadre]
GO
CREATE TABLE [dbo].[empresasmadre] (
[id] int NOT NULL IDENTITY(1,1) ,
[Empresa] nvarchar(4000) NULL ,
[Telefono] nvarchar(4000) NULL ,
[Contacto] nvarchar(4000) NULL ,
[Email] nvarchar(4000) NULL ,
[RFC] nvarchar(4000) NULL ,
[Direccion] nvarchar(4000) NULL ,
[NoExterior] nvarchar(4000) NULL ,
[NoInterior] nvarchar(4000) NULL ,
[CP] nvarchar(4000) NULL ,
[created_at] datetime NOT NULL ,
[updated_at] datetime NOT NULL ,
[NombreComercial] nvarchar(4000) NULL ,
[Url] nvarchar(70) NULL 
)


GO

-- ----------------------------
-- Table structure for Encuestas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Encuestas]
GO
CREATE TABLE [dbo].[Encuestas] (
[ID] int NOT NULL IDENTITY(1,1) ,
[Clave_Enc] varchar(50) NOT NULL ,
[Desc_Enc] varchar(50) NULL ,
[Tipo_Enc] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Encuestas]', RESEED, 7539)
GO

-- ----------------------------
-- Table structure for FormasPag
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[FormasPag]
GO
CREATE TABLE [dbo].[FormasPag] (
[IdFpag] int NOT NULL IDENTITY(1,1) ,
[Forma] nvarchar(MAX) NULL ,
[Clave] nvarchar(MAX) NULL ,
[Status] bit NULL ,
[IdEmpresa] nvarchar(4000) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[FormasPag]', RESEED, 23)
GO

-- ----------------------------
-- Table structure for HistoricoActivo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[HistoricoActivo]
GO
CREATE TABLE [dbo].[HistoricoActivo] (
[consec] numeric(18) NOT NULL IDENTITY(1,1) ,
[IdActivos] int NOT NULL ,
[IdCli] varchar(50) NULL ,
[FechaAsignacion] datetime NULL ,
[FechaDevol] datetime NULL ,
[Status] int NULL ,
[Comentario] varchar(200) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for HistoricoComodato
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[HistoricoComodato]
GO
CREATE TABLE [dbo].[HistoricoComodato] (
[RutaId] int NULL ,
[CodCliente] varchar(50) NULL ,
[Docto] int NULL ,
[Articulo] varchar(50) NULL ,
[Envase] varchar(50) NULL ,
[FechaVence] date NULL ,
[FechaRen] date NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for hotels
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[hotels]
GO
CREATE TABLE [dbo].[hotels] (
[id] int NOT NULL IDENTITY(1,1) ,
[name] nvarchar(4000) NULL ,
[created_at] datetime NOT NULL ,
[updated_at] datetime NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[hotels]', RESEED, 62)
GO

-- ----------------------------
-- Table structure for Incidencias
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Incidencias]
GO
CREATE TABLE [dbo].[Incidencias] (
[Idincid] int NOT NULL IDENTITY(1,1) ,
[Descripcion] varchar(MAX) NULL ,
[Clave] varchar(50) NOT NULL ,
[Status] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Incidencias]', RESEED, 3)
GO

-- ----------------------------
-- Table structure for Liquidacion
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Liquidacion]
GO
CREATE TABLE [dbo].[Liquidacion] (
[Id] int NOT NULL IDENTITY(1,1) ,
[SKU] nvarchar(50) NULL ,
[Producto] nvarchar(MAX) NULL ,
[InvIP] numeric(18,2) NULL ,
[InvIK] numeric(18,2) NULL ,
[SutidoP] numeric(18,2) NULL ,
[SurtidoK] numeric(18,2) NULL ,
[DevolucionP] numeric(18,2) NULL ,
[DevolucionK] numeric(18,2) NULL ,
[PromocionesP] numeric(18,2) NULL ,
[PromocionesK] numeric(18,2) NULL ,
[InvFP] numeric(18,2) NULL ,
[InvFk] numeric(18,2) NULL ,
[VtaTeoricaP] numeric(18,2) NULL ,
[VtaTeoricaK] numeric(18,2) NULL ,
[VtaRealP] numeric(18,2) NULL ,
[VtaRealK] numeric(18,2) NULL ,
[DiferenciaP] numeric(18,2) NULL ,
[DiferenciaK] numeric(18,2) NULL ,
[Importe] numeric(18,2) NULL ,
[RutaId] int NULL ,
[DiaO] int NULL ,
[Fecha] nvarchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ListaCombo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ListaCombo]
GO
CREATE TABLE [dbo].[ListaCombo] (
[id] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(MAX) NULL ,
[Descripcion] nvarchar(MAX) NULL ,
[Caduca] bit NULL ,
[FechaI] datetime NULL ,
[FechaF] datetime NULL ,
[Activa] bit NULL ,
[Monto] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ListaCombo]', RESEED, 12)
GO

-- ----------------------------
-- Table structure for ListaD
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ListaD]
GO
CREATE TABLE [dbo].[ListaD] (
[id] int NOT NULL IDENTITY(1,1) ,
[Lista] nvarchar(MAX) NULL ,
[Tipo] char(1) NULL ,
[Caduca] bit NULL ,
[FechaIni] date NULL ,
[FechaFin] date NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ListaD]', RESEED, 30)
GO

-- ----------------------------
-- Table structure for ListaP
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ListaP]
GO
CREATE TABLE [dbo].[ListaP] (
[id] int NOT NULL IDENTITY(1,1) ,
[Lista] nvarchar(MAX) NULL ,
[Tipo] bit NULL ,
[FechaIni] datetime NULL ,
[FechaFin] datetime NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ListaP]', RESEED, 1796)
GO

-- ----------------------------
-- Table structure for ListaPromo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ListaPromo]
GO
CREATE TABLE [dbo].[ListaPromo] (
[id] int NOT NULL IDENTITY(1,1) ,
[Lista] nvarchar(MAX) NULL ,
[Descripcion] nvarchar(MAX) NULL ,
[Caduca] bit NULL ,
[FechaI] datetime NULL ,
[FechaF] datetime NULL ,
[Grupo] nvarchar(MAX) NULL ,
[Activa] bit NULL ,
[Tipo] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ListaPromo]', RESEED, 1602)
GO

-- ----------------------------
-- Table structure for ListaPromoMaster
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ListaPromoMaster]
GO
CREATE TABLE [dbo].[ListaPromoMaster] (
[Id] int NOT NULL IDENTITY(1,1) ,
[ListaMaster] nvarchar(MAX) NULL ,
[Promociones] nvarchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ListaPromoMaster]', RESEED, 121)
GO

-- ----------------------------
-- Table structure for Medidores
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Medidores]
GO
CREATE TABLE [dbo].[Medidores] (
[IdRow] int NOT NULL IDENTITY(1,1) ,
[IdRuta] int NULL ,
[DiaO] int NULL ,
[OdometroInicial] numeric(18) NULL ,
[OdometroFinal] numeric(18) NULL ,
[TanqueInicial] numeric(18,2) NULL ,
[TanqueFinal] numeric(18,2) NULL ,
[LitrosCargados] numeric(18) NULL ,
[GastoLitros] numeric(18,2) NULL ,
[Rendimiento] numeric(18,2) NULL ,
[KmR] numeric(18,2) NULL ,
[IdEmpresa] varchar(50) NULL ,
[IdVehiculo] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Medidores]', RESEED, 93423)
GO

-- ----------------------------
-- Table structure for Mensajes
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Mensajes]
GO
CREATE TABLE [dbo].[Mensajes] (
[Clave] nvarchar(50) NULL ,
[EnBaseA] nvarchar(MAX) NULL ,
[Descripcion] nvarchar(MAX) NULL ,
[Mensaje] varchar(255) NULL ,
[FechaInicio] datetime NULL ,
[FechaFinal] datetime NULL ,
[Estado] bit NULL ,
[ID] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Mensajes]', RESEED, 49)
GO

-- ----------------------------
-- Table structure for Mermas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Mermas]
GO
CREATE TABLE [dbo].[Mermas] (
[Id] int NOT NULL ,
[Articulo] varchar(50) NULL ,
[Merma] nvarchar(50) NULL ,
[Fecha] datetime NULL ,
[RutaId] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for MotivosNoVenta
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MotivosNoVenta]
GO
CREATE TABLE [dbo].[MotivosNoVenta] (
[IdMot] int NOT NULL IDENTITY(1,1) ,
[Motivo] nvarchar(MAX) NULL ,
[Clave] nvarchar(MAX) NULL ,
[Status] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MotivosNoVenta]', RESEED, 16)
GO

-- ----------------------------
-- Table structure for MotivosSalida
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MotivosSalida]
GO
CREATE TABLE [dbo].[MotivosSalida] (
[ID] int NOT NULL IDENTITY(1,1) ,
[Motivo] varchar(255) NULL 
)


GO

-- ----------------------------
-- Table structure for MovInvAlm
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MovInvAlm]
GO
CREATE TABLE [dbo].[MovInvAlm] (
[IdMov] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[TipoMov] char(1) NULL ,
[Movto] nvarchar(MAX) NULL ,
[Articulo] varchar(50) NULL ,
[StockAnt] numeric(18,2) NULL ,
[StockAct] numeric(18,2) NULL ,
[Origen] varchar(50) NULL ,
[Destino] varchar(50) NULL ,
[Fecha] datetime NULL ,
[Folio] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for MovInvAlmCom
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MovInvAlmCom]
GO
CREATE TABLE [dbo].[MovInvAlmCom] (
[IdMov] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[TipoMov] char(1) NULL ,
[Movto] varchar(MAX) NULL ,
[Articulo] varchar(50) NULL ,
[Comp1] varchar(50) NULL ,
[Comp2] varchar(50) NULL ,
[Comp3] varchar(50) NULL ,
[Comp4] varchar(50) NULL ,
[Comp5] varchar(50) NULL ,
[StockAnt] numeric(18,2) NULL ,
[StockAct] numeric(18,2) NULL ,
[Origen] varchar(50) NULL ,
[Destino] varchar(50) NULL ,
[Fecha] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for MvoDev
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MvoDev]
GO
CREATE TABLE [dbo].[MvoDev] (
[Id] int NOT NULL IDENTITY(1,1) ,
[MvoDev] nvarchar(MAX) NULL ,
[Clave] nvarchar(50) NULL ,
[Status] bit NULL ,
[IdEmpresa] nvarchar(4000) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MvoDev]', RESEED, 47)
GO

-- ----------------------------
-- Table structure for MvoMerma
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MvoMerma]
GO
CREATE TABLE [dbo].[MvoMerma] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Merma] nvarchar(MAX) NULL ,
[Clave] nvarchar(50) NULL ,
[Status] bit NULL ,
[IdEmpresa] nvarchar(4000) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MvoMerma]', RESEED, 20)
GO

-- ----------------------------
-- Table structure for MvtosInv
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[MvtosInv]
GO
CREATE TABLE [dbo].[MvtosInv] (
[IdMovStock] int NOT NULL IDENTITY(1,1) ,
[Movto] nvarchar(MAX) NULL ,
[Articulo] varchar(50) NULL ,
[stockant] numeric(18) NULL ,
[Stockact] numeric(18) NULL ,
[fecha] datetime NULL ,
[RutaO] int NULL ,
[RutaD] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[MvtosInv]', RESEED, 1697896)
GO

-- ----------------------------
-- Table structure for Noventas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Noventas]
GO
CREATE TABLE [dbo].[Noventas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Cliente] varchar(50) NULL ,
[MotivoId] int NULL ,
[Fecha] datetime NULL ,
[DiaO] int NULL ,
[RutaId] int NULL ,
[VendedorId] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Noventas]', RESEED, 2023)
GO

-- ----------------------------
-- Table structure for Opc_Enc
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Opc_Enc]
GO
CREATE TABLE [dbo].[Opc_Enc] (
[Clave_Enc] varchar(50) NOT NULL ,
[Num_Preg] int NOT NULL ,
[Num_Resp] char(1) NOT NULL ,
[Desc_Resp] varchar(255) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for OperacionD
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[OperacionD]
GO
CREATE TABLE [dbo].[OperacionD] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodCliente] nvarchar(MAX) NULL ,
[Cliente] nvarchar(MAX) NULL ,
[HoraI] nvarchar(50) NULL ,
[HoraF] nvarchar(50) NULL ,
[Traslado] nvarchar(50) NULL ,
[Servicio] nvarchar(50) NULL ,
[Operacion] nvarchar(MAX) NULL ,
[Programado] bit NULL ,
[VentasContadoKg] numeric(18,2) NULL ,
[VentasContado] money NULL ,
[VentasCreditoKg] numeric(18,2) NULL ,
[VentasCredito] money NULL ,
[NoVenta] nvarchar(MAX) NULL ,
[DevBEKG] numeric(18,2) NULL ,
[DevBe] numeric(18,2) NULL ,
[DevMEKG] numeric(18,2) NULL ,
[DevMe] numeric(18,2) NULL ,
[DiaO] int NULL ,
[Ruta] int NULL ,
[ClaveRuta] int NULL 
)


GO

-- ----------------------------
-- Table structure for Pagos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Pagos]
GO
CREATE TABLE [dbo].[Pagos] (
[Id] int NOT NULL ,
[Cliente] nvarchar(MAX) NULL ,
[RutaId] int NULL ,
[DiaO] int NULL ,
[Pago] money NULL 
)


GO

-- ----------------------------
-- Table structure for PContado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PContado]
GO
CREATE TABLE [dbo].[PContado] (
[Id] int NOT NULL IDENTITY(1,1) ,
[SKU] nvarchar(50) NULL ,
[RutaId] int NULL ,
[Pz] numeric(18,2) NULL ,
[Kg] numeric(18,2) NULL ,
[DiaO] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for PedDiaSig
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PedDiaSig]
GO
CREATE TABLE [dbo].[PedDiaSig] (
[ID] int NOT NULL IDENTITY(1,1) ,
[IdRuta] int NOT NULL ,
[Articulo] varchar(50) NOT NULL ,
[Cantidad] int NOT NULL ,
[Fecha] date NULL ,
[Diao] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Folio] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[PedDiaSig]', RESEED, 698597)
GO

-- ----------------------------
-- Table structure for PedDiaSigPzs
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PedDiaSigPzs]
GO
CREATE TABLE [dbo].[PedDiaSigPzs] (
[ID] int NOT NULL IDENTITY(1,1) ,
[IdRuta] int NOT NULL ,
[Articulo] varchar(50) NOT NULL ,
[Cantidad] int NOT NULL ,
[Fecha] date NULL ,
[Diao] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Folio] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[PedDiaSigPzs]', RESEED, 12933)
GO

-- ----------------------------
-- Table structure for Pedidos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Pedidos]
GO
CREATE TABLE [dbo].[Pedidos] (
[IdPedido] int NOT NULL IDENTITY(1,1) ,
[Pedido] int NULL ,
[Fecha] datetime NULL ,
[CodCliente] varchar(50) NULL ,
[Tipo] nvarchar(50) NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[Subt] money NULL ,
[Total] money NULL ,
[Items] int NULL ,
[EnLetras] varchar(255) NULL ,
[Status] int NULL ,
[Ruta] int NULL ,
[Vendedor] int NULL ,
[FechaVe] datetime NULL ,
[DiaO] int NULL ,
[DocSalida] varchar(50) NULL ,
[Cancelado] bit NULL ,
[Kg] numeric(18,2) NULL ,
[FechaEntrega] datetime NULL ,
[IdEmpresa] varchar(50) NULL ,
[DiasCred] int NULL ,
[FormaPag] int NULL ,
[idVendedor] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Pedidos]', RESEED, 24244)
GO

-- ----------------------------
-- Table structure for PEDIDOSLIBERADOS
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PEDIDOSLIBERADOS]
GO
CREATE TABLE [dbo].[PEDIDOSLIBERADOS] (
[ID] int NOT NULL IDENTITY(1,1) ,
[PEDIDO] int NULL ,
[RUTA] int NULL ,
[COD_CLIENTE] varchar(50) NULL ,
[FOLIO_BO] varchar(50) NULL ,
[FECHA_PEDIDO] datetime NULL ,
[FECHA_LIBERACION] datetime NULL ,
[FECHA_ENTREGA] datetime NULL ,
[TIPO] varchar(50) NULL ,
[STATUS] int NULL ,
[CANCELADA] bit NULL ,
[INCIDENCIA] int NULL ,
[SUBTOTAL] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[TOTAL] money NULL ,
[KG] numeric(18,2) NULL ,
[IDVENDEDOR] int NULL ,
[ID_AYUDANTE1] int NULL ,
[ID_AYUDANTE2] int NULL ,
[IDEMPRESA] varchar(50) NULL ,
[RutaEnt] int NULL ,
[FormaPag] int NULL ,
[FECHA_PEDIDO_ENTREGADO] datetime NULL ,
[AGENTE_ENTREGA] int NULL ,
[CargaStock] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[PEDIDOSLIBERADOS]', RESEED, 10980)
GO

-- ----------------------------
-- Table structure for Perfiles
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Perfiles]
GO
CREATE TABLE [dbo].[Perfiles] (
[IdPerfiles] int NOT NULL IDENTITY(1,1) ,
[Altas] bit NULL ,
[Bajas] bit NULL ,
[Modi] bit NULL ,
[Listar] bit NULL ,
[Modulo] varchar(50) NULL ,
[IdUser] int NULL ,
[Aceso] bit NULL ,
[Codigo] int NULL ,
[usaPw] bit NULL ,
[Otro] bit NULL ,
[Descripcion] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Perfiles]', RESEED, 1535)
GO

-- ----------------------------
-- Table structure for Preg_Enc
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Preg_Enc]
GO
CREATE TABLE [dbo].[Preg_Enc] (
[Clave_Enc] varchar(50) NOT NULL ,
[Num_Preg] int NOT NULL ,
[Des_Preg] varchar(255) NOT NULL ,
[Tipo_Preg] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for PRegalado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PRegalado]
GO
CREATE TABLE [dbo].[PRegalado] (
[Id] int NOT NULL IDENTITY(1,1) ,
[SKU] nvarchar(50) NULL ,
[RutaId] int NULL ,
[Pz] numeric(18,2) NULL ,
[Kg] numeric(18,2) NULL ,
[DiaO] int NULL ,
[Docto] int NULL ,
[Cliente] varchar(50) NULL ,
[Cant] int NULL ,
[Tipmed] nvarchar(8) NULL ,
[IdEmpresa] varchar(50) NULL ,
[SKU_Base] varchar(50) NULL ,
[Multiplo_Base] numeric(18,2) NULL ,
[UM_Base] varchar(25) NULL ,
[Multiplo_Regalo] numeric(18,2) NULL ,
[UM_Regalo] varchar(25) NULL ,
[Tipo] char(1) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[PRegalado]', RESEED, 767791)
GO

-- ----------------------------
-- Table structure for ProductoEnvase
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ProductoEnvase]
GO
CREATE TABLE [dbo].[ProductoEnvase] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Producto] varchar(50) NULL ,
[Envase] nvarchar(MAX) NULL ,
[Cant_Base] int NULL ,
[Cant_Eq] int NULL ,
[Status] char(1) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ProductoEnvase]', RESEED, 1235)
GO

-- ----------------------------
-- Table structure for ProductoGenerico
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ProductoGenerico]
GO
CREATE TABLE [dbo].[ProductoGenerico] (
[Clave] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] bit NULL 
)


GO

-- ----------------------------
-- Table structure for ProductoNegado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ProductoNegado]
GO
CREATE TABLE [dbo].[ProductoNegado] (
[id] int NOT NULL IDENTITY(1,1) ,
[CodProd] nvarchar(50) NULL ,
[Fecha] nvarchar(50) NULL ,
[Pidieron] numeric(18,2) NULL ,
[Habia] numeric(18,2) NULL ,
[DiaO] int NULL ,
[RutaId] int NULL ,
[Docto] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ProductoPaseado
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ProductoPaseado]
GO
CREATE TABLE [dbo].[ProductoPaseado] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodProd] nvarchar(50) NULL ,
[RutaId] int NOT NULL ,
[DiaO] int NULL ,
[Stock] numeric(18,2) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ProductoPaseado]', RESEED, 91555)
GO

-- ----------------------------
-- Table structure for Productos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Productos]
GO
CREATE TABLE [dbo].[Productos] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(50) NOT NULL ,
[Producto] varchar(255) NULL ,
[CodBarras] varchar(50) NULL ,
[Granel] bit NULL ,
[IVA] tinyint NULL ,
[IEPS] tinyint NULL ,
[UniMed] varchar(50) NOT NULL ,
[VBase] money NULL ,
[Equivalente] float(53) NULL ,
[Ban_Envase] bit NULL ,
[Foto] image NULL ,
[idclasp] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Status] char(1) NULL ,
[cover_file_name] nvarchar(4000) NULL ,
[cover_content_type] nvarchar(4000) NULL ,
[cover_file_size] int NULL ,
[cover_updated_at] datetime NULL ,
[ClaveMarca] varchar(50) NULL ,
[UniMedEq] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Productos]', RESEED, 2579)
GO

-- ----------------------------
-- Table structure for ProductosXPzas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ProductosXPzas]
GO
CREATE TABLE [dbo].[ProductosXPzas] (
[IDP] int NOT NULL IDENTITY(1,1) ,
[Producto] varchar(50) NOT NULL ,
[PzaXCja] int NOT NULL ,
[StockxP] int NULL ,
[IdEmpresa] varchar(50) NOT NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ProductosXPzas]', RESEED, 2150)
GO

-- ----------------------------
-- Table structure for Promociones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Promociones]
GO
CREATE TABLE [dbo].[Promociones] (
[id] int NOT NULL IDENTITY(1,1) ,
[Promocion] nvarchar(MAX) NULL ,
[FecInicio] nvarchar(50) NULL ,
[FecFinal] nvarchar(50) NULL ,
[IdLista] int NULL 
)


GO

-- ----------------------------
-- Table structure for PVendidos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[PVendidos]
GO
CREATE TABLE [dbo].[PVendidos] (
[SKU] nvarchar(50) NOT NULL ,
[RutaId] int NULL ,
[Pz] numeric(18,2) NULL ,
[KG] numeric(18,2) NULL ,
[DiaO] int NULL ,
[ID] int NOT NULL IDENTITY(1,1) ,
[Tipo] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[PVendidos]', RESEED, 21056)
GO

-- ----------------------------
-- Table structure for RanComisiones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RanComisiones]
GO
CREATE TABLE [dbo].[RanComisiones] (
[IdRow] int NOT NULL IDENTITY(1,1) ,
[RangoA] numeric(18,2) NULL ,
[RangoF] numeric(18,2) NULL ,
[ComisionId] int NULL ,
[Valor] nvarchar(50) NULL ,
[Nivel] numeric(18) NULL ,
[Comision] numeric(18,2) NULL 
)


GO

-- ----------------------------
-- Table structure for Recarga
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Recarga]
GO
CREATE TABLE [dbo].[Recarga] (
[ID] int NOT NULL IDENTITY(1,1) ,
[IdRuta] int NOT NULL ,
[Articulo] varchar(50) NOT NULL ,
[Cantidad] int NOT NULL ,
[Fecha] datetime NULL ,
[Diao] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Folio] varchar(50) NULL ,
[Hora] datetime NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Recarga]', RESEED, 21895)
GO

-- ----------------------------
-- Table structure for RegLicencias
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RegLicencias]
GO
CREATE TABLE [dbo].[RegLicencias] (
[Clave] int NOT NULL IDENTITY(1,1) ,
[LicenciaMaster] nvarchar(50) NULL ,
[Empresa] nvarchar(MAX) NULL ,
[RFC] nvarchar(MAX) NULL ,
[Rew34] int NULL ,
[LicPocK] nvarchar(MAX) NULL ,
[Rew35] int NULL ,
[Email] nvarchar(MAX) NULL ,
[Re54] nvarchar(50) NULL ,
[Re55] nvarchar(50) NULL ,
[Re56] nvarchar(50) NULL ,
[Re57] nvarchar(50) NULL ,
[Ref56] nvarchar(MAX) NULL ,
[We34] int NULL ,
[We35] int NULL 
)


GO

-- ----------------------------
-- Table structure for RegPC
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RegPC]
GO
CREATE TABLE [dbo].[RegPC] (
[Clave] int NOT NULL IDENTITY(1,1) ,
[PC] nvarchar(50) NULL ,
[Licencia] nvarchar(50) NULL ,
[Status] int NULL ,
[CodeVer] nvarchar(MAX) NULL 
)


GO

-- ----------------------------
-- Table structure for Rel_EncRut
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Rel_EncRut]
GO
CREATE TABLE [dbo].[Rel_EncRut] (
[Clave_Enc] nvarchar(50) NULL ,
[Id_Ruta] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for RelActivos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelActivos]
GO
CREATE TABLE [dbo].[RelActivos] (
[IdRow] int NOT NULL IDENTITY(1,1) ,
[Cliente] varchar(50) NULL ,
[Activo] int NULL ,
[FechaAsignacion] datetime NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for RelCliClas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelCliClas]
GO
CREATE TABLE [dbo].[RelCliClas] (
[IdRcC] int NOT NULL IDENTITY(1,1) ,
[IdCliente] varchar(50) NULL ,
[Clas1] int NULL ,
[Clas2] int NULL ,
[Clas3] int NULL ,
[Clas4] int NULL ,
[Clas5] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelCliClas]', RESEED, 4762)
GO

-- ----------------------------
-- Table structure for RelCliLis
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelCliLis]
GO
CREATE TABLE [dbo].[RelCliLis] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodCliente] varchar(50) NOT NULL ,
[ListaP] int NULL ,
[ListaD] int NULL ,
[ListaPromo] int NULL ,
[DiaVisita] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelCliLis]', RESEED, 4991)
GO

-- ----------------------------
-- Table structure for RelClirutas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelClirutas]
GO
CREATE TABLE [dbo].[RelClirutas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdCliente] varchar(50) NULL ,
[IdRuta] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Fecha] date NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelClirutas]', RESEED, 164806)
GO

-- ----------------------------
-- Table structure for RelCuoVend
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelCuoVend]
GO
CREATE TABLE [dbo].[RelCuoVend] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CuoId] int NULL ,
[VendeId] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for RelDayCli
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelDayCli]
GO
CREATE TABLE [dbo].[RelDayCli] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodCli] varchar(50) NULL ,
[Lunes] int NULL ,
[Martes] int NULL ,
[Miercoles] int NULL ,
[Jueves] int NULL ,
[Viernes] int NULL ,
[Sabado] int NULL ,
[Domingo] int NULL ,
[RutaId] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[idVendedor] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelDayCli]', RESEED, 206857)
GO

-- ----------------------------
-- Table structure for RelGastos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelGastos]
GO
CREATE TABLE [dbo].[RelGastos] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Folio] int NULL ,
[RutaId] int NULL ,
[DiaO] int NULL ,
[CodigoOP] nvarchar(50) NULL ,
[Total] money NULL ,
[Fecha] datetime NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for RelMens
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelMens]
GO
CREATE TABLE [dbo].[RelMens] (
[IDRow] int NOT NULL IDENTITY(1,1) ,
[MenId] numeric(18) NULL ,
[CodCliente] nvarchar(MAX) NULL ,
[IdCliente] int NULL ,
[CodProducto] nvarchar(MAX) NULL ,
[IdProducto] int NULL ,
[CodRuta] int NULL ,
[IdRuta] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelMens]', RESEED, 120)
GO

-- ----------------------------
-- Table structure for RelOperaciones
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelOperaciones]
GO
CREATE TABLE [dbo].[RelOperaciones] (
[Id] numeric(18) NOT NULL ,
[Folio] int NULL ,
[RutaId] int NULL ,
[DiaO] int NULL ,
[CodCli] varchar(50) NULL ,
[Tipo] nvarchar(50) NULL ,
[Total] money NULL ,
[Fecha] datetime NULL ,
[IdK] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelOperaciones]', RESEED, 17767)
GO

-- ----------------------------
-- Table structure for RelProClas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelProClas]
GO
CREATE TABLE [dbo].[RelProClas] (
[IdRelPClas] int NOT NULL IDENTITY(1,1) ,
[ProductoId] varchar(50) NULL ,
[Clasif] int NULL ,
[Nivel] int NULL ,
[Dep_Clasif] int NULL ,
[Dep_Nivel] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for RelProGrupos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelProGrupos]
GO
CREATE TABLE [dbo].[RelProGrupos] (
[Id] int NOT NULL IDENTITY(1,1) ,
[ProductoId] varchar(50) NULL ,
[IdGrupo] varchar(50) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelProGrupos]', RESEED, 727)
GO

-- ----------------------------
-- Table structure for RelRuClas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelRuClas]
GO
CREATE TABLE [dbo].[RelRuClas] (
[IdRelRClas] int NOT NULL IDENTITY(1,1) ,
[RutaId] int NOT NULL ,
[Clas1] int NOT NULL ,
[Clas2] int NOT NULL ,
[Clas3] int NOT NULL ,
[Clas4] int NOT NULL ,
[Clas5] int NOT NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelRuClas]', RESEED, 173)
GO

-- ----------------------------
-- Table structure for RelVendrutas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RelVendrutas]
GO
CREATE TABLE [dbo].[RelVendrutas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdVendedor] int NULL ,
[IdRuta] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Fecha] datetime NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[RelVendrutas]', RESEED, 72)
GO

-- ----------------------------
-- Table structure for Res_Stock
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Res_Stock]
GO
CREATE TABLE [dbo].[Res_Stock] (
[IdStock] int NOT NULL IDENTITY(1,1) ,
[Articulo] varchar(50) NULL ,
[Stock] int NULL ,
[Ruta] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for ResCuotas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[ResCuotas]
GO
CREATE TABLE [dbo].[ResCuotas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CuoId] int NULL ,
[RutaId] int NULL ,
[Valor] numeric(18,2) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for Resp_Enc
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Resp_Enc]
GO
CREATE TABLE [dbo].[Resp_Enc] (
[Clave_Enc] varchar(50) NULL ,
[Num_Pregunta] int NULL ,
[IdRuta] int NULL ,
[Des_Resp] varchar(255) NULL ,
[IdCliente] varchar(50) NULL ,
[Fecha] datetime NULL ,
[DiaO] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for room_categories
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[room_categories]
GO
CREATE TABLE [dbo].[room_categories] (
[id] int NOT NULL IDENTITY(1,1) ,
[name] nvarchar(4000) NULL ,
[hotel_id] int NULL ,
[created_at] datetime NOT NULL ,
[updated_at] datetime NOT NULL 
)


GO

-- ----------------------------
-- Table structure for Rutas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Rutas]
GO
CREATE TABLE [dbo].[Rutas] (
[IdRutas] int NOT NULL IDENTITY(1,1) ,
[Ruta] varchar(255) NOT NULL ,
[Activa] bit NOT NULL ,
[TipoRuta] varchar(50) NOT NULL ,
[VenSinStock] int NOT NULL DEFAULT ((0)) ,
[Oficina] varchar(255) NOT NULL ,
[Sector] varchar(50) NOT NULL ,
[Vendedor] int NULL ,
[Tipo] int NOT NULL DEFAULT ((0)) ,
[id_ayudante1] int NULL DEFAULT ((0)) ,
[id_ayudante2] int NULL DEFAULT ((0)) ,
[VendedorAyudante] bit NOT NULL DEFAULT ((0)) ,
[Foranea] bit NULL DEFAULT ((0)) ,
[vehiculo] int NULL ,
[idcrutas] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Rutas]', RESEED, 48)
GO

-- ----------------------------
-- Table structure for RutasHistorico
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[RutasHistorico]
GO
CREATE TABLE [dbo].[RutasHistorico] (
[IdRutas] int NOT NULL ,
[Ruta] varchar(50) NOT NULL ,
[Activa] bit NOT NULL ,
[TipoRuta] varchar(255) NOT NULL ,
[VenSinStock] int NOT NULL ,
[Oficina] varchar(255) NOT NULL ,
[Sector] varchar(255) NOT NULL ,
[Vendedor] int NULL ,
[Tipo] int NOT NULL ,
[id_ayudante1] int NULL ,
[id_ayudante2] int NULL ,
[VendedorAyudante] bit NOT NULL ,
[Foranea] bit NULL ,
[vehiculo] int NULL ,
[empresa] varchar(50) NULL ,
[idcrutas] int NULL 
)


GO

-- ----------------------------
-- Table structure for Sabores
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Sabores]
GO
CREATE TABLE [dbo].[Sabores] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(10) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] bit NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Sabores]', RESEED, 76)
GO

-- ----------------------------
-- Table structure for schema_migrations
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[schema_migrations]
GO
CREATE TABLE [dbo].[schema_migrations] (
[version] nvarchar(4000) NOT NULL 
)


GO

-- ----------------------------
-- Table structure for SecRutas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[SecRutas]
GO
CREATE TABLE [dbo].[SecRutas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Rutad] int NULL ,
[Lunes] int NULL ,
[Martes] int NULL ,
[Miercoles] int NULL ,
[Jueves] int NULL ,
[Viernes] int NULL ,
[Sabado] int NULL ,
[Domingo] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for segmentos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[segmentos]
GO
CREATE TABLE [dbo].[segmentos] (
[idsegmento] int NOT NULL ,
[segmento] varchar(75) NOT NULL 
)


GO

-- ----------------------------
-- Table structure for Stock
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Stock]
GO
CREATE TABLE [dbo].[Stock] (
[IdStock] int NOT NULL IDENTITY(1,1) ,
[Articulo] varchar(50) NULL ,
[Stock] int NULL ,
[Ruta] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Stock]', RESEED, 15931)
GO

-- ----------------------------
-- Table structure for StockHistorico
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[StockHistorico]
GO
CREATE TABLE [dbo].[StockHistorico] (
[Articulo] varchar(50) NULL ,
[Stock] int NULL ,
[RutaID] int NULL ,
[Fecha] date NULL DEFAULT (getdate()) ,
[DiaO] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for StockPedidos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[StockPedidos]
GO
CREATE TABLE [dbo].[StockPedidos] (
[id] int NOT NULL IDENTITY(1,1) ,
[CodProd] nvarchar(50) NULL ,
[Stock] int NULL ,
[RutaId] int NULL ,
[Pedido] int NULL ,
[Status] int NULL 
)


GO

-- ----------------------------
-- Table structure for sysdiagrams
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[sysdiagrams]
GO
CREATE TABLE [dbo].[sysdiagrams] (
[name] sysname NOT NULL ,
[principal_id] int NOT NULL ,
[diagram_id] int NOT NULL IDENTITY(1,1) ,
[version] int NULL ,
[definition] varbinary(MAX) NULL 
)


GO

-- ----------------------------
-- Table structure for T_Log
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_Log]
GO
CREATE TABLE [dbo].[T_Log] (
[IdEmpresa] varchar(50) NULL ,
[Fecha] datetime NULL ,
[Ruta] int NULL ,
[Modulo] varchar(150) NULL ,
[Evento] varchar(MAX) NULL ,
[Correcta] bit NULL ,
[EnProceso] bit NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepConCarga
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepConCarga]
GO
CREATE TABLE [dbo].[T_RepConCarga] (
[Ruta] varchar(50) NULL ,
[2010100] int NULL ,
[2010101] int NULL ,
[2010102] int NULL ,
[2010103] int NULL ,
[2010104] int NULL ,
[2010105] int NULL ,
[2010106] int NULL ,
[2010107] int NULL ,
[2010108] int NULL ,
[2010109] int NULL ,
[2010400] int NULL ,
[2010401] int NULL ,
[2010402] int NULL ,
[2010403] int NULL ,
[2010404] int NULL ,
[2010406] int NULL ,
[2010407] int NULL ,
[2010408] int NULL ,
[2010409] int NULL ,
[2020100] int NULL ,
[2020101] int NULL ,
[2020102] int NULL ,
[2020103] int NULL ,
[2020104] int NULL ,
[2020105] int NULL ,
[2020106] int NULL ,
[2020107] int NULL ,
[2020108] int NULL ,
[2020109] int NULL ,
[2020113] int NULL ,
[2020200] int NULL ,
[2020201] int NULL ,
[2020202] int NULL ,
[2020203] int NULL ,
[2020204] int NULL ,
[2020205] int NULL ,
[2020206] int NULL ,
[2020207] int NULL ,
[2020208] int NULL ,
[2020209] int NULL ,
[2020213] int NULL ,
[2020300] int NULL ,
[2020301] int NULL ,
[2020302] int NULL ,
[2020303] int NULL ,
[2020304] int NULL ,
[2020305] int NULL ,
[2020306] int NULL ,
[2020307] int NULL ,
[2020308] int NULL ,
[2020309] int NULL ,
[2020313] int NULL ,
[2020325] int NULL ,
[2020500] int NULL ,
[2020501] int NULL ,
[2020504] int NULL ,
[2020507] int NULL ,
[2020509] int NULL ,
[2020513] int NULL ,
[2020900] int NULL ,
[2020901] int NULL ,
[2020903] int NULL ,
[2020904] int NULL ,
[2020906] int NULL ,
[2020907] int NULL ,
[2020908] int NULL ,
[2020909] int NULL ,
[2020913] int NULL ,
[2021200] int NULL ,
[2021201] int NULL ,
[2021204] int NULL ,
[2021208] int NULL ,
[2021209] int NULL ,
[2021213] int NULL ,
[2021226] int NULL ,
[2021227] int NULL ,
[2021500] int NULL ,
[2021501] int NULL ,
[2021503] int NULL ,
[2021504] int NULL ,
[2021506] int NULL ,
[2021507] int NULL ,
[2021508] int NULL ,
[2021509] int NULL ,
[2021513] int NULL ,
[2040300] int NULL ,
[2040306] int NULL ,
[2040307] int NULL ,
[2040308] int NULL ,
[2040309] int NULL ,
[2040310] int NULL ,
[2040311] int NULL ,
[2040312] int NULL ,
[2040314] int NULL ,
[2050200] int NULL ,
[2050206] int NULL ,
[2050207] int NULL ,
[2050208] int NULL ,
[2050209] int NULL ,
[2050210] int NULL ,
[2050211] int NULL ,
[2050212] int NULL ,
[2070100] int NULL ,
[2090215] int NULL ,
[2100100] int NULL ,
[2100200] int NULL ,
[2100300] int NULL ,
[2100400] int NULL ,
[2100409] int NULL ,
[2100411] int NULL ,
[2100418] int NULL ,
[2100700] int NULL ,
[2100800] int NULL ,
[2140200] int NULL ,
[2140202] int NULL ,
[2140206] int NULL ,
[2140207] int NULL ,
[2140208] int NULL ,
[2140209] int NULL ,
[2140210] int NULL ,
[2140211] int NULL ,
[2140212] int NULL ,
[2140213] int NULL ,
[2140600] int NULL ,
[2140607] int NULL ,
[2140608] int NULL ,
[2140609] int NULL ,
[2140610] int NULL ,
[2140612] int NULL ,
[2150100] int NULL ,
[2150102] int NULL ,
[2150106] int NULL ,
[2150107] int NULL ,
[2150108] int NULL ,
[2150109] int NULL ,
[2150110] int NULL ,
[2150111] int NULL ,
[2150112] int NULL ,
[2150113] int NULL ,
[2150114] int NULL ,
[2150200] int NULL ,
[2150202] int NULL ,
[2150206] int NULL ,
[2150207] int NULL ,
[2150208] int NULL ,
[2150209] int NULL ,
[2150210] int NULL ,
[2150211] int NULL ,
[2150212] int NULL ,
[2150213] int NULL ,
[2190100] int NULL ,
[2190102] int NULL ,
[2190106] int NULL ,
[2190107] int NULL ,
[2190108] int NULL ,
[2190109] int NULL ,
[2190110] int NULL ,
[2190111] int NULL ,
[2190112] int NULL ,
[2190113] int NULL ,
[2200500] int NULL ,
[2200501] int NULL ,
[2200502] int NULL ,
[2200503] int NULL ,
[2200504] int NULL ,
[2200507] int NULL ,
[2200508] int NULL ,
[2200509] int NULL ,
[2200513] int NULL ,
[2200600] int NULL ,
[2200601] int NULL ,
[2200602] int NULL ,
[2200603] int NULL ,
[2200604] int NULL ,
[2200605] int NULL ,
[2200606] int NULL ,
[2200607] int NULL ,
[2200608] int NULL ,
[2200609] int NULL ,
[2200613] int NULL ,
[2201000] int NULL ,
[2201001] int NULL ,
[2201002] int NULL ,
[2201003] int NULL ,
[2201004] int NULL ,
[2201006] int NULL ,
[2201007] int NULL ,
[2201008] int NULL ,
[2201009] int NULL ,
[2201013] int NULL ,
[2201102] int NULL ,
[2203000] int NULL ,
[2203001] int NULL ,
[2203003] int NULL ,
[2203004] int NULL ,
[2203007] int NULL ,
[2203008] int NULL ,
[2203013] int NULL ,
[2203100] int NULL ,
[2203101] int NULL ,
[2203102] int NULL ,
[2203103] int NULL ,
[2203104] int NULL ,
[2203108] int NULL ,
[2203109] int NULL ,
[2203113] int NULL ,
[2203200] int NULL ,
[2203201] int NULL ,
[2203203] int NULL ,
[2203204] int NULL ,
[2203209] int NULL ,
[2203213] int NULL ,
[2203400] int NULL ,
[2203401] int NULL ,
[2203404] int NULL ,
[2203409] int NULL ,
[2203413] int NULL ,
[2203426] int NULL ,
[2203500] int NULL ,
[2203501] int NULL ,
[2203504] int NULL ,
[2203509] int NULL ,
[2203513] int NULL ,
[2203600] int NULL ,
[2203601] int NULL ,
[2203603] int NULL ,
[2203604] int NULL ,
[2203608] int NULL ,
[2203609] int NULL ,
[2203613] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepConEnvComodato
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepConEnvComodato]
GO
CREATE TABLE [dbo].[T_RepConEnvComodato] (
[IdEmpresa] varchar(50) NULL ,
[Ruta] varchar(50) NULL ,
[12 CANASTA DE PLASTICO CAJA ] int NULL ,
[12 CANASTA DE PLASTICO PIEZAS ] int NULL ,
[16 TETRA GEMINA TRADICIONAL GEN.  1000 ML C/12 PZAS PIEZAS ] int NULL ,
[16 TETRA GEMINA TRADICIONAL GEN.  1000 ML C/12 PZAS CAJA ] int NULL ,
[2 Envase Pascual 12 Oz CAJA ] int NULL ,
[2 Envase Pascual 12 Oz PIEZAS ] int NULL ,
[2121501 TETRA GEMINA TRADICIONAL MANGO 1000 ML C/12 PZAS PIEZAS ] int NULL ,
[2121501 TETRA GEMINA TRADICIONAL MANGO 1000 ML C/12 PZAS CAJA ] int NULL ,
[30 CAJA DE PLASTICO CAJA ] int NULL ,
[30 CAJA DE PLASTICO PIEZAS ] int NULL ,
[4 Envase Boing Retornable 12 oz PIEZAS ] int NULL ,
[4 Envase Boing Retornable 12 oz CAJA ] int NULL ,
[6 ENVASE LULÚ 12 oz CAJA ] int NULL ,
[6 ENVASE LULÚ 12 oz PIEZAS ] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepConEnvPromo
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepConEnvPromo]
GO
CREATE TABLE [dbo].[T_RepConEnvPromo] (
[IdEmpresa] varchar(50) NULL ,
[Ruta] varchar(50) NULL ,
[12 CANASTA DE PLASTICO CAJA ] int NULL ,
[12 CANASTA DE PLASTICO PIEZAS ] int NULL ,
[16 TETRA GEMINA TRADICIONAL GEN.  1000 ML C/12 PZAS PIEZAS ] int NULL ,
[16 TETRA GEMINA TRADICIONAL GEN.  1000 ML C/12 PZAS CAJA ] int NULL ,
[2 Envase Pascual 12 Oz CAJA ] int NULL ,
[2 Envase Pascual 12 Oz PIEZAS ] int NULL ,
[2121501 TETRA GEMINA TRADICIONAL MANGO 1000 ML C/12 PZAS PIEZAS ] int NULL ,
[2121501 TETRA GEMINA TRADICIONAL MANGO 1000 ML C/12 PZAS CAJA ] int NULL ,
[30 CAJA DE PLASTICO CAJA ] int NULL ,
[30 CAJA DE PLASTICO PIEZAS ] int NULL ,
[4 Envase Boing Retornable 12 oz PIEZAS ] int NULL ,
[4 Envase Boing Retornable 12 oz CAJA ] int NULL ,
[6 ENVASE LULÚ 12 oz CAJA ] int NULL ,
[6 ENVASE LULÚ 12 oz PIEZAS ] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepConPed
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepConPed]
GO
CREATE TABLE [dbo].[T_RepConPed] (
[IdEmpresa] varchar(50) NULL ,
[Ruta] varchar(50) NULL ,
[2010100] int NULL ,
[2010101] int NULL ,
[2010102] int NULL ,
[2010103] int NULL ,
[2010104] int NULL ,
[2010105] int NULL ,
[2010106] int NULL ,
[2010107] int NULL ,
[2010108] int NULL ,
[2010109] int NULL ,
[2010400] int NULL ,
[2010401] int NULL ,
[2010402] int NULL ,
[2010403] int NULL ,
[2010404] int NULL ,
[2010406] int NULL ,
[2010407] int NULL ,
[2010408] int NULL ,
[2010409] int NULL ,
[2020100] int NULL ,
[2020101] int NULL ,
[2020102] int NULL ,
[2020103] int NULL ,
[2020104] int NULL ,
[2020105] int NULL ,
[2020106] int NULL ,
[2020107] int NULL ,
[2020108] int NULL ,
[2020109] int NULL ,
[2020113] int NULL ,
[2020200] int NULL ,
[2020201] int NULL ,
[2020202] int NULL ,
[2020203] int NULL ,
[2020204] int NULL ,
[2020205] int NULL ,
[2020206] int NULL ,
[2020207] int NULL ,
[2020208] int NULL ,
[2020209] int NULL ,
[2020213] int NULL ,
[2020300] int NULL ,
[2020301] int NULL ,
[2020302] int NULL ,
[2020303] int NULL ,
[2020304] int NULL ,
[2020305] int NULL ,
[2020306] int NULL ,
[2020307] int NULL ,
[2020308] int NULL ,
[2020309] int NULL ,
[2020313] int NULL ,
[2020325] int NULL ,
[2020500] int NULL ,
[2020501] int NULL ,
[2020504] int NULL ,
[2020507] int NULL ,
[2020509] int NULL ,
[2020513] int NULL ,
[2020900] int NULL ,
[2020901] int NULL ,
[2020903] int NULL ,
[2020904] int NULL ,
[2020906] int NULL ,
[2020907] int NULL ,
[2020908] int NULL ,
[2020909] int NULL ,
[2020913] int NULL ,
[2021200] int NULL ,
[2021201] int NULL ,
[2021204] int NULL ,
[2021208] int NULL ,
[2021209] int NULL ,
[2021213] int NULL ,
[2021226] int NULL ,
[2021227] int NULL ,
[2021500] int NULL ,
[2021501] int NULL ,
[2021503] int NULL ,
[2021504] int NULL ,
[2021506] int NULL ,
[2021507] int NULL ,
[2021508] int NULL ,
[2021509] int NULL ,
[2021513] int NULL ,
[2040300] int NULL ,
[2040306] int NULL ,
[2040307] int NULL ,
[2040308] int NULL ,
[2040309] int NULL ,
[2040310] int NULL ,
[2040311] int NULL ,
[2040312] int NULL ,
[2040314] int NULL ,
[2050200] int NULL ,
[2050206] int NULL ,
[2050207] int NULL ,
[2050208] int NULL ,
[2050209] int NULL ,
[2050210] int NULL ,
[2050211] int NULL ,
[2050212] int NULL ,
[2070100] int NULL ,
[2090215] int NULL ,
[2100100] int NULL ,
[2100200] int NULL ,
[2100300] int NULL ,
[2100400] int NULL ,
[2100409] int NULL ,
[2100411] int NULL ,
[2100418] int NULL ,
[2100700] int NULL ,
[2100800] int NULL ,
[2140200] int NULL ,
[2140202] int NULL ,
[2140206] int NULL ,
[2140207] int NULL ,
[2140208] int NULL ,
[2140209] int NULL ,
[2140210] int NULL ,
[2140211] int NULL ,
[2140212] int NULL ,
[2140213] int NULL ,
[2140600] int NULL ,
[2140607] int NULL ,
[2140608] int NULL ,
[2140609] int NULL ,
[2140610] int NULL ,
[2140612] int NULL ,
[2150100] int NULL ,
[2150102] int NULL ,
[2150106] int NULL ,
[2150107] int NULL ,
[2150108] int NULL ,
[2150109] int NULL ,
[2150110] int NULL ,
[2150111] int NULL ,
[2150112] int NULL ,
[2150113] int NULL ,
[2150114] int NULL ,
[2150200] int NULL ,
[2150202] int NULL ,
[2150206] int NULL ,
[2150207] int NULL ,
[2150208] int NULL ,
[2150209] int NULL ,
[2150210] int NULL ,
[2150211] int NULL ,
[2150212] int NULL ,
[2150213] int NULL ,
[2190100] int NULL ,
[2190102] int NULL ,
[2190106] int NULL ,
[2190107] int NULL ,
[2190108] int NULL ,
[2190109] int NULL ,
[2190110] int NULL ,
[2190111] int NULL ,
[2190112] int NULL ,
[2190113] int NULL ,
[2200500] int NULL ,
[2200501] int NULL ,
[2200502] int NULL ,
[2200503] int NULL ,
[2200504] int NULL ,
[2200507] int NULL ,
[2200508] int NULL ,
[2200509] int NULL ,
[2200513] int NULL ,
[2200600] int NULL ,
[2200601] int NULL ,
[2200602] int NULL ,
[2200603] int NULL ,
[2200604] int NULL ,
[2200605] int NULL ,
[2200606] int NULL ,
[2200607] int NULL ,
[2200608] int NULL ,
[2200609] int NULL ,
[2200613] int NULL ,
[2201000] int NULL ,
[2201001] int NULL ,
[2201002] int NULL ,
[2201003] int NULL ,
[2201004] int NULL ,
[2201006] int NULL ,
[2201007] int NULL ,
[2201008] int NULL ,
[2201009] int NULL ,
[2201013] int NULL ,
[2201102] int NULL ,
[2203000] int NULL ,
[2203001] int NULL ,
[2203003] int NULL ,
[2203004] int NULL ,
[2203007] int NULL ,
[2203008] int NULL ,
[2203013] int NULL ,
[2203100] int NULL ,
[2203101] int NULL ,
[2203102] int NULL ,
[2203103] int NULL ,
[2203104] int NULL ,
[2203108] int NULL ,
[2203109] int NULL ,
[2203113] int NULL ,
[2203200] int NULL ,
[2203201] int NULL ,
[2203203] int NULL ,
[2203204] int NULL ,
[2203209] int NULL ,
[2203213] int NULL ,
[2203400] int NULL ,
[2203401] int NULL ,
[2203404] int NULL ,
[2203409] int NULL ,
[2203413] int NULL ,
[2203426] int NULL ,
[2203500] int NULL ,
[2203501] int NULL ,
[2203504] int NULL ,
[2203509] int NULL ,
[2203513] int NULL ,
[2203600] int NULL ,
[2203601] int NULL ,
[2203603] int NULL ,
[2203604] int NULL ,
[2203608] int NULL ,
[2203609] int NULL ,
[2203613] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepConPedPz
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepConPedPz]
GO
CREATE TABLE [dbo].[T_RepConPedPz] (
[IdEmpresa] varchar(50) NULL ,
[Ruta] varchar(50) NULL ,
[2010100] int NULL ,
[2010101] int NULL ,
[2010102] int NULL ,
[2010103] int NULL ,
[2010104] int NULL ,
[2010105] int NULL ,
[2010106] int NULL ,
[2010107] int NULL ,
[2010108] int NULL ,
[2010109] int NULL ,
[2010400] int NULL ,
[2010401] int NULL ,
[2010402] int NULL ,
[2010403] int NULL ,
[2010404] int NULL ,
[2010406] int NULL ,
[2010407] int NULL ,
[2010408] int NULL ,
[2010409] int NULL ,
[2020100] int NULL ,
[2020101] int NULL ,
[2020102] int NULL ,
[2020103] int NULL ,
[2020104] int NULL ,
[2020105] int NULL ,
[2020106] int NULL ,
[2020107] int NULL ,
[2020108] int NULL ,
[2020109] int NULL ,
[2020113] int NULL ,
[2020200] int NULL ,
[2020201] int NULL ,
[2020202] int NULL ,
[2020203] int NULL ,
[2020204] int NULL ,
[2020205] int NULL ,
[2020206] int NULL ,
[2020207] int NULL ,
[2020208] int NULL ,
[2020209] int NULL ,
[2020213] int NULL ,
[2020300] int NULL ,
[2020301] int NULL ,
[2020302] int NULL ,
[2020303] int NULL ,
[2020304] int NULL ,
[2020305] int NULL ,
[2020306] int NULL ,
[2020307] int NULL ,
[2020308] int NULL ,
[2020309] int NULL ,
[2020313] int NULL ,
[2020325] int NULL ,
[2020500] int NULL ,
[2020501] int NULL ,
[2020504] int NULL ,
[2020507] int NULL ,
[2020509] int NULL ,
[2020513] int NULL ,
[2020900] int NULL ,
[2020901] int NULL ,
[2020903] int NULL ,
[2020904] int NULL ,
[2020906] int NULL ,
[2020907] int NULL ,
[2020908] int NULL ,
[2020909] int NULL ,
[2020913] int NULL ,
[2021200] int NULL ,
[2021201] int NULL ,
[2021204] int NULL ,
[2021208] int NULL ,
[2021209] int NULL ,
[2021213] int NULL ,
[2021226] int NULL ,
[2021227] int NULL ,
[2021500] int NULL ,
[2021501] int NULL ,
[2021503] int NULL ,
[2021504] int NULL ,
[2021506] int NULL ,
[2021507] int NULL ,
[2021508] int NULL ,
[2021509] int NULL ,
[2021513] int NULL ,
[2040300] int NULL ,
[2040306] int NULL ,
[2040307] int NULL ,
[2040308] int NULL ,
[2040309] int NULL ,
[2040310] int NULL ,
[2040311] int NULL ,
[2040312] int NULL ,
[2040314] int NULL ,
[2050200] int NULL ,
[2050206] int NULL ,
[2050207] int NULL ,
[2050208] int NULL ,
[2050209] int NULL ,
[2050210] int NULL ,
[2050211] int NULL ,
[2050212] int NULL ,
[2070100] int NULL ,
[2090215] int NULL ,
[2100100] int NULL ,
[2100200] int NULL ,
[2100300] int NULL ,
[2100400] int NULL ,
[2100409] int NULL ,
[2100411] int NULL ,
[2100418] int NULL ,
[2100700] int NULL ,
[2100800] int NULL ,
[2140200] int NULL ,
[2140202] int NULL ,
[2140206] int NULL ,
[2140207] int NULL ,
[2140208] int NULL ,
[2140209] int NULL ,
[2140210] int NULL ,
[2140211] int NULL ,
[2140212] int NULL ,
[2140213] int NULL ,
[2140600] int NULL ,
[2140607] int NULL ,
[2140608] int NULL ,
[2140609] int NULL ,
[2140610] int NULL ,
[2140612] int NULL ,
[2150100] int NULL ,
[2150102] int NULL ,
[2150106] int NULL ,
[2150107] int NULL ,
[2150108] int NULL ,
[2150109] int NULL ,
[2150110] int NULL ,
[2150111] int NULL ,
[2150112] int NULL ,
[2150113] int NULL ,
[2150114] int NULL ,
[2150200] int NULL ,
[2150202] int NULL ,
[2150206] int NULL ,
[2150207] int NULL ,
[2150208] int NULL ,
[2150209] int NULL ,
[2150210] int NULL ,
[2150211] int NULL ,
[2150212] int NULL ,
[2150213] int NULL ,
[2190100] int NULL ,
[2190102] int NULL ,
[2190106] int NULL ,
[2190107] int NULL ,
[2190108] int NULL ,
[2190109] int NULL ,
[2190110] int NULL ,
[2190111] int NULL ,
[2190112] int NULL ,
[2190113] int NULL ,
[2200500] int NULL ,
[2200501] int NULL ,
[2200502] int NULL ,
[2200503] int NULL ,
[2200504] int NULL ,
[2200507] int NULL ,
[2200508] int NULL ,
[2200509] int NULL ,
[2200513] int NULL ,
[2200600] int NULL ,
[2200601] int NULL ,
[2200602] int NULL ,
[2200603] int NULL ,
[2200604] int NULL ,
[2200605] int NULL ,
[2200606] int NULL ,
[2200607] int NULL ,
[2200608] int NULL ,
[2200609] int NULL ,
[2200613] int NULL ,
[2201000] int NULL ,
[2201001] int NULL ,
[2201002] int NULL ,
[2201003] int NULL ,
[2201004] int NULL ,
[2201006] int NULL ,
[2201007] int NULL ,
[2201008] int NULL ,
[2201009] int NULL ,
[2201013] int NULL ,
[2201102] int NULL ,
[2203000] int NULL ,
[2203001] int NULL ,
[2203003] int NULL ,
[2203004] int NULL ,
[2203007] int NULL ,
[2203008] int NULL ,
[2203013] int NULL ,
[2203100] int NULL ,
[2203101] int NULL ,
[2203102] int NULL ,
[2203103] int NULL ,
[2203104] int NULL ,
[2203108] int NULL ,
[2203109] int NULL ,
[2203113] int NULL ,
[2203200] int NULL ,
[2203201] int NULL ,
[2203203] int NULL ,
[2203204] int NULL ,
[2203209] int NULL ,
[2203213] int NULL ,
[2203400] int NULL ,
[2203401] int NULL ,
[2203404] int NULL ,
[2203409] int NULL ,
[2203413] int NULL ,
[2203426] int NULL ,
[2203500] int NULL ,
[2203501] int NULL ,
[2203504] int NULL ,
[2203509] int NULL ,
[2203513] int NULL ,
[2203600] int NULL ,
[2203601] int NULL ,
[2203603] int NULL ,
[2203604] int NULL ,
[2203608] int NULL ,
[2203609] int NULL ,
[2203613] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepFidelidad
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepFidelidad]
GO
CREATE TABLE [dbo].[T_RepFidelidad] (
[Ruta] varchar(50) NULL ,
[IdCliente] varchar(50) NULL ,
[Cliente] varchar(50) NULL ,
[Vendedor] varchar(50) NULL ,
[Status] varchar(50) NULL ,
[Jue 5/5/2016] varchar(1) NULL 
)


GO

-- ----------------------------
-- Table structure for T_RepProdPedidos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_RepProdPedidos]
GO
CREATE TABLE [dbo].[T_RepProdPedidos] (
[FechaP] varchar(50) NULL ,
[FechaE] varchar(50) NULL ,
[Fecha] varchar(50) NULL ,
[Status_Pedido] varchar(50) NULL ,
[promocion] int NULL ,
[Ruta] varchar(50) NULL ,
[Vendedor] varchar(50) NULL ,
[Pedido] varchar(50) NULL ,
[IdCliente] varchar(50) NULL ,
[Cliente] varchar(50) NULL ,
[NombreCorto] varchar(50) NULL ,
[Generico] varchar(50) NULL ,
[Cajas] int NULL ,
[Piezas] int NULL ,
[PzaXCja] varchar(50) NULL ,
[UNICO] int NULL ,
[MANGO] int NULL ,
[TAMARINDO] int NULL ,
[FRESA] int NULL ,
[GUAYABA] int NULL ,
[GUANABANA] int NULL ,
[PINA] int NULL ,
[NARANJA] int NULL ,
[UVA] int NULL ,
[MANZANA] int NULL ,
[FRAMBUESA] int NULL ,
[TORONJA] int NULL ,
[LIMON] int NULL ,
[DURAZNO] int NULL ,
[SANGRIA] int NULL ,
[COLA] int NULL 
)


GO

-- ----------------------------
-- Table structure for T_TempLiquidacionesAux
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[T_TempLiquidacionesAux]
GO
CREATE TABLE [dbo].[T_TempLiquidacionesAux] (
[RutaId] varchar(50) NULL ,
[Fecha] varchar(20) NULL ,
[Clave] varchar(15) NULL ,
[VentaCajas] int NULL ,
[VentaPiezas] int NULL ,
[PromoCajas] int NULL ,
[PromoPiezas] int NULL ,
[TipoObse] varchar(20) NULL ,
[CantObse] int NULL 
)


GO

-- ----------------------------
-- Table structure for TD_Comision
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TD_Comision]
GO
CREATE TABLE [dbo].[TD_Comision] (
[ID_Comision] int NOT NULL ,
[ID_Detalle] int NOT NULL ,
[RangoIni] int NULL ,
[RangoFin] int NULL ,
[Angente] float(53) NULL ,
[Ayudante] float(53) NULL ,
[Tipo] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO

-- ----------------------------
-- Table structure for Td_StockAlmacen
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Td_StockAlmacen]
GO
CREATE TABLE [dbo].[Td_StockAlmacen] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[Articulo] varchar(50) NULL ,
[Comp1] varchar(50) NULL ,
[Comp2] varchar(50) NULL ,
[Comp3] varchar(50) NULL ,
[Comp4] varchar(50) NULL ,
[Comp5] varchar(50) NULL ,
[Stock] numeric(18,4) NULL 
)


GO

-- ----------------------------
-- Table structure for TH_Comision
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TH_Comision]
GO
CREATE TABLE [dbo].[TH_Comision] (
[ID_Comision] int NOT NULL IDENTITY(1,1) ,
[ID_Producto] int NOT NULL ,
[Status] char(1) NULL ,
[Comentarios] varchar(MAX) NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[TH_Comision]', RESEED, 79)
GO

-- ----------------------------
-- Table structure for TH_SecVisitas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TH_SecVisitas]
GO
CREATE TABLE [dbo].[TH_SecVisitas] (
[Id] int NOT NULL IDENTITY(1,1) ,
[CodCli] varchar(50) NOT NULL ,
[RutaId] int NOT NULL ,
[Secuencia] int NULL ,
[Dia] char(2) NULL ,
[Fecha] date NULL ,
[IdEmpresa] varchar(50) NULL ,
[IdVendedor] int NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[TH_SecVisitas]', RESEED, 26919)
GO

-- ----------------------------
-- Table structure for Th_StockAlmacen
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Th_StockAlmacen]
GO
CREATE TABLE [dbo].[Th_StockAlmacen] (
[Id] int NOT NULL IDENTITY(1,1) ,
[IdEmpresa] varchar(50) NULL ,
[Articulo] varchar(50) NULL ,
[Stock] numeric(18,4) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Th_StockAlmacen]', RESEED, 3000)
GO

-- ----------------------------
-- Table structure for TInvFis
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TInvFis]
GO
CREATE TABLE [dbo].[TInvFis] (
[IdRow] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(25) NOT NULL ,
[Descripcion] varchar(250) NULL ,
[UniMed] varchar(5) NULL ,
[Stock_T] int NOT NULL ,
[Stock_R] int NOT NULL ,
[Fec_cad] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for Tmp_Stock
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Tmp_Stock]
GO
CREATE TABLE [dbo].[Tmp_Stock] (
[Articulo] varchar(50) NULL ,
[Stock] int NULL ,
[Ruta] int NULL 
)


GO

-- ----------------------------
-- Table structure for TMP_StockEnvases
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TMP_StockEnvases]
GO
CREATE TABLE [dbo].[TMP_StockEnvases] (
[Articulo] varchar(50) NULL ,
[Stock] int NULL ,
[Ruta] int NULL 
)


GO

-- ----------------------------
-- Table structure for TmpCoordCtes
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TmpCoordCtes]
GO
CREATE TABLE [dbo].[TmpCoordCtes] (
[Codigo] varchar(50) NULL ,
[Latitude] float(53) NULL ,
[Latitud] int NULL ,
[Longitude] float(53) NULL ,
[Longitud] int NULL ,
[Fecha] datetime NULL 
)


GO

-- ----------------------------
-- Table structure for TmpCteLimCred
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[TmpCteLimCred]
GO
CREATE TABLE [dbo].[TmpCteLimCred] (
[IdCli] varchar(50) NULL ,
[LimiteCredito] money NULL 
)


GO

-- ----------------------------
-- Table structure for Usuarios
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Usuarios]
GO
CREATE TABLE [dbo].[Usuarios] (
[IdUsuario] int NOT NULL IDENTITY(1,1) ,
[Nombre] varchar(MAX) NULL ,
[IdPerfil] int NULL ,
[empresa_id] varchar(50) NULL ,
[email] nvarchar(4000) NOT NULL DEFAULT (N'') ,
[encrypted_password] nvarchar(4000) NOT NULL DEFAULT (N'') ,
[usuario] nvarchar(4000) NOT NULL DEFAULT (N'') ,
[reset_password_token] nvarchar(4000) NULL ,
[reset_password_sent_at] datetime NULL ,
[remember_created_at] datetime NULL ,
[sign_in_count] int NOT NULL DEFAULT ((0)) ,
[current_sign_in_at] datetime NULL ,
[last_sign_in_at] datetime NULL ,
[current_sign_in_ip] nvarchar(4000) NULL ,
[last_sign_in_ip] nvarchar(4000) NULL ,
[empresamadre_id] int NULL ,
[password_reset_token] nvarchar(4000) NULL ,
[password_reset_sent_at] datetime NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Usuarios]', RESEED, 140)
GO

-- ----------------------------
-- Table structure for Vehiculos
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Vehiculos]
GO
CREATE TABLE [dbo].[Vehiculos] (
[IdVehiculos] int NOT NULL IDENTITY(1,1) ,
[Clave] varchar(MAX) NULL ,
[Modelo] varchar(MAX) NULL ,
[Placas] varchar(MAX) NULL ,
[Marcas] varchar(50) NOT NULL ,
[Descripcion] varchar(MAX) NULL ,
[Status] nvarchar(MAX) NULL ,
[NumeroEco] varchar(MAX) NULL ,
[Asignado] bit NULL ,
[Poliza] varchar(MAX) NULL ,
[TelSeguro] varchar(50) NULL ,
[MesVerifica] datetime NULL ,
[Kilometraje] varchar(50) NULL ,
[KilometrajeSem] varchar(50) NULL ,
[Aseguradora] varchar(MAX) NULL ,
[FechaVencSeguro] datetime NULL ,
[FechaUltVerif] datetime NULL ,
[IdEmpresa] varchar(50) NULL ,
[Modelo_year] nvarchar(4) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Vehiculos]', RESEED, 76)
GO

-- ----------------------------
-- Table structure for Vendedores
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Vendedores]
GO
CREATE TABLE [dbo].[Vendedores] (
[IdVendedor] int NOT NULL IDENTITY(1,1) ,
[Nombre] varchar(255) NOT NULL ,
[IdVehiculo] varchar(255) NULL ,
[Status] bit NOT NULL ,
[Direccion] varchar(255) NOT NULL ,
[Telefono] varchar(50) NOT NULL ,
[PdaPw] varchar(50) NOT NULL ,
[NumLicencia] varchar(255) NOT NULL ,
[MetaDiaria] varchar(50) NOT NULL ,
[MetaMes] varchar(50) NOT NULL ,
[Movil] varchar(50) NOT NULL ,
[VenceLic] datetime NOT NULL ,
[Clave] varchar(255) NOT NULL ,
[ID_Ayudante1] int NULL ,
[ID_Ayudante2] int NULL ,
[IdEmpresa] varchar(50) NULL ,
[Tipo] nvarchar(4000) NULL ,
[CP] nvarchar(5) NULL ,
[Colonia] nvarchar(100) NULL ,
[Referencia] nvarchar(200) NULL ,
[Latitud] decimal(11,7) NULL ,
[Longitud] decimal(11,7) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Vendedores]', RESEED, 102)
GO

-- ----------------------------
-- Table structure for Venta
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Venta]
GO
CREATE TABLE [dbo].[Venta] (
[Id] int NOT NULL IDENTITY(1,1) ,
[RutaId] int NULL ,
[VendedorId] int NULL ,
[CodCliente] nvarchar(MAX) NULL ,
[Documento] int NULL ,
[Fecha] datetime NULL ,
[TipoVta] nvarchar(50) NULL ,
[DiasCred] int NULL ,
[CreditoDispo] numeric(18,2) NULL ,
[Saldo] numeric(18,2) NULL ,
[Fvence] date NULL ,
[SubTotal] money NULL ,
[IVA] money NULL ,
[IEPS] money NULL ,
[TOTAL] money NULL ,
[EnLetra] nvarchar(MAX) NULL ,
[Items] int NULL ,
[FormaPag] int NULL ,
[DocSalida] nvarchar(50) NULL ,
[DiaO] int NULL ,
[Cancelada] bit NULL ,
[Kg] numeric(18,2) NULL ,
[ID_Ayudante1] int NULL ,
[ID_Ayudante2] int NULL ,
[VenAyunate] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Venta]', RESEED, 711592)
GO

-- ----------------------------
-- Table structure for Visitas
-- ----------------------------
DROP TABLE IF EXISTS [dbo].[Visitas]
GO
CREATE TABLE [dbo].[Visitas] (
[CodCliente] nvarchar(50) NOT NULL ,
[DiaO] int NOT NULL ,
[FechaI] datetime NULL ,
[EnSecuencia] bit NULL ,
[FechaF] datetime NULL ,
[Venta] numeric(18) NULL ,
[Pedido] numeric(18) NULL ,
[Devolucion] numeric(18) NULL ,
[Cobranza] numeric(18) NULL ,
[Id] int NOT NULL IDENTITY(1,1) ,
[IdCe] numeric(18) NULL ,
[ruta] int NULL ,
[IdEmpresa] varchar(50) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[Visitas]', RESEED, 19187)
GO

-- ----------------------------
-- View structure for Etiquetas_Clientes
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Etiquetas_Clientes]
GO
CREATE VIEW [dbo].[Etiquetas_Clientes] AS 
SELECT     Nombre, IdCli
FROM         dbo.Clientes
GO

-- ----------------------------
-- View structure for Nveta
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Nveta]
GO
CREATE VIEW [dbo].[Nveta] AS 
SELECT     dbo.MotivosNoVenta.Motivo, dbo.Rutas.Ruta, dbo.Noventas.Cliente, dbo.Noventas.DiaO
FROM         dbo.MotivosNoVenta INNER JOIN
                      dbo.Noventas ON dbo.MotivosNoVenta.IdMot = dbo.Noventas.MotivoId INNER JOIN
                      dbo.Rutas ON dbo.Noventas.RutaId = dbo.Rutas.IdRutas
GO

-- ----------------------------
-- View structure for V_CantidadPzaxVta
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_CantidadPzaxVta]
GO
CREATE VIEW [dbo].[V_CantidadPzaxVta] AS 
SELECT  DetalleVet.Docto, DetalleVet.Articulo,
	(CASE WHEN ProductosXPzas.pzaxcja = '1' THEN 0 ELSE CASE WHEN DetalleVet.Tipo = 0 THEN ISNULL(dbo.DetalleVet.Pza, 0) * ProductosXPzas.PzaXCja 
	ELSE 0 END END) + (CASE WHEN ProductosXPzas.pzaxcja = '1' THEN ISNULL(dbo.DetalleVet.Pza, 0) ELSE CASE WHEN DetalleVet.Tipo = 1 THEN ISNULL(dbo.DetalleVet.Pza, 0) ELSE 0 END END) AS Pzas,
	DetalleVet.RutaId , DetalleVet.IdEmpresa
	FROM            Productos INNER JOIN
	ProductosXPzas ON Productos.Clave = ProductosXPzas.Producto INNER JOIN
	detallevet ON Productos.Clave = detallevet.articulo
GO

-- ----------------------------
-- View structure for V_CantidadxPedido
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_CantidadxPedido]
GO
CREATE VIEW [dbo].[V_CantidadxPedido] AS 
SELECT  DetallePedidos.Docto, DetallePedidos.SKU,
 CASE WHEN ProductosXPzas.pzaxcja = '1' THEN 0 ELSE CASE WHEN DetallePedidos.Tipo = 0 THEN ISNULL(dbo.DetallePedidos.Pza, 0) ELSE ISNULL(dbo.DetallePedidos.Pza, 0) 
                         / ProductosXPzas.PzaXCja END END AS Cajas, CASE WHEN ProductosXPzas.pzaxcja = '1' THEN ISNULL(dbo.DetallePedidos.Pza, 0) 
                         ELSE CASE WHEN DetallePedidos.Tipo = 1 THEN ISNULL(dbo.DetallePedidos.Pza % ProductosXPzas.PzaXCja, 0) ELSE 0 END END AS Pzas,
						 DetallePedidos.RutaId , DetallePedidos.IdEmpresa
FROM            Productos INNER JOIN
                         ProductosXPzas ON Productos.Clave = ProductosXPzas.Producto INNER JOIN
                         DetallePedidos ON Productos.Clave = DetallePedidos.SKU  -- AND Productos.IdEmpresa = DetallePedidos.IdEmpresa
GO

-- ----------------------------
-- View structure for V_Encuesta
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_Encuesta]
GO
CREATE VIEW [dbo].[V_Encuesta] AS 
SELECT        Encuestas.Clave_Enc, Resp_Enc.Fecha, Resp_Enc.IdRuta AS 'ZonaPrecios', Clientes.NombreCorto AS 'PDV',
Vendedores.IdVendedor AS 'Vendedor', Resp_Enc.Num_Pregunta AS 'NumPregunta', Preg_Enc.Des_Preg AS 'Pregunta', 
                         Resp_Enc.Des_Resp AS 'Respuesta', Resp_Enc.IdCliente, Resp_Enc.IdRuta
FROM            Encuestas INNER JOIN
                         Preg_Enc ON Encuestas.Clave_Enc = Preg_Enc.Clave_Enc AND Encuestas.IdEmpresa = Preg_Enc.IdEmpresa INNER JOIN
                         Resp_Enc ON Preg_Enc.Clave_Enc = Resp_Enc.Clave_Enc AND Preg_Enc.Num_Preg = Resp_Enc.Num_Pregunta INNER JOIN
                         Rutas ON Resp_Enc.IdRuta = Rutas.IdRutas AND Resp_Enc.IdEmpresa = Rutas.IdEmpresa INNER JOIN
                         Clientes ON Resp_Enc.IdCliente = Clientes.IdCli LEFT OUTER JOIN
                         Vendedores ON Rutas.IdEmpresa = Vendedores.IdEmpresa AND Rutas.Vendedor = Vendedores.IdVendedor
GO

-- ----------------------------
-- View structure for V_Envases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_Envases]
GO
CREATE VIEW [dbo].[V_Envases] AS 
Select	CodCli,Articulo,SUM(Cantidad) Saldo,SUM(Devuelto) Devuelto,SUM(Cantidad)-SUM(Devuelto) Saldo_Actual,Envase
	From	DevEnvases
	Where	Tipo='Comodato'
	Group By CodCli,Articulo,Envase
GO

-- ----------------------------
-- View structure for V_ProductosGenericos
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_ProductosGenericos]
GO
CREATE VIEW [dbo].[V_ProductosGenericos] AS 
SELECT PEDIDOSLIBERADOS.FECHA_PEDIDO ,PEDIDOSLIBERADOS.FECHA_ENTREGA, Pedidosliberados.Status,
	CASE WHEN Pedidosliberados.Status=1 THEN 'PENDIENTE' WHEN Pedidosliberados.Status=2 THEN 'LIBERADO' WHEN Pedidosliberados.Status=3 THEN 'SURTIDO' WHEN Pedidosliberados.Status=4 THEN 'EN TRANSITO' 
	WHEN Pedidosliberados.Status=5 THEN 'ENTREGADO' END AS Status_Pedido,
	PEDIDOSLIBERADOS.IDEMPRESA, PEDIDOSLIBERADOS.RUTA AS IdRuta, Rutas.Ruta, PEDIDOSLIBERADOS.PEDIDO, PEDIDOSLIBERADOS.COD_CLIENTE, 
	DETALLEPEDIDOLIBERADO.ARTICULO, Clientes.Nombre, Clientes.NombreCorto,
	(SELECT Descripcion FROM ProductoGenerico WHERE (Clave IN (SELECT SUBSTRING(Clave, 1, 5) AS Expr1 FROM Productos WHERE (Clave IN (DETALLEPEDIDOLIBERADO.ARTICULO))))) AS Generico,
	(SELECT Descripcion FROM Sabores WHERE (Clave IN (SELECT SUBSTRING(Clave, 6, 7) AS Expr1 FROM Productos AS Productos_1  WHERE (Clave IN (DETALLEPEDIDOLIBERADO.ARTICULO))))) AS Sabor, 
	V_CantidadxPedido.Cajas as TotalCajas,
	V_CantidadxPedido.Pzas AS TotalPiezas,
	CASE WHEN detallepedidoliberado.Tipo = '1' THEN detallepedidoliberado.PZA_LIBERADAS ELSE detallepedidoliberado.PZA_LIBERADAS * X.PzaXCja END AS Piezas, X.PzaXCja,Vendedores.Nombre as Vendedor,0  as  Promocion
	FROM            PEDIDOSLIBERADOS INNER JOIN
	Clientes ON PEDIDOSLIBERADOS.COD_CLIENTE = Clientes.IdCli AND PEDIDOSLIBERADOS.IDEMPRESA = Clientes.IdEmpresa INNER JOIN
	RelClirutas ON Clientes.IdCli = RelClirutas.IdCliente AND PEDIDOSLIBERADOS.RUTA = RelClirutas.IdRuta AND PEDIDOSLIBERADOS.IDEMPRESA = RelClirutas.IdEmpresa INNER JOIN
	Rutas ON RelClirutas.IdRuta = Rutas.IdRutas AND RelClirutas.IdEmpresa = Rutas.IdEmpresa INNER JOIN
	DETALLEPEDIDOLIBERADO ON PEDIDOSLIBERADOS.PEDIDO = DETALLEPEDIDOLIBERADO.PEDIDO AND PEDIDOSLIBERADOS.RUTA = DETALLEPEDIDOLIBERADO.RUTA AND 
	PEDIDOSLIBERADOS.IDEMPRESA = DETALLEPEDIDOLIBERADO.IDEMPRESA INNER JOIN
	ProductosXPzas AS X ON DETALLEPEDIDOLIBERADO.ARTICULO = X.Producto INNER JOIN
	V_CantidadxPedido ON DETALLEPEDIDOLIBERADO.ARTICULO = V_CantidadxPedido.SKU AND DETALLEPEDIDOLIBERADO.PEDIDO = V_CantidadxPedido.Docto AND 
	DETALLEPEDIDOLIBERADO.RUTA = V_CantidadxPedido.RutaId AND V_CantidadxPedido.IdEmpresa=DETALLEPEDIDOLIBERADO.IdEmpresa INNER JOIN
	Vendedores ON PEDIDOSLIBERADOS.IDEMPRESA = Vendedores.IdEmpresa AND PEDIDOSLIBERADOS.IDVENDEDOR = Vendedores.IdVendedor
	WHERE (PEDIDOSLIBERADOS.CANCELADA = 0) -- AND  (PEDIDOSLIBERADOS.STATUS = 3) 
	UNION ALL 
	SELECT  PEDIDOSLIBERADOS.FECHA_PEDIDO,  PEDIDOSLIBERADOS.FECHA_ENTREGA, Pedidosliberados.Status,
	CASE WHEN Pedidosliberados.Status=1 THEN 'PENDIENTE' WHEN Pedidosliberados.Status=2 THEN 'LIBERADO' WHEN Pedidosliberados.Status=3 THEN 'SURTIDO' WHEN Pedidosliberados.Status=4 THEN 'EN TRANSITO' 
	WHEN Pedidosliberados.Status=5 THEN 'ENTREGADO' END AS Status_Pedido,
	PEDIDOSLIBERADOS.IDEMPRESA, PEDIDOSLIBERADOS.RUTA AS IdRuta, Rutas.Ruta, PEDIDOSLIBERADOS.PEDIDO, PEDIDOSLIBERADOS.COD_CLIENTE, 
	pregalado.sku, Clientes.Nombre, Clientes.NombreCorto,
	(SELECT Descripcion FROM ProductoGenerico WHERE (Clave IN (SELECT SUBSTRING(Clave, 1, 5) AS Expr1 FROM Productos WHERE (Clave IN (pregalado.sku))))) AS Generico,
	(SELECT Descripcion FROM Sabores WHERE (Clave IN (SELECT SUBSTRING(Clave, 6, 7) AS Expr1 FROM Productos AS Productos_1  WHERE (Clave IN (pregalado.sku))))) AS Sabor, 
	CASE WHEN PRegalado.Tipmed = 'Pzas' THEN  0  ELSE PRegalado.Cant END AS TotalCajas, 
	CASE WHEN PRegalado.Tipmed = 'Pzas' THEN PRegalado.Cant	ELSE 0 END AS TotalPiezas, 
	CASE WHEN PRegalado.Tipmed = 'Pzas' THEN PRegalado.Cant
	ELSE PRegalado.Cant * X.PzaXCja END AS Piezas, 
	X.PzaXCja,Vendedores.Nombre as Vendedor ,1 as  Promocion
	FROM            PEDIDOSLIBERADOS INNER JOIN
	Clientes ON PEDIDOSLIBERADOS.COD_CLIENTE = Clientes.IdCli AND PEDIDOSLIBERADOS.IDEMPRESA = Clientes.IdEmpresa INNER JOIN
	RelClirutas ON Clientes.IdCli = RelClirutas.IdCliente AND PEDIDOSLIBERADOS.RUTA = RelClirutas.IdRuta AND PEDIDOSLIBERADOS.IDEMPRESA = RelClirutas.IdEmpresa INNER JOIN
	Rutas ON RelClirutas.IdRuta = Rutas.IdRutas AND RelClirutas.IdEmpresa = Rutas.IdEmpresa INNER JOIN
	Vendedores ON PEDIDOSLIBERADOS.IDEMPRESA = Vendedores.IdEmpresa AND PEDIDOSLIBERADOS.IDVENDEDOR = Vendedores.IdVendedor INNER JOIN
	PRegalado ON PEDIDOSLIBERADOS.RUTA = PRegalado.RutaId AND PEDIDOSLIBERADOS.PEDIDO = PRegalado.Docto AND PEDIDOSLIBERADOS.COD_CLIENTE = PRegalado.Cliente AND 
	PEDIDOSLIBERADOS.IDEMPRESA = PRegalado.IdEmpresa INNER JOIN
	ProductosXPzas AS X ON PRegalado.SKU = X.Producto 
	WHERE (PEDIDOSLIBERADOS.CANCELADA = 0) --AND (PEDIDOSLIBERADOS.STATUS = 3)
GO

-- ----------------------------
-- View structure for V_StockEnvases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_StockEnvases]
GO
CREATE VIEW [dbo].[V_StockEnvases] AS 
SELECT	S.IdStock,E.Envase,S.Stock/CASE WHEN (X.PzaXCja/E.Cant_Eq)<=0 THEN 1 ELSE (X.PzaXCja/E.Cant_Eq) END Stock,S.Ruta,S.IdEmpresa
	FROM		Stock S INNER JOIN Productos P ON S.Articulo=P.Clave AND S.IdEmpresa=P.IdEmpresa
				INNER JOIN ProductoEnvase E ON P.Clave=E.Producto AND E.IdEmpresa=P.IdEmpresa
				LEFT JOIN ProductosXPzas X ON P.Clave=X.Producto AND X.IdEmpresa=P.IdEmpresa
GO

-- ----------------------------
-- View structure for V_TotalCajaPzs
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[V_TotalCajaPzs]
GO
CREATE VIEW [dbo].[V_TotalCajaPzs] AS 
Select	de.RutaId,v.DIAO,MAX(v.Fecha) Fecha,de.Articulo Clave,
				CASE ISNULL(de.Tipo,0) WHEN 0 THEN IsNull(SUM(de.Pza),0) ELSE 0 END [Venta Cajas],
				CASE ISNULL(de.Tipo,0) WHEN 1 THEN IsNull(SUM(de.Pza),0) ELSE 0 END [Venta Piezas],
				CASE WHEN ISNULL(PR.TipMed,'Caja')='Caja' THEN ISNULL(SUM(PR.Cant),0) ELSE 0 END [Promo Cajas],
				CASE WHEN ISNULL(PR.TipMed,'Caja')='Pzas' THEN ISNULL(SUM(PR.Cant),0) ELSE 0 END [Promo Piezas] 
	From		Venta v inner join DetalleVet de on v.Documento=de.Docto AND v.RutaId=de.RutaId AND V.Cancelada='0' 
				LEFT JOIN PRegalado PR on PR.Docto=v.Documento AND PR.DIAO=v.DIAO AND v.RutaId=PR.RutaId And PR.SKU=de.Articulo 
				LEFT JOIN ProductosXPzas PP ON de.Articulo=PP.Producto 
	Group By de.RutaId,v.DIAO,de.Articulo,PP.PzaXCja,de.Tipo,PR.TipMed
GO

-- ----------------------------
-- View structure for Ventas
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Ventas]
GO
CREATE VIEW [dbo].[Ventas] AS 
SELECT     Venta.CodCliente, Venta.Documento, Venta.Fecha, Venta.TipoVta, Venta.DiasCred, Venta.CreditoDispo, Venta.Saldo, Venta.Fvence, 
                      Venta.SubTotal, Venta.TOTAL, Venta.EnLetra, Venta.Items, Venta.FormaPag, Venta.DocSalida, Venta.DiaO, Venta.Kg, 
                      DetalleVet.Articulo, DetalleVet.Descripcion, DetalleVet.Precio, DetalleVet.Pza, DetalleVet.Kg AS Expr1, DetalleVet.DescPorc, 
                      DetalleVet.DescMon, DetalleVet.Tipo, DetalleVet.Docto, DetalleVet.RutaId, DetalleVet.Importe, DetalleVet.IVA AS Expr2, 
                      DetalleVet.IEPS AS Expr3, Venta.IEPS, Venta.IVA, Venta.Cancelada , venta.idempresa
FROM         Venta INNER JOIN
                      DetalleVet ON Venta.Documento = DetalleVet.Docto AND Venta.RutaId = DetalleVet.RutaId
GO

-- ----------------------------
-- View structure for View Bitacora
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View Bitacora]
GO
CREATE VIEW [dbo].[View Bitacora] AS 
SELECT B.HI AS HoraInicio, B.HF AS HoraFin, B.TS AS TiempoVisita, R.Ruta, C.IdCli, C.NombreCorto, B.Latitude, B.Longitude, B.Pila FROM BitacoraTiempos As B Inner Join Rutas AS R on B.RutaId=R.IdRutas Inner Join Clientes AS C on B.Codigo=C.idCli
GO

-- ----------------------------
-- View structure for View_Consolidado_Carga
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View_Consolidado_Carga]
GO
CREATE VIEW [dbo].[View_Consolidado_Carga] AS 
SELECT        TOP (100) PERCENT dbo.ProductoPaseado.RutaId, dbo.Productos.Clave, dbo.Productos.Producto, dbo.DiasO.Fecha, ISNULL(SUM(CAST(dbo.ProductoPaseado.Stock / dbo.ProductosXPzas.PzaXCja AS int)), 0) 
                         AS SobrantexCaja, ISNULL(SUM(CAST(dbo.ProductoPaseado.Stock % dbo.ProductosXPzas.PzaXCja AS int)), 0) AS SobrantexPza, 
                         ISNULL(SUM(CAST(dbo.PedDiaSig.Cantidad / dbo.ProductosXPzas.PzaXCja AS int)), 0) AS PedDiaSigxCaja, ISNULL(SUM(CAST(dbo.PedDiaSigPzs.Cantidad % dbo.ProductosXPzas.PzaXCja AS int)), 0) 
                         AS PedDiaSigxPza, ISNULL(SUM(CAST(dbo.StockHistorico.Stock / dbo.ProductosXPzas.PzaXCja AS int)), 0) AS StockxCaja, ISNULL(SUM(CAST(dbo.StockHistorico.Stock % dbo.ProductosXPzas.PzaXCja AS int)), 0) 
                         AS StockxPza
FROM            dbo.PedDiaSigPzs RIGHT OUTER JOIN
                         dbo.StockHistorico FULL OUTER JOIN
                         dbo.DiasO INNER JOIN
                         dbo.ProductoPaseado ON dbo.DiasO.DiaO = dbo.ProductoPaseado.DiaO AND dbo.DiasO.RutaId = dbo.ProductoPaseado.RutaId RIGHT OUTER JOIN
                         dbo.Productos ON dbo.ProductoPaseado.CodProd = dbo.Productos.Clave ON dbo.StockHistorico.Articulo = dbo.Productos.Clave AND dbo.StockHistorico.Fecha = dbo.DiasO.Fecha AND 
                         dbo.StockHistorico.RutaID = dbo.ProductoPaseado.RutaId LEFT OUTER JOIN
                         dbo.ProductosXPzas ON dbo.Productos.Clave = dbo.ProductosXPzas.Producto LEFT OUTER JOIN
                         dbo.PedDiaSig ON dbo.ProductoPaseado.DiaO = dbo.PedDiaSig.Diao AND dbo.ProductoPaseado.RutaId = dbo.PedDiaSig.IdRuta AND dbo.ProductoPaseado.CodProd = dbo.PedDiaSig.Articulo ON 
                         dbo.PedDiaSigPzs.IdRuta = dbo.PedDiaSig.IdRuta AND dbo.PedDiaSigPzs.Articulo = dbo.PedDiaSig.Articulo AND dbo.PedDiaSigPzs.Diao = dbo.PedDiaSig.Diao
WHERE        (dbo.ProductoPaseado.DiaO > 0)
GROUP BY dbo.ProductoPaseado.RutaId, dbo.Productos.Clave, dbo.Productos.Producto, dbo.DiasO.Fecha, dbo.PedDiaSig.Fecha, dbo.PedDiaSigPzs.Fecha, dbo.StockHistorico.Stock
GO

-- ----------------------------
-- View structure for View_PedDiaSig
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View_PedDiaSig]
GO
CREATE VIEW [dbo].[View_PedDiaSig] AS 
SELECT        N.Clave, N.Producto, P.Fecha, R.Ruta, P.Cantidad
FROM            dbo.PedDiaSig AS P LEFT OUTER JOIN
                         dbo.Productos AS N ON P.Articulo = N.Clave INNER JOIN
                         dbo.Rutas AS R ON R.IdRutas = P.IdRuta
GO

-- ----------------------------
-- View structure for View_PedDiaSigCa
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View_PedDiaSigCa]
GO
CREATE VIEW [dbo].[View_PedDiaSigCa] AS 
SELECT        N.Clave, N.Producto, P.Fecha, R.Ruta, P.Cantidad
FROM            dbo.PedDiaSig AS P INNER JOIN
                         dbo.Productos AS N ON P.Articulo = N.Clave INNER JOIN
                         dbo.Rutas AS R ON R.IdRutas = P.IdRuta
GO

-- ----------------------------
-- View structure for View_PedDiaSigP
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View_PedDiaSigP]
GO
CREATE VIEW [dbo].[View_PedDiaSigP] AS 
SELECT        N.Clave, N.Producto, P.Fecha, R.Ruta, P.Cantidad
FROM            dbo.PedDiaSigPzs AS P LEFT OUTER JOIN
                         dbo.Productos AS N ON P.Articulo = N.Clave INNER JOIN
                         dbo.Rutas AS R ON R.IdRutas = P.IdRuta
GO

-- ----------------------------
-- View structure for View_Promociones
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View_Promociones]
GO
CREATE VIEW [dbo].[View_Promociones] AS 
Select DISTINCT P.Clave as SKU, R.Ruta as Ruta, V.Fecha as Fecha, P.Producto, PR.Cant as Cantidad, PR.TipMed, PR.DocTo as Documento, PR.Cliente, C.NombreCorto from PRegalado PR inner join Productos P On PR.SKU = P.Clave inner join Venta V on PR.Docto=V.Documento AND PR.DIAO=V.DIAO AND V.RutaId=PR.RutaId Inner Join Rutas R on PR.RutaId=R.IdRutas Inner Join Clientes C on C.Idcli=Pr.cliente
GO

-- ----------------------------
-- View structure for View1
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View1]
GO
CREATE VIEW [dbo].[View1] AS 
SELECT Distinct R.Ruta,PP.DiaO,cast(D.Fecha as date) Fecha,P.Clave,P.Producto Descripcion,Cast((PP.Stock-IsNull(PDS.Cantidad,0)-IsNull(PDSP.Cantidad,0)) as int)/IsNull(PZ.PzaXCja,1) CAJAS,Cast((PP.Stock-IsNull(PDS.Cantidad,0)-IsNull(PDSP.Cantidad,0)) as int)%IsNull(PZ.PzaXCja,1) PIEZAS, P.VBase/IsNull(PZ.PzaXCja,1)*(PP.Stock-IsNull(PDS.Cantidad,0)-IsNull(PDSP.Cantidad,0)) AS ValorStock FROM ProductoPaseado PP INNER JOIN (Productos P Left Join ProductosXPzas PZ On P.Clave=PZ.Producto) ON PP.CodProd=P.Clave INNER JOIN rutas R ON PP.RutaId = R.IdRutas INNER JOIN diasO D ON PP.DiaO = D.DiaO AND PP.RutaId=D.RutaId Left Join PedDiaSig PDS On PP.CodProd=PDS.Articulo And PP.RutaId=PDS.IdRuta And PDS.Fecha=D.Fecha Left Join PedDiaSigPzs PDSP On PP.CodProd=PDSP.Articulo And PP.RutaId=PDSP.IdRuta And PDSP.Fecha=D.Fecha
GO

-- ----------------------------
-- View structure for View2
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[View2]
GO
CREATE VIEW [dbo].[View2] AS 
Select Top (100) Percent dbo.ProductoPaseado.RutaId, dbo.Productos.Clave, dbo.Productos.Producto, dbo.DiasO.Fecha, IsNull(Sum(Cast(dbo.ProductoPaseado.Stock / dbo.ProductosXPzas.PzaXCja As int)), 0) As SobrantexCaja, IsNull(Sum(Cast(dbo.ProductoPaseado.Stock % dbo.ProductosXPzas.PzaXCja As int)), 0) As SobrantexPza, IsNull(Sum(Cast(dbo.PedDiaSig.Cantidad / dbo.ProductosXPzas.PzaXCja As int)), 0) As PedDiaSigxCaja, IsNull(Sum(Cast(dbo.PedDiaSigPzs.Cantidad % dbo.ProductosXPzas.PzaXCja As int)), 0) As PedDiaSigxPza, IsNull(Sum(Cast(dbo.StockHistorico.Stock / dbo.ProductosXPzas.PzaXCja As int)), 0) As StockxCaja, IsNull(Sum(Cast(dbo.StockHistorico.Stock % dbo.ProductosXPzas.PzaXCja As int)), 0) As StockxPza From dbo.PedDiaSigPzs Right Outer Join (dbo.StockHistorico Full Outer Join (dbo.DiasO Inner Join dbo.ProductoPaseado On dbo.DiasO.DiaO = dbo.ProductoPaseado.DiaO And dbo.DiasO.RutaId = dbo.ProductoPaseado.RutaId Right Outer Join dbo.Productos On dbo.ProductoPaseado.CodProd = dbo.Productos.Clave) On dbo.StockHistorico.Articulo = dbo.Productos.Clave And dbo.StockHistorico.Fecha = dbo.DiasO.Fecha And dbo.StockHistorico.RutaID = dbo.ProductoPaseado.RutaId Left Outer Join dbo.ProductosXPzas On dbo.Productos.Clave = dbo.ProductosXPzas.Producto Left Outer Join dbo.PedDiaSig On dbo.ProductoPaseado.DiaO = dbo.PedDiaSig.Diao And dbo.ProductoPaseado.RutaId = dbo.PedDiaSig.IdRuta And dbo.ProductoPaseado.CodProd = dbo.PedDiaSig.Articulo) On dbo.PedDiaSigPzs.IdRuta = dbo.PedDiaSig.IdRuta And dbo.PedDiaSigPzs.Articulo = dbo.PedDiaSig.Articulo And dbo.PedDiaSigPzs.Diao = dbo.PedDiaSig.Diao Where dbo.ProductoPaseado.DiaO > 0 Group By dbo.ProductoPaseado.RutaId, dbo.Productos.Clave, dbo.Productos.Producto, dbo.DiasO.Fecha, dbo.PedDiaSig.Fecha, dbo.PedDiaSigPzs.Fecha, dbo.StockHistorico.Stock Order By dbo.ProductoPaseado.RutaId, dbo.Productos.Clave, dbo.Productos.Producto
GO

-- ----------------------------
-- View structure for Vw_Cobranza
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_Cobranza]
GO
CREATE VIEW [dbo].[Vw_Cobranza] AS 
Select	H.id IdCobranza,H.Cliente,H.Documento,H.Saldo,Case When Min(D.SaldoAnt) IS NULL Then H.Saldo Else Min(D.SaldoAnt) End SaldoAnt,
				Case When Min(D.Saldo) Is Null Then H.Saldo Else Min(D.Saldo) End SaldoAct,H.FechaVence,H.TipoDoc,H.FolioInterno,H.Status,H.RutaId Ruta,
				H.FechaReg,IsNull(SUM(D.Abono),0) Abono,H.IdEmpresa , max(D.Fecha) AS FechaPago
	From		Cobranza H Left Outer Join DetalleCob D On H.id=D.IdCobranza And H.IdEmpresa=D.IdEmpresa And D.Cancelada=0
	Group By H.IdEmpresa,H.id,D.IdCobranza,H.Cliente,H.Documento,H.Saldo,H.FechaVence,H.TipoDoc,H.FolioInterno,H.Status,H.RutaId,H.FechaReg
GO

-- ----------------------------
-- View structure for Vw_StockHist
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_StockHist]
GO
CREATE VIEW [dbo].[Vw_StockHist] AS 
SELECT	SH.IdEmpresa,SH.RutaID,SH.Fecha,SH.Articulo,P.Producto Descripcion,
			CAST(ISNULL(SH.Stock,0)/ISNULL(PX.PzaXCja,1) AS int) Cajas,
			CAST(ISNULL(SH.Stock,0)%ISNULL(PX.PzaXCja,1) AS int) Pza
	FROM	StockHistorico SH JOIN Productos P ON SH.Articulo=P.Clave
			LEFT OUTER JOIN ProductosXPzas PX ON SH.Articulo=PX.Producto
	WHERE	P.Ban_Envase=0
GO

-- ----------------------------
-- View structure for Vw_StockHistEnvases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_StockHistEnvases]
GO
CREATE VIEW [dbo].[Vw_StockHistEnvases] AS 
SELECT	SH.IdEmpresa,SH.RutaID,SH.Fecha,SH.Articulo,P.Producto Descripcion,
			CAST(ISNULL(SH.Stock,0)/ISNULL(PX.PzaXCja,1) AS int) Cajas,
			CAST(ISNULL(SH.Stock,0)%ISNULL(PX.PzaXCja,1) AS int) Pza
	FROM	StockHistorico SH JOIN Productos P ON SH.Articulo=P.Clave
			LEFT OUTER JOIN ProductosXPzas PX ON SH.Articulo=PX.Producto
	WHERE	P.Ban_Envase=1
GO

-- ----------------------------
-- View structure for Vw_TotalesComodatosEnvases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesComodatosEnvases]
GO
CREATE VIEW [dbo].[Vw_TotalesComodatosEnvases] AS 
SELECT	P.IdEmpresa,P.RutaId,P.DiaO,O.Fecha,P.Envase Articulo,A.Producto Descripcion,
			Case When ISNULL(X.PzaXCja,1)>1 Then
			cast(case When P.Cantidad-P.Devuelto>0 then P.Cantidad-P.Devuelto Else 0 End as int)/ISNULL(X.PzaXCja,1) Else 0 End As EnComodatoCajas,
			Case When ISNULL(X.PzaXCja,1)=1 Then case When P.Cantidad-P.Devuelto>0 then P.Cantidad-P.Devuelto Else 0 End Else
			Cast(case When P.Cantidad-P.Devuelto>0 then P.Cantidad-P.Devuelto Else 0 End as Int)%ISNULL(X.PzaXCja,1) End As EnComodatoPzas,
			Case When ISNULL(X.PzaXCja,1)>1 Then
			case When P.Devuelto-P.Cantidad>0 Then P.Devuelto-P.Cantidad Else 0 End/ISNULL(X.PzaXCja,1) Else 0 End as ComodatoDevueltoCajas,
			Case When ISNULL(X.PzaXCja,1)=1 Then case When P.Devuelto-P.Cantidad>0 Then P.Devuelto-P.Cantidad Else 0 End Else
			case When P.Devuelto-P.Cantidad>0 Then P.Devuelto-P.Cantidad Else 0 End%ISNULL(X.PzaXCja,1) End as ComodatoDevueltoPiezas
	FROM	DevEnvases P INNER JOIN Productos A On P.Envase=A.Clave
			Left Join ProductosXPzas X ON P.Envase=X.Producto
			INNER JOIN DiasO O ON P.RutaId=O.RutaId AND P.IdEmpresa=O.IdEmpresa AND P.DiaO=O.DiaO
	Where	Cantidad-Devuelto>0 Or Devuelto-Cantidad>0
GO

-- ----------------------------
-- View structure for Vw_TotalesDevol
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesDevol]
GO
CREATE VIEW [dbo].[Vw_TotalesDevol] AS 
SELECT	D.IdEmpresa,D.RutaId,H.DiaO,H.CodCliente,H.Docto Documento,Cast(H.Fecha as Date) Fecha,D.SKU Articulo,P.Producto Descripcion,
			CASE WHEN isnull(D.Tipo,0)=0 then D.Pza ELSE 0 end Caja,
			CASE WHEN isnull(D.Tipo,0)=1 then D.Pza ELSE 0 end piezas
	FROM	DetalleDevol D INNER JOIN  Devoluciones H ON H.Devol=D.Docto AND H.Ruta=D.RutaId AND D.IdEmpresa=H.IdEmpresa
			INNER JOIN Productos P On D.SKU=P.Clave
	WHERE	D.Motivo <> 'Comodato' And P.Ban_Envase=0
GO

-- ----------------------------
-- View structure for Vw_TotalesDevolEnvases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesDevolEnvases]
GO
CREATE VIEW [dbo].[Vw_TotalesDevolEnvases] AS 
SELECT	D.IdEmpresa,D.RutaId,H.DiaO,H.CodCliente,H.Docto Documento,Cast(H.Fecha as Date) Fecha,D.SKU Articulo,P.Producto Descripcion,
			CASE WHEN isnull(D.Tipo,0)=0 then D.Pza ELSE 0 end Caja,
			CASE WHEN isnull(D.Tipo,0)=1 then D.Pza ELSE 0 end piezas
	FROM	DetalleDevol D INNER JOIN  Devoluciones H ON H.Devol=D.Docto AND H.Ruta=D.RutaId AND D.IdEmpresa=H.IdEmpresa
			INNER JOIN Productos P On D.SKU=P.Clave
	WHERE	D.Motivo <> 'Comodato' And P.Ban_Envase=1
GO

-- ----------------------------
-- View structure for Vw_TotalesPedDiaSig
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesPedDiaSig]
GO
CREATE VIEW [dbo].[Vw_TotalesPedDiaSig] AS 
SELECT	P.IdEmpresa,P.RutaId,P.DiaO,O.Fecha,P.CodProd Articulo,A.Producto Descripcion,
			cast(P.Stock as int)/ISNULL(X.PzaXCja,1) As StockSobranteCajas,Cast(P.Stock as Int)%ISNULL(X.PzaXCja,1) As StockSobrantePzas,
			ISNULL(S.Cantidad,0)/ISNULL(X.PzaXCja,1) as PedDiaSigCajas,ISNULL(Z.Cantidad,0) as PedDiaSigPiezas
	FROM	ProductoPaseado P INNER JOIN Productos A On P.CodProd=A.Clave
			Left Join ProductosXPzas X ON P.CodProd=X.Producto
			INNER JOIN DiasO O ON P.RutaId=O.RutaId AND P.IdEmpresa=O.IdEmpresa AND P.DiaO=O.DiaO
			LEFT OUTER JOIN PedDiaSigPzs Z ON P.RutaId=Z.IdRuta AND P.DiaO=Z.Diao AND P.IdEmpresa=Z.IdEmpresa AND P.CodProd=Z.Articulo
			LEFT OUTER JOIN PedDiaSig S ON P.RutaId=S.IdRuta AND P.DiaO=S.Diao AND P.IdEmpresa=S.IdEmpresa AND P.CodProd=S.Articulo
	WHERE	A.Ban_Envase=0
GO

-- ----------------------------
-- View structure for Vw_TotalesStockSobranteEnvases
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesStockSobranteEnvases]
GO
CREATE VIEW [dbo].[Vw_TotalesStockSobranteEnvases] AS 
SELECT	P.IdEmpresa,P.RutaId,P.DiaO,O.Fecha,P.CodProd Articulo,A.Producto Descripcion,
			cast(P.Stock as int)/ISNULL(X.PzaXCja,1) As StockSobranteCajas,Cast(P.Stock as Int)%ISNULL(X.PzaXCja,1) As StockSobrantePzas
	FROM	ProductoPaseado P INNER JOIN Productos A On P.CodProd=A.Clave
			Left Join ProductosXPzas X ON P.CodProd=X.Producto
			INNER JOIN DiasO O ON P.RutaId=O.RutaId AND P.IdEmpresa=O.IdEmpresa AND P.DiaO=O.DiaO
	WHERE	A.Ban_Envase=1
GO

-- ----------------------------
-- View structure for Vw_TotalesVenta
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesVenta]
GO
CREATE VIEW [dbo].[Vw_TotalesVenta] AS 
SELECT	V.IdEmpresa,V.RutaId,V.DiaO,V.CodCliente,V.Documento,cast(V.Fecha as  date) Fecha,D.Articulo,D.Descripcion,
			CASE WHEN D.Tipo=0 then CAST(D.Pza AS int) else 0 end AS Caja,
			CASE WHEN D.Tipo=1 then CAST(D.Pza AS int) else 0 end AS Pza,
			'X' AS Tipo,0 as Cajas_Promo,0 AS Pzs_Promo,0 Cajas_Obse,0 Pzs_Obse
	FROM	Venta V INNER JOIN DetalleVet D ON V.RutaId=D.RutaId AND V.Documento=D.Docto AND V.IdEmpresa=D.IdEmpresa
			Inner Join Productos P On D.Articulo=P.Clave
	WHERE	D.Pza>0 And V.Cancelada=0 and P.Ban_Envase=0 and V.TipoVta!='Obsequio'
	UNION
	SELECT	V.IdEmpresa,V.RutaId,V.DiaO,V.CodCliente,V.Documento,cast(V.Fecha as  date) Fecha,D.Articulo,D.Descripcion,
			0 Caja,0 Pza,'P' AS Tipo,0 as Cajas_Promo,0 AS Pzs_Promo,
			CASE WHEN D.Tipo=0 then CAST(D.Pza AS int) else 0 end AS Cajas_Obse,
			CASE WHEN D.Tipo=1 then CAST(D.Pza AS int) else 0 end AS Pzs_Obse
	FROM	Venta V INNER JOIN DetalleVet D ON V.RutaId=D.RutaId AND V.Documento=D.Docto AND V.IdEmpresa=D.IdEmpresa
			Inner Join Productos P On D.Articulo=P.Clave
	WHERE	D.Pza>0 And V.Cancelada=0 and P.Ban_Envase=0 and V.TipoVta='Obsequio'
	UNION
	SELECT	R.IdEmpresa,R.RutaId,R.DiaO,R.Cliente CodCliente,R.Docto Documento,D.Fecha,R.SKU Articulo,P.Producto Descripcion,0 AS Caja,0 AS Pza,
			'P' Tipo,Case R.Tipmed When 'Caja' Then R.Cant Else 0 End Cajas_Promo,Case R.Tipmed When 'Pzas' Then R.Cant Else 0 End Pzs_Promo,0 Cajas_Obse,0 Pzs_Obse
	FROM	PRegalado R INNER JOIN Productos P ON R.SKU=P.Clave
			Join Venta V On R.RutaId=V.RutaId And R.DiaO=V.DiaO And R.Docto=V.Documento And R.Cliente=V.CodCliente And R.IdEmpresa=V.IdEmpresa
			Join DiasO D On R.DiaO=D.DiaO And R.RutaId=D.RutaId And R.IdEmpresa=D.IdEmpresa
	where	V.Cancelada=0 and P.Ban_Envase=0 and V.TipoVta!='Obsequio'
GO

-- ----------------------------
-- View structure for Vw_TotalesVentaEnvase
-- ----------------------------
DROP VIEW IF EXISTS [dbo].[Vw_TotalesVentaEnvase]
GO
CREATE VIEW [dbo].[Vw_TotalesVentaEnvase] AS 
SELECT	V.IdEmpresa,V.RutaId,V.DiaO,V.CodCliente,V.Documento,cast(V.Fecha as  date) Fecha,D.Articulo,D.Descripcion,
			CASE WHEN D.Tipo=0 then CAST(D.Pza AS int) else 0 end AS Caja,
			CASE WHEN D.Tipo=1 then CAST(D.Pza AS int) else 0 end AS Pza,
			'X' AS Tipo,0 as Cajas_Promo,0 AS Pzs_Promo,0 Cajas_Obse,0 Pzs_Obse
	FROM	Venta V INNER JOIN DetalleVet D ON V.RutaId=D.RutaId AND V.Documento=D.Docto AND V.IdEmpresa=D.IdEmpresa
			Inner Join Productos P On D.Articulo=P.Clave
	WHERE	D.Pza>0 And V.Cancelada=0 and P.Ban_Envase=1 and V.TipoVta!='Obsequio'
	UNION
	SELECT	V.IdEmpresa,V.RutaId,V.DiaO,V.CodCliente,V.Documento,cast(V.Fecha as  date) Fecha,D.Articulo,D.Descripcion,
			0 Caja,0 Pza,'P' AS Tipo,0 as Cajas_Promo,0 AS Pzs_Promo,
			CASE WHEN D.Tipo=0 then CAST(D.Pza AS int) else 0 end AS Cajas_Obse,
			CASE WHEN D.Tipo=1 then CAST(D.Pza AS int) else 0 end AS Pzs_Obse
	FROM	Venta V INNER JOIN DetalleVet D ON V.RutaId=D.RutaId AND V.Documento=D.Docto AND V.IdEmpresa=D.IdEmpresa
			Inner Join Productos P On D.Articulo=P.Clave
	WHERE	D.Pza>0 And V.Cancelada=0 and P.Ban_Envase=1 and V.TipoVta='Obsequio'
	UNION
	SELECT	R.IdEmpresa,R.RutaId,R.DiaO,R.Cliente CodCliente,R.Docto Documento,D.Fecha,R.SKU Articulo,P.Producto Descripcion,0 AS Caja,0 AS Pza,
			'P' Tipo,Case R.Tipmed When 'Caja' Then R.Cant Else 0 End Cajas_Promo,Case R.Tipmed When 'Pzas' Then R.Cant Else 0 End Pzs_Promo,0 Cajas_Obse,0 Pzs_Obse
	FROM	PRegalado R INNER JOIN Productos P ON R.SKU=P.Clave
			Join Venta V On R.RutaId=V.RutaId And R.DiaO=V.DiaO And R.Docto=V.Documento And R.Cliente=V.CodCliente And R.IdEmpresa=V.IdEmpresa
			Join DiasO D On R.DiaO=D.DiaO And R.RutaId=D.RutaId And R.IdEmpresa=D.IdEmpresa
	where	V.Cancelada=0 and P.Ban_Envase=1 and V.TipoVta!='Obsequio'
GO

-- ----------------------------
-- Procedure structure for EliminaDetallePedidosLiberadosRepetidos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[EliminaDetallePedidosLiberadosRepetidos]
GO

CREATE PROCEDURE [dbo].[EliminaDetallePedidosLiberadosRepetidos]
AS
Declare @IdEmpresa varchar(50)
Declare @Ruta int
Declare @Pedido int
Declare @Articulo varchar(50)
Declare @Cuenta int
declare @ID int  
begin
	Declare C Cursor For
		select  idEmpresa,Ruta,Pedido,Articulo ,count(pedido)   as  Cuenta
		from  detallepedidoliberado 
		--where  IdEmpresa=141
		group by idEmpresa,Ruta,Pedido,Articulo
		order  by idempresa, ruta, pedido asc
	Open C
	Fetch Next From C Into @IdEmpresa,@Ruta,@Pedido,@Articulo,@Cuenta
	WHILE @@fetch_status = 0
	BEGIN
		if  @Cuenta>1
		begin
			set @ID= (select min(ID) from detallepedidoliberado where IdEmpresa=@IdEmpresa and Ruta=@Ruta and Pedido=@Pedido and Articulo=@Articulo)
			print @ID
			delete from detallepedidoliberado where IdEmpresa=@IdEmpresa and Ruta=@Ruta and Pedido=@Pedido and Articulo=@Articulo and  ID<>@ID
		end
	  FETCH NEXT FROM c INTO @IdEmpresa,@Ruta,@Pedido,@Articulo,@Cuenta
	END
	Close C
	Deallocate C
end

GO

-- ----------------------------
-- Procedure structure for EliminaPedidosLiberadosRepetidos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[EliminaPedidosLiberadosRepetidos]
GO

CREATE PROCEDURE [dbo].[EliminaPedidosLiberadosRepetidos]
AS
Declare @IdEmpresa varchar(50)
Declare @Ruta int
Declare @Pedido int
Declare @Cod_cliente varchar(50)
Declare @Fecha_pedido date
Declare @IdVendedor int
Declare @Cuenta int
declare @ID int  
begin
	Declare C Cursor For
		select IdEmpresa,Ruta,Pedido,Cod_cliente,IdVendedor,count(pedido) as Cuenta
		from  pedidosliberados 
		--where  IdEmpresa=109
		group by IdEmpresa,Ruta,Pedido,Cod_cliente,Fecha_pedido,IdVendedor
		order  by idempresa, ruta, pedido asc
	Open C
	Fetch Next From C Into @IdEmpresa,@Ruta,@Pedido,@Cod_cliente,@IdVendedor,@Cuenta
	WHILE @@fetch_status = 0
	BEGIN
		if  @Cuenta>1
		begin
			set @ID= (select min(ID) from pedidosliberados where IdEmpresa=@IdEmpresa and Ruta=@Ruta and Pedido=@Pedido and Cod_cliente=@Cod_cliente)
			print @ID
			delete from pedidosliberados where IdEmpresa=@IdEmpresa and Ruta=@Ruta and Pedido=@Pedido and Cod_cliente=@Cod_cliente and  ID<>@ID
		end
	  FETCH NEXT FROM c INTO  @IdEmpresa,@Ruta,@Pedido,@Cod_cliente,@IdVendedor,@Cuenta
	END
	Close C
	Deallocate C
end

GO

-- ----------------------------
-- Procedure structure for sp_RelRuClas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[sp_RelRuClas]
GO

CREATE PROCEDURE [dbo].[sp_RelRuClas]
@Operacion CHAR(1),
@IdRuta INT,
@Clas1 INT,
@Clas2 INT,
@Clas3 INT,
@Clas4 INT,
@Clas5 INT
AS
begin
-- Agrega ruta
	if @Operacion='A'
	begin
		DECLARE @Existe INT
		SELECT @Existe=COUNT(*) FROM RelRuClas WHERE RutaId=@IdRuta
		IF @Existe>0
		BEGIN
			RAISERROR('La ruta ya existe', 16, 1)
			return
		END
		ELSE
		BEGIN
			INSERT INTO RelRuClas 
			(RutaId, Clas1, Clas2, Clas3, Clas4, Clas5) 
			VALUES 
			(@IdRuta, @Clas1, @Clas2, @Clas3, @Clas4, @Clas5)
			SELECT MAX(IdRelRClas) FROM RelRuClas
		END
	end
	ELSE
	BEGIN
		-- Actualiza ruta
		IF @Operacion='U'
		BEGIN
			UPDATE RelRuClas
			SET Clas1=@Clas1,
			Clas2=@Clas2,
			Clas3=@Clas3,
			Clas4=@Clas4,
			Clas5=@Clas5
			WHERE RutaId=@IdRuta
		END
		ELSE
		BEGIN
			-- Borra ruta
			IF @Operacion='D'
			BEGIN
				DELETE RelRuClas WHERE RutaId=@IdRuta
			END
			ELSE
			BEGIN
				-- Consulta todas las rutas
				IF @Operacion='S'
				BEGIN
					SELECT * FROM RelRuClas WHERE RutaId=@IdRuta
				END
			END
		END
	END
end

GO

-- ----------------------------
-- Procedure structure for sp_Rutas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[sp_Rutas]
GO

CREATE PROCEDURE [dbo].[sp_Rutas]
@Operacion VARCHAR(2),
@IdRuta INT='0',
@Ruta varchar(255)='',
@Activa BIT='0',
@TipoRuta VARCHAR(255)='',
@VenSinStock INT='0',
@Oficina VARCHAR(255)='',
@Sector VARCHAR(255)='',
@Vendedor INT='0',
@Tipo INT='0',
@Id_Ayudante1 INT='0',
@Id_Ayudante2 INT='0',
@VendedorAyudante BIT='0',
@Foranea BIT='0',
@IdEmpresa VARCHAR(50)=NULL
AS
begin
-- Agrega ruta
	if @Operacion='A'
	begin
		DECLARE @Existe INT
		SELECT @Existe=COUNT(*) FROM Rutas WHERE Ruta=@Ruta And IdEmpresa=@IdEmpresa
		IF @Existe>0
		BEGIN
			RAISERROR('La ruta ya existe', 16, 1)
			return
		END
		ELSE
		BEGIN
			INSERT INTO Rutas (Ruta,Activa,TipoRuta,VenSinStock,Oficina,Sector,Vendedor,Tipo,id_ayudante1,id_ayudante2,VendedorAyudante,Foranea,IdEmpresa) 
			VALUES (@Ruta,@Activa,@TipoRuta,@VenSinStock,@Oficina,@Sector,@Vendedor,@Tipo,@Id_Ayudante1,@Id_Ayudante2,@VendedorAyudante,@Foranea,@IdEmpresa)
			SELECT MAX(IdRutas) FROM Rutas
		END
	end
	-- Actualiza ruta
	IF @Operacion='U'
	BEGIN
		UPDATE	Rutas
		SET		Ruta=@Ruta,Activa=@Activa,TipoRuta=@TipoRuta,VenSinStock=@VenSinStock,Oficina=@Oficina,Sector=@Sector,Vendedor=@Vendedor,Tipo=@Tipo,
					id_ayudante1=@Id_Ayudante1,id_ayudante2=@Id_Ayudante2,VendedorAyudante=@VendedorAyudante,Foranea=@Foranea
		WHERE		IdRutas=@IdRuta And IdEmpresa=@IdEmpresa
	END
	-- Borra ruta
	IF @Operacion='D'
	BEGIN
		DELETE Rutas WHERE IdRutas=@IdRuta And IdEmpresa=@IdEmpresa
	END
	-- Consulta todas las rutas
	IF @Operacion='S'
	BEGIN
		SELECT * FROM Rutas Where IdEmpresa=@IdEmpresa
	END
	-- Consulta las rutas activas
	IF @Operacion='SA'
	BEGIN
		SELECT * FROM Rutas WHERE Activa='1' And IdEmpresa=@IdEmpresa
	END
	-- Consulta una ruta especifica
	IF @Operacion='SE'
	BEGIN
		SELECT * FROM Rutas WHERE IdRutas=@IdRuta And IdEmpresa=@IdEmpresa
	END
end

GO

-- ----------------------------
-- Procedure structure for sp_Vehiculos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[sp_Vehiculos]
GO

CREATE PROCEDURE [dbo].[sp_Vehiculos]
@Operacion VARCHAR(2),
@IdVehiculos INT='0',
@Clave varchar(50)='',
@Modelo varchar(50)='',
@Placas VARCHAR(50)='',
@Marcas varchar(50)='',
@Descripcion varchar(255)='',
@Status varchar(255)='',
@NumeroEco varchar(50)='',
@Asignado BIT='0',
@Poliza varchar(255)='',
@TelSeguro varchar(50)='',
@MesVerifica VARCHAR(50)='',
@Kilometraje varchar(50)='',
@KilometrajeSem varchar(50)='',
@Aseguradora varchar(50)='',
@FechaVencSeguro DATETIME='1901-01-01 12:00:00',
@FechaUltVerif DATETIME='1901-01-01 12:00:00'
AS
begin
-- Agrega vendedor
	if @Operacion='A'
	begin
		DECLARE @Existe INT
		SELECT @Existe=COUNT(*) FROM Vehiculos WHERE Clave=@Clave
		IF @Existe>0
		BEGIN
			RAISERROR('El vehiculo ya existe', 16, 1)
			return
		END
		ELSE
		BEGIN
			INSERT INTO Vehiculos 
			(Clave, Modelo, Placas, Marcas, Descripcion, Status, NumeroEco, Asignado, Poliza, TelSeguro, MesVerifica, Kilometraje, KilometrajeSem, Aseguradora, FechaVencSeguro, FechaUltVerif) 
			VALUES 
			(@Clave, @Modelo, @Placas, @Marcas, @Descripcion, @Status, @NumeroEco, @Asignado, @Poliza, @TelSeguro, @MesVerifica, @Kilometraje, @KilometrajeSem, @Aseguradora, @FechaVencSeguro, @FechaUltVerif) 
			SELECT MAX(IdVehiculos) FROM Vehiculos
		END
	end
	ELSE
	BEGIN
		-- Actualiza vendedor
		IF @Operacion='U'
		BEGIN
			UPDATE Vehiculos
			SET Modelo=@Modelo,
			Placas=@Placas,
			Marcas=@Marcas,
			Descripcion=@Descripcion,
			Status=@Status,
			NumeroEco=@NumeroEco,
			Asignado=@Asignado,
			Poliza=@Poliza,
			TelSeguro=@TelSeguro,
			MesVerifica=@MesVerifica,
			Kilometraje=@Kilometraje,
			KilometrajeSem=@KilometrajeSem,
			Aseguradora=@Aseguradora,
			FechaVencSeguro=@FechaVencSeguro,
			FechaUltVerif=@FechaUltVerif
			WHERE IdVehiculos=@IdVehiculos
		END
		ELSE
		BEGIN
			-- Borra vendedor
			IF @Operacion='D'
			BEGIN
				DELETE Vehiculos WHERE IdVehiculos=@IdVehiculos
			END
			ELSE
			BEGIN
				-- Consulta todos los vendedores
				IF @Operacion='S'
				BEGIN
					SELECT * FROM Vehiculos
				END
				ELSE
				BEGIN
					-- Consulta los vendedores activos
					IF @Operacion='SA'
					BEGIN
						SELECT * FROM Vehiculos WHERE Status='1'
					END
					ELSE
					BEGIN
						-- Consulta un vendedor especifico
						IF @Operacion='SE'
						BEGIN
							SELECT * FROM Vehiculos WHERE IdVehiculos=@IdVehiculos
						END
					END
				END
			END
		END
	END
end

GO

-- ----------------------------
-- Procedure structure for sp_Vendedores
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[sp_Vendedores]
GO

CREATE PROCEDURE [dbo].[sp_Vendedores]
@Operacion VARCHAR(2),
@IdVendedor INT='0',
@Nombre varchar(255)='',
@IdVehiculo varchar(255)='',
@Status BIT='0',
@Direccion varchar(255)='',
@Telefono varchar(50)='',
@PdaPw varchar(50)='',
@NumLicencia varchar(255)='',
@MetaDiaria varchar(50)='',
@MetaMes varchar(50)='',
@Movil varchar(50)='',
@VenceLic DATETIME='1901-01-01 12:00:00',
@Clave varchar(255)=''
AS
begin
-- Agrega vendedor
	if @Operacion='A'
	begin
		DECLARE @Existe INT
		SELECT @Existe=COUNT(*) FROM Vendedores WHERE Nombre=@Nombre
		IF @Existe>0
		BEGIN
			RAISERROR('El vendedor ya existe', 16, 1)
			return
		END
		ELSE
		BEGIN
			INSERT INTO Vendedores 
			(Nombre, IdVehiculo, Status, Direccion, Telefono, PdaPw, NumLicencia, MetaDiaria, MetaMes, Movil, VenceLic, Clave, ID_Ayudante1, ID_Ayudante2) 
			VALUES 
			(@Nombre, @IdVehiculo, @Status, @Direccion, @Telefono, @PdaPw, @NumLicencia, @MetaDiaria, @MetaMes, @Movil, @VenceLic, @Clave, 0, 0)
			SELECT MAX(IdVendedor) FROM Vendedores
		END
	end
	ELSE
	BEGIN
		-- Actualiza vendedor
		IF @Operacion='U'
		BEGIN
			UPDATE Vendedores
			SET Nombre=@Nombre,
			IdVehiculo=@IdVehiculo,
			Status=@Status,
			Direccion=@Direccion,
			Telefono=@Telefono,
			PdaPw=@PdaPw,
			NumLicencia=@NumLicencia,
			MetaDiaria=@MetaDiaria,
			MetaMes=@MetaMes,
			Movil=@Movil,
			VenceLic=@VenceLic,
			Clave=@Clave
			WHERE IdVendedor=@IdVendedor
		END
		ELSE
		BEGIN
			-- Borra vendedor
			IF @Operacion='D'
			BEGIN
				DELETE Vendedores WHERE IdVendedor=@IdVendedor
			END
			ELSE
			BEGIN
				-- Consulta todos los vendedores
				IF @Operacion='S'
				BEGIN
					SELECT * FROM Vendedores
				END
				ELSE
				BEGIN
					-- Consulta los vendedores activos
					IF @Operacion='SA'
					BEGIN
						SELECT * FROM Vendedores WHERE Status='1'
					END
					ELSE
					BEGIN
						-- Consulta un vendedor especifico
						IF @Operacion='SE'
						BEGIN
							SELECT * FROM Vendedores WHERE IdVendedor=@IdVendedor
						END
					END
				END
			END
		END
	END
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaBitacora
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaBitacora]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaBitacora](
@Codigo			varchar(50),
@Descripcion	varchar(255),
@HI				datetime,
@HF				datetime,
@HT				varchar(20),
@TS				varchar(20),
@Visita			bit,
@Programado		bit,
@DiaO				int,
@RutaId			int,
@Cerrado			bit,
@IdV				Numeric(18,0),
@Tip				Varchar(20),
@Latitude		Float,
@Longitude		Float,
@Pila				Tinyint,
@IdEmpresa		varchar(50))
AS
Begin
	DECLARE @Fecha Date
	--Inserto el día Operativo si es que no existe
	if Not Exists(Select * From DiasO WHERE DiaO=@DiaO AND RutaId=@RutaId And IdEmpresa=@IdEmpresa)
	begin
		Insert Into DiasO(DiaO,Fecha,RutaId,IdEmpresa)
		Values(@DiaO,@HI,@RutaId,@IdEmpresa)
	end
	--Inserto la bitácora
	if not exists(SELECT * FROM BitacoraTiempos Where RutaId=@RutaId And IdEmpresa=@IdEmpresa And DiaO=@Diao And Codigo=@Codigo)
	begin
		Insert into BitacoraTiempos(Codigo,Descripcion,HI,HF,HT,TS,Visita,Programado,DiaO,RutaId,Cerrado,IdV,Tip,latitude,longitude,pila,IdEmpresa)
		Values(@Codigo,@Descripcion,@HI,@HF,@HT,@TS,@Visita,@Programado,@DiaO,@RutaId,@Cerrado,@IdV,@Tip,@Latitude,@Longitude,@Pila,@IdEmpresa)
	end
	Select @Fecha=Fecha From DiasO Where DiaO=@DiaO AND RutaId=@RutaId And IdEmpresa=@IdEmpresa
	--Actualizo continuidad de ruta
	if Not Exists(SELECT * FROM Continuidad WHERE RutaID=@RutaId And IdEmpresa=@IdEmpresa)
	begin
		Insert into Continuidad(RutaID,UDiaO,IdEmpresa)
		Values(@RutaId,@DiaO,@IdEmpresa)
	end
	else
	begin
		Update Continuidad SET UDiaO=@DiaO WHERE RutaID=@RutaId And IdEmpresa=@IdEmpresa
	end
	--Si es inicio de día Operativo A18253, actualizo el histórico de visitas
	if @Codigo='A18253'
	begin
		DECLARE @Dia Varchar(10)
		Select @Dia=Case DatePart(dw,@HI) When 1 Then 'Domingo' When 2 Then 'Lunes' When 3 Then 'Martes' When 4 Then 'Miercoles' When 5 Then 'Jueves' When 6 Then 'Viernes' When 7 Then 'Sabado' End
		if Not Exists(Select * From TH_SecVisitas Where RutaId=@RutaId And Dia=Substring(@Dia,1,2) And Fecha=@Fecha And IdEmpresa=@IdEmpresa)
		begin
			Declare @dd varchar(2)
			Set @dd=Substring(@Dia,1,2)
			Declare @Cmd varchar(MAX)
			Set @Cmd='Insert into TH_SecVisitas(CodCli,RutaId,Secuencia,Dia,Fecha,IdEmpresa) '
			Set @Cmd=@Cmd + 'Select CodCli,RutaId,'+@Dia+','''+@dd+''','''+Convert(varchar(10),@Fecha,102)+''','''+@IdEmpresa+''' From RelDayCli Where RutaId='+Cast(@RutaId as varchar(20))+' And IdEmpresa='''+@IdEmpresa+''' And iSnULL('+@Dia+',0)>=1'
			Execute(@Cmd)
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaCobranza
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaCobranza]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaCobranza](
@RutaId			int,
@CodCliente		varchar(50),
@Documento		int,
@IdEmpresa		varchar(50))
AS
Begin
	--Valida si el total coincide con el detalle
	if Exists(Select	V.CodCliente,V.Documento,V.Items,SUM(D.Pza) pza
				From		DetalleVet D Inner Join Venta V On D.RutaId=V.RutaId And D.Docto=V.Documento And D.IdEmpresa=V.IdEmpresa 
				Where		D.RutaId=@RutaId And V.CodCliente=@CodCliente And D.Docto=@Documento
				Group By V.CodCliente,V.Documento,V.Items,V.SubTotal,V.IVA,V.IEPS,V.TOTAL,V.Kg
				Having	V.Items!=SUM(D.Pza) And V.SubTotal!=ROUND(SUM(D.Importe),2))
	Begin
		DECLARE @Items		int
		DECLARE @SubTotal	money
		DECLARE @IVA		money
		DECLARE @IEPS		money
		DECLARE @TOTAL		money
		DECLARE @Kg			Numeric(18,2)
		Select	@Items=SUM(D.Pza),@SubTotal=ROUND(SUM(D.Importe),2),@IVA=ROUND(SUM(D.IVA),2),
					@IEPS=ROUND(SUM(D.IEPS),2),@TOTAL=ROUND(SUM(D.Importe+D.IVA+D.IEPS),2),@Kg=ROUND(SUM(D.Kg),2)
		From		DetalleVet D Inner Join Venta V On D.RutaId=V.RutaId And D.Docto=V.Documento And D.IdEmpresa=V.IdEmpresa 
		Where		D.RutaId=@RutaId And V.CodCliente=@CodCliente And D.Docto=@Documento
		Group By V.CodCliente,V.Documento,V.Items,V.SubTotal,V.IVA,V.IEPS,V.TOTAL,V.Kg
		Having	V.Items!=SUM(D.Pza) And V.SubTotal!=ROUND(SUM(D.Importe),2)
		Update	Venta Set Items=@Items,SubTotal=@SubTotal,IVA=@IVA,IEPS=@IEPS,Total=@TOTAL,Kg=@Kg
		Where		RutaId=@RutaId And CodCliente=@CodCliente And Documento=@Documento
	end
	--Actualizo la cobranza
	if Not Exists(Select * From Cobranza Where RutaId=@RutaId And Documento=@Documento)
	begin
		Insert Into Cobranza(Cliente,Documento,Saldo,Status,RutaId,FechaReg,FechaVence,FolioInterno,TipoDoc,DiaO,IdEmpresa)
		Select	CodCliente,Documento,TOTAL,1,RutaId,Fecha,FVence,0,DocSalida,DiaO,IdEmpresa
		From		Venta
		Where		RutaId=@RutaId And CodCliente=@CodCliente And Documento=@Documento And TipoVta='Credito' And Cancelada=0
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaComision]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaComision](@ID_Comision int,@Status char(1),@Comentarios varchar(MAX)) as
begin
	--Fecha de creación 11/04/2014
	--modifica los datos de la comision
	--verifico que el status este dentro del rago
	if (@Status<>'A' and @Status <>'I')
	begin
		RAISERROR('El status solo puede ser  A= Activo o I=Inactivo', 16, 1)
		return
	end
	--Actualizando el registro
	update 
		TH_Comision
	set 
		Status=@Status
		,Comentarios=@Comentarios
	 where 
		ID_Comision=@ID_Comision

		
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaDetalleComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaDetalleComision]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaDetalleComision](
@ID_Comision int,
@ID_Detalle int,
@RangoIni int,
@RangoFin int,
@Angente float,
@Ayudante float,
@Tipo int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--Fecha de modificación 04/03/2015
	--proceidimiento almacenado que actualiza un rango de una comision
	--verifica que no se traslapen los rangos
	if @RangoIni<1
	begin
		RAISERROR('El valor minimo que puede tomar un rango es 1', 16, 1)
		return
	end
	--Validando llaves foráneas
	if @RangoFin<=@RangoIni
	begin
		RAISERROR('El rango final debe ser mayor que el inicial', 16, 1)
		return
	end
	--verifico que no se traslapen los rahgos
	if exists(select * from TD_Comision d where d.ID_Comision=@ID_Comision and IdEmpresa=@Idempresa and d.ID_Detalle<>@ID_Detalle and ((@RangoIni>=d.RangoIni and @RangoIni<=d.RangoFin) or (@RangoFin>=d.RangoIni and @RangoFin<=d.RangoFin)))
	begin
		RAISERROR('El rango no se puede traslapar con otros dentro de la comision', 16, 1)
		return
	end
	--Actualizando el registro
	update	TD_Comision
	set		RangoIni=@RangoIni,RangoFin=@RangoFin,Angente=@Angente,Ayudante=@Ayudante,Tipo=@Tipo
	where		ID_Comision=@ID_Comision and ID_Detalle=@ID_Detalle And IdEmpresa=@Idempresa
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaDetDevoluciones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaDetDevoluciones]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaDetDevoluciones](
@SKU			varchar(50),
@Pza			int,
@Kg			Numeric(18,2),
@Precio		money,
@Importe		money,
@EDO			bit,
@Motivo		nvarchar(MAX),
@IVA			money,
@IEPS			money,
@Docto		int,
@RutaId		int,
@Tipo			int,
@IdEmpresa	varchar(50))
AS
Begin
	if Not Exists(SELECT * FROM DetalleDevol Where SKU=@SKU And RutaId=@RutaId And IdEmpresa=@IdEmpresa And Docto=@Docto)
	begin
		Insert into DetalleDevol(SKU,Pza,KG,Precio,Importe,EDO,Motivo,IVA,IEPS,Docto,RutaId,Tipo,IdEmpresa)
		Values(@SKU,@Pza,@KG,@Precio,@Importe,@EDO,@Motivo,@IVA,@IEPS,@Docto,@RutaId,@Tipo,@IdEmpresa)
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaDetVentas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaDetVentas]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaDetVentas](
@Articulo		varchar(50),
@Descripcion	nvarchar(MAX),
@Precio			money,
@Pza				int,
@Kg				Numeric(18,2),
@DescPorc		nvarchar(100),
@DescMon			Numeric(18,2),
@Tipo				int,
@Docto			int,
@Importe			money,
@IVA				money,
@IEPS				money,
@RutaId			int,
@DiaO				int,
@PromoId			int,
@IdEmpresa		varchar(50))
AS
Begin
	if Not Exists(SELECT * FROM DetalleVet Where Articulo=@Articulo And RutaId=@RutaId And IdEmpresa=@IdEmpresa And Docto=@Docto)
	begin
		Insert into DetalleVet(Articulo,Descripcion,Precio,Pza,Kg,DescPorc,DescMon,Tipo,Docto,Importe,IVA,IEPS,RutaId,IdEmpresa)
		Values(@Articulo,@Descripcion,@Precio,@Pza,@Kg,@DescPorc,@DescMon,@Tipo,@Docto,@Importe,@IVA,@IEPS,@RutaId,@IdEmpresa)
		if Exists(Select * From Venta Where RutaId=@RutaId And IdEmpresa=@IdEmpresa And Documento=@Docto And Cancelada=0)
		begin
			if Not Exists(SELECT * FROM PVendidos WHERE DiaO=@DiaO AND SKU=@Articulo AND RutaId=@RutaId AND Tipo=@PromoId And IdEmpresa=@IdEmpresa)
			begin
				Insert Into PVendidos(SKU,RutaId,Pz,KG,DiaO,Tipo,IdEmpresa)
				Values(@Articulo,@RutaId,@Pza,@Kg,@DiaO,@PromoId,@IdEmpresa)
			end
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaDevoluciones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaDevoluciones]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaDevoluciones](
@CodCliente		varchar(50),
@Ruta			int,
@Vendedor		int,
@Items			int,
@Kg				Numeric(18,2)=NULL,
@IVA			money,
@IEPS			money,
@SubTotal		money,
@TOTAL			money,
@EnLetras		nvarchar(MAX),
@DiaO			int,
@Docto			int,
@Cancelada		bit,
@IdEmpresa		varchar(50))
AS
Begin
	Declare @Fecha	datetime
	Select @Fecha=Fecha From DiasO Where RutaId=@Ruta And IdEmpresa=@IdEmpresa And DiaO=@DiaO
	if Not Exists(SELECT * FROM Devoluciones Where Ruta=@Ruta And IdEmpresa=@IdEmpresa And DiaO=@Diao And CodCliente=@CodCliente And Docto=@Docto)
	begin
		Insert into Devoluciones(CodCliente,Devol,Fecha,Status,Ruta,Vendedor,Items,KG,IVA,IEPS,Subtotal,Total,EnLetras,DiaO,Docto,Cancelada,IdEmpresa)
		Values(@CodCliente,@Docto,@Fecha,1,@Ruta,@Vendedor,@Items,@KG,@IVA,@IEPS,@Subtotal,@Total,@EnLetras,@DiaO,@Docto,@Cancelada,@IdEmpresa)
	end
	else
	begin
		if exists(Select * From DetalleDevol Where RutaId=@Ruta And IdEmpresa=@IdEmpresa And Docto=@Docto)
		begin
			RAISERROR ('Folio de Devolución Existente', 16, 1)
			return 0
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaDoctosCobranza
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaDoctosCobranza]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaDoctosCobranza](
@DiaO        int,
@Cliente    nvarchar(100),
@Documento    nvarchar(100),
@RutaId        int)
AS
DECLARE @FolioI        Int
DECLARE @IdCobranza    Int
DECLARE @TotalP        Decimal
DECLARE @SaldoDoc    Decimal
DECLARE @Saldo        Decimal
DECLARE @Salir        Int
BEGIN
    --Actualizo el Folio Interno de la tabla Cobranza
    SET @Salir=0
    While @Salir=0
    Begin
        if not Exists(Select * From Cobranza Where Cliente=@Cliente And IsNull(FolioInterno ,0)=0)
            SET @Salir=1
        else
        begin
            Select @IdCobranza=id From Cobranza Where Cliente=@Cliente And IsNull(FolioInterno ,0)=0
            Select @FolioI=IsNull(Max(FolioInterno),0)+1 From Cobranza Where Cliente=@Cliente
            Update Cobranza Set FolioInterno=@FolioI Where Id=@IdCobranza
        end
    End
    ---Se debe actualizar el saldo del cliente
    Select    @Saldo=SUM(Saldo)
    From    Cobranza
    Where    Cliente=@Cliente
    Group By Cliente
    Select    @TotalP=IsNull(SUM(Abono),0)
    From    DetalleCob
    Where    IdCobranza In (Select Id From Cobranza Where Cliente=@Cliente)
    Group by IdCobranza
    SET @SaldoDoc=@Saldo-@TotalP
    IF @SaldoDoc<@TotalP
        SET @SaldoDoc=0
    --Update    Clientes SET Saldo=Cast(@SaldoDoc as nvarchar(100)) Where    IdCli=@Cliente
END

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaEncuestas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaEncuestas]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaEncuestas](
@Clave_Enc		Varchar(50),
@Num_Pregunta	int,
@IdRuta			int,
@Des_Resp		Varchar(255),
@IdCliente		varchar(50),
@Fecha			DateTime,
@IdEmpresa		varchar(50))
AS
begin
	if Not Exists(Select * From Resp_Enc Where Clave_Enc=@Clave_Enc And Num_Pregunta=@Num_Pregunta And IdRuta=@IdRuta And IdCliente=@IdCliente And Fecha=@Fecha And IdEmpresa=@IdEmpresa)
	begin
		insert into Resp_Enc(Clave_Enc,Num_Pregunta,IdRuta,Des_Resp,IdCliente,Fecha,IdEmpresa)
		Values(@Clave_Enc,@Num_Pregunta,@IdRuta,@Des_Resp,@IdCliente,@Fecha,@IdEmpresa)
	end
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaEstadoLog
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaEstadoLog]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaEstadoLog](
@Ruta		int,
@EnProceso	bit,
@IdEmpresa	varchar(50))
AS
Begin
	Update T_Log Set EnProceso=@EnProceso Where Ruta=@Ruta And IdEmpresa=@IdEmpresa And Modulo='Daemon'
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaNoVentas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaNoVentas]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaNoVentas](
@Cliente		varchar(50),
@MotivoId		int,
@Fecha			DateTime=NULL,
@DiaO			int,
@RutaId			int,
@VendedorId		int,
@IdEmpresa		varchar(50))
AS
Begin
	if Not Exists(Select * From Noventas Where RutaId=@RutaId And Cliente=@Cliente And DiaO=@DiaO)
	Begin
		Insert into Noventas(Cliente,MotivoId,Fecha,DiaO,RutaId,VendedorId,IdEmpresa)
		Values(@Cliente,@MotivoId,@Fecha,@DiaO,@RutaId,@VendedorId,@IdEmpresa)
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaOperaciones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaOperaciones]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaOperaciones](
@Id		int,
@Folio	int,
@RutaId	int,
@DiaO	int,
@CodCli	varchar(50),
@Tipo	nvarchar(50),
@Total	money,
@Fecha	datetime,
@IdEmpresa	varchar(50))
AS
Begin
	if exists(Select * From RelOperaciones Where RutaId=@RutaId And IdEmpresa=@IdEmpresa And DiaO=@Diao And CodCli=@CodCli And Folio=@Folio)
	begin
		RAISERROR ('Operación Existente', 16, 1)
		return 0
	end
	Insert into RelOperaciones(Id,Folio,RutaId,DiaO,CodCli,Tipo,Total,Fecha,IdEmpresa)
	Values(@Id,@Folio,@RutaId,@DiaO,@CodCli,@Tipo,@Total,@Fecha,@IdEmpresa)
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaPagosCobranza
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaPagosCobranza]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaPagosCobranza](
@DiaO		int,
@Cliente	nvarchar(100),
@Documento	nvarchar(100),
@Abono		decimal,
@Fecha		Datetime,
@RutaId		int,
@SaldoAnt	decimal,
@Saldo		decimal,
@FormaP		Int,
@Cancelada	bit,
@IdEmpresa	varchar(50))
AS
DECLARE @IdCobranza	Int
DECLARE @TotalP		Decimal
DECLARE @SaldoDoc	Decimal
DECLARE @Status		Int
BEGIN
	--Obtengo el IdCobranza de la tabla Cobranza
	Select @IdCobranza=id,@SaldoDoc=Saldo From Cobranza Where Cliente=@Cliente And Documento=@Documento And IdEmpresa=@IdEmpresa
	---Se deben registrar los pagos efectuados en DetalleCob
	if Not Exists(Select * From DetalleCob Where RutaId=@RutaId And DiaO=@DiaO And Documento=@Documento And Cliente=@Cliente And Fecha=@Fecha)
	begin
		Insert Into DetalleCob(IdCobranza,Abono,Fecha,RutaId,SaldoAnt,Saldo,FormaP,DiaO,Documento,Cliente,Cancelada,IdEmpresa)
		Values(@IdCobranza,@Abono,@Fecha,@RutaId,@SaldoAnt,@Saldo,@FormaP,@DiaO,@Documento,@Cliente,@Cancelada,@IdEmpresa)
	end
	else
	begin
			RAISERROR ('Pago Existente', 16, 1)
			return 0
	end
	if @Cancelada=0
	begin
		---Se debe actualizar el status si ya se liquidó el total del saldo. Actualizar RutaID y UltPago si hubo abonos para el docto
		Select @TotalP=SUM(Abono) From DetalleCob Where IdCobranza=@IdCobranza And Cancelada=0 And IdEmpresa=@IdEmpresa Group By IdEmpresa,IdCobranza,Cancelada
		IF @SaldoDoc>@TotalP
			SET @Status=1
		ELSE
			SET @Status=2
		Update	Cobranza SET Status=@Status,RutaId=@RutaId,UltPago=Cast(@Abono as nvarchar(100)),DiaO=@DiaO
		Where	Id=@IdCobranza And IdEmpresa=@IdEmpresa
	end
END

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaPDS_PDSPz_Rec
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaPDS_PDSPz_Rec]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaPDS_PDSPz_Rec](
@Tipo			char(1),
@IdRuta			int,
@Articulo		varchar(50),
@Cantidad		int,
@DiaO			int,
@Folio			varchar(50),
@IdEmpresa		varchar(50),
@Hora			DateTime=NULL)
AS
Begin
	--Valida si el total coincide con el detalle
	DECLARE @Fecha	Date
	Select @Fecha=Fecha From DiasO Where RutaId=@IdRuta And DiaO=@DiaO
	if @Tipo='P' --Pedido Piezas
	begin
		if Not Exists(Select * From PedDiaSigPzs Where IdRuta=@IdRuta And Articulo=@Articulo And DiaO=@DiaO)
		Begin
			Insert into PedDiaSigPzs(IdRuta,Articulo,Cantidad,Fecha,IdEmpresa,DiaO,Folio)
			Values(@IdRuta,@Articulo,@Cantidad,@Fecha,@IdEmpresa,@DiaO,@Folio)
		end
	end
	if @Tipo='D' --Pedido Dia Siguiente
	begin
		if Not Exists(Select * From PedDiaSig Where IdRuta=@IdRuta And Articulo=@Articulo And DiaO=@DiaO)
		Begin
			Insert into PedDiaSig(IdRuta,Articulo,Cantidad,Fecha,IdEmpresa,DiaO,Folio)
			Values(@IdRuta,@Articulo,@Cantidad,@Fecha,@IdEmpresa,@DiaO,@Folio)
		end
	end
	if @Tipo='R' --Recarga
	begin
		if Not Exists(Select * From Recarga Where IdRuta=@IdRuta And Articulo=@Articulo And DiaO=@DiaO)
		Begin
			Insert into Recarga(IdRuta,Articulo,Cantidad,Fecha,IdEmpresa,DiaO,Folio,Hora)
			Values(@IdRuta,@Articulo,@Cantidad,@Fecha,@IdEmpresa,@DiaO,@Folio,@Hora)
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaPromos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaPromos]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaPromos](
@SKU		varchar(50),
@RutaId		int,
@DiaO		int,
@Docto		int,
@Cliente	varchar(50),
@Cant		int,
@Tipmed		varchar(50),
@IdEmpresa	varchar(50))
AS
Begin
	if Not Exists(Select * From PRegalado Where RutaId=@RutaId And Cliente=@Cliente And DiaO=@DiaO And SKU=@SKU And Docto=@Docto)
	Begin
		Insert into PRegalado(SKU,RutaId,DiaO,Docto,Cliente,Cant,Tipmed,IdEmpresa)
		Values(@SKU,@RutaId,@DiaO,@Docto,@Cliente,@Cant,@Tipmed,@IdEmpresa)
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaSaldoEnvases
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaSaldoEnvases]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaSaldoEnvases](
@RutaId			int,
@DiaO			int,
@CodCli			varchar(50),
@Docto			int,
@Articulo		varchar(50),
@Cantidad		int,
@Devuelto		int,
@Tipo			varchar(50),
@Envase			varchar(50),
@IdEmpresa		varchar(50))
AS
Begin
	if Not Exists(Select * From DevEnvases Where RutaId=@RutaId And CodCli=@CodCli And DiaO=@DiaO And Articulo=@Articulo And Envase=@Envase And Cantidad=@Cantidad)
	Begin
		Insert into DevEnvases(RutaId,DiaO,CodCli,Docto,Articulo,Cantidad,Devuelto,Tipo,Envase,Status,IdEmpresa)
		Values(@RutaId,@DiaO,@CodCli,@Docto,@Articulo,@Cantidad,@Devuelto,@Tipo,@Envase,'A',@IdEmpresa)
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaStock
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaStock]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaStock](
@Articulo	varchar(50),
@RutaId		int,
@DiaO		int,
@Cant		int,
@CantA		int,
@IdEmpresa	varchar(50))
AS
Begin
	Declare @Fecha DateTime
	Declare @UDiaO int
	Select @Fecha=Fecha From DiasO Where RutaId=@RutaId And DiaO=@DiaO
	if Not Exists(Select * From MvtosInv Where RutaD=@RutaId And fecha=@Fecha And Articulo=@Articulo And IdEmpresa=@IdEmpresa)
	Begin
		Declare @Movto as varchar(MAX)
		Set @Movto='Descarga Stock Producto ' + @Articulo + ' En Ruta ' + cast(@RutaId as varchar(10))
		Insert into MvtosInv(Movto,Articulo,stockant,Stockact,fecha,RutaO,RutaD,IdEmpresa)
		Values(@Movto,@Articulo,@CantA,@Cant,@Fecha,@RutaId,@RutaId,@IdEmpresa)
	end
	Select Top 1 @UDiaO=DiaO From DiasO Where RutaId=@RutaId Order By Fecha Desc
	if @UDiaO=@DiaO
	begin
		if Not Exists(Select * From Stock Where Articulo=@Articulo And Ruta=@RutaId And IdEmpresa=@IdEmpresa)
		begin
			insert Into Stock(Articulo,Stock,Ruta,IdEmpresa)
			Values(@Articulo,@Cant,@RutaId,@IdEmpresa)
		end
		else
		begin
			Update Stock Set stock=@Cant Where Articulo=@Articulo And Ruta=@RutaId And IdEmpresa=@IdEmpresa
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaVentas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaVentas]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaVentas](
@RutaId			int,
@VendedorId		int,
@CodCliente		varchar(50),
@Documento		int,
@Fecha			datetime,
@TipoVta			varchar(100),
@DiasCred		int,
@CreditoDispo	Numeric(18,2)=NULL,
@Fvence			date,
@SubTotal		money,
@IVA				money,
@IEPS				money,
@TOTAL			money,
@EnLetra			nvarchar(MAX),
@Items			int,
@FormaPag		int,
@DocSalida		nvarchar(50),
@DiaO				int,
@Cancelada		bit,
@Kg				Numeric(18,2)=NULL,
@ID_Ayudante1	int=NULL,
@ID_Ayudante2	int=NULL,
@IdEmpresa		varchar(50))
AS
Begin
	if Not Exists(SELECT * FROM Venta Where RutaId=@RutaId And IdEmpresa=@IdEmpresa And DiaO=@Diao And CodCliente=@CodCliente And Documento=@Documento)
	begin
		Insert into Venta(RutaId,VendedorId,CodCliente,Documento,Fecha,TipoVta,DiasCred,CreditoDispo,Fvence,SubTotal,IVA,IEPS,TOTAL,EnLetra,
								Items,FormaPag,DocSalida,DiaO,Cancelada,Kg,ID_Ayudante1,ID_Ayudante2,IdEmpresa)
		Values(@RutaId,@VendedorId,@CodCliente,@Documento,@Fecha,@TipoVta,@DiasCred,@CreditoDispo,@Fvence,@SubTotal,@IVA,@IEPS,@TOTAL,@EnLetra,
				@Items,@FormaPag,@DocSalida,@DiaO,@Cancelada,@Kg,@ID_Ayudante1,@ID_Ayudante2,@IdEmpresa)
	end
	else
	begin
		if exists(Select * From DetalleVet Where RutaId=@RutaId And IdEmpresa=@IdEmpresa And Docto=@Documento)
		begin
			RAISERROR ('Folio de Venta Existente', 16, 1)
			return 0
		end
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ActualizaVisitas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ActualizaVisitas]
GO

CREATE PROCEDURE [dbo].[SPAD_ActualizaVisitas](
@CodCliente		varchar(50),
@DiaO				int,
@FechaI			datetime,
@EnSecuencia	bit,
@FechaF			datetime=NULL,
@Venta			Numeric(18,0),
@Pedido			Numeric(18,0),
@Devolucion		Numeric(18,0),
@Cobranza		Numeric(18,0),
@IdCe				Numeric(18,0),
@ruta				int,
@IdEmpresa	varchar(50))
AS
Begin
	if Not Exists(SELECT * FROM Visitas Where ruta=@ruta And IdEmpresa=@IdEmpresa And DiaO=@Diao And CodCliente=@CodCliente)
	begin
		Insert into Visitas(CodCliente,DiaO,FechaI,EnSecuencia,FechaF,Venta,Pedido,Devolucion,Cobranza,IdCe,ruta,IdEmpresa)
		Values(@CodCliente,@DiaO,@FechaI,@EnSecuencia,@FechaF,@Venta,@Pedido,@Devolucion,@Cobranza,@IdCe,@ruta,@IdEmpresa)
	end
End

GO

-- ----------------------------
-- Procedure structure for SPAD_AgregaDetalleComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_AgregaDetalleComision]
GO

CREATE PROCEDURE [dbo].[SPAD_AgregaDetalleComision](
@ID_Comision int,
@RangoIni int,
@RangoFin int,
@Angente float,
@Ayudante float,
@Tipo int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--procedimiento almacenado que agrega un detalle a la comision
	--Verifica que no se traslapen los rangos con otras comisiones y que tampoco existan rangos vacios entre comisiones
	declare @ID_Detalle int --variable utilizada para generar la llave
	if @RangoIni<1
	begin
		RAISERROR('El valor minimo que puede tomar un rango es 1', 16, 1)
		return
	end
	--Validando llaves foráneas
	if @RangoFin<=@RangoIni
	begin
		RAISERROR('El rango final debe ser mayor que el inicial', 16, 1)
		return
	end
	if not exists(select * from TH_Comision where ID_Comision=@ID_Comision And IdEmpresa=@IdEmpresa)
	begin
		RAISERROR('No se reconoce la comision', 16, 1)
		return
	end
	--verifico que no se traslapen los rahgos
	if exists(select * from TD_Comision where ID_Comision=@ID_Comision and IdEmpresa=@IdEmpresa and ((@RangoIni>=RangoIni and @RangoIni<=RangoFin) or (@RangoFin>=RangoIni and @RangoFin<=RangoFin)))
	begin
		RAISERROR('El rango no se puede traslapar con otros dentro de la comision', 16, 1)
		return
	end
	--genero la clave
	if not exists( select * from TD_Comision where ID_Comision=@ID_Comision And IdEmpresa=@Idempresa)
	begin
		select @ID_Detalle=1
	end
	else
	begin
		select @ID_Detalle=max(ID_Detalle)+1 from TD_Comision where ID_Comision=@ID_Comision And IdEmpresa=@IdEmpresa 
	end
	--agregando el registro
	insert into TD_Comision(ID_Comision,RangoIni,RangoFin,Angente,Ayudante,ID_Detalle,Tipo,IdEmpresa)
	values(@ID_Comision,@RangoIni,@RangoFin,@Angente,@Ayudante,@ID_Detalle,@Tipo,@IdEmpresa)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_AgregaSaldoIncial
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_AgregaSaldoIncial]
GO

CREATE PROCEDURE [dbo].[SPAD_AgregaSaldoIncial]
AS
begin
	Insert into Cobranza(Cliente, Documento, Saldo, Status, FechaReg, FechaVence, FolioInterno, TipoDoc, RutaId)
	Select C.IdCli, 0, C.Saldo, 1, getdate(), getdate()+cast(C.DiasCreedito as int), 0, 'Saldo Inicial', ISNULL((SELECT TOP 1 IdRuta FROM RelClirutas WHERE IdCliente=C.IdCli),0)
	From	Clientes C
	Where	C.IdCli Not In (Select Cliente From Cobranza Where Documento=0 AND IdCli=C.IdCli) And C.Credito=1 And IsNull(C.Saldo,0)>0
end

GO

-- ----------------------------
-- Procedure structure for SPAD_AgregaSaldoIncialCte
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_AgregaSaldoIncialCte]
GO

CREATE PROCEDURE [dbo].[SPAD_AgregaSaldoIncialCte](
@Cve_Cte	Varchar(50),
@IdEmpresa	Varchar(50))
AS
begin
	Declare @IdRuta	as Int
	SELECT TOP 1 @IdRuta=IdRuta FROM RelClirutas WHERE IdCliente=@Cve_Cte And IdEmpresa=@IdEmpresa
	if Exists(Select * From Cobranza Where Cliente=@Cve_Cte And Documento='0' And IdEmpresa=@IdEmpresa)
	begin
		if Exists(Select * From Clientes Where IdCli=@Cve_Cte And Credito=1 And IsNull(Saldo,0)>0 And IdEmpresa=@IdEmpresa)
			Update Cobranza Set Saldo=C.Saldo,FechaReg=GETDATE(),FechaVence=GETDATE()+Cast(C.DiasCreedito as int)
			From Cobranza Co Inner Join Clientes C On Co.Cliente=C.IdCli And Co.Documento='0'
			Where C.IdCli=@Cve_Cte And Co.Documento='0' And Co.Saldo!=C.Saldo And C.IdEmpresa=@IdEmpresa
		else
		begin
			if exists(Select * From Cobranza Where Cliente=@Cve_Cte And Documento='0')
			begin
				Delete DetalleCob Where IdCobranza In (Select id From Cobranza Where Cliente=@Cve_Cte And Documento='0' And IdEmpresa=@IdEmpresa) And IdEmpresa=@IdEmpresa
				Delete Cobranza Where Cliente=@Cve_Cte And Documento='0' And IdEmpresa=@IdEmpresa
			end
		end
	end
	else
	begin
		if @IdRuta is Not NULL
		begin
			Insert into Cobranza(Cliente, Documento, Saldo, Status, FechaReg, FechaVence, FolioInterno, TipoDoc, RutaId, IdEmpresa)
			Select C.IdCli, '0', C.Saldo, 1, getdate(), getdate()+cast(C.DiasCreedito as int), 0, 'Saldo Inicial',IsNull(@IdRuta,0),@IdEmpresa
			From	Clientes C
			Where	C.IdCli=@Cve_Cte And C.Credito=1 And IsNull(C.Saldo,0)>0 And IdEmpresa=@IdEmpresa
		end
		else
		begin
			Insert into Cobranza(Cliente, Documento, Saldo, Status, FechaReg, FechaVence, FolioInterno, TipoDoc, IdEmpresa)
			Select C.IdCli, '0', C.Saldo, 1, getdate(), getdate()+cast(C.DiasCreedito as int), 0, 'Saldo Inicial',@IdEmpresa
			From	Clientes C
			Where	C.IdCli=@Cve_Cte And C.Credito=1 And IsNull(C.Saldo,0)>0 And IdEmpresa=@IdEmpresa
		end
	end
end

GO

-- ----------------------------
-- Procedure structure for SPAD_BorraDetalleComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_BorraDetalleComision]
GO

CREATE PROCEDURE [dbo].[SPAD_BorraDetalleComision](
@ID_Comision int,
@ID_Detalle int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--procedimiento almacenado que elimina un detalle de la comision
	--eliminando el registro
	delete	TD_Comision
	 where	ID_Comision=@ID_Comision and ID_Detalle=@ID_Detalle and IdEmpresa=@IdEmpresa
end

GO

-- ----------------------------
-- Procedure structure for SPAD_CargaComisiones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_CargaComisiones]
GO

CREATE PROCEDURE [dbo].[SPAD_CargaComisiones](
@nombra varchar(MAX),
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--regresa la lista de todas las comisiones dadas de alta en el sistema
	select	h.ID_Comision,h.ID_Producto,h.Status,h.Comentarios,p.Producto
	from		TH_Comision h Inner Join Productos p On h.ID_Producto=p.Id And h.IdEmpresa=p.IdEmpresa
	where		p.Producto like '%'+@nombra+'%' And h.IdEmpresa=@IdEmpresa
	order by p.Producto
end

GO

-- ----------------------------
-- Procedure structure for SPAD_CargaDetallesComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_CargaDetallesComision]
GO

CREATE PROCEDURE [dbo].[SPAD_CargaDetallesComision](
@ID_Comision int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--Fecha de modificación 04/03/2015
	--regresa la lista de detalles de la comision seleccionada
	select	ID_Comision,ID_Detalle,RangoIni,RangoFin,Case Tipo When 0 Then 'CJ' When 1 Then 'PZ' End UnidadMedida,Angente,Ayudante
	from 		TD_Comision
	where		ID_Comision=@ID_Comision And IdEmpresa=@Idempresa
	order by ID_Detalle
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ConcentradoEnvasesComodato
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ConcentradoEnvasesComodato]
GO

CREATE PROCEDURE [dbo].[SPAD_ConcentradoEnvasesComodato](
@FechaIni Date,
@FechaFin Date,
@NumRuta	 int=0,
@IdEmpresa varchar(5))
AS
begin
	Declare @IdRuta	int
	Declare @Ruta		Varchar(50)
	Declare @Cmnd		Varchar(max)
	Declare @Cmnd1		Varchar(max)
	Declare @Cmnd2		Varchar(max)
	Declare @Cmnd3		Varchar(max)
	Declare @Cmnd4		Varchar(max)
	Declare @Cmnd5		Varchar(max)
	Declare @Clave		Varchar(50)
	Declare @Descrip	varchar(200)
	declare @Tipo		int
	Declare @Cant		int
	Declare @Hist		int
	Declare @ProductoEnvases table(Articulo varchar(15), Descripcion varchar(100),Tipo int)  

	insert into @ProductoEnvases
	SELECT Clave, Clave + ' ' + Producto + ' CAJA ' AS Descrip,1 FROM Productos WHERE Ban_Envase=1
	UNION ALL
	SELECT Clave, Clave + ' ' + Producto + ' PIEZAS ' AS Descrip,2 FROM Productos WHERE Ban_Envase=1

	Declare C Cursor For
		Select Descripcion From @ProductoEnvases order by Articulo asc
	Open C
	Fetch Next From C Into @Descrip
	if Exists (Select name From sysobjects Where name='T_RepConEnvComodato')
		Drop Table T_RepConEnvComodato

	Set @Cmnd='CREATE TABLE T_RepConEnvComodato(IdEmpresa varchar(50),Ruta varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Descrip+'] Int,'
		Set @Cmnd3=@Cmnd3+'['+@Descrip+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@Descrip+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@Descrip+'])+'
		Fetch Next From C Into @Descrip
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
 	Execute (@Cmnd)
		if (@NumRuta>0)
			Declare R Cursor For
				Select IdRutas,Ruta From Rutas where IdRutas=@NumRuta Order By Ruta
		else
			Declare R Cursor For
				Select IdRutas,Ruta From Rutas Order By Ruta
	Open R
	Fetch Next From R Into @IdRuta,@Ruta
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			SELECT * FROM @ProductoEnvases  ORDER  BY ARTICULO ASC
		Open D
		SELECT @Cmnd='Insert Into T_RepConEnvComodato(IdEmpresa,Ruta,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave,@Descrip,@Tipo
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			SELECT @Cant=CASE WHEN P.Tipo=1 THEN SUM(E.Cantidad) / ProductosXPzas.PzaXCja ELSE SUM(E.Cantidad) % ProductosXPzas.PzaXCja END
			FROM            DevEnvases AS E INNER JOIN
			Clientes AS C ON E.CodCli = C.IdCli INNER JOIN
			@ProductoEnvases AS P ON E.Envase = P.Articulo INNER JOIN
			ProductosXPzas ON P.Articulo = ProductosXPzas.Producto INNER JOIN
			DiasO ON E.DiaO = DiasO.DiaO AND E.RutaId = DiasO.RutaId AND E.IdEmpresa = DiasO.IdEmpresa
			WHERE (E.Tipo = 'Comodato') AND  E.RutaId=@IdRuta AND DiasO.Fecha BETWEEN @FechaIni AND @FechaFin AND P.Descripcion=@Descrip
			GROUP BY ProductosXPzas.PzaXCja,P.Tipo

			if @Cant Is NULL
				SET @Cant=0
			
			SET @Cmnd1=@Cmnd1+'['+@Descrip+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			print @Cmnd1
			print @Cmnd2
			Fetch Next From D Into @Clave,@Descrip,@Tipo
		end

		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('+IsNull('''' + @IdEmpresa + '''','NULL')+','''+@Ruta+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		print  @Cmnd  
		Close D
		Deallocate D
		Execute (@Cmnd)
		Fetch Next From R Into @IdRuta,@Ruta
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConEnvComodato 	UNION
	Select '+IsNull('''' + @IdEmpresa + '''','NULL')+',''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConEnvComodato'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ConcentradoEnvasesPromo
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ConcentradoEnvasesPromo]
GO

CREATE PROCEDURE [dbo].[SPAD_ConcentradoEnvasesPromo](
@FechaIni Date,
@FechaFin Date,
@NumRuta	 int=0,
@IdEmpresa varchar(5))
AS
begin
	Declare @IdRuta	int
	Declare @Ruta		Varchar(50)
	Declare @Cmnd		Varchar(max)
	Declare @Cmnd1		Varchar(max)
	Declare @Cmnd2		Varchar(max)
	Declare @Cmnd3		Varchar(max)
	Declare @Cmnd4		Varchar(max)
	Declare @Cmnd5		Varchar(max)
	Declare @Clave		Varchar(50)
	Declare @Descrip	varchar(200)
	declare @Tipo		int
	Declare @Cant		int
	Declare @Hist		int
	Declare @ProductoEnvases table(Articulo varchar(15), Descripcion varchar(100),Tipo int)  

	insert into @ProductoEnvases
	SELECT Clave, Clave + ' ' + Producto + ' CAJA ' AS Descrip,1 FROM Productos WHERE Ban_Envase=1
	UNION ALL
	SELECT Clave, Clave + ' ' + Producto + ' PIEZAS ' AS Descrip,2 FROM Productos WHERE Ban_Envase=1

	Declare C Cursor For
		Select Descripcion From @ProductoEnvases order by Articulo asc
	Open C
	Fetch Next From C Into @Descrip
	if Exists (Select name From sysobjects Where name='T_RepConEnvPromo')
		Drop Table T_RepConEnvPromo

	Set @Cmnd='CREATE TABLE T_RepConEnvPromo(IdEmpresa varchar(50),Ruta varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Descrip+'] Int,'
		Set @Cmnd3=@Cmnd3+'['+@Descrip+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@Descrip+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@Descrip+'])+'
		Fetch Next From C Into @Descrip
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
 	Execute (@Cmnd)
		if (@NumRuta>0)
			Declare R Cursor For
				Select IdRutas,Ruta From Rutas where IdRutas=@NumRuta Order By Ruta
		else
			Declare R Cursor For
				Select IdRutas,Ruta From Rutas Order By Ruta

	Open R
	Fetch Next From R Into @IdRuta,@Ruta
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			SELECT * FROM @ProductoEnvases  ORDER  BY ARTICULO ASC
		Open D
		SELECT @Cmnd='Insert Into T_RepConEnvPromo(IdEmpresa,Ruta,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave,@Descrip,@Tipo
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			SELECT @Cant=CASE WHEN P.Tipo=1 THEN SUM(E.Cantidad) / ProductosXPzas.PzaXCja ELSE SUM(E.Cantidad) % ProductosXPzas.PzaXCja END
			FROM            DevEnvases AS E INNER JOIN
			Clientes AS C ON E.CodCli = C.IdCli INNER JOIN
			@ProductoEnvases AS P ON E.Envase = P.Articulo INNER JOIN
			ProductosXPzas ON P.Articulo = ProductosXPzas.Producto INNER JOIN
			DiasO ON E.DiaO = DiasO.DiaO AND E.RutaId = DiasO.RutaId AND E.IdEmpresa = DiasO.IdEmpresa
			WHERE (E.Tipo = 'Promocion') AND  E.RutaId=@IdRuta AND DiasO.Fecha BETWEEN @FechaIni AND @FechaFin AND P.Descripcion=@Descrip
			GROUP BY ProductosXPzas.PzaXCja,P.Tipo

			if @Cant Is NULL
				SET @Cant=0
			
			SET @Cmnd1=@Cmnd1+'['+@Descrip+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			Fetch Next From D Into @Clave,@Descrip,@Tipo
		end

		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('+IsNull('''' + @IdEmpresa + '''','NULL')+','''+@Ruta+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		Close D
		Deallocate D
		Execute (@Cmnd)
		Fetch Next From R Into @IdRuta,@Ruta
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConEnvPromo 	UNION
	Select '+IsNull('''' + @IdEmpresa + '''','NULL')+',''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConEnvPromo'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_CreaComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_CreaComision]
GO

CREATE PROCEDURE [dbo].[SPAD_CreaComision](
@ID_Producto int,
@Status char(1),
@Comentarios varchar(MAX),
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--procediiento almacenado que da de alta una comision
	--Validando que no sean vacios
	if(ltrim(@Status)='')
	begin
		RAISERROR('Falta el status. Puede ser A= Activo o I=Inactivo', 16, 1)
		return
	end
	--Validando llaves foráneas
	if not exists( select * from Productos where Id=@ID_Producto )
	begin
		RAISERROR('No se reconoce el producto', 16, 1)
		return
	end
	if exists( select * from TH_Comision h where h.ID_Producto=@ID_Producto)
	begin
		RAISERROR('Solo puede existir una comision por producto', 16, 1)
		return
	end
	--agregando el registro
	insert into TH_Comision(ID_Producto,Status,Comentarios,IdEmpresa)
	  values(@ID_Producto,@Status,@Comentarios,@IdEmpresa)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_CrearRegistroPorVendedorRuta
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_CrearRegistroPorVendedorRuta]
GO
-- sp
CREATE PROCEDURE [dbo].[SPAD_CrearRegistroPorVendedorRuta](
@Cliente	Varchar(50),
@Ruta		Varchar(50),
@Vendedor	Varchar(50),
@Empresa	Varchar(50)
)
AS
begin
	if not exists (Select * From RelDayCli Where Lunes IS NULL AND Martes IS NULL AND Miercoles IS NULL AND Jueves IS NULL AND Viernes IS NULL AND Sabado IS NULL AND Domingo IS NULL AND CodCli = @Cliente AND RutaId = @Ruta AND idVendedor = @Vendedor AND IdEmpresa = @Empresa)
	begin
		INSERT INTO RelDayCli (CodCli, RutaId, idVendedor, IdEmpresa) VALUES (@Cliente, @Ruta, @Vendedor, @Empresa)
	end
end;
GO

-- ----------------------------
-- Procedure structure for SPAD_DameComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_DameComision]
GO

CREATE PROCEDURE [dbo].[SPAD_DameComision](
@ID_Comision int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--regresa la lista de todas las comisiones dadas de alta en el sistema
	select	h.ID_Comision,h.ID_Producto,h.Status,h.Comentarios,p.Producto
	from		TH_Comision h Inner Join Productos p On h.ID_Producto=p.Id And h.IdEmpresa=p.IdEmpresa
	where		h.ID_Comision=@ID_Comision And h.IdEmpresa=@IdEmpresa
end

GO

-- ----------------------------
-- Procedure structure for SPAD_DameDetalleComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_DameDetalleComision]
GO

CREATE PROCEDURE [dbo].[SPAD_DameDetalleComision](
@ID_Comision int,
@ID_Detalle int,
@IdEmpresa varchar(50))
AS
begin
	--Fecha de creación 11/04/2014
	--Fecha de modificación 04/03/2015
	--regresa un solo registro de detalle
	select	ID_Comision,ID_Detalle,RangoIni,RangoFin,Angente,Ayudante,Tipo
	from		TD_Comision
	where		ID_Comision=@ID_Comision and ID_Detalle=@ID_Detalle And IdEmpresa=@IdEmpresa
end

GO

-- ----------------------------
-- Procedure structure for SPAD_DameRutas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_DameRutas]
GO

CREATE PROCEDURE [dbo].[SPAD_DameRutas](
@IdEmpresa	Varchar(50))
AS
begin
	--Fecha de creación 28/04/2014
	--procedimiento almacenado que regresa la lista de rutas
	select	IdRutas,Ruta
	from		Rutas
	where		IdEmpresa=@IdEmpresa
	order by Ruta
end

GO

-- ----------------------------
-- Procedure structure for SPAD_DiasEntregaxVendedor
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_DiasEntregaxVendedor]
GO

CREATE PROCEDURE [dbo].[SPAD_DiasEntregaxVendedor](
@Dia Varchar(30),
@Fecha Date,
@IdRuta as int,
@CveVend as int,
@IdEmpresa as varchar(10))
AS
begin
	Declare @Fechalarga date
	Declare @FechaIni date
	Declare @FechaFin date 
	Declare @FechaDia date 
	Declare @IdVendedor int
	Declare @Cod_cliente Varchar(50)
	Declare @CuentaCli int
	Declare @DiaSinAcentos varchar(50)
	Declare @Cont	int
	Declare @pedido int
	Declare @DiaAux  varchar(50)
	DECLARE @TablaTemporalFechas AS TABLE (Fecha Date , Dia Varchar(20))
	SET DATEFORMAT YMD
	set language 'spanish'
	set @FechaIni=@Fecha
	set @FechaFin=dateadd(d,7,@FechaIni)
	set @DiaSinAcentos= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Dia, 'á', 'a'), 'é','e'), 'í', 'i'), 'ó', 'o'), 'ú','u')
	WHILE (@FechaIni < @FechaFin)
		BEGIN
			set  @DiaAux= REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DATENAME(WEEKDAY, @FechaIni), 'á', 'a'), 'é','e'), 'í', 'i'), 'ó', 'o'), 'ú','u')
			INSERT INTO @TablaTemporalFechas (Fecha,Dia) VALUES (@FechaIni, @DiaAux)
			SET @FechaIni= DATEADD(DAY,1,@FechaIni)
		END
		--end 
		set @IdVendedor=@CveVend
		SELECT @FechaDia=Fecha FROM @TablaTemporalFechas where  Dia=@Dia
	Declare R Cursor For
		select  cod_cliente
		from pedidosliberados where  status in (3,4) and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega<=@FechaDia
		order  by  pedido
	Open R
	Fetch Next From R Into @Cod_cliente
	While @@FETCH_STATUS=0
	Begin
		set @CuentaCli=0
		select  @CuentaCli=count(Codcli) from reldaycli where rutaid=@IdRuta and idempresa=@IdEmpresa  and  CodCli=@Cod_cliente and idVendedor=@IdVendedor
		if (@CuentaCli=0)
		begin
			PRINT 'HACE INSERT'
			PRINT @Cod_cliente
			print @CuentaCli
			insert into reldaycli (CodCli,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,RutaId,IdEmpresa,IdVendedor) values(@Cod_cliente,0,0,0,0,0,0,0,@IdRuta,@Idempresa,@IdVendedor)
		end 
		Fetch Next From R Into @Cod_cliente
	End
	Close R
	Deallocate R
	select  * from reldaycli
    if (@DiaSinAcentos='LUNES')
		Update reldaycli  set Lunes =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor-- where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='MARTES')
		Update reldaycli  set  Martes =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor ---where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='MIERCOLES')
		Update reldaycli  set  Miercoles =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor ---where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='JUEVES')
		Update reldaycli  set  Jueves =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor ---where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='VIERNES')
		Update reldaycli  set  Viernes =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor -- where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='SABADO')
		Update reldaycli  set  Sabado =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor -- where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
    else if (@DiaSinAcentos='DOMINGO')
		Update reldaycli  set  Domingo =0 where IdEmpresa= @IdEmpresa and Rutaid=@IdRuta and idvendedor=@IdVendedor -- where codcli in (select  cod_cliente from pedidosliberados where  status=3 and idempresa=@IdEmpresa  and ruta=@IdRuta and  fecha_entrega=@FechaDia) and  idvendedor=@IdVendedor
	set @Cont=1
	Declare D Cursor For
		select  cod_cliente,min(pedido) from pedidosliberados where status  in (3,4) and  idempresa=@IdEmpresa and ruta=@IdRuta and  fecha_entrega<=@FechaDia
		group by cod_cliente
		order  by  min(pedido)
	Open D
	Fetch Next From D Into @Cod_cliente,@pedido
	While @@FETCH_STATUS=0
	Begin
		if (@DiaSinAcentos='LUNES')
			Update reldaycli  set Lunes=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='MARTES')
			Update reldaycli  set Martes=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='MIERCOLES')			
			Update reldaycli  set Miercoles=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='JUEVES' )
			Update reldaycli  set Jueves=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='VIERNES')
			Update reldaycli  set Viernes=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='SABADO')
			Update reldaycli  set Sabado=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		else if (@DiaSinAcentos='DOMINGO')
			Update reldaycli  set Domingo=@Cont  where codcli=@Cod_cliente and  idvendedor=@IdVendedor and rutaid=@IdRuta and idempresa=@IdEmpresa
		set @Cont=@cont+1
		PRINT 'CONTADOR'
		PRINT @Cont
		Fetch Next From D Into @Cod_cliente,@pedido
	End
	Close D
	Deallocate D
end



GO

-- ----------------------------
-- Procedure structure for SPAD_EliminaComision
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_EliminaComision]
GO

CREATE PROCEDURE [dbo].[SPAD_EliminaComision](
@ID_Comision int,
@IdEmpresa varchar(50)=NULL)
AS
begin
	--Fecha de creación 11/04/2014
	--procediiento almacenado que da de baja una promocion y sus detalles
	--validando llaves foraneas
	--eliminacion en cascada
	 delete	T2
	 from 	TH_Comision T1,TD_Comision T2
	 where 	T1.ID_Comision=@ID_Comision and T2.ID_Comision=T1.ID_Comision And T2.IdEmpresa=@IdEmpresa
	--eliminando el registro
	delete	TH_Comision
	 where	ID_Comision=@ID_Comision And IdEmpresa=@IdEmpresa
end

GO

-- ----------------------------
-- Procedure structure for SPAD_Encuestas_tmp
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_Encuestas_tmp]
GO
CREATE PROCEDURE [dbo].[SPAD_Encuestas_tmp](
                @Clave_Enc	varchar(50),
				@Desc_Enc varchar(255),
				@Tipo_Enc varchar(255),
				@IdEmpresa varchar(50))
                AS
                BEGIN
					
                    if Not Exists(Select * From Encuestas Where Clave_Enc=@Clave_Enc AND IdEmpresa=@IdEmpresa)
                    begin
						INSERT INTO Encuestas (
							Clave_Enc,
							Desc_Enc,
							Tipo_Enc,
							IdEmpresa
						) VALUES (
							@Clave_Enc,
							@Desc_Enc,
							@Tipo_Enc,
							@IdEmpresa
						)
                    end
					else
                    begin
						UPDATE Encuestas 
						SET 
							Clave_Enc = @Clave_Enc,
							Desc_Enc = @Desc_Enc,
							Tipo_Enc = @Tipo_Enc,
							IdEmpresa = @IdEmpresa
						WHERE Clave_Enc=@Clave_Enc AND IdEmpresa=@IdEmpresa
                    end
                END
GO

-- ----------------------------
-- Procedure structure for SPAD_LiberarPedidos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_LiberarPedidos]
GO

CREATE PROCEDURE [dbo].[SPAD_LiberarPedidos](
@Fecha DATE,
@Ruta  AS INT,
@IdEmpresa	varchar(50))
AS
BEGIN
	DECLARE @Pedido		VARCHAR(50)
	DECLARE @CodCliente	VARCHAR(50)
	DECLARE @Tipo		VARCHAR(50)
	DECLARE @Subt		DECIMAL(18,2)
	DECLARE @IVA		DECIMAL(18,2)
	DECLARE @IEPS		DECIMAL(18,2)
	DECLARE @Total		DECIMAL(18,2)
	DECLARE @Kg			DECIMAL(18,2)
	DECLARE @Vendedor	INT
	DECLARE @SKU		VARCHAR(50)
	DECLARE	@Pza		INT
	DECLARE @MontoCred	DECIMAL(18,2)
	DECLARE	@Msg		VARCHAR(100)
	DECLARE @Stock		DECIMAL
	DECLARE @FolioBO	VARCHAR(50)
	DECLARE @FBO		VARCHAR(50)
	DECLARE @Consec		INT
	DECLARE @Ban		INT	
	DECLARE @Motivo		VARCHAR(50)
	DECLARE @FechaEnt	DATETIME
	DECLARE @FormaPag	VARCHAR(20)
	DECLARE @BanBO		AS INT
	DECLARE @BanStock	AS INT
	DECLARE @ContStock	as int
	SET @Msg=''
	SET @Consec=0
	SET @Stock=0
	SET @BanBO=0
	SET @BanStock=0
	set @ContStock=0
	DECLARE R CURSOR FOR
		SELECT	Pedido,CodCliente,Tipo,Subt,IVA,IEPS,Total,Kg,Vendedor,fechaEntrega,FormaPag 
		FROM	dbo.Pedidos
		where	cancelado=0 AND Pedido NOT IN (SELECT Pedido FROM dbo.PEDIDOSLIBERADOS Where IdEmpresa=@IdEmpresa  AND ruta=@Ruta) 
				AND Pedido NOT IN (SELECT pedido FROM dbo.BACKORDER Where IdEmpresa=@IdEmpresa  AND ruta=@Ruta) AND ruta=@Ruta AND total>0
				AND CAST(pedidos.fecha AS DATE)=@Fecha And IdEmpresa=@IdEmpresa
		ORDER BY fecha
	Open R
	Fetch Next From R Into @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag
	WHILE @@FETCH_STATUS=0
	BEGIN
	SET @Ban=0
		IF @Tipo='Credito'
		BEGIN	
			SELECT @MontoCred=ISNULL(LimiteCredito,0) FROM Clientes WHERE IdCli=@CodCliente And IdEmpresa=@IdEmpresa
			IF @MontoCred<@Total
			BEGIN	
				SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
				IF  @Consec =0  
				BEGIN
					SET @Consec=@Consec+1
					SET @FolioBO=@Pedido + REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
				END	
				ELSE
				BEGIN	
					SELECT @FolioBO=FOLIO_BO FROM dbo.BACKORDER WHERE PEDIDO=@Pedido AND CAST(FECHA_BO AS date)=@Fecha  And IdEmpresa=@IdEmpresa and ruta=@Ruta
					IF @FolioBO=''
					BEGIN	
						SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
						SET @Consec=@Consec+1
						SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
					END  
				END
				SET @Msg='Credito Insuficiente'
				SET @Ban=1
				INSERT INTO BACKORDER(PEDIDO,RUTA,CODCLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_BO,FECHA_ENTREGA,FECHA_LIBERACION,TIPO,STATUS, 
									CANCELADA,SUBTOTAL,IVA,IEPS,TOTAL,KG,IDEMPRESA,Motivo)
				VALUES(@Pedido,@Ruta,@CodCliente,@FolioBO,@Fecha,GETDATE(),@FechaEnt,NULL,@Tipo,1,0,@Subt,@IVA,@IEPS,@Total,@Kg,@IdEmpresa,@Msg)
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	@pedido,@FolioBO,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,0,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,DetallePedidos.Importe,
						DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,1,@IdEmpresa,0
				FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa
						AND Pedidos.Ruta=DetallePedidos.RutaId
				WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa
				ORDER BY DetallePedidos.SKU
				--Insertamos las promociones del pedido como parte del BackOrder
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	P.Pedido,@FolioBO,P.Ruta,R.SKU,R.Cant,0,Case When R.TipMed='Pzas' Then 1 Else 0 End Tipo,
						case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End * IsNull(A.Equivalente,0) KG,0 Precio,0 Importe,
						0 IVA,0 IEPS,0 DesctoV,1,1,@IdEmpresa,0
				FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
						Join ProductosXPzas X On R.SKU=X.Producto
						Join Productos A On A.Clave=R.SKU
				WHERE	(R.RutaId=@Ruta) AND (CAST(P.Fecha AS DATE)=@Fecha) AND (P.Pedido=@Pedido) And P.IdEmpresa=@IdEmpresa
				ORDER BY R.SKU
				FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag
			END
		END
		SET @BanBO=0
		DECLARE P CURSOR FOR
					--Se obtiene el detalle del pedido en piezas y con sus promociones (solo crédito y contado)
			SELECT  D.SKU,Case When D.Tipo=0 Then D.Pza*X.PzaXCja Else D.Pza End
					+ Case When R.SKU Is Not NULL Then Case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End Else 0 End Cantidad
			FROM	Pedidos P INNER JOIN DetallePedidos D ON P.Pedido=D.Docto AND P.IdEmpresa=D.IdEmpresa AND P.Ruta=D.RutaId
					Left Join PRegalado R On R.RutaId=D.RutaId And R.Docto=D.Docto And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=D.IdEmpresa And R.SKU=D.SKU
					Join ProductosXPzas X On D.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo!='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
			UNION	--Si el pedido es deobserquio se agrega el detalle sin promociones
			SELECT  D.SKU,Case When D.Tipo=0 Then D.Pza*X.PzaXCja Else D.Pza End Cantidad
			FROM	Pedidos P INNER JOIN DetallePedidos D ON P.Pedido=D.Docto AND P.IdEmpresa=D.IdEmpresa AND P.Ruta=D.RutaId
					Join ProductosXPzas X On D.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta  AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
			UNION	--Agrego los productos de promoción que no estén en el pedido
			SELECT  R.SKU,Case When R.SKU Is Not NULL Then Case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End Else 0 End Cantidad
			FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
					Join ProductosXPzas X On R.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo!='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta  AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
		Open P
		Fetch Next From P Into @SKU,@Pza
		WHILE @@FETCH_STATUS=0
		BEGIN
			SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
			IF @Stock<@Pza
			BEGIN
				SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
				IF  @Consec =0  
				BEGIN
					SET @Consec=@Consec+1
					SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
				END	
				ELSE
				BEGIN	
					SELECT @FolioBO=FOLIO_BO FROM dbo.BACKORDER WHERE PEDIDO=@Pedido AND CAST(FECHA_BO AS date)=@Fecha And IdEmpresa=@IdEmpresa and ruta=@Ruta
					IF @FolioBO=''
					BEGIN	
						SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
						SET @Consec=@Consec+1
						SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
					END  
				END 
				SET @Msg='Stock Insuficiente'
				SET @Ban=1
				IF @BanBO=0 
				BEGIN
					INSERT INTO BACKORDER(PEDIDO, RUTA, CODCLIENTE, FOLIO_BO, FECHA_PEDIDO, FECHA_BO, TIPO, STATUS, 
					CANCELADA, SUBTOTAL, IVA, IEPS, TOTAL, KG, IDEMPRESA, Motivo)
					VALUES(@Pedido,@Ruta, @CodCliente,@FolioBO,@Fecha, GETDATE(),@Tipo,1, 0, @Subt,@IVA,@IEPS,@Total,@Kg,@IdEmpresa,@Msg)
					INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
					SELECT	@pedido,@FolioBO,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,0,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,DetallePedidos.Importe, 
							DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,1,@IdEmpresa,0
					FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa
							AND Pedidos.Ruta=DetallePedidos.RutaId
					WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido)
					ORDER BY DetallePedidos.SKU
					SET @BanBO=1
				END 
				--Insertamos las promociones del pedido como parte del BackOrder
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	P.Pedido,@FolioBO,P.Ruta,R.SKU,R.Cant,0,Case When R.TipMed='Pzas' Then 1 Else 0 End Tipo,
						case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End * IsNull(A.Equivalente,0) KG,0 Precio,0 Importe,
						0 IVA,0 IEPS,0 DesctoV,1,1,@IdEmpresa,0
				FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
						Join ProductosXPzas X On R.SKU=X.Producto
						Join Productos A On A.Clave=R.SKU
				WHERE	(R.RutaId=@Ruta) AND (CAST(P.Fecha AS DATE)=@Fecha) AND (P.Pedido=@Pedido) And P.IdEmpresa=@IdEmpresa AND  R.SKU=@SKU
				ORDER BY R.SKU
				FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag
			END
			FETCH NEXT FROM P INTO  @SKU,@Pza
		END
		CLOSE P
		DEALLOCATE P

		IF @Ban=0
		BEGIN	
			INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FECHA_PEDIDO,FECHA_ENTREGA,TIPO,STATUS,CANCELADA, 
										SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,IDEMPRESA,FormaPag)
			VALUES(@Pedido,@Ruta,@codCliente,@Fecha,@FechaEnt,@Tipo,2,0,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,@IdEmpresa,@FormaPag)
			INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA, 
											IDEMPRESA)
			SELECT	@Pedido,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,DetallePedidos.Pza,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,
					DetallePedidos.Importe,DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,2,0,@IdEmpresa
			FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa AND Pedidos.Ruta=DetallePedidos.RutaId
			WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa
			ORDER BY DetallePedidos.SKU
			UPDATE PEDIDOS  SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
		END
		FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag
	END
	CLOSE R
	DEALLOCATE R

	--Revisamos si hay BO por liberar
	DECLARE R CURSOR FOR
		SELECT	BACKORDER.PEDIDO,BACKORDER.CODCLIENTE,BACKORDER.TIPO,BACKORDER.SUBTOTAL,BACKORDER.IVA,BACKORDER.IEPS,BACKORDER.TOTAL,BACKORDER.KG, 
				BACKORDER.FOLIO_BO,Pedidos.Vendedor,MOTIVO,Pedidos.fechaEntrega,Pedidos.FormaPag
		FROM	BACKORDER INNER JOIN Pedidos ON BACKORDER.PEDIDO=Pedidos.Pedido and  BACKORDER.IdEmpresa=Pedidos.IdEmpresa
		WHERE	(BACKORDER.CANCELADA=0) AND (BACKORDER.STATUS=1) And BACKORDER.IdEmpresa=@IdEmpresa and BACKORDER.ruta=@Ruta 
		ORDER BY BACKORDER.pedido,BACKORDER.codcliente

	Open R
	Fetch Next From R Into @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @Ban=0
		IF @Motivo='Credito Insuficiente'
		BEGIN	
			SELECT @MontoCred=ISNULL(LimiteCredito,0) FROM Clientes WHERE IdCli=@CodCliente And IdEmpresa=@IdEmpresa
			IF @MontoCred>=@Total 
			BEGIN
				SET @BanStock=0
				SET @ContStock=0
				DECLARE P CURSOR FOR
					SELECT  bo.articulo as SKU ,Case When bo.Tipo=0 Then bo.Pza*X.PzaXCja Else bo.Pza End PZA  
					FROM	DETALLE_BO bo INNER JOIN ProductosXPzas X On bo.articulo=X.Producto
					WHERE bo.RUTA=@Ruta AND bo.FOLIO_BO=@FBO And bo.IdEmpresa=@IdEmpresa 
				Open P
				Fetch Next From P Into @SKU,@Pza
				WHILE @@FETCH_STATUS=0
				BEGIN
					SET @ContStock=@ContStock+1
					SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
					IF @Stock>=@Pza
					BEGIN
						SET @BanStock=@BanStock+1
					END
				FETCH NEXT FROM P INTO @SKU,@Pza
				END
				CLOSE P
				DEALLOCATE P
						IF @ContStock=@BanStock
						BEGIN
							INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_LIBERACION,FECHA_ENTREGA,TIPO,STATUS,CANCELADA,INCIDENCIA,
														SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,ID_AYUDANTE1,ID_AYUDANTE2,IDEMPRESA,RutaEnt,FormaPag)
							VALUES (@Pedido,@Ruta,@codCliente,@FBO,@Fecha,NULL,@FechaEnt,@Tipo,2,0,NULL,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,NULL,NULL,@IdEmpresa,NULL,@FormaPag)
							INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA,
															INCIDENCIA,IDEMPRESA)
							SELECT	@Pedido,@FBO,D.Ruta,D.Articulo,D.Pza,D.Pza,D.Tipo,D.KG,D.Precio,D.Importe,D.IVA,D.IEPS,D.Descuento,D.Ban_Promo,2,0,NULL,@IdEmpresa
							FROM Pedidos INNER JOIN Detalle_BO D ON Pedidos.Pedido=D.Pedido AND Pedidos.IdEmpresa=D.IdEmpresa AND Pedidos.Ruta=D.Ruta
							WHERE (D.Ruta=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa And D.Folio_BO=@FBO
							ORDER BY D.Articulo
							UPDATE  BACKORDER SET STATUS=0 WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa
							UPDATE PEDIDOS SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
							FETCH NEXT FROM R INTO @Pedido, @CodCliente,@Tipo, @Subt, @IVA, @IEPS, @Total, @Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
						END
						Else
						Begin
							UPDATE BACKORDER SET Motivo='Stock Insuficiente' WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa and ruta=@Ruta
						End
			END
		END	
		IF @Motivo='Stock Insuficiente'
		BEGIN
			SET @BanStock=0
			SET @ContStock=0
			DECLARE P CURSOR FOR
				SELECT  bo.articulo as SKU ,Case When bo.Tipo=0 Then bo.Pza*X.PzaXCja Else bo.Pza End PZA  
				FROM	DETALLE_BO bo INNER JOIN ProductosXPzas X On bo.articulo=X.Producto
				WHERE bo.RUTA=@Ruta AND bo.FOLIO_BO=@FBO And bo.IdEmpresa=@IdEmpresa 
			Open P
			Fetch Next From P Into @SKU,@Pza
			WHILE @@FETCH_STATUS=0
			BEGIN
				SET @ContStock=@ContStock+1
				SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
				IF @Stock>=@Pza
				BEGIN
					SET @BanStock=@BanStock+1
				END
			FETCH NEXT FROM P INTO @SKU,@Pza
			END
			CLOSE P
			DEALLOCATE P
					--print @Pedido
					--print @ContStock
					--print @BanStock
					IF @ContStock=@BanStock
					BEGIN
						INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_LIBERACION,FECHA_ENTREGA,TIPO,STATUS,CANCELADA,INCIDENCIA,
													SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,ID_AYUDANTE1,ID_AYUDANTE2,IDEMPRESA,RutaEnt,FormaPag)
						VALUES (@Pedido,@Ruta,@codCliente,@FBO,@Fecha,NULL,@FechaEnt,@Tipo,2,0,NULL,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,NULL,NULL,@IdEmpresa,NULL,@FormaPag)
						INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA,
														INCIDENCIA,IDEMPRESA)
						SELECT	@Pedido,@FBO,D.Ruta,D.Articulo,D.Pza,D.Pza,D.Tipo,D.KG,D.Precio,D.Importe,D.IVA,D.IEPS,D.Descuento,D.Ban_Promo,2,0,NULL,@IdEmpresa
						FROM Pedidos INNER JOIN Detalle_BO D ON Pedidos.Pedido=D.Pedido AND Pedidos.IdEmpresa=D.IdEmpresa AND Pedidos.Ruta=D.Ruta
						WHERE (D.Ruta=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa And D.Folio_BO=@FBO
						ORDER BY D.Articulo
						UPDATE  BACKORDER SET STATUS=0 WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa
						UPDATE PEDIDOS SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
						FETCH NEXT FROM R INTO @Pedido, @CodCliente,@Tipo, @Subt, @IVA, @IEPS, @Total, @Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
					END
		END	
		FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
	END
	CLOSE R
	DEALLOCATE R
END





GO

-- ----------------------------
-- Procedure structure for SPAD_LiberarPedidos_Todos
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_LiberarPedidos_Todos]
GO

CREATE PROCEDURE [dbo].[SPAD_LiberarPedidos_Todos](
@IdEmpresa	varchar(50))
AS
BEGIN
	DECLARE @Pedido		VARCHAR(50)
	DECLARE @CodCliente	VARCHAR(50)
	DECLARE @Tipo		VARCHAR(50)
	DECLARE @Subt		DECIMAL(18,2)
	DECLARE @IVA		DECIMAL(18,2)
	DECLARE @IEPS		DECIMAL(18,2)
	DECLARE @Total		DECIMAL(18,2)
	DECLARE @Kg			DECIMAL(18,2)
	DECLARE @Vendedor	INT
	DECLARE @SKU		VARCHAR(50)
	DECLARE	@Pza		INT
	DECLARE @MontoCred	DECIMAL(18,2)
	DECLARE	@Msg		VARCHAR(100)
	DECLARE @Stock		DECIMAL
	DECLARE @FolioBO	VARCHAR(50)
	DECLARE @FBO		VARCHAR(50)
	DECLARE @Consec		INT
	DECLARE @Ban		INT	
	DECLARE @Motivo		VARCHAR(50)
	DECLARE @FechaEnt	DATETIME
	DECLARE @FormaPag	VARCHAR(20)
	DECLARE @Ruta		INT
	DECLARE @Fecha		DATE
	DECLARE @BanBO		AS INT
	DECLARE @BanStock	AS INT
	DECLARE @ContStock	as int
	SET @Msg=''
	SET @Consec=0
	SET @Stock=0
	SET @BanBO=0
	SET @BanStock=0
	set @ContStock=0
	DECLARE R CURSOR FOR
		SELECT	Pedido,CodCliente,Tipo,Subt,IVA,IEPS,Total,Kg,Vendedor,fechaEntrega,FormaPag,Ruta, fecha 
		FROM	dbo.Pedidos
		where	cancelado=0 AND Pedido NOT IN (SELECT Pedido FROM dbo.PEDIDOSLIBERADOS Where IdEmpresa=@IdEmpresa) 
				AND Pedido NOT IN (SELECT pedido FROM dbo.BACKORDER Where IdEmpresa=@IdEmpresa) AND total>0
				AND IdEmpresa=@IdEmpresa
		ORDER BY fecha
	Open R
	Fetch Next From R Into @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag,@Ruta,@Fecha
	WHILE @@FETCH_STATUS=0
	BEGIN
	SET @Ban=0
		IF @Tipo='Credito'
		BEGIN	
			SELECT @MontoCred=ISNULL(LimiteCredito,0) FROM Clientes WHERE IdCli=@CodCliente And IdEmpresa=@IdEmpresa
			IF @MontoCred<@Total
			BEGIN	
				SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
				IF  @Consec =0  
				BEGIN
					SET @Consec=@Consec+1
					SET @FolioBO=@Pedido + REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
				END	
				ELSE
				BEGIN	
					SELECT @FolioBO=FOLIO_BO FROM dbo.BACKORDER WHERE PEDIDO=@Pedido AND CAST(FECHA_BO AS date)=@Fecha  And IdEmpresa=@IdEmpresa and ruta=@Ruta
					IF @FolioBO=''
					BEGIN	
						SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
						SET @Consec=@Consec+1
						SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
					END  
				END
				SET @Msg='Credito Insuficiente'
				SET @Ban=1
				INSERT INTO BACKORDER(PEDIDO,RUTA,CODCLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_BO,FECHA_ENTREGA,FECHA_LIBERACION,TIPO,STATUS, 
									CANCELADA,SUBTOTAL,IVA,IEPS,TOTAL,KG,IDEMPRESA,Motivo)
				VALUES(@Pedido,@Ruta,@CodCliente,@FolioBO,@Fecha,GETDATE(),@FechaEnt,NULL,@Tipo,1,0,@Subt,@IVA,@IEPS,@Total,@Kg,@IdEmpresa,@Msg)
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	@pedido,@FolioBO,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,0,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,DetallePedidos.Importe,
						DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,1,@IdEmpresa,0
				FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa
						AND Pedidos.Ruta=DetallePedidos.RutaId
				WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa
				ORDER BY DetallePedidos.SKU
				--Insertamos las promociones del pedido como parte del BackOrder
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	P.Pedido,@FolioBO,P.Ruta,R.SKU,R.Cant,0,Case When R.TipMed='Pzas' Then 1 Else 0 End Tipo,
						case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End * IsNull(A.Equivalente,0) KG,0 Precio,0 Importe,
						0 IVA,0 IEPS,0 DesctoV,1,1,@IdEmpresa,0
				FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
						Join ProductosXPzas X On R.SKU=X.Producto
						Join Productos A On A.Clave=R.SKU
				WHERE	(R.RutaId=@Ruta) AND (CAST(P.Fecha AS DATE)=@Fecha) AND (P.Pedido=@Pedido) And P.IdEmpresa=@IdEmpresa
				ORDER BY R.SKU
				FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag,@Ruta,@Fecha
			END
		END
		SET @BanBO=0
		DECLARE P CURSOR FOR
					--Se obtiene el detalle del pedido en piezas y con sus promociones (solo crédito y contado)
			SELECT  D.SKU,Case When D.Tipo=0 Then D.Pza*X.PzaXCja Else D.Pza End
					+ Case When R.SKU Is Not NULL Then Case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End Else 0 End Cantidad
			FROM	Pedidos P INNER JOIN DetallePedidos D ON P.Pedido=D.Docto AND P.IdEmpresa=D.IdEmpresa AND P.Ruta=D.RutaId
					Left Join PRegalado R On R.RutaId=D.RutaId And R.Docto=D.Docto And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=D.IdEmpresa And R.SKU=D.SKU
					Join ProductosXPzas X On D.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo!='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
			UNION	--Si el pedido es deobserquio se agrega el detalle sin promociones
			SELECT  D.SKU,Case When D.Tipo=0 Then D.Pza*X.PzaXCja Else D.Pza End Cantidad
			FROM	Pedidos P INNER JOIN DetallePedidos D ON P.Pedido=D.Docto AND P.IdEmpresa=D.IdEmpresa AND P.Ruta=D.RutaId
					Join ProductosXPzas X On D.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta  AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
			UNION	--Agrego los productos de promoción que no estén en el pedido
			SELECT  R.SKU,Case When R.SKU Is Not NULL Then Case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End Else 0 End Cantidad
			FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
					Join ProductosXPzas X On R.SKU=X.Producto
			Where	P.Cancelado=0 And P.Tipo!='Obsequio'
					And P.Pedido=@Pedido And P.Ruta=@Ruta  AND CAST(P.fecha AS DATE)=@Fecha And P.IdEmpresa=@IdEmpresa
		Open P
		Fetch Next From P Into @SKU,@Pza
		WHILE @@FETCH_STATUS=0
		BEGIN
			SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
			IF @Stock<@Pza
			BEGIN
				SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
				IF  @Consec =0  
				BEGIN
					SET @Consec=@Consec+1
					SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
				END	
				ELSE
				BEGIN	
					SELECT @FolioBO=FOLIO_BO FROM dbo.BACKORDER WHERE PEDIDO=@Pedido AND CAST(FECHA_BO AS date)=@Fecha And IdEmpresa=@IdEmpresa and ruta=@Ruta
					IF @FolioBO=''
					BEGIN	
						SELECT @Consec=COUNT(FOLIO_BO) FROM dbo.BACKORDER WHERE PEDIDO=@Pedido And IdEmpresa=@IdEmpresa and ruta=@Ruta
						SET @Consec=@Consec+1
						SET @FolioBO=@Pedido+REPLICATE('0', (3 - LEN(@Consec))) + CAST(@Consec AS VARCHAR(2))
					END  
				END 
				SET @Msg='Stock Insuficiente'
				SET @Ban=1
				IF @BanBO=0 
				BEGIN
					INSERT INTO BACKORDER(PEDIDO, RUTA, CODCLIENTE, FOLIO_BO, FECHA_PEDIDO, FECHA_BO, TIPO, STATUS, 
					CANCELADA, SUBTOTAL, IVA, IEPS, TOTAL, KG, IDEMPRESA, Motivo)
					VALUES(@Pedido,@Ruta, @CodCliente,@FolioBO,@Fecha, GETDATE(),@Tipo,1, 0, @Subt,@IVA,@IEPS,@Total,@Kg,@IdEmpresa,@Msg)
					INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
					SELECT	@pedido,@FolioBO,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,0,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,DetallePedidos.Importe, 
							DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,1,@IdEmpresa,0
					FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa
							AND Pedidos.Ruta=DetallePedidos.RutaId
					WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido)
					ORDER BY DetallePedidos.SKU
					SET @BanBO=1
				END 
				--Insertamos las promociones del pedido como parte del BackOrder
				INSERT INTO DETALLE_BO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_ENTREGADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,IDEMPRESA,CANCELADA)
				SELECT	P.Pedido,@FolioBO,P.Ruta,R.SKU,R.Cant,0,Case When R.TipMed='Pzas' Then 1 Else 0 End Tipo,
						case When R.TipMed='Pzas' Then R.Cant Else R.Cant*X.PzaXCja End * IsNull(A.Equivalente,0) KG,0 Precio,0 Importe,
						0 IVA,0 IEPS,0 DesctoV,1,1,@IdEmpresa,0
				FROM	Pedidos P INNER Join PRegalado R On R.RutaId=P.Ruta And R.Docto=P.Pedido And R.Cliente=P.CodCliente And R.DiaO=P.DiaO And R.IdEmpresa=P.IdEmpresa
						Join ProductosXPzas X On R.SKU=X.Producto
						Join Productos A On A.Clave=R.SKU
				WHERE	(R.RutaId=@Ruta) AND (CAST(P.Fecha AS DATE)=@Fecha) AND (P.Pedido=@Pedido) And P.IdEmpresa=@IdEmpresa AND  R.SKU=@SKU
				ORDER BY R.SKU
				FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag,@Ruta,@Fecha
			END
			FETCH NEXT FROM P INTO  @SKU,@Pza
		END
		CLOSE P
		DEALLOCATE P

		IF @Ban=0
		BEGIN	
			print @PEDIDO 
			print @codcliente 
			print @RUTA 
			print @IdEmpresa

			INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FECHA_PEDIDO,FECHA_ENTREGA,TIPO,STATUS,CANCELADA, 
										SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,IDEMPRESA,FormaPag)
			VALUES(@Pedido,@Ruta,@codCliente,@Fecha,@FechaEnt,@Tipo,2,0,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,@IdEmpresa,@FormaPag)
			INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA, 
											IDEMPRESA)
			SELECT	@Pedido,Pedidos.Ruta,DetallePedidos.SKU,DetallePedidos.Pza,DetallePedidos.Pza,DetallePedidos.Tipo,DetallePedidos.KG,DetallePedidos.Precio,
					DetallePedidos.Importe,DetallePedidos.IVA,DetallePedidos.IEPS,DetallePedidos.DesctoV,0,2,0,@IdEmpresa
			FROM	Pedidos INNER JOIN DetallePedidos ON Pedidos.Pedido=DetallePedidos.Docto AND Pedidos.IdEmpresa=DetallePedidos.IdEmpresa AND Pedidos.Ruta=DetallePedidos.RutaId
			WHERE	(DetallePedidos.RutaId=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa
			ORDER BY DetallePedidos.SKU
			UPDATE PEDIDOS  SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
		END
		FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@Vendedor,@FechaEnt,@FormaPag,@Ruta,@Fecha
	END
	CLOSE R
	DEALLOCATE R
	--Revisamos si hay BO por liberar
	DECLARE R CURSOR FOR
		SELECT	BACKORDER.PEDIDO,BACKORDER.CODCLIENTE,BACKORDER.TIPO,BACKORDER.SUBTOTAL,BACKORDER.IVA,BACKORDER.IEPS,BACKORDER.TOTAL,BACKORDER.KG, 
				BACKORDER.FOLIO_BO,Pedidos.Vendedor,MOTIVO,Pedidos.fechaEntrega,Pedidos.FormaPag
		FROM	BACKORDER INNER JOIN Pedidos ON BACKORDER.PEDIDO=Pedidos.Pedido and  BACKORDER.IdEmpresa=Pedidos.IdEmpresa
		WHERE	(BACKORDER.CANCELADA=0) AND (BACKORDER.STATUS=1) And BACKORDER.IdEmpresa=@IdEmpresa and BACKORDER.ruta=@Ruta 
		ORDER BY BACKORDER.pedido,BACKORDER.codcliente

	Open R
	Fetch Next From R Into @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
	WHILE @@FETCH_STATUS=0
	BEGIN
		SET @Ban=0
		IF @Motivo='Credito Insuficiente'
		BEGIN	
			SELECT @MontoCred=ISNULL(LimiteCredito,0) FROM Clientes WHERE IdCli=@CodCliente And IdEmpresa=@IdEmpresa
			IF @MontoCred>=@Total 
			BEGIN
				SET @BanStock=0
				SET @ContStock=0
				DECLARE P CURSOR FOR
					SELECT  bo.articulo as SKU ,Case When bo.Tipo=0 Then bo.Pza*X.PzaXCja Else bo.Pza End PZA  
					FROM	DETALLE_BO bo INNER JOIN ProductosXPzas X On bo.articulo=X.Producto
					WHERE bo.RUTA=@Ruta AND bo.FOLIO_BO=@FBO And bo.IdEmpresa=@IdEmpresa 
				Open P
				Fetch Next From P Into @SKU,@Pza
				WHILE @@FETCH_STATUS=0
				BEGIN
					SET @ContStock=@ContStock+1
					SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
					IF @Stock>=@Pza
					BEGIN
						SET @BanStock=@BanStock+1
					END
				FETCH NEXT FROM P INTO @SKU,@Pza
				END
				CLOSE P
				DEALLOCATE P
						IF @ContStock=@BanStock
						BEGIN
							INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_LIBERACION,FECHA_ENTREGA,TIPO,STATUS,CANCELADA,INCIDENCIA,
														SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,ID_AYUDANTE1,ID_AYUDANTE2,IDEMPRESA,RutaEnt,FormaPag)
							VALUES (@Pedido,@Ruta,@codCliente,@FBO,@Fecha,NULL,@FechaEnt,@Tipo,2,0,NULL,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,NULL,NULL,@IdEmpresa,NULL,@FormaPag)
							INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA,
															INCIDENCIA,IDEMPRESA)
							SELECT	@Pedido,@FBO,D.Ruta,D.Articulo,D.Pza,D.Pza,D.Tipo,D.KG,D.Precio,D.Importe,D.IVA,D.IEPS,D.Descuento,D.Ban_Promo,2,0,NULL,@IdEmpresa
							FROM Pedidos INNER JOIN Detalle_BO D ON Pedidos.Pedido=D.Pedido AND Pedidos.IdEmpresa=D.IdEmpresa AND Pedidos.Ruta=D.Ruta
							WHERE (D.Ruta=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa And D.Folio_BO=@FBO
							ORDER BY D.Articulo
							UPDATE  BACKORDER SET STATUS=0 WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa
							UPDATE PEDIDOS SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
							FETCH NEXT FROM R INTO @Pedido, @CodCliente,@Tipo, @Subt, @IVA, @IEPS, @Total, @Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
						END
						Else
						Begin
							UPDATE BACKORDER SET Motivo='Stock Insuficiente' WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa and ruta=@Ruta
						End
			END
		END	
		IF @Motivo='Stock Insuficiente'
		BEGIN 
			SET @BanStock=0
			SET @ContStock=0
			DECLARE P CURSOR FOR
				SELECT  bo.articulo as SKU ,Case When bo.Tipo=0 Then bo.Pza*X.PzaXCja Else bo.Pza End PZA  
				FROM	DETALLE_BO bo INNER JOIN ProductosXPzas X On bo.articulo=X.Producto
				WHERE bo.RUTA=@Ruta AND bo.FOLIO_BO=@FBO And bo.IdEmpresa=@IdEmpresa 
			Open P
			Fetch Next From P Into @SKU,@Pza
			WHILE @@FETCH_STATUS=0
			BEGIN
				SET @ContStock=@ContStock+1
				SELECT @Stock=Stock FROM dbo.Th_StockAlmacen WHERE Articulo=@SKU and IdEmpresa=@IdEmpresa
				IF @Stock>=@Pza
				BEGIN
					SET @BanStock=@BanStock+1
				END
			FETCH NEXT FROM P INTO @SKU,@Pza
			END
			CLOSE P
			DEALLOCATE P
					IF @ContStock=@BanStock
					BEGIN
						INSERT INTO PEDIDOSLIBERADOS(PEDIDO,RUTA,COD_CLIENTE,FOLIO_BO,FECHA_PEDIDO,FECHA_LIBERACION,FECHA_ENTREGA,TIPO,STATUS,CANCELADA,INCIDENCIA,
													SUBTOTAL,IVA,IEPS,TOTAL,KG,IDVENDEDOR,ID_AYUDANTE1,ID_AYUDANTE2,IDEMPRESA,RutaEnt,FormaPag)
						VALUES (@Pedido,@Ruta,@codCliente,@FBO,@Fecha,NULL,@FechaEnt,@Tipo,2,0,NULL,@subt,@IVA,@IEPS,@Total,@Kg,@vendedor,NULL,NULL,@IdEmpresa,NULL,@FormaPag)
						INSERT INTO DETALLEPEDIDOLIBERADO(PEDIDO,FOLIO_BO,RUTA,ARTICULO,PZA,PZA_LIBERADAS,TIPO,KG,PRECIO,IMPORTE,IVA,IEPS,DESCUENTO,BAN_PROMO,STATUS,CANCELADA,
														INCIDENCIA,IDEMPRESA)
						SELECT	@Pedido,@FBO,D.Ruta,D.Articulo,D.Pza,D.Pza,D.Tipo,D.KG,D.Precio,D.Importe,D.IVA,D.IEPS,D.Descuento,D.Ban_Promo,2,0,NULL,@IdEmpresa
						FROM Pedidos INNER JOIN Detalle_BO D ON Pedidos.Pedido=D.Pedido AND Pedidos.IdEmpresa=D.IdEmpresa AND Pedidos.Ruta=D.Ruta
						WHERE (D.Ruta=@Ruta) AND (CAST(Pedidos.Fecha AS DATE)=@Fecha) AND (Pedidos.Pedido=@Pedido) And Pedidos.IdEmpresa=@IdEmpresa And D.Folio_BO=@FBO
						ORDER BY D.Articulo
						UPDATE  BACKORDER SET STATUS=0 WHERE FOLIO_BO=@FBO And IdEmpresa=@IdEmpresa
						UPDATE PEDIDOS SET STATUS=2 WHERE PEDIDO=@PEDIDO AND CODCLIENTE=@codcliente AND RUTA=@RUTA And IdEmpresa=@IdEmpresa
						FETCH NEXT FROM R INTO @Pedido, @CodCliente,@Tipo, @Subt, @IVA, @IEPS, @Total, @Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
					END
		END	
		FETCH NEXT FROM R INTO @Pedido,@CodCliente,@Tipo,@Subt,@IVA,@IEPS,@Total,@Kg,@FBO,@Vendedor,@Motivo,@FechaEnt,@FormaPag
	END
	CLOSE R
	DEALLOCATE R
END

GO

-- ----------------------------
-- Procedure structure for SPAD_OrdenaPromo
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_OrdenaPromo]
GO

CREATE PROCEDURE [dbo].[SPAD_OrdenaPromo](
@IdPromo	int)
AS
Begin
	Declare @Cont int
	Declare @Max int
	Declare @Articulo varchar(50)
	Select @Max=COUNT(Articulo) From DetallePromo Where PromoId=@IdPromo And Tipo=1
	Set @Cont=0
	If @Max>0
	Begin
		While @Cont<@Max
		Begin
			Select Top 1 @Articulo=Articulo From DetallePromo Where PromoId=@IdPromo And Tipo=1 And Nivel>=@Cont Order By Nivel
			Update DetallePromo Set Nivel=@Cont Where PromoId=@IdPromo And Tipo=1 And Articulo=@Articulo
			Set @Cont=@Cont+1
		End
	End
End

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteCargaConcentrado
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteCargaConcentrado]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteCargaConcentrado](
@Fecha Date,
@IdEmp varchar(50)=NULL)
AS
begin
	Declare @IdProd varchar(10)
	Declare @Producto Varchar(50)
	Declare @Nivel int
	Declare @Clasificacion Varchar(50)
	Declare @NivelDep  Varchar(50)
	Declare @Cmnd   Varchar(max)
	Declare @Cmnd1  Varchar(max)
	Declare @Cmnd2  Varchar(max)
	Declare @Cmnd3  Varchar(max)
	Declare @Cmnd4  Varchar(max)
	Declare @Cmnd5  Varchar(max)
	Declare @ClaveR  Varchar(50)
	Declare @DescripR varchar(200)
	Declare @Clasif1 varchar(50)
	Declare @Clasif2 varchar(50)
	Declare @Clasif3 varchar(50)
	Declare @Clasif4 varchar(50)
	Declare @Clasif5 varchar(50)
	declare @idProdNew VARCHAR(50)
	declare @idProdNewTemp VARCHAR(50)
	Declare @Cant   int
	Declare @Hist	 int
	SET @Hist=0
	Declare C Cursor For
		Select Ruta as ClaveR, Ruta as DescripR From rutas 
		--Select Clave, Clave as Descrip From Productos Where Ban_Envase=0 
		--Select Clave  From Productos Where Ban_Envase=0 
	Open C
	Fetch Next From C Into @ClaveR,@DescripR
	if Exists (Select name From sysobjects Where name='T_RepConCarga')
		Drop Table T_RepConCarga
	Set @Cmnd='CREATE TABLE T_RepConCarga (IdEmpresa varchar(50),Sector varchar(50),Presentacion varchar(50),Familia varchar(50),Marca varchar(50),Unidades varchar(50),IdProd varchar(50),Producto varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	set @idProdNew=0
	SET @idProdNewTemp=0
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@DescripR+'] Int,'
		Set @Cmnd3=@Cmnd3+'['+@DescripR+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@DescripR+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@DescripR+'])+'
		Fetch Next From C Into @ClaveR,@DescripR
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
	--Select @Cmnd
 	Execute (@Cmnd)
	Declare R Cursor For
		--Select IdRutas,Ruta From Rutas Order By Ruta
		--Select Clave as IdProd,Producto as Producto From Productos Where Ban_Envase=0 
			SELECT        Productos.Clave AS IdProd, Productos.Producto, ISNULL(RelProClas.Nivel, 0) AS Nivel, ISNULL(ClasProductos.Descripcion, '') AS Clasificacion, ISNULL(ClasProductos_1.NivelNum, '') AS NivelDep
			FROM            Productos LEFT OUTER JOIN
											 RelProClas ON Productos.Clave = RelProClas.ProductoId LEFT OUTER JOIN
											 ClasProductos AS ClasProductos_1 ON RelProClas.Dep_Clasif = ClasProductos_1.IdClasP LEFT OUTER JOIN
											 ClasProductos ON RelProClas.Clasif = ClasProductos.IdClasP
			ORDER BY IdProd, Nivel

	Open R
	Fetch Next From R Into @IdProd,@Producto,@Nivel,@Clasificacion,@NivelDep
	----if exists(Select * From StockHistorico Where Fecha Between @Fecha And DATEADD(d,1,@Fecha))
	----	SET @Hist=1
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			--Select Clave,Clave as Descrip From Productos Where Ban_Envase=0 Order By Clave
			Select IdRutas as ClaveR, Ruta as DescripR From rutas 
		Open D
		SELECT @Cmnd='Insert Into T_RepConCarga(IdEmpresa,Sector,Presentacion,Familia,Marca,Unidades,IdProd,Producto,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		SET @Clasif1=''
		SET @Clasif2=''
		SET @Clasif3=''
		SET @Clasif4=''
		SET @Clasif5=''
		Fetch Next From D Into @ClaveR,@DescripR
		While @@FETCH_STATUS=0
		Begin
			set @idProdNew=@IdProd
			SET @Cant=NULL
			----if @Hist=0
			----	Select	@Cant=P.Stock/IsNull(A.PzaXCja,1)
			----	From		Stock P Left Join ProductosXPzas A On P.Articulo=A.Producto
			----	Where		P.Articulo=@IdProd And P.Ruta=@ClaveR --And P.Fecha Between @Fecha And DateAdd(d,1,@Fecha)
			----else
			----	Select	@Cant=P.Stock/IsNull(A.PzaXCja,1)
			----	From		StockHistorico P Left Join ProductosXPzas A On P.Articulo=A.Producto
			----	Where		P.Articulo=@IdProd And P.RutaID=@ClaveR And P.Fecha = @Fecha --And DateAdd(d,1,@Fecha)

			SELECT @Cant= (ISNULL(ProductoPaseado.Stock,0) + ISNULL(PedDiaSig.Cantidad,0) + ISNULL(PedDiaSigPzs.Cantidad,0))/ISNULL(ProductosXPzas.PzaXCja,1) 
			FROM ProductoPaseado INNER JOIN
			ProductosXPzas ON ProductoPaseado.CodProd = ProductosXPzas.Producto AND ProductoPaseado.IdEmpresa = ProductosXPzas.IdEmpresa INNER JOIN
			DiasO ON ProductoPaseado.RutaId = DiasO.RutaId AND ProductoPaseado.IdEmpresa = DiasO.IdEmpresa AND ProductoPaseado.DiaO = DiasO.DiaO LEFT OUTER JOIN
			PedDiaSigPzs ON ProductoPaseado.RutaId = PedDiaSigPzs.IdRuta AND ProductoPaseado.DiaO = PedDiaSigPzs.Diao AND ProductoPaseado.IdEmpresa = PedDiaSigPzs.IdEmpresa AND 
			ProductoPaseado.CodProd = PedDiaSigPzs.Articulo LEFT OUTER JOIN
			PedDiaSig ON ProductoPaseado.RutaId = PedDiaSig.IdRuta AND ProductoPaseado.DiaO = PedDiaSig.Diao AND ProductoPaseado.IdEmpresa = PedDiaSig.IdEmpresa AND 
			ProductoPaseado.CodProd = PedDiaSig.Articulo
			WHERE DiasO.Fecha =@Fecha AND ProductoPaseado.RutaId =@ClaveR AND ProductoPaseado.CodProd =@IdProd
			
			if @Cant Is NULL
				SET @Cant=0
			
			SET @Cmnd1=@Cmnd1+'['+@DescripR+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			print @Cmnd2
			Fetch Next From D Into @ClaveR,@DescripR
		end
		if @Nivel=1
			set @Clasif1=@Clasificacion
		else
			set @Clasif1=''
		if @Nivel=2
			set @Clasif2=@Clasificacion
		else
			set @Clasif2=''
		if @Nivel=3
			set @Clasif3=@Clasificacion
		else
			set @Clasif3=''
		if @Nivel=4
			set @Clasif4=@Clasificacion
		else
			set @Clasif4=''
		if @Nivel=5
			set @Clasif5=@Clasificacion
		else
			set @Clasif5=''
			PRINT @idProdNew
			PRINT @idProdNewTEMP
		if @idProdNew<>@idProdNewTemp 
			begin
				SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('+IsNull('''' + @IdEmp + '''','NULL')+','''+@Clasif1+''','''+@Clasif2+''','''+@Clasif3+''','''+@Clasif4+''','''+@Clasif5+''','''+@IdProd+''','''+@Producto+''','
				+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
				set @idProdNewTemp=@idProdNew
			end 
		else
		BEGIN
			if @Nivel=2
				SET @Cmnd='UPDATE T_RepConCarga SET Presentacion='''+@Clasif2 + ''' WHERE IdProd='''+@IdProd+''''
			if @Nivel=3
				SET @Cmnd='UPDATE T_RepConCarga SET Familia='''+@Clasif3 + ''' WHERE IdProd='''+@IdProd+''''
			if @Nivel=4
				SET @Cmnd='UPDATE T_RepConCarga SET Marca='''+@Clasif4+''' WHERE IdProd='''+@IdProd+''''
			if @Nivel=5
				SET @Cmnd='UPDATE T_RepConCarga SET Unidades='''+@Clasif5+''' WHERE IdProd='''+@IdProd+''''
		END 
		
		Close D
		Deallocate D
--		Select @Cmnd
		PRINT @Cmnd
		Execute (@Cmnd)
		Fetch Next From R Into @IdProd,@Producto,@Nivel,@Clasificacion,@NivelDep
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConCarga 	UNION
	Select '+IsNull('''' + @IdEmp + '''','NULL')+','''','''','''','''','''','''',''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConCarga order by producto'
	print @Cmnd
	print @Cmnd4
	print @Cmnd5
--Select @Cmnd
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteCargaConcentradoOldFormat
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteCargaConcentradoOldFormat]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteCargaConcentradoOldFormat](
@Fecha Date,
@IdEmpresa varchar(5))
AS
begin
	Declare @IdRuta int
	Declare @Ruta	 Varchar(50)
	Declare @Cmnd   Varchar(max)
	Declare @Cmnd1  Varchar(max)
	Declare @Cmnd2  Varchar(max)
	Declare @Cmnd3  Varchar(max)
	Declare @Cmnd4  Varchar(max)
	Declare @Cmnd5  Varchar(max)
	Declare @Clave  Varchar(50)
	Declare @Cant   int
	Declare @Hist	 int
	SET @Hist=0
	Declare C Cursor For
		Select Clave From Productos Where Ban_Envase=0 Order By Clave
	Open C
	Fetch Next From C Into @Clave
	if Exists (Select name From sysobjects Where name='T_RepConCarga')
		Drop Table T_RepConCarga
	Set @Cmnd='CREATE TABLE T_RepConCarga (Ruta varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Clave+'] int,'
		Set @Cmnd3=@Cmnd3+'['+@Clave+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@Clave+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@Clave+'])+'
		Fetch Next From C Into @Clave
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
	--Select @Cmnd
 	Execute (@Cmnd)
	Declare R Cursor For
		Select IdRutas,Ruta From Rutas where IdEmpresa=@IdEmpresa Order By Ruta
	Open R
	Fetch Next From R Into @IdRuta,@Ruta
	if exists(Select * From StockHistorico Where IdEmpresa=@IdEmpresa AND Fecha Between @Fecha And DATEADD(d,1,@Fecha))
		SET @Hist=1
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			Select Clave From Productos Where Ban_Envase=0 Order By Clave
		Open D
		SET @Cmnd='Insert Into T_RepConCarga(Ruta,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			SELECT	@Cant=(ISNULL(ProductoPaseado.Stock,0)+ISNULL(PedDiaSig.Cantidad,0)+ISNULL(PedDiaSigPzs.Cantidad,0))/ISNULL(ProductosXPzas.PzaXCja,1) 
			FROM	ProductoPaseado INNER JOIN ProductosXPzas ON ProductoPaseado.CodProd=ProductosXPzas.Producto AND ProductoPaseado.IdEmpresa=ProductosXPzas.IdEmpresa
					INNER JOIN DiasO ON ProductoPaseado.RutaId=DiasO.RutaId AND ProductoPaseado.IdEmpresa=DiasO.IdEmpresa AND ProductoPaseado.DiaO=DiasO.DiaO
					LEFT OUTER JOIN PedDiaSigPzs ON ProductoPaseado.RutaId=PedDiaSigPzs.IdRuta AND ProductoPaseado.DiaO=PedDiaSigPzs.Diao AND ProductoPaseado.IdEmpresa=PedDiaSigPzs.IdEmpresa
					AND ProductoPaseado.CodProd=PedDiaSigPzs.Articulo
					LEFT OUTER JOIN PedDiaSig ON ProductoPaseado.RutaId=PedDiaSig.IdRuta AND ProductoPaseado.DiaO=PedDiaSig.Diao AND ProductoPaseado.IdEmpresa=PedDiaSig.IdEmpresa
					AND ProductoPaseado.CodProd=PedDiaSig.Articulo
			WHERE	DiasO.Fecha=@Fecha AND ProductoPaseado.RutaId=@IdRuta AND ProductoPaseado.CodProd=@Clave AND ProductoPaseado.IdEmpresa=@IdEmpresa
			if @Cant Is NULL
				SET @Cant=0
			SET @Cmnd1=@Cmnd1+'['+@Clave+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			Fetch Next From D Into @Clave
		end
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''+@Ruta+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		Close D
		Deallocate D
		--Select @Cmnd
		Execute (@Cmnd)
		Fetch Next From R Into @IdRuta,@Ruta
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConCarga
	UNION
	Select ''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConCarga'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteComisiones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteComisiones]
GO


CREATE  PROCEDURE [dbo].[SPAD_ReporteComisiones](
@ID_Ruta			int,
@FechaInicial	datetime,
@FechaFinal		datetime,
@IdEmpresa		varchar(50))
AS
begin
	--variables internas
	declare @tmp table(ID_Ruta int,Ruta varchar(MAX),Articulo varchar(MAX),NombreArticulo varchar(MAX),Cantidad numeric,PrecioUnitario numeric,Importe numeric,
							IdVendedor int,Vendedor varchar(MAX),ID_Ayudante1 int,ID_Ayudante2 int,VenAyunate int,analizado int, Nregistro int identity,Notas varchar(MAX),
							ComisionVendedor float,ComisioAyudante1 float,ComisionAyudante2 float,NombreAyudante1 varchar(MAX),NombreAyudante2 varchar(MAX),ID_Producto int,
							foranea bit,Tipo int,IdEmpresa varchar(50),Fecha Datetime,PorcVend float,PorcAyud1 float,PorcAyud2 float,ClaveVend varchar(20),ClaveAyud1 varchar(20),ClaveAyud2 varchar(20))
	declare @Articulo			varchar(MAX)
	declare @Importe			numeric
	declare @Cantidad			numeric
	declare @IdVendedor			int
	declare @ID_Ayudante1		int
	declare @ID_Ayudante2		int
	declare @VenAyunate			int
	declare @Nregistro			int
	declare @ComisionAgente		float
	declare @ComisionAyudante	float
	declare @msg				varchar(MAX)
	declare @NombreAyudante1	varchar(MAX)
	declare @NombreAyudante2	varchar(MAX)
	declare @ComisionExtra		float
	declare @ID_Producto		varchar(50)
	declare @foranea			bit
	declare @Tipo				int
	declare @FechaT				datetime
	declare @PorcVend			float
	declare @PorcVend1			float
	declare @PorcAyud2			float
	declare @ClaveVend			varchar(20)
	declare @ClaveAyud1			varchar(20)
	declare @ClaveAyud2			varchar(20)
	--genero el concentrado

	insert @tmp(ID_Ruta,Ruta,Articulo,NombreArticulo,Cantidad,PrecioUnitario,Importe,IdVendedor,Vendedor,ID_Ayudante1,ID_Ayudante2,VenAyunate,analizado,
	Notas,foranea,Tipo,IdEmpresa,Fecha,PorcVend,PorcAyud1,PorcAyud2,ClaveVend,ClaveAyud1,ClaveAyud2)
	--select	r.IdRutas ClaveRuta,r.Ruta,d.Articulo,p.Producto,sum(d.Pza) as Cantidad,d.Precio PrecioUnitario,sum(d.Importe+d.IVA) as Importe,
	--h.VendedorId,v.Nombre, h.ID_Ayudante1,h.ID_Ayudante2,
	--h.VenAyunate,0,'',p.Id,r.Foranea,d.Tipo,r.IdEmpresa,max(h.Fecha) as Fecha,0,0,0,
	--ISNULL(v.Clave,'') AS ClaveVend,ISNULL(Ayudantes.Clave,'') AS ClaveAyud1, ISNULL(Ayudantes_1.Clave,'') AS ClaveAyud2
	--FROM Rutas AS r INNER JOIN
	--Venta AS h ON h.RutaId = r.IdRutas AND r.IdEmpresa = h.IdEmpresa INNER JOIN
	--DetalleVet AS d ON d.Docto = h.Documento AND d.RutaId = h.RutaId AND d.IdEmpresa = h.IdEmpresa INNER JOIN
	--Productos AS p ON p.Clave = d.Articulo AND p.IdEmpresa = d.IdEmpresa INNER JOIN
	--Vendedores AS v ON v.IdVendedor = h.VendedorId AND v.IdEmpresa = h.IdEmpresa LEFT OUTER JOIN
	--Ayudantes ON h.ID_Ayudante1 = Ayudantes.id_ayudante AND h.IdEmpresa = Ayudantes.IdEmpresa LEFT OUTER JOIN
	--Ayudantes AS Ayudantes_1 ON h.ID_Ayudante2 = Ayudantes_1.id_ayudante AND h.IdEmpresa = Ayudantes_1.IdEmpresa
	--where		r.IdRutas=@ID_Ruta and h.Fecha BETWEEN @FechaInicial and  dateadd(d,1,@FechaFinal) And r.IdEmpresa=@IdEmpresa
	--			and h.Cancelada<>1 AND (h.TipoVta IN ('Contado', 'Credito')) ---filtra las canceladas para no tomarlas en cuenta
	--group by r.IdEmpresa,r.IdRutas,r.Ruta,d.Articulo,p.Producto,d.Precio,h.VendedorId,v.Nombre,h.ID_Ayudante1,h.ID_Ayudante2,h.VenAyunate,p.Id,r.Foranea,d.Tipo,v.Clave, Ayudantes.Clave,  Ayudantes_1.Clave
	SELECT        r.IdRutas AS ClaveRuta, r.Ruta, d.Articulo, p.Producto, SUM(V_CantidadPzaxVta.Pzas) AS Cantidad, d.Precio AS PrecioUnitario, SUM(d.Importe + d.IVA) AS Importe, h.VendedorId, 
	v.Nombre, h.ID_Ayudante1, h.ID_Ayudante2, h.VenAyunate, 0 AS Expr1, '' AS Expr2,  r.Foranea,1,r.IdEmpresa, MAX(h.Fecha) AS Fecha, 0 AS Expr3, 0 AS Expr4, 0 AS Expr5, ISNULL(v.Clave, '') AS ClaveVend, 
	ISNULL(Ayudantes.Clave, '') AS ClaveAyud1, ISNULL(Ayudantes_1.Clave, '') AS ClaveAyud2
	FROM            Rutas AS r INNER JOIN
	Venta AS h ON h.RutaId = r.IdRutas AND r.IdEmpresa = h.IdEmpresa INNER JOIN
	DetalleVet AS d ON d.Docto = h.Documento AND d.RutaId = h.RutaId AND d.IdEmpresa = h.IdEmpresa INNER JOIN
	Productos AS p ON p.Clave = d.Articulo  INNER JOIN ---AND p.IdEmpresa = d.IdEmpresa
	Vendedores AS v ON v.IdVendedor = h.VendedorId AND v.IdEmpresa = h.IdEmpresa INNER JOIN
	V_CantidadPzaxVta ON d.Docto = V_CantidadPzaxVta.Docto AND d.Articulo = V_CantidadPzaxVta.Articulo AND d.RutaId = V_CantidadPzaxVta.RutaId AND 
	d.IdEmpresa = V_CantidadPzaxVta.IdEmpresa LEFT OUTER JOIN
	Ayudantes ON h.ID_Ayudante1 = Ayudantes.id_ayudante AND h.IdEmpresa = Ayudantes.IdEmpresa LEFT OUTER JOIN
	Ayudantes AS Ayudantes_1 ON h.ID_Ayudante2 = Ayudantes_1.id_ayudante AND h.IdEmpresa = Ayudantes_1.IdEmpresa
	where		r.IdRutas=@ID_Ruta and h.Fecha BETWEEN @FechaInicial and @FechaFinal And r.IdEmpresa=@IdEmpresa
				and h.Cancelada<>1 AND (h.TipoVta IN ('Contado', 'Credito')) ---filtra las canceladas para no tomarlas en cuenta
	GROUP BY r.IdEmpresa, r.IdRutas, r.Ruta, d.Articulo, p.Producto, d.Precio, h.VendedorId, v.Nombre, h.ID_Ayudante1, h.ID_Ayudante2, h.VenAyunate, r.Foranea, v.Clave, Ayudantes.Clave, Ayudantes_1.Clave
	
	--recorro  cada registro para hacer el calculo de la comision	
	while(exists(select * from @tmp where analizado=0))
	begin
		--me traigo el primer registro
		select Top 1 @Articulo=t.Articulo,@Importe=t.Importe,@Cantidad=t.Cantidad,@IdVendedor=t.IdVendedor,@ID_Ayudante1=t.ID_Ayudante1,@ID_Ayudante2=t.ID_Ayudante2,
			   @VenAyunate=t.VenAyunate,@Nregistro=t.Nregistro,@ID_Producto=t.ID_Producto,@foranea=t.foranea,@Tipo=t.Tipo,@FechaT=t.Fecha, @ClaveVend=ClaveVend,
					@ClaveAyud1=ClaveAyud1,@ClaveAyud2=ClaveAyud2
		from		@tmp t
		where		t.analizado=0
		Order By Articulo,Tipo
		--lo marco como analizado
		update @tmp set analizado=1 where Nregistro=@Nregistro
		--me traigo la comision que le correcponde al articulo
		if not exists( select * from TH_Comision hc where hc.ID_Producto=@Articulo)
		begin
			--no existe comision para este titulo, por lo que asigno un mensaje al reporte
			print  'producto  validado'
			print  @Articulo
			update	@tmp
			set		Notas='No seha definido una comision para este producto',ComisionVendedor=0,ComisioAyudante1=0,ComisionAyudante2=0
			where		Nregistro=@Nregistro
		end
		else
		begin
			--si hay comision asignada, por lo que busco la que le corresponde por el volumen de venta
			--Calculo la cantidad en base al tipo de unidad de medida 0-cajas y 1-Piezas
			Declare @CantCom float
			Declare @CantAux float
			Declare @CantXCaja float
			set @ComisionAgente=0
			set @CantCom=0

			SET @CantAux=cast(@Cantidad as float)
			Select @CantXCaja=IsNull(PzaXCja,1) From ProductosXPzas Where Producto=@Articulo
			if @CantXCaja IS NULL SET @CantXCaja=1
			print 'Tipo'
			print @Tipo
			print 'pzaxcja'
			print @CantXCaja
			print @CantAux
			
			--Select	@CantCom=Case Tipo When 1 Then CASE @Tipo When 0 THEN @CantAux WHEN 1 THEN @CantAux/@CantXCaja END
			--						When 0 Then CASE @Tipo When 0 THEN @CantAux*@CantXCaja WHEN 1 THEN @CantAux END
			--			End
			--From		TD_Comision D Inner Join TH_Comision H On D.ID_Comision=H.ID_Comision And D.IdEmpresa=H.IdEmpresa
			--Where		H.ID_Producto=@Articulo And H.IdEmpresa=@IdEmpresa

			Select	@CantCom=Case Tipo When 1 Then CASE @Tipo When 0 THEN CAST(@CantAux*@CantXCaja AS INT) WHEN 1 THEN @CantAux END
							 When 0 Then CASE @Tipo When 0 THEN @CantAux WHEN 1 THEN @CantAux/@CantXCaja END
			End
			From		TD_Comision D Inner Join TH_Comision H On D.ID_Comision=H.ID_Comision And D.IdEmpresa=H.IdEmpresa
			Where		H.ID_Producto=@Articulo And H.IdEmpresa=@IdEmpresa


			if not exists(select	*
							from		TH_Comision hc Join TD_Comision dc On dc.ID_Comision=hc.ID_Comision And hc.IdEmpresa=dc.IdEmpresa
							where		hc.ID_Producto=@Articulo and @CantCom BETWEEN cast(dc.RangoIni as float) and cast(dc.RangoFin as float)
										And hc.IdEmpresa=@IdEmpresa)
			begin
				--como no tiene un rango definido, me traigo el maximo sin pasarse
				select	@ComisionAgente=dc.Angente,@ComisionAyudante=dc.Ayudante
				from		TH_Comision hc Inner Join TD_Comision dc On dc.ID_Comision=hc.ID_Comision And hc.IdEmpresa=dc.IdEmpresa
				where		hc.ID_Producto=@Articulo and cast(dc.RangoFin as float)<=@CantCom And hc.IdEmpresa=@IdEmpresa
				order by dc.RangoFin desc
				print 'Cantidad com'
				print @CantCom
				print 'ciclo uno'
				print @ComisionAgente
			end
			else
			begin
				select	@ComisionAgente=dc.Angente,@ComisionAyudante=dc.Ayudante
				from		TH_Comision hc Inner Join TD_Comision dc On dc.ID_Comision=hc.ID_Comision And hc.IdEmpresa=dc.IdEmpresa
				where		hc.ID_Producto=@Articulo and @CantCom BETWEEN cast(dc.RangoIni as float) and cast(dc.RangoFin as float) And hc.IdEmpresa=@IdEmpresa
				print @CantCom
				print 'ciclo dos'
			end
			-- ahora hago el calculo de la comision dependiendo de estos casos
			--1. Cuando el agente se va solo.
			--2. Cuando el agente va con sus dos ayudantes
			--3. Cuando el agente va con un solo ayudante y no hace labor de ayudante
			--4. Cuando el vendedor se va con un ayudante y hace el trabajo del otro ayudante
			--analizando primer caso

			if((@ID_Ayudante1=0 or @ID_Ayudante1 is null) and ( @ID_Ayudante2=0 or  @ID_Ayudante2 is null))
			begin
				set @NombreAyudante1=''
				set @NombreAyudante2=''
				set @ClaveAyud1=''
				set @ClaveAyud2=''
				--en este caso, el agente se lleva el 100% de la comision
				print 'Comision agente'
				print @ComisionAgente
				print 'Multiplicado por  la  cantidad de  comision'
				print @CantCom
				print  'Producto'
				print @Articulo
				update	@tmp
				SET		ComisionVendedor=case @foranea when 0 then (@ComisionAgente+@ComisionAyudante)*@CantCom else @ComisionAgente*@CantCom end,
							ComisioAyudante1 =0,ComisionAyudante2 =0,NombreAyudante1='',NombreAyudante2='', ClaveAyud1=@ClaveAyud1,ClaveAyud2=@ClaveAyud2,
							PorcVend=case @foranea when 0 then (@ComisionAgente+@ComisionAyudante) else @ComisionAgente end,PorcAyud1=0,PorcAyud2=0
				where		Nregistro=@Nregistro
			end
			--segundo caso. Cuando el agente va con sus dos ayudantes
			if((@ID_Ayudante1 is not null or @ID_Ayudante1>0) and (@ID_Ayudante2 is not null or @ID_Ayudante2>0))
			begin
				set @NombreAyudante1=''
				set @NombreAyudante2=''
				set @ClaveAyud1=''
				set @ClaveAyud2=''
				--en este caso, el agente se lleva su coision y la comision del ayudante se divide a la mitad
				--me traigo los nombre de ambos ayudantes
				print 'vendedor' 
				print @IdVendedor
				print 'Ayudante1' 
				print  @ID_Ayudante1
				print 'Ayudante2'
				print  @ID_Ayudante2
				IF (@IdVendedor=@ID_Ayudante1)
				BEGIN 
					select @NombreAyudante1=V.Nombre, @ClaveAyud1=V.Clave from Vendedores V where V.IdVendedor=@ID_Ayudante1 And V.IdEmpresa=@IdEmpresa
				END
				ELSE
				BEGIN
					select @NombreAyudante1=a.Nombre, @ClaveAyud1=a.Clave from Ayudantes a where a.id_ayudante=@ID_Ayudante1 And a.IdEmpresa=@IdEmpresa
				END 
				IF (@IdVendedor=@ID_Ayudante2)
				BEGIN 
					select @NombreAyudante2=V.Nombre, @ClaveAyud2=V.Clave from Vendedores V where V.IdVendedor=@ID_Ayudante2 And V.IdEmpresa=@IdEmpresa
				END
				ELSE
				BEGIN
					select @NombreAyudante2=a.Nombre, @ClaveAyud2=a.Clave from Ayudantes a where a.id_ayudante=@ID_Ayudante2 And a.IdEmpresa=@IdEmpresa
				END 

				-- actualizo los datos.
				update	@tmp
				set		ComisionVendedor=@ComisionAgente*@CantCom,
						   ComisioAyudante1=case @foranea when 0 then (@ComisionAyudante/2)*@CantCom else @ComisionAyudante*@CantCom  end,
							ComisionAyudante2=case @foranea when 0 then (@ComisionAyudante/2)*@CantCom else @ComisionAyudante*@CantCom end,
							NombreAyudante1=@NombreAyudante1,NombreAyudante2=@NombreAyudante2,
							PorcVend=@ComisionAgente, ClaveAyud1=@ClaveAyud1,ClaveAyud2=@ClaveAyud2,
							PorcAyud1=case @foranea when 0 then (@ComisionAyudante/2) else @ComisionAyudante end,
							PorcAyud2=case @foranea when 0 then (@ComisionAyudante/2) else @ComisionAyudante end
				where		Nregistro=@Nregistro
			end
			--casos en que se va con un solo ayudante
			if((@ID_Ayudante1 is not null  or @ID_Ayudante1>0 ) and (@ID_Ayudante2 is null or @ID_Ayudante2=0))
			begin
				set @NombreAyudante1=''
				set @NombreAyudante2=''
				set @ClaveAyud1=''
				set @ClaveAyud2=''
				--me traigo el nombre del yudante
				IF (@IdVendedor=@ID_Ayudante1)
				BEGIN 
					select @NombreAyudante1=V.Nombre, @ClaveAyud1=V.Clave from Vendedores V where V.IdVendedor=@ID_Ayudante1 And V.IdEmpresa=@IdEmpresa
				END
				ELSE
				BEGIN
					select @NombreAyudante1=a.Nombre, @ClaveAyud1=a.Clave from Ayudantes a where a.id_ayudante=@ID_Ayudante1 And a.IdEmpresa=@IdEmpresa
				END 
				--tercer caso.Cuando el agente va solo con un ayudante y no hace nada
				if(@VenAyunate=0 or @VenAyunate is null)
				begin
					--en este caso, el ayudante se lleva toda la comision de ambos ayudante
					if (@NombreAyudante1='')
					begin
						update	@tmp
						set		ComisionVendedor=@ComisionAgente*@CantCom,
						ComisioAyudante1=0,ComisionAyudante2=0,
						NombreAyudante2='',
						NombreAyudante1=@NombreAyudante1, ClaveAyud1=@ClaveAyud1,ClaveAyud2='',
						PorcVend=@ComisionAgente,PorcAyud1=0,PorcAyud2=0
						where		Nregistro=@Nregistro
					end
					else
					begin
						update	@tmp
						set		ComisionVendedor=@ComisionAgente*@CantCom,
						ComisioAyudante1=@ComisionAyudante*@CantCom,ComisionAyudante2=0,
						NombreAyudante2='',
						NombreAyudante1=@NombreAyudante1,ClaveAyud1=@ClaveAyud1,ClaveAyud2='',
						PorcVend=@ComisionAgente,PorcAyud1=@ComisionAyudante,PorcAyud2=0
						where		Nregistro=@Nregistro
					end
				end
				else
				begin
					--ultimo caso en el que el agente hace labor de ayudante
					--me raigo la comision estra del agente
					select @ComisionExtra=cg.Valor from ConfiguracionGral cg where cg.ID_Config='COMAG'
					update	@tmp
					set		ComisionVendedor=case @foranea when 0 then (@ComisionAgente+(@ComisionAyudante*@ComisionExtra/100))*@CantCom else @ComisionAgente*@CantCom end,
								ComisioAyudante1=case @foranea when 0 then (@ComisionAyudante+(@ComisionAyudante*(100-@ComisionExtra)/100))*@CantCom else @ComisionAyudante*@CantCom end,
								ComisionAyudante2=0,
								PorcVend=case @foranea when 0 then (@ComisionAgente+(@ComisionAyudante*@ComisionExtra/100)) else @ComisionAgente end,
								PorcAyud1=case @foranea when 0 then (@ComisionAyudante+(@ComisionAyudante*(100-@ComisionExtra)/100)) else @ComisionAyudante end,
								PorcAyud2=0
					where		Nregistro=@Nregistro
				end
			end
		end
	end
	---NombreAyudante2='',NombreAyudante1=@NombreAyudante1,
	--regreso los datos  CASE  WHEN t.Tipo=1 THEN 'Piezas' ELSE 'Cajas' END AS Tipo,
	select	t.Fecha,t.Articulo,t.NombreArticulo,IsNull(t.Cantidad,0) Cantidad,IsNull(t.PrecioUnitario,0) PrecioUnitario,
			IsNull(t.Importe,0) Importe,IdVendedor,t.ClaveVend,t.Vendedor, IsNull(t.PorcVend,0) PorcVend , cast(IsNull(t.ComisionVendedor,0) as Decimal (18,2)) ComisionVendedor,
			ID_Ayudante1,ClaveAyud1,t.NombreAyudante1, IsNull(t.PorcAyud1,0) PorcAyud1 , cast(IsNull(t.ComisioAyudante1,0) as Decimal (18,2)) ComisionAyudante1,
				ID_Ayudante2, ClaveAyud2, t.NombreAyudante2,IsNull(t.PorcAyud2,0) PorcAyud2,cast(IsNull(t.ComisionAyudante2,0) as Decimal (18,2)) ComisionAyudante2,t.Notas
	from		@tmp t

end







GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteConcentradoDS
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteConcentradoDS]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteConcentradoDS](
@Fecha Date,
@IdEmpresa varchar(5))
AS
begin
	Declare @IdRuta int
	Declare @Ruta	 Varchar(50)
	Declare @Cmnd   Varchar(max)
	Declare @Cmnd1  Varchar(max)
	Declare @Cmnd2  Varchar(max)
	Declare @Cmnd3  Varchar(max)
	Declare @Cmnd4  Varchar(max)
	Declare @Cmnd5  Varchar(max)
	Declare @Clave  Varchar(50)
	Declare @Cant   int
	Declare C Cursor For
		Select Clave From Productos Where Ban_Envase=0 Order By Clave
	Open C
	Fetch Next From C Into @Clave
	if Exists (Select name From sysobjects Where name='T_RepConPed')
		Drop Table T_RepConPed
	Set @Cmnd='CREATE TABLE T_RepConPed (IdEmpresa varchar(50),Ruta varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Clave+'] int,'
		Set @Cmnd3=@Cmnd3+'['+@Clave+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@Clave+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@Clave+'])+'
		Fetch Next From C Into @Clave
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
	--Select @Cmnd
	Execute (@Cmnd)
	Declare R Cursor For
		Select IdRutas,Ruta From Rutas where IdEmpresa=@IdEmpresa Order By Ruta
	Open R
	Fetch Next From R Into @IdRuta,@Ruta
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			Select Clave From Productos Where Ban_Envase=0 Order By Clave
		Open D
		SET @Cmnd='Insert Into T_RepConPed(IdEmpresa,Ruta,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			Select	@Cant=P.Cantidad/IsNull(A.PzaXCja,1)
			From PedDiaSig P Left Join ProductosXPzas A On P.Articulo=A.Producto
			Where P.Articulo=@Clave And P.IdRuta=@IdRuta And P.Fecha = @Fecha  and  P.IdEmpresa=@IdEmpresa --And DateAdd(d,1,@Fecha)
			if @Cant Is NULL
				SET @Cant=0
			SET @Cmnd1=@Cmnd1+'['+@Clave+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			Fetch Next From D Into @Clave
		end
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('+IsNull('''' + @IdEmpresa + '''','NULL')+','''+@Ruta+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		Close D
		Deallocate D
		--Select @Cmnd
		Execute (@Cmnd)
		Fetch Next From R Into @IdRuta,@Ruta
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConPed
	UNION
	Select '+IsNull('''' + @IdEmpresa + '''','NULL')+',''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConPed'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteConcentradoDSP
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteConcentradoDSP]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteConcentradoDSP](
@Fecha Date,
@IdEmpresa varchar(5))
AS
begin
	Declare @IdRuta int
	Declare @Ruta	 Varchar(50)
	Declare @Cmnd   Varchar(max)
	Declare @Cmnd1  Varchar(max)
	Declare @Cmnd2  Varchar(max)
	Declare @Cmnd3  Varchar(max)
	Declare @Cmnd4  Varchar(max)
	Declare @Cmnd5  Varchar(max)
	Declare @Clave  Varchar(50)
	Declare @Cant   int
	Declare C Cursor For
		Select Clave From Productos Where Ban_Envase=0 Order By Clave
	Open C
	Fetch Next From C Into @Clave
	if Exists (Select name From sysobjects Where name='T_RepConPedPz')
		Drop Table T_RepConPedPz
	Set @Cmnd='CREATE TABLE T_RepConPedPz (IdEmpresa varchar(50),Ruta varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd3=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Clave+'] int,'
		Set @Cmnd3=@Cmnd3+'['+@Clave+']+'
		Set @Cmnd4=@Cmnd4+'SUM(['+@Clave+']),'
		Set @Cmnd5=@Cmnd5+'SUM(['+@Clave+'])+'
		Fetch Next From C Into @Clave
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')
'
	--Select @Cmnd
	Execute (@Cmnd)
	Declare R Cursor For
		Select IdRutas,Ruta From Rutas WHERE IdEmpresa=@IdEmpresa Order By Ruta
	Open R
	Fetch Next From R Into @IdRuta,@Ruta
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			Select Clave From Productos Where Ban_Envase=0 Order By Clave
		Open D
		SET @Cmnd='Insert Into T_RepConPedPz(IdEmpresa,Ruta,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			Select @Cant=P.Cantidad
			From PedDiaSigPzs P
			Where P.Articulo=@Clave And P.IdRuta=@IdRuta And P.Fecha = @Fecha and P.IdEmpresa=@IdEmpresa --And DateAdd(d,1,@Fecha)
			if @Cant Is NULL
				SET @Cant=0
			SET @Cmnd1=@Cmnd1+'['+@Clave+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			Fetch Next From D Into @Clave
		end
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('+IsNull('''' + @IdEmpresa + '''','NULL')+','''+@Ruta+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		Close D
		Deallocate D
		--Select @Cmnd
		Execute (@Cmnd)
		Fetch Next From R Into @IdRuta,@Ruta
	End
	Close R
	Deallocate R
	Set @Cmnd='Select *,'+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepConPedPz
	UNION
	Select '+IsNull('''' + @IdEmpresa + '''','NULL')+',''Total'','+@Cmnd4+@Cmnd5+'0 From T_RepConPedPz'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteEncuestas
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteEncuestas]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteEncuestas](
@IdRuta int,
@FechaIni Date,
@FechaFin Date,
@IdEmpresa varchar(5))
AS
begin
	Declare @Ruta			Varchar(50)
	Declare @Vendedor		varchar(50)
	Declare @Fecha			varchar(50)
	Declare @Cod_cliente	Varchar(50)
	Declare @Nombre			Varchar(50)
	Declare @NombreCorto	Varchar(50)
	Declare @Cmnd			Varchar(max)
	Declare @Cmnd1			Varchar(max)
	Declare @Cmnd2			Varchar(max)
	Declare @Cmnd3			Varchar(max)
	Declare @Cmnd4			Varchar(max)
	Declare @Cmnd5			Varchar(max)
	Declare @CmndAux		Varchar(max)
	Declare @Cmnd1Aux		Varchar(max)
	Declare @Pregunta		Varchar(50)
	Declare @Hist			int
	Declare @Resp			Varchar(50)
	SET @Hist=0
	Declare C Cursor For
		SELECT Clave_Enc + ' - ' + Des_Preg as Pregunta
		FROM Preg_Enc 
		WHERE IdEmpresa=@IdEmpresa
		ORDER BY Clave_Enc,Num_Preg asc
	Open C
	Fetch Next From C Into @Pregunta
	if Exists (Select name From sysobjects Where name='T_RepEncuestas')
		Drop Table T_RepEncuestas
	--if @Clave <> ''
	--begin 
		Set @Cmnd='CREATE TABLE T_RepEncuestas(Ruta varchar(50),Vendedor varchar(50),Fecha varchar(50),IdCliente varchar(50),Cliente varchar(50),NombreCorto varchar(50),'
		Set @Cmnd1=''
		Set @Cmnd2=''
		Set @Cmnd3=''
		Set @Cmnd4=''
		Set @Cmnd5=''
		While @@FETCH_STATUS=0
		Begin
			Set @Cmnd1=@Cmnd1+'['+@Pregunta+'] int,'
			Set @Cmnd3=@Cmnd3+'['+@Pregunta+']+'
			Set @Cmnd4=@Cmnd4+'SUM(['+@Pregunta+']),'
			Set @Cmnd5=@Cmnd5+'SUM(['+@Pregunta+'])+'
			Fetch Next From C Into @Pregunta
		End
		Close C
		Deallocate C
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
 		Execute (@Cmnd)
	--end 
	Declare R Cursor For
		SELECT Rutas.Ruta,Vendedores.Nombre AS NombreVendedor,Resp_Enc.Fecha,Resp_Enc.IdCliente,Clientes.Nombre,Clientes.Nombrecorto 
		FROM BitacoraTiempos INNER JOIN
		Vendedores ON BitacoraTiempos.IdVendedor = Vendedores.IdVendedor AND BitacoraTiempos.IdEmpresa = Vendedores.IdEmpresa INNER JOIN
		Resp_Enc INNER JOIN
		Preg_Enc ON Resp_Enc.Clave_Enc = Preg_Enc.Clave_Enc AND Resp_Enc.Num_Pregunta = Preg_Enc.Num_Preg AND Resp_Enc.IdEmpresa = Preg_Enc.IdEmpresa INNER JOIN
		Rutas ON Resp_Enc.IdRuta = Rutas.IdRutas INNER JOIN
		Clientes ON Resp_Enc.IdCliente = Clientes.IdCli AND Resp_Enc.IdEmpresa = Clientes.IdEmpresa ON BitacoraTiempos.DiaO = Resp_Enc.DiaO AND BitacoraTiempos.RutaId = Resp_Enc.IdRuta AND 
		BitacoraTiempos.Codigo = Resp_Enc.IdCliente
		WHERE  Resp_Enc.Fecha between @FechaIni  and @FechaFin and Rutas.IdRutas=@IdRuta and Resp_Enc.IdEmpresa=@IdEmpresa
		GROUP BY Rutas.Ruta,Vendedores.Nombre,Resp_Enc.Fecha,Resp_Enc.IdCliente,Clientes.Nombre,Clientes.Nombrecorto	
		Open R
	Fetch Next From R Into @Ruta,@Vendedor,@Fecha,@Cod_cliente,@Nombre,@NombreCorto
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			SELECT Clave_Enc + ' - ' + Des_Preg as Pregunta
			FROM Preg_Enc 
			WHERE IdEmpresa=@IdEmpresa
			ORDER BY Clave_Enc,Num_Preg asc
		Open D
		SET @Cmnd='Insert Into T_RepEncuestas(Ruta,Vendedor,Fecha,IdCliente,Cliente,NombreCorto,'
		SET @Cmnd1=''
		SET @Cmnd2=''
		Fetch Next From D Into @Pregunta
		While @@FETCH_STATUS=0
		Begin
			SET @Resp=''
			SELECT @Resp=Resp_Enc.Des_Resp
			FROM BitacoraTiempos INNER JOIN
			Vendedores ON BitacoraTiempos.IdVendedor = Vendedores.IdVendedor AND BitacoraTiempos.IdEmpresa = Vendedores.IdEmpresa INNER JOIN
			Resp_Enc INNER JOIN
			Preg_Enc ON Resp_Enc.Clave_Enc = Preg_Enc.Clave_Enc AND Resp_Enc.Num_Pregunta = Preg_Enc.Num_Preg AND Resp_Enc.IdEmpresa = Preg_Enc.IdEmpresa INNER JOIN
			Rutas ON Resp_Enc.IdRuta = Rutas.IdRutas INNER JOIN
			Clientes ON Resp_Enc.IdCliente = Clientes.IdCli AND Resp_Enc.IdEmpresa = Clientes.IdEmpresa ON BitacoraTiempos.DiaO = Resp_Enc.DiaO AND BitacoraTiempos.RutaId = Resp_Enc.IdRuta AND 
			BitacoraTiempos.Codigo = Resp_Enc.IdCliente
			where Resp_Enc.IdEmpresa=@IdEmpresa and Rutas.IdRutas=@IdRuta and Resp_Enc.Fecha between @FechaIni and @FechaFin and Resp_Enc.IdCliente=@Cod_Cliente and Resp_Enc.Clave_Enc + ' - ' + Preg_Enc.Des_Preg=@Pregunta

			SET @Cmnd1=@Cmnd1+'['+@Pregunta+'],'
			SET @Cmnd1Aux=@Cmnd1Aux+'['+@Pregunta+'],'
			SET @Cmnd2=@Cmnd2+cast(@Resp as varchar(50))+','
			Fetch Next From D Into @Pregunta
		end
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''+@Ruta+''','''+@Vendedor+''','''+CAST(DAY(@Fecha) AS VARCHAR(2))+'/'+CAST(MONTH(@Fecha) AS VARCHAR(2))+'/'+CAST(YEAR(@Fecha) AS VARCHAR(4))+''','''+@Cod_cliente+''','''+@Nombre+''','''+@NombreCorto+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		print @Cmnd
		Close D
		Deallocate D
		Execute (@Cmnd)
		Fetch Next From R Into @Ruta,@Vendedor,@Fecha,@Cod_cliente,@Nombre,@NombreCorto
	End
	Close R
	Deallocate R

	--SET @CmndAux=@CmndAux+SUBSTRING(@Cmnd1Aux,1,Len(@Cmnd1Aux)-1)+','''') Select ''Total'','''','''','''','''','''','+@Cmnd4+''''' From T_RepProdPedidos'
	--print @CmndAux
	--Execute (@CmndAux)


	--SET @CmndAux=@CmndAux+SUBSTRING(@Cmnd1Aux,1,Len(@Cmnd1Aux)-1)+') Select '''',''Total'','''','''','''','''','''','''','''','''','''','''','''','+SUBSTRING(@Cmnd4,1,Len(@Cmnd4)-1)+' From T_RepProdPedidos'
	--Execute (@CmndAux)
	--set @Cmnd= 'Update T_RepProdPedidos set Cajas=((case when ('+Substring(@Cmnd3,1,len(@Cmnd3)-1)+')=0 then 1 else ('+Substring(@Cmnd3,1,len(@Cmnd3)-1)+') end)/pzaxcja), piezas=(('+ Substring(@Cmnd3,1,len(@Cmnd3)-1)+')%pzaxcja)  where fecha!=''Total'''
	--print @Cmnd
	--Execute (@Cmnd)
	--set @Cmnd= 'update T_RepProdPedidos set Cajas=(select sum(cajas) from T_RepProdPedidos where fecha!=''Total''), Piezas=(select sum(Piezas) from T_RepProdPedidos where fecha!=''Total'') where fecha=''Total'''
	--print @Cmnd
	--Execute (@Cmnd)
	Set @Cmnd='Select * FROM T_RepEncuestas'
	print @Cmnd
	Execute (@Cmnd)

end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteFidelidad
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteFidelidad]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteFidelidad](
@FechaIni Date,
@FechaFin Date,
@CveRuta  as  int,
@CveCte	  as  varchar(50))
AS
begin
	declare @Fechalarga		date
	Declare @IdRuta			int
	Declare @Ruta			Varchar(50)
	Declare @Cmnd			Varchar(max)
	Declare @CmndAux1		Varchar(max)
	Declare @CmndAux2		Varchar(max)
	Declare @Cmnd1			Varchar(max)
	Declare @Cmnd2			Varchar(max)
	Declare @Cmnd2Aux1		Varchar(max)
	Declare @Cmnd2Aux2		Varchar(max)
	Declare @Cmnd4			Varchar(max)
	Declare @Cmnd5			Varchar(max)
	Declare @Fecha			varchar(20)
	Declare @Hist			int
	declare @IdCli			varchar(50)
	declare @Nombre			varchar(50) 
	declare @Vendedor		varchar(50)
	declare @NombreVend		varchar(50)
	declare @Status			varchar(20)	
	declare @Cont			int 
	declare @prog			char(1)
	declare @visit			char(1)
	declare @venta			char(1)
	SET @Hist=0
	DECLARE @TablaTemporalFechas AS TABLE (Fecha Varchar(20),FechaLarga Date)
	SET DATEFORMAT YMD
	set language 'spanish'
	set @FechaIni=@FechaIni

	WHILE (@FechaIni <= @FechaFin)
		BEGIN
			INSERT INTO @TablaTemporalFechas (Fecha,FechaLarga) VALUES (CAST(DATENAME(WEEKDAY, @FechaIni) AS VARCHAR(3))+ ' ' + CAST(DAY(@FechaIni) AS VARCHAR(2)) + '/' + CAST(MONTH(@FechaIni) AS VARCHAR(2))  + '/' + CAST(YEAR(@FechaIni) AS VARCHAR(4)) ,@FechaIni)
			SET @FechaIni= DATEADD(DAY,1,@FechaIni)
		END

	Declare C Cursor For
			SELECT Fecha FROM @TablaTemporalFechas
	Open C
	Fetch Next From C Into @Fecha
	if Exists (Select name From sysobjects Where name='T_RepFidelidad')
		Drop Table T_RepFidelidad
	Set @Cmnd='CREATE TABLE T_RepFidelidad(Ruta varchar(50),IdCliente varchar(50),Cliente varchar(50),Vendedor varchar(50),Status varchar(50),'
	Set @Cmnd1=''
	Set @Cmnd2=''
	Set @Cmnd4=''
	Set @Cmnd5=''
	While @@FETCH_STATUS=0
	Begin
		Set @Cmnd1=@Cmnd1+'['+@Fecha+'] varchar(1),'
		Fetch Next From C Into @Fecha
	End
	Close C
	Deallocate C
	SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
 	Execute (@Cmnd)
	print @CveRuta
	print @CveCte
	if (@CveRuta>0)
		Declare R Cursor For
		SELECT Rutas.IdRutas, Rutas.Ruta, Clientes.IdCli, Clientes.Nombre, Rutas.Vendedor, Vendedores.Nombre AS NombreVend 
		FROM  Rutas INNER JOIN
		RelClirutas ON Rutas.IdRutas = RelClirutas.IdRuta INNER JOIN
		Clientes ON RelClirutas.IdCliente = Clientes.IdCli INNER JOIN
		Vendedores ON Rutas.Vendedor = Vendedores.IdVendedor
		WHERE Rutas.IdRutas=@CveRuta
		ORDER BY Rutas.IdRutas, Clientes.IdCli
	else
	if (@CveCte<>'')
		Declare R Cursor For
		SELECT Rutas.IdRutas, Rutas.Ruta, Clientes.IdCli, Clientes.Nombre, Rutas.Vendedor, Vendedores.Nombre AS NombreVend 
		FROM  Rutas INNER JOIN
		RelClirutas ON Rutas.IdRutas = RelClirutas.IdRuta INNER JOIN
		Clientes ON RelClirutas.IdCliente = Clientes.IdCli INNER JOIN
		Vendedores ON Rutas.Vendedor = Vendedores.IdVendedor
		WHERE Clientes.IdCli=@CveCte
		ORDER BY Rutas.IdRutas, Clientes.IdCli

	Open R
	Fetch Next From R Into @IdRuta,@Ruta, @IdCli, @Nombre, @Vendedor,@NombreVend 
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			Select Fecha,FechaLarga From @TablaTemporalFechas 
		Open D
			SELECT @Cmnd='Insert Into T_RepFidelidad(Ruta,IdCliente,Cliente,Vendedor,Status ,'
			SELECT @CmndAux1='Insert Into T_RepFidelidad(Ruta,IdCliente,Cliente,Vendedor,Status ,'
			SELECT @CmndAux2='Insert Into T_RepFidelidad(Ruta,IdCliente,Cliente,Vendedor,Status ,'
			SET @Cmnd1=''
			SET @Cmnd2=''
			SET @Cmnd2Aux1=''
			SET @Cmnd2Aux2=''
			Fetch Next From D Into @Fecha,@FechaLarga
			While @@FETCH_STATUS=0
			Begin
			SET @Status=''
				SET @Cmnd1=@Cmnd1+'['+@Fecha+'],'

				set @prog=dbo.EsVtaProgramada(@IdRuta,@Idcli,@FechaLarga,CAST(DATENAME(WEEKDAY, @FechaLarga) AS VARCHAR(3)))
				set @Visit=dbo.CteVisitado(@IdRuta,@Idcli,@FechaLarga)
				set @venta=dbo.CteConVenta(@IdRuta,@Idcli,@FechaLarga)

				if @prog=1  set @prog='X' 	else  set @prog=' '
				if @Visit=1  set @Visit='X' 	else  set @Visit=' '
				if @venta=1  set @venta='X' 	else  set @venta=' '

				SET @Cmnd2=@Cmnd2+''''+cast(@prog as varchar(7))+''','
				SET @Cmnd2Aux1=@Cmnd2Aux1+''''+cast(@Visit as varchar(7))+''','
				SET @Cmnd2Aux2=@Cmnd2Aux2+''''+cast(@venta as varchar(7))+''','

				Fetch Next From D Into @Fecha,@FechaLarga

			end
				SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''+@Ruta+''','''+@IdCli+''','''+@Nombre+''','''+@NombreVend+''',''PROGRAMADO'','
				+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'

				SET @CmndAux1=@CmndAux1+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''','''','''','''',''VISITADO'','
				+SUBSTRING(@Cmnd2Aux1,1,Len(@Cmnd2Aux1)-1)+')'

				SET @CmndAux2=@CmndAux2+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''','''','''','''',''VENTA'','
				+SUBSTRING(@Cmnd2Aux2,1,Len(@Cmnd2Aux2)-1)+')'
				print @Cmnd
			Close D
			Deallocate D
			Execute (@Cmnd)
			Execute (@CmndAux1)
			Execute (@CmndAux2)

		Fetch Next From R Into @IdRuta,@Ruta, @IdCli, @Nombre, @Vendedor,@NombreVend 
	End
	Close R
	Deallocate R

	Set @Cmnd='Select * From T_RepFidelidad'
	Execute (@Cmnd)
end

GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteLiquidaciones
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteLiquidaciones]
GO

---------------------  22 de Septiembre 2016 correccion en el reporte de liquidaciones (Mostraba  mal la cantidad de  Cajas  y piezas  de  promocion)

CREATE PROCEDURE [dbo].[SPAD_ReporteLiquidaciones](
@FechaIni		Date,
@FechaFin		Date,
@CveRuta		int,
@IdEmpresa		varchar(5),
@DiaORuta		int)
AS
begin
	Declare @Cmnd						Varchar(max)
	Declare @Cmnd1						Varchar(max)
	Declare @Cmnd2						Varchar(max)
	Declare @Cmnd3						Varchar(max)
	Declare @Cmnd4						Varchar(max)
	Declare @Cmnd5						Varchar(max)
	Declare @Cmnd6						Varchar(max)
	declare @RutaId						varchar(50)	
	declare @CodCliente					varchar(50)
	declare @Documento					varchar(50)
	declare @Fecha						datetime
	declare @Articulo					varchar(15)
	declare @Descripcion				varchar(100)
	declare @Caja						varchar(5)
	declare @Pza						varchar(5)
	declare @Tipo						varchar(20)
	declare @promocion					varchar(5)
	declare @RutaIdAux					varchar(50)
	---declare @DiaOAux					varchar(5)
	declare @ClienteAux					varchar(50)
	declare @DoctoAux					varchar(50)
	declare @SKUAux						varchar(15)
	declare @ProductoAux				varchar(100)
	declare @PzAux						varchar(5)
	declare @CajaAux					varchar(5)
	declare @TipmedAux					varchar(20)
	declare @CantAux					varchar(5)
	Declare @Hist						int
	declare @ArticuloAux				varchar(50)	
	declare @TipmedPromAux				as varchar(20)
	declare @TipoProm					as varchar(20)
	declare @DiaAux						as int 
	declare @FechaStock					as date
	Declare @T_TempLiquidaciones		As Table (RutaId varchar(50),CodCliente varchar(50),Documento varchar(50),Fecha Varchar(20), Articulo varchar(15),Descripcion varchar(100),Caja varchar(5),
												  Pza varchar(5),Tipo	Varchar(20),promocion varchar(5),TipoProm varchar(20),TipoObse Varchar(20),CantObse int)
	Declare @T_TempDevol As Table (Articulo varchar(15),Cajas varchar(5),Pza varchar(5))
	Declare @T_TempPedDiaSig As Table (Articulo varchar(15),Cajas varchar(5),Pza varchar(5))
	--Declare @T_TempLiquidacionesAux As Table (RutaId varchar(50),DiaO varchar(5),Fecha Varchar(20),Clave varchar(15),VentaCajas int,VentaPiezas int,PromoCajas int,PromoPiezas int,TipoObse Varchar(20),CantObse int)
	SET DATEFORMAT YMD
	set language 'spanish'
	SET @Hist=0
	if Exists (Select name From sysobjects Where name='T_TempLiquidaciones')
		Drop Table T_TempLiquidaciones
	--Set @Cmnd='CREATE TABLE T_TempLiquidaciones(RutaId varchar(50),DiaO varchar(5),CodCliente varchar(50),Documento varchar(50),Fecha Varchar(20), Articulo varchar(15),Descripcion varchar(100),Caja varchar(5),Pza varchar(5),Tipo	Varchar(20),promocion varchar(5),TipoProm varchar(20),TipoObse Varchar(20),CantObse int)'
	if Exists (Select name From sysobjects Where name='T_TempDevol')
		Drop Table T_TempDevol
	--Set @Cmnd4='CREATE TABLE T_TempDevol(Articulo varchar(15),Cajas varchar(5),Pza varchar(5))'
	if Exists (Select name From sysobjects Where name='T_TempPedDiaSig')
		Drop Table T_TempPedDiaSig
	--Set @Cmnd5='CREATE TABLE T_TempPedDiaSig(Articulo varchar(15),Cajas varchar(5),Pza varchar(5))'
	if Exists (Select name From sysobjects Where name='T_TempLiquidacionesAux')
		Drop Table T_TempLiquidacionesAux
	CREATE TABLE T_TempLiquidacionesAux(RutaId varchar(50),Fecha Varchar(20),Clave varchar(15),VentaCajas int,VentaPiezas int,PromoCajas int,PromoPiezas int,TipoObse Varchar(20),CantObse int)
	insert into  @T_TempLiquidaciones
	SELECT	Venta.RutaId,Venta.CodCliente,Venta.Documento,cast(Venta.Fecha as  date),DetalleVet.Articulo,DetalleVet.Descripcion,
				CASE WHEN DetalleVet.Tipo=0 then CASE WHEN Venta.TipoVta='Obsequio' THEN 0 ELSE CAST(DetalleVet.Pza AS int) END else 0 end AS Caja,
				CASE WHEN DetalleVet.Tipo=1 then CASE WHEN Venta.TipoVta='Obsequio' THEN 0 ELSE CAST(DetalleVet.Pza AS int) END else 0 end AS Pza,
				CASE WHEN Venta.TipoVta='Obsequio' THEN 'P' ELSE 'X' END AS Tipo,0 as promocion,'' AS TipoProm ,'',0
	FROM Venta INNER JOIN DetalleVet ON Venta.RutaId=DetalleVet.RutaId AND Venta.Documento=DetalleVet.Docto AND Venta.IdEmpresa=DetalleVet.IdEmpresa
	WHERE Venta.Cancelada=0 and  Venta.RutaId=@CveRuta and Venta.IdEmpresa=@IdEmpresa and Venta.DiaO=@DiaORuta ---and Venta.Fecha between @FechaIni and @FechaFin 
	Declare R Cursor For
	SELECT	RutaId, CodCliente, Documento, Fecha, Articulo, Descripcion, Caja, Pza, Tipo, promocion, TipoProm
	FROM		@T_TempLiquidaciones
	Open R
	Fetch Next From R Into @RutaId,@CodCliente,@Documento,@Fecha,@Articulo,@Descripcion,@Caja,@Pza,@Tipo,@promocion,@TipoProm
	While @@FETCH_STATUS=0
	Begin
		UPDATE	@T_TempLiquidaciones SET Tipo=CASE WHEN LEN(T.Documento)>1 THEN 'P' END
		FROM		PRegalado PR INNER JOIN Venta V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND PR.RutaId=V.RutaId
					INNER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.Docto=T.Documento AND PR.SKU=T.Articulo
		WHERE		T.Pza='0' AND T.Caja='0' and PR.IdEmpresa=@IdEmpresa
		UPDATE	@T_TempLiquidaciones SET Tipo=CASE WHEN LEN(T.Documento)>1 THEN 'P' END
		FROM		PRegalado PR INNER JOIN Venta V ON PR.Docto = V.Documento AND PR.DiaO=V.DiaO AND PR.RutaId=V.RutaId
					INNER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.Docto=T.Documento AND PR.SKU=T.Articulo
		WHERE		T.Tipo='X' AND PR.docto NOT in (select documento from ventas where TipoVta='Obsequio' and RutaId=@RutaId and PR.IdEmpresa=@IdEmpresa and DiaO=@DiaORuta)--and Fecha between @FechaIni and @FechaFin  
		Declare D Cursor For
			SELECT	PRegalado.RutaId,PRegalado.Cliente,PRegalado.Docto,PRegalado.SKU,Productos.Producto,'0' AS Caja,'0' AS Pz,'2' as Tipomed,PRegalado.Cant,PRegalado.Tipmed as TipoProm
			FROM		PRegalado INNER JOIN Productos ON PRegalado.SKU=Productos.Clave AND PRegalado.IdEmpresa=Productos.IdEmpresa
			where		PRegalado.RutaId=@RutaId and PRegalado.DiaO=@DiaORuta and PRegalado.Docto=@Documento 
			and PRegalado.docto NOT in (select documento from ventas where TipoVta='Obsequio' and RutaId=@RutaId and IdEmpresa=@IdEmpresa and DiaO=@DiaORuta) ---and Fecha between @FechaIni and @FechaFin
			and productos.Ban_Envase=0 and PRegalado.IdEmpresa=@IdEmpresa
		Open D
		Fetch Next From D Into @RutaIdAux,@ClienteAux,@DoctoAux,@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux
		While @@FETCH_STATUS=0
		Begin
			set @ArticuloAux=''
			select	@ArticuloAux=articulo FROM @T_TempLiquidaciones WHERE RutaId=@RutaIdAux AND documento=@DoctoAux AND Articulo=@SKUAux
			if(@ArticuloAux='')
			begin
				insert into @T_TempLiquidaciones(RutaId,CodCliente,Documento,Fecha,Articulo,Descripcion,Caja,Pza,Tipo,promocion,TipoProm,TipoObse,CantObse)
				Values(@RutaIdAux,@ClienteAux,@DoctoAux,CAST(YEAR(@Fecha) AS VARCHAR(4))+'/'+CAST(MONTH(@Fecha) AS VARCHAR(2))+'/'+CAST(DAY(@Fecha) AS VARCHAR(2)),
						@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux,'',0)
			end
			else
			begin
				Update @T_TempLiquidaciones set promocion=@CantAux,TipoProm=@TipmedPromAux where RutaId=@RutaIdAux AND documento=@DoctoAux AND Articulo=@SKUAux and Tipo='P'
			end
			Fetch Next From D Into  @RutaIdAux,@ClienteAux,@DoctoAux,@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux
		end
		Close D
		Deallocate D
		Fetch Next From R Into @RutaId,@CodCliente,@Documento,@Fecha,@Articulo,@Descripcion,@Caja,@Pza,@Tipo,@promocion,@TipoProm
	End
	Close R
	Deallocate R
	UPDATE	@T_TempLiquidaciones SET TipoObse=PR.TipMed
	FROM		PRegalado AS PR INNER JOIN Productos P ON PR.SKU=P.Clave
				INNER JOIN Venta AS V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND V.RutaId=PR.RutaId
				INNER JOIN Clientes C ON PR.Cliente=C.IdCli
				INNER JOIN ProductosXPzas PX ON PR.SKU=PX.Producto
				RIGHT OUTER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.DiaO=@DiaORuta AND PR.SKU=T.Articulo
	WHERE		V.Cancelada='0' AND V.TipoVta='Obsequio' AND PR.RutaId=@CveRuta and PR.IdEmpresa=@IdEmpresa and V.DiaO=@DiaORuta ---V.Fecha BETWEEN @FechaIni AND @FechaFin
	and P.Ban_Envase=0  
	UPDATE	@T_TempLiquidaciones SET CantObse=PR.Cant
	FROM		PRegalado PR INNER JOIN Productos P ON PR.SKU=P.Clave
				INNER JOIN Venta V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND V.RutaId=PR.RutaId
				INNER JOIN Clientes ON PR.Cliente=Clientes.IdCli
				INNER JOIN ProductosXPzas PX ON PR.SKU=PX.Producto
				RIGHT OUTER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.DiaO=@DiaORuta AND PR.SKU=T.Articulo AND PR.Docto=T.Documento
	WHERE		V.Cancelada='0' AND V.TipoVta='Obsequio' AND PR.RutaId=@CveRuta and PR.IdEmpresa=@IdEmpresa and v.DiaO=@DiaORuta  --- V.Fecha BETWEEN @FechaIni AND @FechaFin 
	and P.Ban_Envase=0  
	UPDATE	T SET TipoProm=T1.TipoProm
	FROM		@T_TempLiquidaciones T INNER JOIN @T_TempLiquidaciones AS T1 ON T.RutaId=T1.RutaId AND T1.TipoProm<>'' AND T.CodCliente=T1.CodCliente AND T.Articulo=T1.Articulo
	AND T.Documento=T1.Documento
	UPDATE	T SET tipo=T1.tipo
	FROM		@T_TempLiquidaciones T INNER JOIN @T_TempLiquidaciones T1 ON T.RutaId=T1.RutaId AND T1.Tipo<>'P' AND T.CodCliente=T1.CodCliente AND T.Articulo=T1.Articulo
	
	INSERT INTO T_TempLiquidacionesAux
	SELECT	RutaId, max(cast(Fecha as date)) as Fecha, Articulo as Clave,
				IsNull(SUM(cast(Caja as int)),0) AS [Venta Cajas],
				IsNull(SUM(cast(Pza as int)),0) AS [Venta Piezas],
				CASE WHEN ISNULL(TipoProm,'Caja')='Caja' THEN Case When IsNull(PzaXCja,1)=1 Then 0 Else ISNULL(SUM(cast(promocion as int)),0) End ELSE ISNULL(SUM(cast(promocion as  int)),0)/IsNull(PzaXCja,1) END [Promo Cajas],
				Case When IsNull(PzaXCja,1)=1 Then ISNULL(SUM(cast(promocion as  int)),0) Else CASE WHEN ISNULL(TipoProm,'Caja')='Pzas' THEN ISNULL(SUM(cast(promocion as  int)),0)%IsNull(PzaXCja,1) ELSE 0 END End [Promo Piezas],
				TipoObse,sum(CantObse) 
	FROM		@T_TempLiquidaciones T Left Join ProductosXPzas P On T.Articulo=P.Producto
	group  by RutaId,Articulo,Tipo,TipoProm,TipoObse,P.PzaXCja
	
	insert into @T_TempDevol
	SELECT	DetalleDevol.SKU,
				CASE WHEN isnull(DetalleDevol.Tipo,0)=0 then ISNULL(SUM(DetalleDevol.Pza),0) WHEN isnull(DetalleDevol.Tipo,0)=1 then ISNULL(SUM(DetalleDevol.Pza)/ProductosXPzas.PzaXCja,0) ELSE 0 end as Cajas,
				CASE WHEN isnull(DetalleDevol.Tipo,0)=1 then ISNULL(SUM(DetalleDevol.Pza)%ProductosXPzas.PzaXCja,0) ELSE 0 end as piezas
	FROM		DetalleDevol INNER JOIN  Devoluciones ON Devoluciones.Devol=DetalleDevol.Docto AND Devoluciones.Ruta=DetalleDevol.RutaId AND DetalleDevol.IdEmpresa=Devoluciones.IdEmpresa
				INNER JOIN ProductosXPzas ON DetalleDevol.SKU=ProductosXPzas.Producto AND DetalleDevol.IdEmpresa=ProductosXPzas.IdEmpresa
	WHERE		(DetalleDevol.Motivo <> 'Comodato') AND (Devoluciones.DiaO=@DiaORuta) AND DetalleDevol.RutaId=@CveRuta  and DetalleDevol.IdEmpresa=@IdEmpresa ---(Devoluciones.Fecha BETWEEN @FechaIni AND @FechaFin)
	GROUP BY DetalleDevol.SKU,DetalleDevol.Tipo,ProductosXPzas.PzaXCja,DetalleDevol.RutaId

	set	@DiaAux=(select MAX(DiaO) from DiasO where IdEmpresa=@IdEmpresa and rutaid=@CveRuta and diao<@DiaORuta)  --(select top 1 DiaO from Diaso where  idempresa=@IdEmpresa and rutaid=@CveRuta and Fecha BETWEEN @FechaIni AND @FechaFin)
	set	@FechaStock=(select MAX(Fecha) from stockhistorico where RutaId=@CveRuta and DiaO=@DiaORuta) --- fecha<=@FechaIni)

	INSERT INTO @T_TempPedDiaSig
	SELECT	ProductoPaseado.CodProd,(ISNULL(PedDiaSig.Cantidad,0))/ISNULL(ProductosXPzas.PzaXCja,1) as PedDiaSigCajas,ISNULL(PedDiaSigPzs.Cantidad,0) as PedDiaSigPiezas
	FROM		ProductoPaseado INNER JOIN ProductosXPzas ON ProductoPaseado.CodProd=ProductosXPzas.Producto ---AND ProductoPaseado.IdEmpresa=ProductosXPzas.IdEmpresa
				INNER JOIN DiasO ON ProductoPaseado.RutaId=DiasO.RutaId AND ProductoPaseado.IdEmpresa=DiasO.IdEmpresa AND ProductoPaseado.DiaO=DiasO.DiaO
				LEFT OUTER JOIN PedDiaSigPzs ON ProductoPaseado.RutaId=PedDiaSigPzs.IdRuta AND ProductoPaseado.DiaO=PedDiaSigPzs.Diao AND ProductoPaseado.IdEmpresa=PedDiaSigPzs.IdEmpresa
				AND ProductoPaseado.CodProd=PedDiaSigPzs.Articulo
				LEFT OUTER JOIN PedDiaSig ON ProductoPaseado.RutaId=PedDiaSig.IdRuta AND ProductoPaseado.DiaO=PedDiaSig.Diao AND ProductoPaseado.IdEmpresa=PedDiaSig.IdEmpresa
				AND ProductoPaseado.CodProd = PedDiaSig.Articulo
	WHERE		ProductoPaseado.RutaId=@CveRuta and ProductoPaseado.DiaO=@DiaORuta and ProductoPaseado.IdEmpresa=@IdEmpresa
	order by  ProductoPaseado.CodProd

	SELECT	P.Clave,P.Producto,
				ISNULL((SELECT CAST(ISNULL(SH.Stock,0)/ISNULL(PX.PzaXCja,1) AS int) Expr1
						FROM		StockHistorico AS SH LEFT OUTER JOIN ProductosXPzas AS PX ON SH.Articulo=PX.Producto
						WHERE		(SH.Articulo=S.Articulo) AND (SH.RutaID=S.RutaID) AND (SH.DiaO=S.DiaO)),0) [Stock Inicial Cajas],
				ISNULL((SELECT	CAST(ISNULL(SH.Stock,0)%ISNULL(PX.PzaXCja,1) AS int) Expr1
						FROM		StockHistorico AS SH LEFT OUTER JOIN ProductosXPzas AS PX ON SH.Articulo=PX.Producto
						WHERE		(SH.Articulo=S.Articulo) AND (SH.RutaID=S.RutaID) AND (SH.DiaO=S.DiaO)),0) [Stock Inicial Piezas],
				ISNULL(SUM(CP.VentaCajas),0) AS [Venta Cajas],ISNULL(SUM(CP.VentaPiezas),0) AS [Venta Piezas],ISNULL(SUM(CP.PromoCajas),0) [Promo Cajas],
				ISNULL(SUM(CP.PromoPiezas), 0) AS [Promo Piezas], ISNULL(Recarga.Cantidad / PP.PzaXCja, 0) Recarga,
				ISNULL(CASE WHEN CP.TipoObse='Caja' THEN SUM(CP.CantObse) ELSE CAST(CP.CantObse AS int)/ISNULL(ProductosXPzas.PzaXCja,1) END,0) [Obsequio Cajas], 
				ISNULL(CASE WHEN CP.TipoObse='Pzas' THEN CAST(CP.CantObse AS int)%ISNULL(ProductosXPzas.PzaXCja,1) END,0) [Obsequio Piezas],
				ISNULL(T.Cajas,0) [Devol Cajas], 
				ISNULL(T.Pza,0) [Devol Piezas],CAST(ProductoPaseado.Stock AS int)/ISNULL(ProductosXPzas.PzaXCja,1) [Stock final Cajas],
				CAST(ProductoPaseado.Stock AS int)%ISNULL(ProductosXPzas.PzaXCja,1) [Stock Final Piezas],
				ISNULL(TPD.Cajas,0) [Pedido Dia Sig Cajas],ISNULL(TPD.Pza,0) [Pedido Dia Sig Piezas]
	FROM		StockHistorico S RIGHT OUTER JOIN Recarga
				RIGHT OUTER JOIN @T_TempDevol T
				RIGHT OUTER JOIN @T_TempPedDiaSig TPD
				RIGHT OUTER JOIN Productos P
				INNER JOIN ProductoPaseado ON P.Clave=ProductoPaseado.CodProd AND ProductoPaseado.RutaId=@CveRuta AND ProductoPaseado.DiaO=@DiaORuta
				INNER JOIN ProductosXPzas ON ProductoPaseado.CodProd=ProductosXPzas.Producto
				ON TPD.Articulo=P.Clave
				ON T.Articulo=P.Clave
				ON Recarga.Articulo=ProductoPaseado.CodProd AND Recarga.IdRuta=ProductoPaseado.RutaId AND Recarga.Diao=ProductoPaseado.DiaO and  recarga.idEmpresa=ProductoPaseado.IdEmpresa
				ON S.Articulo=P.Clave AND S.RutaID=@CveRuta AND S.DiaO=@DiaAux  ---S.Fecha BETWEEN @FechaStock AND  DATEADD(D,1,@FechaStock)
				LEFT OUTER JOIN ProductosXPzas PP ON S.Articulo=PP.Producto
				LEFT OUTER JOIN T_TempLiquidacionesAux AS CP ON P.Clave=CP.Clave AND CP.RutaId=@CveRuta ---AND CP.Fecha BETWEEN @FechaIni AND @FechaFin
	WHERE        (P.Ban_Envase = 0)
	GROUP BY P.Clave,P.Producto,PP.PzaXCja,S.Articulo,S.Fecha,S.RutaID,S.Stock,Recarga.Cantidad,ProductoPaseado.Stock,ProductosXPzas.PzaXCja,CP.TipoObse,CP.CantObse,T.Cajas,T.Pza,TPD.Cajas,TPD.Pza, S.Diao 
	ORDER BY P.Clave,S.Fecha

end


GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteLiquidaciones_Envases
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteLiquidaciones_Envases]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteLiquidaciones_Envases](
@FechaIni Date,
@FechaFin Date,
@CveRuta  as  int,
@IdEmpresa varchar(5),
@DiaORuta		int)
AS
begin
	Declare @Cmnd						Varchar(max)
	Declare @Cmnd1						Varchar(max)
	Declare @Cmnd2						Varchar(max)
	Declare @Cmnd3						Varchar(max)
	Declare @Cmnd4						Varchar(max)
	Declare @Cmnd5						Varchar(max)
	Declare @Cmnd6						Varchar(max)
	declare @RutaId						varchar(50)	
	declare @CodCliente					varchar(50)
	declare @Documento					varchar(50)
	declare @Fecha						datetime
	declare @Articulo					varchar(15)
	declare @Descripcion				varchar(100)
	declare @Caja						varchar(5)
	declare @Pza						varchar(5)
	declare @Tipo						varchar(20)
	declare @promocion					varchar(5)
	declare @RutaIdAux					varchar(50)
	declare @DiaOAux					varchar(5)
	declare @ClienteAux					varchar(50)
	declare @DoctoAux					varchar(50)
	declare @SKUAux						varchar(15)
	declare @ProductoAux				varchar(100)
	declare @PzAux						varchar(5)
	declare @CajaAux					varchar(5)
	declare @TipmedAux					varchar(20)
	declare @CantAux					varchar(5)
	Declare @Hist						int
	declare @ArticuloAux				varchar(50)	
	declare @TipmedPromAux				as varchar(20)
	declare @TipoProm					as varchar(20)
	declare @DiaAux						as int 
	declare @FechaStock					as date
	Declare @T_TempLiquidaciones As Table (RutaId varchar(50),CodCliente varchar(50),Documento varchar(50),Fecha Varchar(20), Articulo varchar(15),Descripcion varchar(100),Caja varchar(5),
														Pza varchar(5),Tipo	Varchar(20),promocion varchar(5),TipoProm varchar(20),TipoObse Varchar(20),CantObse int)
	Declare @T_TempDevol As Table (Articulo varchar(15),Cajas varchar(5),Pza varchar(5))
	Declare @T_TempPedDiaSig As Table (Articulo varchar(15),Cajas varchar(5),Pza varchar(5))
	--Declare @T_TempLiquidacionesAux As Table (RutaId varchar(50),DiaO varchar(5),Fecha Varchar(20),Clave varchar(15),VentaCajas int,VentaPiezas int,PromoCajas int,PromoPiezas int,TipoObse Varchar(20),CantObse int)
	SET DATEFORMAT YMD
	set language 'spanish'
	SET @Hist=0
	if Exists (Select name From sysobjects Where name='T_TempLiquidaciones')
		Drop Table T_TempLiquidaciones
	--Set @Cmnd='CREATE TABLE T_TempLiquidaciones(RutaId varchar(50),DiaO varchar(5),CodCliente varchar(50),Documento varchar(50),Fecha Varchar(20), Articulo varchar(15),Descripcion varchar(100),Caja varchar(5),Pza varchar(5),Tipo	Varchar(20),promocion varchar(5),TipoProm varchar(20),TipoObse Varchar(20),CantObse int)'
	if Exists (Select name From sysobjects Where name='T_TempDevol')
		Drop Table T_TempDevol
	--Set @Cmnd4='CREATE TABLE T_TempDevol(Articulo varchar(15),Cajas varchar(5),Pza varchar(5))'
	if Exists (Select name From sysobjects Where name='T_TempPedDiaSig')
		Drop Table T_TempPedDiaSig
	--Set @Cmnd5='CREATE TABLE T_TempPedDiaSig(Articulo varchar(15),Cajas varchar(5),Pza varchar(5))'
	if Exists (Select name From sysobjects Where name='T_TempLiquidacionesAux')
		Drop Table T_TempLiquidacionesAux
	CREATE TABLE T_TempLiquidacionesAux(RutaId varchar(50),Fecha Varchar(20),Clave varchar(15),VentaCajas int,VentaPiezas int,PromoCajas int,PromoPiezas int,TipoObse Varchar(20),CantObse int)
	insert into  @T_TempLiquidaciones
	SELECT	Venta.RutaId,Venta.CodCliente,Venta.Documento,cast(Venta.Fecha as  date),DetalleVet.Articulo,DetalleVet.Descripcion,
				CASE WHEN DetalleVet.Tipo=0 then CASE WHEN Venta.TipoVta='Obsequio' THEN 0 ELSE CAST(DetalleVet.Pza AS int) END else 0 end AS Caja,
				CASE WHEN DetalleVet.Tipo=1 then CASE WHEN Venta.TipoVta='Obsequio' THEN 0 ELSE CAST(DetalleVet.Pza AS int) END else 0 end AS Pza,
				CASE WHEN Venta.TipoVta='Obsequio' THEN 'P' ELSE 'X' END AS Tipo,0 as promocion,'' AS TipoProm ,'',0
	FROM		Venta INNER JOIN DetalleVet ON Venta.RutaId=DetalleVet.RutaId AND Venta.Documento=DetalleVet.Docto AND Venta.IdEmpresa=DetalleVet.IdEmpresa
	WHERE		Venta.Cancelada=0 and  Venta.RutaId=@CveRuta and Venta.IdEmpresa=@IdEmpresa and Venta.DiaO=@DiaORuta  --and Venta.Fecha between @FechaIni and @FechaFin
	Declare R Cursor For
	SELECT	RutaId, CodCliente, Documento, Fecha, Articulo, Descripcion, Caja, Pza, Tipo, promocion, TipoProm
	FROM		@T_TempLiquidaciones
	Open R
	Fetch Next From R Into @RutaId,@CodCliente,@Documento,@Fecha,@Articulo,@Descripcion,@Caja,@Pza,@Tipo,@promocion,@TipoProm
	While @@FETCH_STATUS=0
	Begin
		UPDATE	@T_TempLiquidaciones SET Tipo=CASE WHEN LEN(T.Documento)>1 THEN 'P' END
		FROM		PRegalado PR INNER JOIN Venta V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND PR.RutaId=V.RutaId
					INNER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.Docto=T.Documento AND PR.SKU=T.Articulo
		WHERE		T.Pza='0' AND T.Caja='0'  and PR.IdEmpresa=@IdEmpresa
		UPDATE	@T_TempLiquidaciones SET Tipo=CASE WHEN LEN(T.Documento)>1 THEN 'P' END
		FROM		PRegalado PR INNER JOIN Venta V ON PR.Docto = V.Documento AND PR.DiaO=V.DiaO AND PR.RutaId=V.RutaId
					INNER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.Docto=T.Documento AND PR.SKU=T.Articulo
		WHERE		T.Tipo='X' AND PR.docto NOT in (select documento from ventas where TipoVta='Obsequio'  and RutaId=@RutaId and PR.IdEmpresa=@IdEmpresa and DiaO=@DiaORuta) and PR.RutaId=@RutaId and PR.IdEmpresa=@IdEmpresa
		Declare D Cursor For
			SELECT	PRegalado.RutaId,PRegalado.Cliente,PRegalado.Docto,PRegalado.SKU,Productos.Producto,'0' AS Caja,'0' AS Pz,'2' as Tipomed,PRegalado.Cant,PRegalado.Tipmed as TipoProm
			FROM		PRegalado INNER JOIN Productos ON PRegalado.SKU=Productos.Clave AND PRegalado.IdEmpresa=Productos.IdEmpresa
			where		PRegalado.RutaId=@RutaId and PRegalado.DiaO=@DiaORuta and PRegalado.Docto=@Documento 
			and PRegalado.docto NOT in (select documento from ventas where TipoVta='Obsequio' and RutaId=@RutaId and IdEmpresa=@IdEmpresa and DiaO=@DiaORuta) 
			and productos.Ban_Envase=1 and PRegalado.IdEmpresa=@IdEmpresa
		Open D
		Fetch Next From D Into @RutaIdAux,@ClienteAux,@DoctoAux,@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux
		While @@FETCH_STATUS=0
		Begin
			set @ArticuloAux=''
			select	@ArticuloAux=articulo FROM @T_TempLiquidaciones WHERE RutaId=@RutaIdAux AND documento=@DoctoAux AND Articulo=@SKUAux
			if(@ArticuloAux='')
			begin
				insert into @T_TempLiquidaciones(RutaId,CodCliente,Documento,Fecha,Articulo,Descripcion,Caja,Pza,Tipo,promocion,TipoProm,TipoObse,CantObse)
				Values(@RutaIdAux,@ClienteAux,@DoctoAux,CAST(YEAR(@Fecha) AS VARCHAR(4))+'/'+CAST(MONTH(@Fecha) AS VARCHAR(2))+'/'+CAST(DAY(@Fecha) AS VARCHAR(2)),
						@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux,'',0)
			end
			else
			begin
				Update @T_TempLiquidaciones set promocion=@CantAux,TipoProm=@TipmedPromAux where RutaId=@RutaIdAux AND documento=@DoctoAux AND Articulo=@SKUAux and Tipo='P'
			end
			Fetch Next From D Into @RutaIdAux,@ClienteAux,@DoctoAux,@SKUAux,@ProductoAux,@CajaAux,@PzAux,@TipmedAux,@CantAux,@TipmedPromAux
		end
		Close D
		Deallocate D
		Fetch Next From R Into @RutaId,@CodCliente,@Documento,@Fecha,@Articulo,@Descripcion,@Caja,@Pza,@Tipo,@promocion,@TipoProm
	End
	Close R
	Deallocate R
	UPDATE	@T_TempLiquidaciones SET TipoObse=PR.TipMed
	FROM		PRegalado AS PR INNER JOIN Productos P ON PR.SKU=P.Clave
				INNER JOIN Venta AS V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND V.RutaId=PR.RutaId
				INNER JOIN Clientes C ON PR.Cliente=C.IdCli
				INNER JOIN ProductosXPzas PX ON PR.SKU=PX.Producto
				RIGHT OUTER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND  PR.SKU=T.Articulo
	WHERE		V.Cancelada='0' AND V.TipoVta='Obsequio' AND PR.RutaId=@CveRuta  and PR.IdEmpresa=@IdEmpresa and V.DiaO=@DiaORuta ---V.Fecha BETWEEN @FechaIni AND @FechaFin AND
	and P.Ban_Envase=1 
	UPDATE	@T_TempLiquidaciones SET CantObse=PR.Cant
	FROM		PRegalado PR INNER JOIN Productos P ON PR.SKU=P.Clave
				INNER JOIN Venta V ON PR.Docto=V.Documento AND PR.DiaO=V.DiaO AND V.RutaId=PR.RutaId
				INNER JOIN Clientes ON PR.Cliente=Clientes.IdCli
				INNER JOIN ProductosXPzas PX ON PR.SKU=PX.Producto
				RIGHT OUTER JOIN @T_TempLiquidaciones T ON PR.RutaId=T.RutaId AND PR.SKU=T.Articulo AND PR.Docto=T.Documento
	WHERE		V.Cancelada='0' AND V.TipoVta='Obsequio' AND PR.RutaId=@CveRuta and PR.IdEmpresa=@IdEmpresa and v.DiaO=@DiaORuta  ---V.Fecha BETWEEN @FechaIni AND @FechaFin AND 
	and P.Ban_Envase=1  

	UPDATE	T SET TipoProm=T1.TipoProm
	FROM		@T_TempLiquidaciones T INNER JOIN @T_TempLiquidaciones AS T1 ON T.RutaId=T1.RutaId AND T1.TipoProm<>'' AND T.CodCliente=T1.CodCliente AND T.Articulo=T1.Articulo
	UPDATE	T SET tipo=T1.tipo
	FROM		@T_TempLiquidaciones T INNER JOIN @T_TempLiquidaciones T1 ON T.RutaId=T1.RutaId AND T1.Tipo<>'P' AND T.CodCliente=T1.CodCliente AND T.Articulo=T1.Articulo
	INSERT INTO T_TempLiquidacionesAux
	SELECT	RutaId,max(cast(Fecha as date)) as Fecha,Articulo as Clave,
				IsNull(SUM(cast(Caja as int)),0) AS [Venta Cajas],
				IsNull(SUM(cast(Pza as int)),0) AS [Venta Piezas],
				CASE WHEN ISNULL(TipoProm,'Caja')='Caja' THEN Case When IsNull(PzaXCja,1)=1 Then 0 Else ISNULL(SUM(cast(promocion as int)),0) End ELSE ISNULL(SUM(cast(promocion as  int)),0)/IsNull(PzaXCja,1) END [Promo Cajas],
				Case When IsNull(PzaXCja,1)=1 Then ISNULL(SUM(cast(promocion as  int)),0) Else CASE WHEN ISNULL(TipoProm,'Caja')='Pzas' THEN ISNULL(SUM(cast(promocion as  int)),0)%IsNull(PzaXCja,1) ELSE 0 END End [Promo Piezas],
				TipoObse,sum(CantObse) 
	FROM		@T_TempLiquidaciones T Left Join ProductosXPzas P On T.Articulo=P.Producto
	group  by RutaId,Articulo,Tipo,TipoProm,TipoObse,P.PzaXCja

	insert into @T_TempDevol
	SELECT	DetalleDevol.SKU,
				CASE WHEN isnull(DetalleDevol.Tipo,0)=0 then ISNULL(SUM(DetalleDevol.Pza),0) WHEN isnull(DetalleDevol.Tipo,0)=1 then ISNULL(SUM(DetalleDevol.Pza)/ProductosXPzas.PzaXCja,0) ELSE 0 end as Cajas,
				CASE WHEN isnull(DetalleDevol.Tipo,0)=1 then ISNULL(SUM(DetalleDevol.Pza)%ProductosXPzas.PzaXCja,0) ELSE 0 end as piezas
	FROM		DetalleDevol INNER JOIN  Devoluciones ON Devoluciones.Devol=DetalleDevol.Docto AND Devoluciones.Ruta=DetalleDevol.RutaId AND DetalleDevol.IdEmpresa=Devoluciones.IdEmpresa
				INNER JOIN ProductosXPzas ON DetalleDevol.SKU=ProductosXPzas.Producto AND DetalleDevol.IdEmpresa=ProductosXPzas.IdEmpresa
	WHERE		(DetalleDevol.Motivo <> 'Comodato') AND (Devoluciones.DiaO=@DiaORuta) AND DetalleDevol.RutaId=@CveRuta  and DetalleDevol.IdEmpresa=@IdEmpresa ---(Devoluciones.Fecha BETWEEN @FechaIni AND @FechaFin)
	GROUP BY DetalleDevol.SKU,DetalleDevol.Tipo,ProductosXPzas.PzaXCja,DetalleDevol.RutaId

	set	@DiaAux=(select MAX(DiaO) from DiasO where IdEmpresa=@IdEmpresa and rutaid=@CveRuta and diao<@DiaORuta)  --(select top 1 DiaO from Diaso where  idempresa=@IdEmpresa and rutaid=@CveRuta and Fecha BETWEEN @FechaIni AND @FechaFin)
	set	@FechaStock=(select MAX(Fecha) from stockhistorico where RutaId=@CveRuta and DiaO=@DiaORuta) --- fecha<=@FechaIni)

	INSERT INTO @T_TempPedDiaSig
	SELECT	ProductoPaseado.CodProd,(ISNULL(PedDiaSig.Cantidad,0))/ISNULL(ProductosXPzas.PzaXCja,1) as PedDiaSigCajas,ISNULL(PedDiaSigPzs.Cantidad,0) as PedDiaSigPiezas
	FROM		ProductoPaseado INNER JOIN ProductosXPzas ON ProductoPaseado.CodProd=ProductosXPzas.Producto AND ProductoPaseado.IdEmpresa=ProductosXPzas.IdEmpresa
				INNER JOIN DiasO ON ProductoPaseado.RutaId=DiasO.RutaId AND ProductoPaseado.IdEmpresa=DiasO.IdEmpresa AND ProductoPaseado.DiaO=DiasO.DiaO
				LEFT OUTER JOIN PedDiaSigPzs ON ProductoPaseado.RutaId=PedDiaSigPzs.IdRuta AND ProductoPaseado.DiaO=PedDiaSigPzs.Diao AND ProductoPaseado.IdEmpresa=PedDiaSigPzs.IdEmpresa
				AND ProductoPaseado.CodProd=PedDiaSigPzs.Articulo
				LEFT OUTER JOIN PedDiaSig ON ProductoPaseado.RutaId=PedDiaSig.IdRuta AND ProductoPaseado.DiaO=PedDiaSig.Diao AND ProductoPaseado.IdEmpresa=PedDiaSig.IdEmpresa
				AND ProductoPaseado.CodProd = PedDiaSig.Articulo
	WHERE		ProductoPaseado.RutaId=@CveRuta and ProductoPaseado.DiaO=@DiaORuta and ProductoPaseado.IdEmpresa=@IdEmpresa
	order by  ProductoPaseado.CodProd

	SELECT	P.Clave,P.Producto,
				ISNULL((SELECT CAST(ISNULL(SH.Stock,0)/ISNULL(PX.PzaXCja,1) AS int) Expr1
						FROM		StockHistorico AS SH LEFT OUTER JOIN ProductosXPzas AS PX ON SH.Articulo=PX.Producto
						WHERE		(SH.Articulo=S.Articulo) AND (SH.RutaID=S.RutaID) AND (SH.DiaO=S.DiaO)),0) [Stock Inicial Cajas],
				ISNULL((SELECT	CAST(ISNULL(SH.Stock,0)%ISNULL(PX.PzaXCja,1) AS int) Expr1
						FROM		StockHistorico AS SH LEFT OUTER JOIN ProductosXPzas AS PX ON SH.Articulo=PX.Producto
						WHERE		(SH.Articulo=S.Articulo) AND (SH.RutaID=S.RutaID) AND (SH.DiaO=S.DiaO)),0) [Stock Inicial Piezas],
				ISNULL(SUM(CP.VentaCajas),0) AS [Venta Cajas],ISNULL(SUM(CP.VentaPiezas),0) AS [Venta Piezas],ISNULL(SUM(CP.PromoCajas),0) [Promo Cajas],
				ISNULL(SUM(CP.PromoPiezas), 0) AS [Promo Piezas], 
				ISNULL(CASE WHEN CP.TipoObse='Caja' THEN SUM(CP.CantObse) ELSE CAST(CP.CantObse AS int)/ISNULL(ProductosXPzas.PzaXCja,1) END,0) [Obsequio Cajas], 
				ISNULL(CASE WHEN CP.TipoObse='Pzas' THEN CAST(CP.CantObse AS int)%ISNULL(ProductosXPzas.PzaXCja,1) END,0) [Obsequio Piezas],
				ISNULL(T.Cajas,0) [Devol Cajas], 
				ISNULL(T.Pza,0) [Devol Piezas],CAST(ProductoPaseado.Stock AS int)/ISNULL(ProductosXPzas.PzaXCja,1) [Stock final Cajas],
				CAST(ProductoPaseado.Stock AS int)%ISNULL(ProductosXPzas.PzaXCja,1) [Stock Final Piezas]
	FROM		StockHistorico S RIGHT OUTER JOIN @T_TempDevol T
				RIGHT OUTER JOIN @T_TempPedDiaSig TPD
				RIGHT OUTER JOIN Productos P
				INNER JOIN ProductoPaseado ON P.Clave=ProductoPaseado.CodProd AND ProductoPaseado.RutaId=@CveRuta AND ProductoPaseado.DiaO=@DiaORuta
				INNER JOIN ProductosXPzas ON ProductoPaseado.CodProd=ProductosXPzas.Producto
				ON TPD.Articulo=P.Clave
				ON T.Articulo=P.Clave
				ON S.Articulo=P.Clave AND S.RutaID=@CveRuta AND S.DiaO=@DiaAux  --- S.Fecha=@FechaStock
				LEFT OUTER JOIN ProductosXPzas PP ON S.Articulo=PP.Producto
				LEFT OUTER JOIN T_TempLiquidacionesAux AS CP ON P.Clave=CP.Clave AND CP.RutaId=@CveRuta ---AND CP.Fecha BETWEEN @FechaIni AND @FechaFin
	WHERE        (P.Ban_Envase = 1)
	GROUP BY P.Clave,P.Producto,PP.PzaXCja,S.Articulo,S.Fecha,S.RutaID,S.Stock,ProductoPaseado.Stock,ProductosXPzas.PzaXCja,CP.TipoObse,CP.CantObse,T.Cajas,T.Pza,TPD.Cajas,TPD.Pza,S.Diao
	ORDER BY P.Clave,S.Fecha
end


GO

-- ----------------------------
-- Procedure structure for SPAD_ReporteProductosxPedido
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_ReporteProductosxPedido]
GO

CREATE PROCEDURE [dbo].[SPAD_ReporteProductosxPedido](
@Fecha Date,
@IdRuta int,
@status int,
@IdEmpresa varchar(5))
AS
begin
	Declare @Ruta			Varchar(50)
	declare @Pedido			Varchar(50)
	Declare @Cod_cliente	Varchar(50)
	Declare @Nombre			Varchar(50)
	Declare @NombreCorto	Varchar(50)
	Declare @Status_Pedido	Varchar(50)
	Declare @FechaC			varchar(50)
	Declare @FechaP			varchar(50)
	Declare @FechaE			varchar(50)
	Declare @Generico		varchar(50)
	Declare @Cajas			varchar(50)
	Declare @Piezas			varchar(50) 
	Declare @Cmnd			Varchar(max)
	Declare @Cmnd1			Varchar(max)
	Declare @Cmnd2			Varchar(max)
	Declare @Cmnd3			Varchar(max)
	Declare @Cmnd4			Varchar(max)
	Declare @Cmnd5			Varchar(max)
	Declare @CmndAux		Varchar(max)
	Declare @Cmnd1Aux		Varchar(max)
	Declare @Clave			Varchar(50)
	Declare @Cant			int
	Declare @Hist			int
	Declare @Articulo		Varchar(50)
	Declare @PzaXCja		varchar(50)
	Declare @Vendedor		varchar(50)
	Declare @Promocion		varchar(5)
	SET @Hist=0
	Declare C Cursor For
		SELECT DESCRIPCION
		FROM Sabores WHERE  STATUS=1
		ORDER BY clave
	Open C
	Fetch Next From C Into @Clave

	if Exists (Select name From sysobjects Where name='T_RepProdPedidos')
		Drop Table T_RepProdPedidos
	--if @Clave <> ''
	--begin 
		Set @Cmnd='CREATE TABLE T_RepProdPedidos(FechaP varchar(50),FechaE varchar(50),Fecha varchar(50),Status_Pedido varchar(50),promocion int, Ruta varchar(50),Vendedor varchar(50),Pedido varchar(50),IdCliente varchar(50),Cliente varchar(50),NombreCorto varchar(50),Generico Varchar(50),Cajas int,Piezas int,PzaXCja varchar(50),'
		Set @Cmnd1=''
		Set @Cmnd2=''
		Set @Cmnd3=''
		Set @Cmnd4=''
		Set @Cmnd5=''
		While @@FETCH_STATUS=0
		Begin
			Set @Cmnd1=@Cmnd1+'['+@Clave+'] int,'
			Set @Cmnd3=@Cmnd3+'['+@Clave+']+'
			Set @Cmnd4=@Cmnd4+'SUM(['+@Clave+']),'
			Set @Cmnd5=@Cmnd5+'SUM(['+@Clave+'])+'
			Fetch Next From C Into @Clave
		End
		Close C
		Deallocate C
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,len(@Cmnd1)-1)+')'
 		Execute (@Cmnd)

	IF (@status>0)
		Declare R Cursor For
			SELECT FECHA_PEDIDO,DATEADD(d,-1,Fecha_Entrega) AS FechaEmbarque,Fecha_Entrega, Status_Pedido,Promocion,Ruta,Vendedor,Pedido,Cod_cliente,Nombre,NombreCorto,Generico ,SUM(TotalCajas) AS Cajas,SUM(TotalPiezas) as Piezas,PzaXCja
			FROM V_ProductosGenericos 
			WHERE IDEMPRESA = @idEmpresa AND  IdRuta = @IdRuta AND DATEADD(d,-1,Fecha_Entrega) = @Fecha  
			AND  Status=@status ---AND dateadd(d,1,@Fecha) 
			GROUP BY FECHA_PEDIDO,Fecha_Entrega,Status_Pedido,Promocion,Ruta,Vendedor,Pedido,Cod_cliente,Nombre,NombreCorto,Generico,PzaXCja 
	else
	IF (@status=0)
		Declare R Cursor For
			SELECT FECHA_PEDIDO,DATEADD(d,-1,Fecha_Entrega) AS FechaEmbarque,Fecha_Entrega, Status_Pedido,Promocion,Ruta,Vendedor,Pedido,Cod_cliente,Nombre,NombreCorto,Generico ,SUM(TotalCajas) AS Cajas,SUM(TotalPiezas) as Piezas,PzaXCja
			FROM V_ProductosGenericos 
			WHERE IDEMPRESA = @idEmpresa AND  IdRuta = @IdRuta AND DATEADD(d,-1,Fecha_Entrega) = @Fecha  ---AND dateadd(d,1,@Fecha) 
			GROUP BY FECHA_PEDIDO,Fecha_Entrega,Status_Pedido,Promocion,Ruta,Vendedor,Pedido,Cod_cliente,Nombre,NombreCorto,Generico,PzaXCja 

	Open R
	Fetch Next From R Into @FechaP,@FechaE,@FechaC,@Status_Pedido,@promocion,@Ruta,@Vendedor,@Pedido,@Cod_cliente,@Nombre,@NombreCorto,@Generico,@Cajas,@Piezas,@PzaXCja
	While @@FETCH_STATUS=0
	Begin
		Declare D Cursor For
			SELECT DESCRIPCION
			FROM Sabores WHERE  STATUS=1
			ORDER BY clave
		Open D
		SET @Cmnd='Insert Into T_RepProdPedidos(FechaP,FechaE,Fecha,Status_Pedido,Promocion,Ruta,Vendedor,Pedido,IdCliente,Cliente,NombreCorto,Generico,Cajas,Piezas,PzaXCja,'
		SET @Cmnd1=''
		SET @CmndAux='Insert Into T_RepProdPedidos(FechaP,FechaE,Fecha,Status_Pedido,Promocion,Ruta,Vendedor,Pedido,IdCliente,Cliente,NombreCorto,Generico,Cajas,Piezas,PzaXCja,'
		SET @Cmnd1Aux=''
		SET @Cmnd2=''
		Fetch Next From D Into @Clave
		While @@FETCH_STATUS=0
		Begin
			SET @Cant=NULL
			SELECT @Cant=ISNULL(piezas,0)  
			FROM V_ProductosGenericos 
			WHERE IDEMPRESA = @IdEmpresa AND  IdRuta = @IdRuta AND DATEADD(d,-1,Fecha_Entrega) = @Fecha and  pedido=@Pedido  and Sabor=@Clave and  Generico=@Generico and Promocion=@promocion and Status_Pedido=@Status_Pedido

			if @Cant Is NULL
				SET @Cant=0
			SET @Cmnd1=@Cmnd1+'['+@Clave+'],'
			SET @Cmnd1Aux=@Cmnd1Aux+'['+@Clave+'],'
			SET @Cmnd2=@Cmnd2+cast(@Cant as varchar(7))+','
			Fetch Next From D Into @Clave
		end
		print 'Promo'
		print @promocion
		SET @Cmnd=@Cmnd+SUBSTRING(@Cmnd1,1,Len(@Cmnd1)-1)+') Values('''+CAST(DAY(@FechaP) AS VARCHAR(2))+'/'+CAST(MONTH(@FechaP) AS VARCHAR(2))+'/'+CAST(YEAR(@FechaE) AS VARCHAR(4))+''','''+CAST(DAY(@FechaE) AS VARCHAR(2))+'/'+CAST(MONTH(@FechaE) AS VARCHAR(2))+'/'+CAST(YEAR(@FechaE) AS VARCHAR(4))+''','''+CAST(DAY(@FechaC) AS VARCHAR(2))+'/'+CAST(MONTH(@FechaC) AS VARCHAR(2))+'/'+CAST(YEAR(@FechaC) AS VARCHAR(4))+''','''+@Status_Pedido+''','''+@Promocion+''','''+@Ruta+''','''+@Vendedor+''','''+@Pedido+''','''+@Cod_cliente+''','''+@Nombre+''','''+@NombreCorto+''','''+@Generico+''','+@Cajas+','+@Piezas+','''+@PzaXCja+''','
		+SUBSTRING(@Cmnd2,1,Len(@Cmnd2)-1)+')'
		Close D
		Deallocate D
		Execute (@Cmnd)
		Fetch Next From R Into @FechaP,@FechaE,@FechaC,@Status_Pedido,@Promocion,@Ruta,@Vendedor,@Pedido,@Cod_cliente,@Nombre,@NombreCorto,@Generico,@Cajas,@Piezas,@PzaXCja
	End
	Close R
	Deallocate R

	--SET @CmndAux=@CmndAux+SUBSTRING(@Cmnd1Aux,1,Len(@Cmnd1Aux)-1)+','''') Select ''Total'','''','''','''','''','''','+@Cmnd4+''''' From T_RepProdPedidos'
	--print @CmndAux
	--Execute (@CmndAux)


	SET @CmndAux=@CmndAux+SUBSTRING(@Cmnd1Aux,1,Len(@Cmnd1Aux)-1)+') Select '''','''',''Total'','''','''','''','''','''','''','''','''','''','''','''','''','+SUBSTRING(@Cmnd4,1,Len(@Cmnd4)-1)+' From T_RepProdPedidos'
	Execute (@CmndAux)
	set @Cmnd= 'Update T_RepProdPedidos set Cajas=((case when ('+Substring(@Cmnd3,1,len(@Cmnd3)-1)+')=0 then 1 else ('+Substring(@Cmnd3,1,len(@Cmnd3)-1)+') end)/pzaxcja), piezas=(('+ Substring(@Cmnd3,1,len(@Cmnd3)-1)+')%pzaxcja)  where fecha!=''Total'''
	print @Cmnd
	Execute (@Cmnd)
	set @Cmnd= 'update T_RepProdPedidos set Cajas=(select sum(cajas) from T_RepProdPedidos where fecha!=''Total''), Piezas=(select sum(Piezas) from T_RepProdPedidos where fecha!=''Total'') where fecha=''Total'''
	print @Cmnd
	Execute (@Cmnd)
	Set @Cmnd='Select FechaP as [Fecha Pedido],FechaE as [Fecha de Preparacion de Pedido], Fecha as [Fecha Entrega],Status_Pedido,cast(Promocion as bit) as Promocion,Ruta,Vendedor,Pedido,IdCliente,Cliente,NombreCorto,Generico,Cajas,Piezas,'+SUBSTRING(@Cmnd1Aux,1,Len(@Cmnd1Aux)-1)+','+Substring(@Cmnd3,1,len(@Cmnd3)-1)+' Total From T_RepProdPedidos'
	print @Cmnd
	Execute (@Cmnd)

end

GO

-- ----------------------------
-- Procedure structure for SPAD_SaldoEnvases
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[SPAD_SaldoEnvases]
GO

CREATE PROCEDURE [dbo].[SPAD_SaldoEnvases](
@IdRuta		int,
@IdEmpresa	varchar(50))
AS
Begin
	Select	D.CodCli,D.Docto,D.Articulo,SUM(D.Cantidad-D.Devuelto) Saldo,D.Envase,MIN(V.Fecha) Fecha
	From		DevEnvases D Inner Join (Select Distinct RutaId,CodCliente,Documento,IdEmpresa,Fecha From Venta) V
				On D.RutaID=V.RutaId And D.Docto=V.Documento And D.CodCli=V.CodCliente And D.IdEmpresa=V.IdEmpresa
	Where		D.RutaId=@IdRuta And D.IdEmpresa=@IdEmpresa And D.Tipo='Comodato'
	Group By D.CodCli,D.Docto,D.Articulo,D.Envase
	Having SUM(D.Cantidad-D.Devuelto)>0

End

GO

-- ----------------------------
-- Procedure structure for TableNameSelect
-- ----------------------------
DROP PROCEDURE IF EXISTS [dbo].[TableNameSelect]
GO

CREATE PROCEDURE [dbo].[TableNameSelect]
(
    @TableCatalog AS VARCHAR(100)
) AS
SELECT Table_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'BASE TABLE'
and 	TABLE_CATALOG = @TableCatalog
and 	TABLE_NAME <>   'dtproperties'

GO

-- ----------------------------
-- Function structure for CteConVenta
-- ----------------------------
DROP FUNCTION IF EXISTS [dbo].[CteConVenta]
GO

CREATE FUNCTION [dbo].[CteConVenta]
 (@IdRuta int,
  @Cliente Varchar(50),
  @Fecha date
 )
 returns char(1)
 as
 begin 
   declare @resultado char(1)
   set @resultado=0

	SELECT @resultado=CASE WHEN LEN(CodCli)>0 THEN 1 ELSE 0 END
	FROM  RelOperaciones INNER JOIN
	Venta ON RelOperaciones.RutaId = Venta.RutaId AND RelOperaciones.CodCli = Venta.CodCliente
	WHERE Venta.Cancelada = 0 AND Tipo='Venta' And RelOperaciones.RutaId=@IdRuta AND CodCli=@cliente And cast(RelOperaciones.Fecha as date) = @fecha

/*	SELECT @resultado=CASE WHEN LEN(CodCli)>0 THEN 1 ELSE 0 END
//	FROM  RelOperaciones INNER JOIN
//	Venta ON RelOperaciones.RutaId = Venta.RutaId AND RelOperaciones.CodCli = Venta.CodCliente and  RelOperaciones.diao = Venta.diao and  RelOperaciones.Folio = Venta.Documento
	WHERE Venta.Cancelada = 0 AND Tipo='Venta' And RelOperaciones.RutaId=@IdRuta AND CodCli=@cliente And cast(RelOperaciones.Fecha as date) = @fecha
*/

   return @resultado

 end

GO

-- ----------------------------
-- Function structure for CteVisitado
-- ----------------------------
DROP FUNCTION IF EXISTS [dbo].[CteVisitado]
GO

CREATE FUNCTION [dbo].[CteVisitado]
 (@IdRuta int,
  @Cliente Varchar(50),
  @Fecha date
 )
 returns char(1)
 as
 begin 
   declare @resultado char(1)
   set @resultado=0

	--SELECT @resultado=CASE WHEN LEN(RelOperaciones.CodCli)>0 THEN 1 ELSE 0 END
	--FROM Rutas FULL OUTER JOIN
	--RelOperaciones ON Rutas.IdRutas = RelOperaciones.RutaId FULL OUTER JOIN
	--BitacoraTiempos ON RelOperaciones.RutaId = BitacoraTiempos.RutaId AND RelOperaciones.CodCli = BitacoraTiempos.Codigo AND 
	--RelOperaciones.DiaO = BitacoraTiempos.DiaO
	--WHERE RelOperaciones.Tipo='Venta' and  RelOperaciones.RutaId=@IdRuta And RelOperaciones.CodCli=@cliente And cast(RelOperaciones.Fecha as date)=@fecha

	SELECT @resultado=CASE WHEN LEN(Codigo)>0 THEN 1 ELSE 0 END
	FROM BitacoraTiempos INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas 
	WHERE visita=1  and  RutaId=@IdRuta And BitacoraTiempos.Codigo=@cliente And cast(BitacoraTiempos.HI as date)=@fecha

   return @resultado

 end

GO

-- ----------------------------
-- Function structure for EsVtaProgramada
-- ----------------------------
DROP FUNCTION IF EXISTS [dbo].[EsVtaProgramada]
GO

CREATE FUNCTION [dbo].[EsVtaProgramada]
 (@IdRuta int,
  @Cliente Varchar(50),
  @Fecha date,
  @Dia	varchar(20)
 )
 returns char(1)
 as
 begin 
   declare @resultado char(1)
	declare @ValDia char(1)
   set @resultado=0
	set @ValDia=0
   SELECT @resultado = CASE WHEN LEN(Codcli)>0 THEN 1 ELSE 0 END 
   FROM TH_SecVisitas 
   WHERE RutaId=@IdRuta And CodCli=@cliente AND  cast(fecha as date) =@fecha
   order by fecha

	if(@resultado=0)
	begin
		SELECT @ValDia= CASE WHEN @Dia = 'Lun' THEN isnull(Lunes, 0) 
				 WHEN @Dia = 'Mar' THEN isnull(Martes, 0)
				 WHEN @Dia = 'Mié' THEN isnull(Miercoles, 0)
				 WHEN @Dia = 'Jue' THEN isnull(Jueves, 0)
				 WHEN @Dia = 'Vie' THEN isnull(Viernes, 0)
				 WHEN @Dia = 'Sáb' THEN isnull(Sabado, 0)
				 WHEN @Dia = 'Dom' THEN isnull(Domingo, 0)
		ELSE 0 END
		FROM RelDayCli
		WHERE RutaId =@IdRuta AND CodCli = @cliente
		if (@ValDia>0)
			set @resultado=1
	end 
   return @resultado
 end

GO

-- ----------------------------
-- Function structure for FNAD_DameRutasCte
-- ----------------------------
DROP FUNCTION IF EXISTS [dbo].[FNAD_DameRutasCte]
GO

CREATE FUNCTION [dbo].[FNAD_DameRutasCte](
@CodCliente varchar(50))
RETURNS varchar(MAX)
AS
BEGIN
	DECLARE @ResultVar Varchar(MAX)
	Declare @Ruta		 Varchar(50)
	DECLARE @Tabla Table (Ruta Varchar(50))
	SET @ResultVar=''
	Insert Into @Tabla(Ruta)
	Select	R.Ruta
	From		Clientes C Inner Join RelClirutas L On C.IdCli=L.IdCliente
				Inner Join Rutas R On L.IdRuta=R.IdRutas
	Where		C.IdCli=@CodCliente
	While Exists(Select * From @Tabla)
	begin
		SELECT Top 1 @Ruta=Ruta From @Tabla
		SET @ResultVar=@ResultVar+@Ruta+','
		Delete @Tabla Where Ruta=@Ruta
	end
	-- Return the result of the function
	if LEN(@ResultVar)>0
		Set @ResultVar=SUBSTRING(@ResultVar,1,Len(@ResultVar)-1)
	RETURN @ResultVar
END

GO

-- ----------------------------
-- Function structure for GetLocalDate
-- ----------------------------
DROP FUNCTION IF EXISTS [dbo].[GetLocalDate]
GO

CREATE FUNCTION [dbo].[GetLocalDate]()
RETURNS DATETIME
AS
BEGIN
    DECLARE @LocalTime DATETIME
    SELECT @LocalTime = CONVERT(DATETIMEOFFSET, GETDATE()) AT TIME ZONE 'Central Standard Time (Mexico)'
    RETURN @LocalTime
END

GO

-- ----------------------------
-- Indexes structure for table Activos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Activos
-- ----------------------------
ALTER TABLE [dbo].[Activos] ADD PRIMARY KEY ([IdActivos])
GO

-- ----------------------------
-- Indexes structure for table Ayudantes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Ayudantes
-- ----------------------------
ALTER TABLE [dbo].[Ayudantes] ADD PRIMARY KEY ([id_ayudante])
GO

-- ----------------------------
-- Indexes structure for table BACKORDER
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table BACKORDER
-- ----------------------------
ALTER TABLE [dbo].[BACKORDER] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table bi_project
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table bi_project
-- ----------------------------
ALTER TABLE [dbo].[bi_project] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table bi_salary_survey
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table bi_salary_survey
-- ----------------------------
ALTER TABLE [dbo].[bi_salary_survey] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table bi_sales
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table bi_sales
-- ----------------------------
ALTER TABLE [dbo].[bi_sales] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table BitacoraCuotas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table BitacoraCuotas
-- ----------------------------
ALTER TABLE [dbo].[BitacoraCuotas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table BitacoraTiempos
-- ----------------------------
CREATE INDEX [idx_dia] ON [dbo].[BitacoraTiempos]
([DiaO] ASC, [RutaId] ASC) 
GO
CREATE INDEX [idx_hi] ON [dbo].[BitacoraTiempos]
([HI] ASC) 
GO
CREATE INDEX [idxbitiedesdu] ON [dbo].[BitacoraTiempos]
([Codigo] ASC, [DiaO] ASC, [RutaId] ASC, [HI] ASC) 
GO
CREATE INDEX [idxbitiediarut] ON [dbo].[BitacoraTiempos]
([DiaO] ASC, [RutaId] ASC) 
GO
CREATE INDEX [idxbitiefec] ON [dbo].[BitacoraTiempos]
([HI] ASC) 
GO
CREATE INDEX [idxbitiefech] ON [dbo].[BitacoraTiempos]
([HF] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table BitacoraTiempos
-- ----------------------------
ALTER TABLE [dbo].[BitacoraTiempos] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Cancelaciones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Cancelaciones
-- ----------------------------
ALTER TABLE [dbo].[Cancelaciones] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table CargaRuta
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CargaRuta
-- ----------------------------
ALTER TABLE [dbo].[CargaRuta] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table CatBancos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CatBancos
-- ----------------------------
ALTER TABLE [dbo].[CatBancos] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table CatGrupos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CatGrupos
-- ----------------------------
ALTER TABLE [dbo].[CatGrupos] ADD PRIMARY KEY ([Clave], [TipoGrupo])
GO

-- ----------------------------
-- Indexes structure for table CatMarcas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CatMarcas
-- ----------------------------
ALTER TABLE [dbo].[CatMarcas] ADD PRIMARY KEY ([Clave], [TipoMarca])
GO

-- ----------------------------
-- Indexes structure for table CatUnidadMedida
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CatUnidadMedida
-- ----------------------------
ALTER TABLE [dbo].[CatUnidadMedida] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table ClasClientes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ClasClientes
-- ----------------------------
ALTER TABLE [dbo].[ClasClientes] ADD PRIMARY KEY ([IdClasC])
GO

-- ----------------------------
-- Indexes structure for table ClasifComp
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ClasifComp
-- ----------------------------
ALTER TABLE [dbo].[ClasifComp] ADD PRIMARY KEY ([Id_Clasif])
GO

-- ----------------------------
-- Indexes structure for table ClasProductos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ClasProductos
-- ----------------------------
ALTER TABLE [dbo].[ClasProductos] ADD PRIMARY KEY ([IdClasP])
GO

-- ----------------------------
-- Indexes structure for table ClasRutas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ClasRutas
-- ----------------------------
ALTER TABLE [dbo].[ClasRutas] ADD PRIMARY KEY ([IdCRutas])
GO

-- ----------------------------
-- Indexes structure for table Clientes
-- ----------------------------
CREATE INDEX [idx_CP] ON [dbo].[Clientes]
([CP] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Clientes
-- ----------------------------
ALTER TABLE [dbo].[Clientes] ADD PRIMARY KEY ([IdCli])
GO

-- ----------------------------
-- Triggers structure for table Clientes
-- ----------------------------
DROP TRIGGER IF EXISTS [dbo].[SaldosCobranza]
GO
CREATE TRIGGER [dbo].[SaldosCobranza]
ON [dbo].[Clientes]
AFTER INSERT, UPDATE
AS
Declare @Cve_Cte	varchar(50)
	Declare @IdEmpresa	varchar(50)
	Select @Cve_Cte=IdCli,@Idempresa=Idempresa From Inserted
	EXEC dbo.SPAD_AgregaSaldoIncialCte @Cve_Cte,@Idempresa

GO

-- ----------------------------
-- Indexes structure for table Clientes_copy
-- ----------------------------
CREATE INDEX [idx_CP] ON [dbo].[Clientes_copy]
([CP] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Clientes_copy
-- ----------------------------
ALTER TABLE [dbo].[Clientes_copy] ADD PRIMARY KEY ([IdCli])
GO

-- ----------------------------
-- Indexes structure for table Cobranza
-- ----------------------------
CREATE INDEX [idxcobfecr] ON [dbo].[Cobranza]
([FechaReg] ASC) 
GO
CREATE INDEX [idxcobfecv] ON [dbo].[Cobranza]
([FechaVence] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Cobranza
-- ----------------------------
ALTER TABLE [dbo].[Cobranza] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table CodesOp
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CodesOp
-- ----------------------------
ALTER TABLE [dbo].[CodesOp] ADD PRIMARY KEY ([Codi])
GO

-- ----------------------------
-- Indexes structure for table Comisiones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Comisiones
-- ----------------------------
ALTER TABLE [dbo].[Comisiones] ADD PRIMARY KEY ([IdRow])
GO

-- ----------------------------
-- Indexes structure for table ComponentesArt
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ComponentesArt
-- ----------------------------
ALTER TABLE [dbo].[ComponentesArt] ADD PRIMARY KEY ([Complemento])
GO

-- ----------------------------
-- Indexes structure for table ConfigRutasP
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ConfigRutasP
-- ----------------------------
ALTER TABLE [dbo].[ConfigRutasP] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Configuracion
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Configuracion
-- ----------------------------
ALTER TABLE [dbo].[Configuracion] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table ConfiguracionGral
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ConfiguracionGral
-- ----------------------------
ALTER TABLE [dbo].[ConfiguracionGral] ADD PRIMARY KEY ([ID_Config])
GO

-- ----------------------------
-- Indexes structure for table Consolidado
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Consolidado
-- ----------------------------
ALTER TABLE [dbo].[Consolidado] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table ContadorNC
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ContadorNC
-- ----------------------------
ALTER TABLE [dbo].[ContadorNC] ADD PRIMARY KEY ([Idrow])
GO

-- ----------------------------
-- Indexes structure for table Continuidad
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Continuidad
-- ----------------------------
ALTER TABLE [dbo].[Continuidad] ADD PRIMARY KEY ([RutaID])
GO

-- ----------------------------
-- Indexes structure for table CP
-- ----------------------------
CREATE INDEX [idx_CP] ON [dbo].[CP]
([CP] ASC) 
GO

-- ----------------------------
-- Indexes structure for table CTiket
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CTiket
-- ----------------------------
ALTER TABLE [dbo].[CTiket] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table Cuotas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Cuotas
-- ----------------------------
ALTER TABLE [dbo].[Cuotas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table CuotasVenta
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table CuotasVenta
-- ----------------------------
ALTER TABLE [dbo].[CuotasVenta] ADD PRIMARY KEY ([year])
GO

-- ----------------------------
-- Indexes structure for table DETALLE_BO
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DETALLE_BO
-- ----------------------------
ALTER TABLE [dbo].[DETALLE_BO] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table DetalleCob
-- ----------------------------
CREATE INDEX [idxdetcobfec] ON [dbo].[DetalleCob]
([Fecha] ASC) 
GO
CREATE INDEX [idxdetcobfk] ON [dbo].[DetalleCob]
([IdCobranza] ASC) 
GO
CREATE INDEX [idxfdetcobfpag] ON [dbo].[DetalleCob]
([FormaP] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DetalleCob
-- ----------------------------
ALTER TABLE [dbo].[DetalleCob] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DetalleCombo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DetalleCombo
-- ----------------------------
ALTER TABLE [dbo].[DetalleCombo] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DetalleDevol
-- ----------------------------
CREATE INDEX [idxdetdevruta] ON [dbo].[DetalleDevol]
([RutaId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DetalleDevol
-- ----------------------------
ALTER TABLE [dbo].[DetalleDevol] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DetalleLD
-- ----------------------------
CREATE INDEX [idxdetldart] ON [dbo].[DetalleLD]
([Articulo] ASC) 
GO
CREATE INDEX [idxdetldlista] ON [dbo].[DetalleLD]
([ListaId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DetalleLD
-- ----------------------------
ALTER TABLE [dbo].[DetalleLD] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table DetalleLP
-- ----------------------------
CREATE INDEX [idxdetlpdart] ON [dbo].[DetalleLP]
([Articulo] ASC) 
GO
CREATE INDEX [idxdetlplista] ON [dbo].[DetalleLP]
([ListaId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DetalleLP
-- ----------------------------
ALTER TABLE [dbo].[DetalleLP] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table DetalleLProMaster
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DetalleLProMaster
-- ----------------------------
ALTER TABLE [dbo].[DetalleLProMaster] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DETALLEPEDIDOLIBERADO
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DETALLEPEDIDOLIBERADO
-- ----------------------------
ALTER TABLE [dbo].[DETALLEPEDIDOLIBERADO] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table DetallePedidos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DetallePedidos
-- ----------------------------
ALTER TABLE [dbo].[DetallePedidos] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DetallePromo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DetallePromo
-- ----------------------------
ALTER TABLE [dbo].[DetallePromo] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DetalleVet
-- ----------------------------
CREATE INDEX [idx_articulo] ON [dbo].[DetalleVet]
([Articulo] ASC) 
GO
CREATE INDEX [idx_ruta] ON [dbo].[DetalleVet]
([Docto] ASC, [RutaId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DetalleVet
-- ----------------------------
ALTER TABLE [dbo].[DetalleVet] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Uniques structure for table DetalleVet
-- ----------------------------
ALTER TABLE [dbo].[DetalleVet] ADD UNIQUE ([Articulo] ASC, [Docto] ASC, [RutaId] ASC)
GO

-- ----------------------------
-- Indexes structure for table DevEnvases
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DevEnvases
-- ----------------------------
ALTER TABLE [dbo].[DevEnvases] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table Devoluciones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Devoluciones
-- ----------------------------
ALTER TABLE [dbo].[Devoluciones] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Dias
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Dias
-- ----------------------------
ALTER TABLE [dbo].[Dias] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table DiasO
-- ----------------------------
CREATE INDEX [idx_Fecha] ON [dbo].[DiasO]
([Fecha] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table DiasO
-- ----------------------------
ALTER TABLE [dbo].[DiasO] ADD PRIMARY KEY ([DiaO], [RutaId])
GO

-- ----------------------------
-- Indexes structure for table DiasOperativos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table DiasOperativos
-- ----------------------------
ALTER TABLE [dbo].[DiasOperativos] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table Empresas
-- ----------------------------
CREATE INDEX [index_empresas_on_empresamadre_id] ON [dbo].[Empresas]
([empresamadre_id] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Empresas
-- ----------------------------
ALTER TABLE [dbo].[Empresas] ADD PRIMARY KEY ([IdEmpresa])
GO

-- ----------------------------
-- Indexes structure for table empresasmadre
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table empresasmadre
-- ----------------------------
ALTER TABLE [dbo].[empresasmadre] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Encuestas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Encuestas
-- ----------------------------
ALTER TABLE [dbo].[Encuestas] ADD PRIMARY KEY ([Clave_Enc])
GO

-- ----------------------------
-- Indexes structure for table FormasPag
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table FormasPag
-- ----------------------------
ALTER TABLE [dbo].[FormasPag] ADD PRIMARY KEY ([IdFpag])
GO

-- ----------------------------
-- Indexes structure for table HistoricoActivo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table HistoricoActivo
-- ----------------------------
ALTER TABLE [dbo].[HistoricoActivo] ADD PRIMARY KEY ([consec])
GO

-- ----------------------------
-- Indexes structure for table hotels
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table hotels
-- ----------------------------
ALTER TABLE [dbo].[hotels] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Incidencias
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Incidencias
-- ----------------------------
ALTER TABLE [dbo].[Incidencias] ADD PRIMARY KEY ([Idincid])
GO

-- ----------------------------
-- Indexes structure for table Liquidacion
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Liquidacion
-- ----------------------------
ALTER TABLE [dbo].[Liquidacion] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table ListaCombo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ListaCombo
-- ----------------------------
ALTER TABLE [dbo].[ListaCombo] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ListaD
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ListaD
-- ----------------------------
ALTER TABLE [dbo].[ListaD] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ListaP
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ListaP
-- ----------------------------
ALTER TABLE [dbo].[ListaP] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ListaPromo
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ListaPromo
-- ----------------------------
ALTER TABLE [dbo].[ListaPromo] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ListaPromoMaster
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ListaPromoMaster
-- ----------------------------
ALTER TABLE [dbo].[ListaPromoMaster] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Medidores
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Medidores
-- ----------------------------
ALTER TABLE [dbo].[Medidores] ADD PRIMARY KEY ([IdRow])
GO

-- ----------------------------
-- Indexes structure for table Mensajes
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Mensajes
-- ----------------------------
ALTER TABLE [dbo].[Mensajes] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table Mermas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Mermas
-- ----------------------------
ALTER TABLE [dbo].[Mermas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table MotivosNoVenta
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MotivosNoVenta
-- ----------------------------
ALTER TABLE [dbo].[MotivosNoVenta] ADD PRIMARY KEY ([IdMot])
GO

-- ----------------------------
-- Indexes structure for table MotivosSalida
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MotivosSalida
-- ----------------------------
ALTER TABLE [dbo].[MotivosSalida] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table MovInvAlm
-- ----------------------------
CREATE INDEX [IDXMOVINVALMFEC] ON [dbo].[MovInvAlm]
([Fecha] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table MovInvAlm
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlm] ADD PRIMARY KEY ([IdMov])
GO

-- ----------------------------
-- Checks structure for table MovInvAlm
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlm] ADD CHECK (([TIPOMOV] IS NULL OR ([TIPOMOV]='S' OR [TIPOMOV]='E') AND [TIPOMOV]=upper([TIPOMOV])))
GO

-- ----------------------------
-- Indexes structure for table MovInvAlmCom
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MovInvAlmCom
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlmCom] ADD PRIMARY KEY ([IdMov])
GO

-- ----------------------------
-- Checks structure for table MovInvAlmCom
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlmCom] ADD CHECK (([TIPOMOV] IS NULL OR ([TIPOMOV]='S' OR [TIPOMOV]='E') AND [TIPOMOV]=upper([TIPOMOV])))
GO

-- ----------------------------
-- Indexes structure for table MvoDev
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MvoDev
-- ----------------------------
ALTER TABLE [dbo].[MvoDev] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table MvoMerma
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table MvoMerma
-- ----------------------------
ALTER TABLE [dbo].[MvoMerma] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table MvtosInv
-- ----------------------------
CREATE INDEX [idxmvtinvfec] ON [dbo].[MvtosInv]
([fecha] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table MvtosInv
-- ----------------------------
ALTER TABLE [dbo].[MvtosInv] ADD PRIMARY KEY ([IdMovStock])
GO

-- ----------------------------
-- Indexes structure for table Noventas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Noventas
-- ----------------------------
ALTER TABLE [dbo].[Noventas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table OperacionD
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table OperacionD
-- ----------------------------
ALTER TABLE [dbo].[OperacionD] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Pagos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Pagos
-- ----------------------------
ALTER TABLE [dbo].[Pagos] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table PContado
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PContado
-- ----------------------------
ALTER TABLE [dbo].[PContado] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table PedDiaSig
-- ----------------------------
CREATE INDEX [idxpdsfec] ON [dbo].[PedDiaSig]
([Fecha] ASC) 
GO
CREATE INDEX [IX_PedDiaSig] ON [dbo].[PedDiaSig]
([Articulo] ASC, [Diao] ASC, [IdRuta] ASC, [IdEmpresa] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table PedDiaSig
-- ----------------------------
ALTER TABLE [dbo].[PedDiaSig] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table PedDiaSigPzs
-- ----------------------------
CREATE INDEX [IX_PedDiaSigPzs] ON [dbo].[PedDiaSigPzs]
([Articulo] ASC, [Diao] ASC, [IdRuta] ASC, [IdEmpresa] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table PedDiaSigPzs
-- ----------------------------
ALTER TABLE [dbo].[PedDiaSigPzs] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table Pedidos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Pedidos
-- ----------------------------
ALTER TABLE [dbo].[Pedidos] ADD PRIMARY KEY ([IdPedido])
GO

-- ----------------------------
-- Indexes structure for table PEDIDOSLIBERADOS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PEDIDOSLIBERADOS
-- ----------------------------
ALTER TABLE [dbo].[PEDIDOSLIBERADOS] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table Perfiles
-- ----------------------------
CREATE INDEX [idx_modulo] ON [dbo].[Perfiles]
([Modulo] ASC) 
GO
CREATE INDEX [idx_perfil] ON [dbo].[Perfiles]
([IdUser] ASC, [Codigo] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Perfiles
-- ----------------------------
ALTER TABLE [dbo].[Perfiles] ADD PRIMARY KEY ([IdPerfiles])
GO

-- ----------------------------
-- Indexes structure for table Preg_Enc
-- ----------------------------
CREATE INDEX [idx_clave] ON [dbo].[Preg_Enc]
([Clave_Enc] ASC) 
GO
CREATE INDEX [idx_pregunta] ON [dbo].[Preg_Enc]
([Num_Preg] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Preg_Enc
-- ----------------------------
ALTER TABLE [dbo].[Preg_Enc] ADD PRIMARY KEY ([Clave_Enc], [Num_Preg])
GO

-- ----------------------------
-- Indexes structure for table PRegalado
-- ----------------------------
CREATE INDEX [idxpregcli] ON [dbo].[PRegalado]
([Cliente] ASC) 
GO
CREATE INDEX [idxpregdiarut] ON [dbo].[PRegalado]
([DiaO] ASC, [RutaId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table PRegalado
-- ----------------------------
ALTER TABLE [dbo].[PRegalado] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table ProductoGenerico
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ProductoGenerico
-- ----------------------------
ALTER TABLE [dbo].[ProductoGenerico] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table ProductoNegado
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ProductoNegado
-- ----------------------------
ALTER TABLE [dbo].[ProductoNegado] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table ProductoPaseado
-- ----------------------------
CREATE INDEX [IX_ProductoPaseado] ON [dbo].[ProductoPaseado]
([CodProd] ASC, [DiaO] ASC, [IdEmpresa] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table ProductoPaseado
-- ----------------------------
ALTER TABLE [dbo].[ProductoPaseado] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Productos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Productos
-- ----------------------------
ALTER TABLE [dbo].[Productos] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table ProductosXPzas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ProductosXPzas
-- ----------------------------
ALTER TABLE [dbo].[ProductosXPzas] ADD PRIMARY KEY ([Producto], [IdEmpresa])
GO

-- ----------------------------
-- Indexes structure for table Promociones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Promociones
-- ----------------------------
ALTER TABLE [dbo].[Promociones] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table PVendidos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PVendidos
-- ----------------------------
ALTER TABLE [dbo].[PVendidos] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table RanComisiones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RanComisiones
-- ----------------------------
ALTER TABLE [dbo].[RanComisiones] ADD PRIMARY KEY ([IdRow])
GO

-- ----------------------------
-- Indexes structure for table Recarga
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Recarga
-- ----------------------------
ALTER TABLE [dbo].[Recarga] ADD PRIMARY KEY ([ID])
GO

-- ----------------------------
-- Indexes structure for table RegLicencias
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RegLicencias
-- ----------------------------
ALTER TABLE [dbo].[RegLicencias] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table RegPC
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RegPC
-- ----------------------------
ALTER TABLE [dbo].[RegPC] ADD PRIMARY KEY ([Clave])
GO

-- ----------------------------
-- Indexes structure for table Rel_EncRut
-- ----------------------------
CREATE INDEX [idx_clave] ON [dbo].[Rel_EncRut]
([Clave_Enc] ASC) 
GO

-- ----------------------------
-- Indexes structure for table RelActivos
-- ----------------------------
CREATE INDEX [idxrelactcli] ON [dbo].[RelActivos]
([Cliente] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelActivos
-- ----------------------------
ALTER TABLE [dbo].[RelActivos] ADD PRIMARY KEY ([IdRow])
GO

-- ----------------------------
-- Indexes structure for table RelCliClas
-- ----------------------------
CREATE INDEX [idxrelclscli] ON [dbo].[RelCliClas]
([IdCliente] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelCliClas
-- ----------------------------
ALTER TABLE [dbo].[RelCliClas] ADD PRIMARY KEY ([IdRcC])
GO

-- ----------------------------
-- Indexes structure for table RelCliLis
-- ----------------------------
CREATE INDEX [idxrelliscli] ON [dbo].[RelCliLis]
([CodCliente] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelCliLis
-- ----------------------------
ALTER TABLE [dbo].[RelCliLis] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelClirutas
-- ----------------------------
CREATE INDEX [idx_cliente] ON [dbo].[RelClirutas]
([IdCliente] ASC) 
GO
CREATE INDEX [idx_ruta] ON [dbo].[RelClirutas]
([IdRuta] ASC) 
GO
CREATE INDEX [idxrelrutcli] ON [dbo].[RelClirutas]
([IdCliente] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelClirutas
-- ----------------------------
ALTER TABLE [dbo].[RelClirutas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelCuoVend
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelCuoVend
-- ----------------------------
ALTER TABLE [dbo].[RelCuoVend] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelDayCli
-- ----------------------------
CREATE INDEX [idx_cliente] ON [dbo].[RelDayCli]
([CodCli] ASC) 
GO
CREATE INDEX [idx_ruta] ON [dbo].[RelDayCli]
([RutaId] ASC) 
GO
CREATE INDEX [idxreldaycli] ON [dbo].[RelDayCli]
([CodCli] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelDayCli
-- ----------------------------
ALTER TABLE [dbo].[RelDayCli] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelGastos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelGastos
-- ----------------------------
ALTER TABLE [dbo].[RelGastos] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelMens
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelMens
-- ----------------------------
ALTER TABLE [dbo].[RelMens] ADD PRIMARY KEY ([IDRow])
GO

-- ----------------------------
-- Indexes structure for table RelOperaciones
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelOperaciones
-- ----------------------------
ALTER TABLE [dbo].[RelOperaciones] ADD PRIMARY KEY ([IdK])
GO

-- ----------------------------
-- Indexes structure for table RelProClas
-- ----------------------------
CREATE INDEX [idx_ProductoId] ON [dbo].[RelProClas]
([ProductoId] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table RelProClas
-- ----------------------------
ALTER TABLE [dbo].[RelProClas] ADD PRIMARY KEY ([IdRelPClas])
GO

-- ----------------------------
-- Indexes structure for table RelProGrupos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelProGrupos
-- ----------------------------
ALTER TABLE [dbo].[RelProGrupos] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table RelRuClas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelRuClas
-- ----------------------------
ALTER TABLE [dbo].[RelRuClas] ADD PRIMARY KEY ([IdRelRClas])
GO

-- ----------------------------
-- Indexes structure for table RelVendrutas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table RelVendrutas
-- ----------------------------
ALTER TABLE [dbo].[RelVendrutas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table ResCuotas
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ResCuotas
-- ----------------------------
ALTER TABLE [dbo].[ResCuotas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Resp_Enc
-- ----------------------------
CREATE INDEX [idx_clave] ON [dbo].[Resp_Enc]
([Clave_Enc] ASC) 
GO
CREATE INDEX [idx_pregunta] ON [dbo].[Resp_Enc]
([Num_Pregunta] ASC, [IdRuta] ASC) 
GO

-- ----------------------------
-- Indexes structure for table room_categories
-- ----------------------------
CREATE INDEX [index_room_categories_on_hotel_id] ON [dbo].[room_categories]
([hotel_id] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table room_categories
-- ----------------------------
ALTER TABLE [dbo].[room_categories] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table Rutas
-- ----------------------------
CREATE INDEX [idxrutayud] ON [dbo].[Rutas]
([id_ayudante1] ASC) 
GO
CREATE INDEX [idxrutvend] ON [dbo].[Rutas]
([Vendedor] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Rutas
-- ----------------------------
ALTER TABLE [dbo].[Rutas] ADD PRIMARY KEY ([IdRutas])
GO

-- ----------------------------
-- Indexes structure for table Sabores
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Sabores
-- ----------------------------
ALTER TABLE [dbo].[Sabores] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table schema_migrations
-- ----------------------------
CREATE UNIQUE INDEX [unique_schema_migrations] ON [dbo].[schema_migrations]
([version] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Indexes structure for table SecRutas
-- ----------------------------
CREATE INDEX [idxRuta] ON [dbo].[SecRutas]
([Rutad] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table SecRutas
-- ----------------------------
ALTER TABLE [dbo].[SecRutas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table segmentos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table segmentos
-- ----------------------------
ALTER TABLE [dbo].[segmentos] ADD PRIMARY KEY ([idsegmento])
GO

-- ----------------------------
-- Indexes structure for table Stock
-- ----------------------------
CREATE INDEX [idxstockart] ON [dbo].[Stock]
([Articulo] ASC) 
GO
CREATE INDEX [idxstockrut] ON [dbo].[Stock]
([Ruta] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Stock
-- ----------------------------
ALTER TABLE [dbo].[Stock] ADD PRIMARY KEY ([IdStock])
GO

-- ----------------------------
-- Indexes structure for table StockHistorico
-- ----------------------------
CREATE INDEX [idxArticulo] ON [dbo].[StockHistorico]
([Articulo] ASC) 
GO
CREATE INDEX [idxFecha] ON [dbo].[StockHistorico]
([Fecha] ASC) 
GO
CREATE INDEX [idxRuta] ON [dbo].[StockHistorico]
([RutaID] ASC) 
GO
CREATE INDEX [idxsthidfec] ON [dbo].[StockHistorico]
([Fecha] ASC) 
GO

-- ----------------------------
-- Indexes structure for table StockPedidos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table StockPedidos
-- ----------------------------
ALTER TABLE [dbo].[StockPedidos] ADD PRIMARY KEY ([id])
GO

-- ----------------------------
-- Indexes structure for table sysdiagrams
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table sysdiagrams
-- ----------------------------
ALTER TABLE [dbo].[sysdiagrams] ADD PRIMARY KEY ([diagram_id])
GO

-- ----------------------------
-- Uniques structure for table sysdiagrams
-- ----------------------------
ALTER TABLE [dbo].[sysdiagrams] ADD UNIQUE ([principal_id] ASC, [name] ASC)
GO

-- ----------------------------
-- Indexes structure for table TD_Comision
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TD_Comision
-- ----------------------------
ALTER TABLE [dbo].[TD_Comision] ADD PRIMARY KEY ([ID_Comision], [ID_Detalle])
GO

-- ----------------------------
-- Indexes structure for table Td_StockAlmacen
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Td_StockAlmacen
-- ----------------------------
ALTER TABLE [dbo].[Td_StockAlmacen] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table TH_Comision
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TH_Comision
-- ----------------------------
ALTER TABLE [dbo].[TH_Comision] ADD PRIMARY KEY ([ID_Comision])
GO

-- ----------------------------
-- Indexes structure for table TH_SecVisitas
-- ----------------------------
CREATE INDEX [IX_TH_SecVisitas] ON [dbo].[TH_SecVisitas]
([RutaId] ASC, [Dia] ASC, [IdEmpresa] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table TH_SecVisitas
-- ----------------------------
ALTER TABLE [dbo].[TH_SecVisitas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table TInvFis
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TInvFis
-- ----------------------------
ALTER TABLE [dbo].[TInvFis] ADD PRIMARY KEY ([IdRow])
GO

-- ----------------------------
-- Indexes structure for table Usuarios
-- ----------------------------
CREATE UNIQUE INDEX [index_usuarios_on_usuario] ON [dbo].[Usuarios]
([usuario] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE UNIQUE INDEX [index_usuarios_on_email] ON [dbo].[Usuarios]
([email] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO
CREATE INDEX [index_usuarios_on_empresamadre_id] ON [dbo].[Usuarios]
([empresamadre_id] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Usuarios
-- ----------------------------
ALTER TABLE [dbo].[Usuarios] ADD PRIMARY KEY ([IdUsuario])
GO

-- ----------------------------
-- Indexes structure for table Vehiculos
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table Vehiculos
-- ----------------------------
ALTER TABLE [dbo].[Vehiculos] ADD PRIMARY KEY ([IdVehiculos])
GO

-- ----------------------------
-- Indexes structure for table Vendedores
-- ----------------------------
CREATE UNIQUE INDEX [idxvendedores] ON [dbo].[Vendedores]
([Clave] ASC) 
WITH (IGNORE_DUP_KEY = ON)
GO

-- ----------------------------
-- Primary Key structure for table Vendedores
-- ----------------------------
ALTER TABLE [dbo].[Vendedores] ADD PRIMARY KEY ([IdVendedor])
GO

-- ----------------------------
-- Indexes structure for table Venta
-- ----------------------------
CREATE INDEX [idxvenfec] ON [dbo].[Venta]
([Fecha] ASC) 
GO
CREATE INDEX [IX_Venta] ON [dbo].[Venta]
([RutaId] ASC, [Documento] ASC) 
GO
CREATE INDEX [IX_Venta_TipoVta] ON [dbo].[Venta]
([TipoVta] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Venta
-- ----------------------------
ALTER TABLE [dbo].[Venta] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Indexes structure for table Visitas
-- ----------------------------
CREATE INDEX [idxvisfec] ON [dbo].[Visitas]
([FechaI] ASC) 
GO
CREATE INDEX [idxvisfecf] ON [dbo].[Visitas]
([FechaF] ASC) 
GO
CREATE INDEX [IX_Visitas] ON [dbo].[Visitas]
([CodCliente] ASC, [DiaO] ASC, [ruta] ASC) 
GO

-- ----------------------------
-- Primary Key structure for table Visitas
-- ----------------------------
ALTER TABLE [dbo].[Visitas] ADD PRIMARY KEY ([Id])
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Activos]
-- ----------------------------
ALTER TABLE [dbo].[Activos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Ayudantes]
-- ----------------------------
ALTER TABLE [dbo].[Ayudantes] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[BACKORDER]
-- ----------------------------
ALTER TABLE [dbo].[BACKORDER] ADD FOREIGN KEY ([IDEMPRESA]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[BitacoraCuotas]
-- ----------------------------
ALTER TABLE [dbo].[BitacoraCuotas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[BitacoraTiempos]
-- ----------------------------
ALTER TABLE [dbo].[BitacoraTiempos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[BitacoraTiempos] ADD FOREIGN KEY ([DiaO], [RutaId]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Cancelaciones]
-- ----------------------------
ALTER TABLE [dbo].[Cancelaciones] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CargaRuta]
-- ----------------------------
ALTER TABLE [dbo].[CargaRuta] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[CargaRuta] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ClasClientes]
-- ----------------------------
ALTER TABLE [dbo].[ClasClientes] ADD FOREIGN KEY ([idsegmento]) REFERENCES [dbo].[segmentos] ([idsegmento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ClasifComp]
-- ----------------------------
ALTER TABLE [dbo].[ClasifComp] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Clientes]
-- ----------------------------
ALTER TABLE [dbo].[Clientes] ADD FOREIGN KEY ([idclasc]) REFERENCES [dbo].[ClasClientes] ([IdClasC]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes] ADD FOREIGN KEY ([idclasc]) REFERENCES [dbo].[segmentos] ([idsegmento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Clientes_copy]
-- ----------------------------
ALTER TABLE [dbo].[Clientes_copy] ADD FOREIGN KEY ([idclasc]) REFERENCES [dbo].[ClasClientes] ([IdClasC]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes_copy] ADD FOREIGN KEY ([idclasc]) REFERENCES [dbo].[segmentos] ([idsegmento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes_copy] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes_copy] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Clientes_copy] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Cobranza]
-- ----------------------------
ALTER TABLE [dbo].[Cobranza] ADD FOREIGN KEY ([Cliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Cobranza] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Cobranza] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CodesOp]
-- ----------------------------
ALTER TABLE [dbo].[CodesOp] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Comisiones]
-- ----------------------------
ALTER TABLE [dbo].[Comisiones] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ComponentesArt]
-- ----------------------------
ALTER TABLE [dbo].[ComponentesArt] ADD FOREIGN KEY ([Id_Clasif]) REFERENCES [dbo].[ClasifComp] ([Id_Clasif]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[ComponentesArt] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ConfigRutasP]
-- ----------------------------
ALTER TABLE [dbo].[ConfigRutasP] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ConfigRutasP] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Configuracion]
-- ----------------------------
ALTER TABLE [dbo].[Configuracion] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ConfiguracionGral]
-- ----------------------------
ALTER TABLE [dbo].[ConfiguracionGral] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Consolidado]
-- ----------------------------
ALTER TABLE [dbo].[Consolidado] ADD FOREIGN KEY ([Id_Empresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Consolidado] ADD FOREIGN KEY ([Ruta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ContadorNC]
-- ----------------------------
ALTER TABLE [dbo].[ContadorNC] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CteImagen]
-- ----------------------------
ALTER TABLE [dbo].[CteImagen] ADD FOREIGN KEY ([IdCli]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[CTiket]
-- ----------------------------
ALTER TABLE [dbo].[CTiket] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Cuotas]
-- ----------------------------
ALTER TABLE [dbo].[Cuotas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Cuotas] ADD FOREIGN KEY ([Producto]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DETALLE_BO]
-- ----------------------------
ALTER TABLE [dbo].[DETALLE_BO] ADD FOREIGN KEY ([IDEMPRESA]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleCob]
-- ----------------------------
ALTER TABLE [dbo].[DetalleCob] ADD FOREIGN KEY ([IdCobranza]) REFERENCES [dbo].[Cobranza] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[DetalleCob] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleCob] ADD FOREIGN KEY ([FormaP]) REFERENCES [dbo].[FormasPag] ([IdFpag]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleCombo]
-- ----------------------------
ALTER TABLE [dbo].[DetalleCombo] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleCombo] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleDevol]
-- ----------------------------
ALTER TABLE [dbo].[DetalleDevol] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleDevol] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleLD]
-- ----------------------------
ALTER TABLE [dbo].[DetalleLD] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleLD] ADD FOREIGN KEY ([ListaId]) REFERENCES [dbo].[ListaD] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[DetalleLD] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleLP]
-- ----------------------------
ALTER TABLE [dbo].[DetalleLP] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleLP] ADD FOREIGN KEY ([ListaId]) REFERENCES [dbo].[ListaP] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[DetalleLP] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleLProMaster]
-- ----------------------------
ALTER TABLE [dbo].[DetalleLProMaster] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DETALLEPEDIDOLIBERADO]
-- ----------------------------
ALTER TABLE [dbo].[DETALLEPEDIDOLIBERADO] ADD FOREIGN KEY ([IDEMPRESA]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[DETALLEPEDIDOLIBERADO] ADD FOREIGN KEY ([INCIDENCIA]) REFERENCES [dbo].[Incidencias] ([Idincid]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetallePedidos]
-- ----------------------------
ALTER TABLE [dbo].[DetallePedidos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetallePromo]
-- ----------------------------
ALTER TABLE [dbo].[DetallePromo] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetallePromo] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleSalidas]
-- ----------------------------
ALTER TABLE [dbo].[DetalleSalidas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleSalidas] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DetalleVet]
-- ----------------------------
ALTER TABLE [dbo].[DetalleVet] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetalleVet] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DevEnvases]
-- ----------------------------
ALTER TABLE [dbo].[DevEnvases] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DevEnvases] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Devoluciones]
-- ----------------------------
ALTER TABLE [dbo].[Devoluciones] ADD FOREIGN KEY ([DiaO], [Ruta]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Devoluciones] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DiasO]
-- ----------------------------
ALTER TABLE [dbo].[DiasO] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[DiasOperativos]
-- ----------------------------
ALTER TABLE [dbo].[DiasOperativos] ADD FOREIGN KEY ([Dia], [RutaId]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Empresas]
-- ----------------------------
ALTER TABLE [dbo].[Empresas] ADD FOREIGN KEY ([empresamadre_id]) REFERENCES [dbo].[empresasmadre] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Encuestas]
-- ----------------------------
ALTER TABLE [dbo].[Encuestas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Liquidacion]
-- ----------------------------
ALTER TABLE [dbo].[Liquidacion] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ListaCombo]
-- ----------------------------
ALTER TABLE [dbo].[ListaCombo] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ListaD]
-- ----------------------------
ALTER TABLE [dbo].[ListaD] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ListaP]
-- ----------------------------
ALTER TABLE [dbo].[ListaP] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ListaPromo]
-- ----------------------------
ALTER TABLE [dbo].[ListaPromo] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ListaPromoMaster]
-- ----------------------------
ALTER TABLE [dbo].[ListaPromoMaster] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Medidores]
-- ----------------------------
ALTER TABLE [dbo].[Medidores] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Medidores] ADD FOREIGN KEY ([DiaO], [IdRuta]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Mensajes]
-- ----------------------------
ALTER TABLE [dbo].[Mensajes] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Mermas]
-- ----------------------------
ALTER TABLE [dbo].[Mermas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Mermas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[MovInvAlm]
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlm] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[MovInvAlmCom]
-- ----------------------------
ALTER TABLE [dbo].[MovInvAlmCom] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[MovInvAlmCom] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[MvtosInv]
-- ----------------------------
ALTER TABLE [dbo].[MvtosInv] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[MvtosInv] ADD FOREIGN KEY ([RutaD]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Noventas]
-- ----------------------------
ALTER TABLE [dbo].[Noventas] ADD FOREIGN KEY ([Cliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Noventas] ADD FOREIGN KEY ([VendedorId]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Noventas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Noventas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Opc_Enc]
-- ----------------------------
ALTER TABLE [dbo].[Opc_Enc] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Opc_Enc] ADD FOREIGN KEY ([Clave_Enc]) REFERENCES [dbo].[Encuestas] ([Clave_Enc]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[OperacionD]
-- ----------------------------
ALTER TABLE [dbo].[OperacionD] ADD FOREIGN KEY ([DiaO], [Ruta]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Pagos]
-- ----------------------------
ALTER TABLE [dbo].[Pagos] ADD FOREIGN KEY ([DiaO], [RutaId]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PContado]
-- ----------------------------
ALTER TABLE [dbo].[PContado] ADD FOREIGN KEY ([DiaO], [RutaId]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[PContado] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PedDiaSig]
-- ----------------------------
ALTER TABLE [dbo].[PedDiaSig] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PedDiaSig] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PedDiaSig] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PedDiaSigPzs]
-- ----------------------------
ALTER TABLE [dbo].[PedDiaSigPzs] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PedDiaSigPzs] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Pedidos]
-- ----------------------------
ALTER TABLE [dbo].[Pedidos] ADD FOREIGN KEY ([DiaO], [Ruta]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Pedidos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Pedidos] ADD FOREIGN KEY ([FormaPag]) REFERENCES [dbo].[FormasPag] ([IdFpag]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PEDIDOSLIBERADOS]
-- ----------------------------
ALTER TABLE [dbo].[PEDIDOSLIBERADOS] ADD FOREIGN KEY ([IDEMPRESA]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[PEDIDOSLIBERADOS] ADD FOREIGN KEY ([INCIDENCIA]) REFERENCES [dbo].[Incidencias] ([Idincid]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[PEDIDOSLIBERADOS] ADD FOREIGN KEY ([FormaPag]) REFERENCES [dbo].[FormasPag] ([IdFpag]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Preg_Enc]
-- ----------------------------
ALTER TABLE [dbo].[Preg_Enc] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Preg_Enc] ADD FOREIGN KEY ([Clave_Enc]) REFERENCES [dbo].[Encuestas] ([Clave_Enc]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PRegalado]
-- ----------------------------
ALTER TABLE [dbo].[PRegalado] ADD FOREIGN KEY ([Cliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[PRegalado] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ProductoEnvase]
-- ----------------------------
ALTER TABLE [dbo].[ProductoEnvase] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ProductoEnvase] ADD FOREIGN KEY ([Producto]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ProductoNegado]
-- ----------------------------
ALTER TABLE [dbo].[ProductoNegado] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ProductoNegado] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ProductoPaseado]
-- ----------------------------
ALTER TABLE [dbo].[ProductoPaseado] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ProductoPaseado] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Productos]
-- ----------------------------
ALTER TABLE [dbo].[Productos] ADD FOREIGN KEY ([idclasp]) REFERENCES [dbo].[ClasProductos] ([IdClasP]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Productos] ADD FOREIGN KEY ([idclasp]) REFERENCES [dbo].[ClasProductos] ([IdClasP]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Productos] ADD FOREIGN KEY ([UniMed]) REFERENCES [dbo].[CatUnidadMedida] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Productos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ProductosXPzas]
-- ----------------------------
ALTER TABLE [dbo].[ProductosXPzas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ProductosXPzas] ADD FOREIGN KEY ([Producto]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[PVendidos]
-- ----------------------------
ALTER TABLE [dbo].[PVendidos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PVendidos] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Recarga]
-- ----------------------------
ALTER TABLE [dbo].[Recarga] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Recarga] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Rel_EncRut]
-- ----------------------------
ALTER TABLE [dbo].[Rel_EncRut] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Rel_EncRut] ADD FOREIGN KEY ([Id_Ruta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelActivos]
-- ----------------------------
ALTER TABLE [dbo].[RelActivos] ADD FOREIGN KEY ([Activo]) REFERENCES [dbo].[Activos] ([IdActivos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelActivos] ADD FOREIGN KEY ([Cliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelActivos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelCliClas]
-- ----------------------------
ALTER TABLE [dbo].[RelCliClas] ADD FOREIGN KEY ([IdCliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelCliClas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelCliLis]
-- ----------------------------
ALTER TABLE [dbo].[RelCliLis] ADD FOREIGN KEY ([CodCliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelCliLis] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelClirutas]
-- ----------------------------
ALTER TABLE [dbo].[RelClirutas] ADD FOREIGN KEY ([IdCliente]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelClirutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelClirutas] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelCuoVend]
-- ----------------------------
ALTER TABLE [dbo].[RelCuoVend] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelCuoVend] ADD FOREIGN KEY ([VendeId]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelDayCli]
-- ----------------------------
ALTER TABLE [dbo].[RelDayCli] ADD FOREIGN KEY ([CodCli]) REFERENCES [dbo].[Clientes] ([IdCli]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelDayCli] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelGastos]
-- ----------------------------
ALTER TABLE [dbo].[RelGastos] ADD FOREIGN KEY ([CodigoOP]) REFERENCES [dbo].[CodesOp] ([Codi]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelGastos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelGastos] ADD FOREIGN KEY ([Id]) REFERENCES [dbo].[RelGastos] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelGastos] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelMens]
-- ----------------------------
ALTER TABLE [dbo].[RelMens] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelMens] ADD FOREIGN KEY ([CodRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelOperaciones]
-- ----------------------------
ALTER TABLE [dbo].[RelOperaciones] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelOperaciones] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelProClas]
-- ----------------------------
ALTER TABLE [dbo].[RelProClas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelProClas] ADD FOREIGN KEY ([ProductoId]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelProGrupos]
-- ----------------------------
ALTER TABLE [dbo].[RelProGrupos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelProGrupos] ADD FOREIGN KEY ([ProductoId]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelRuClas]
-- ----------------------------
ALTER TABLE [dbo].[RelRuClas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelRuClas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[RelVendrutas]
-- ----------------------------
ALTER TABLE [dbo].[RelVendrutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RelVendrutas] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[RelVendrutas] ADD FOREIGN KEY ([IdVendedor]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[ResCuotas]
-- ----------------------------
ALTER TABLE [dbo].[ResCuotas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ResCuotas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Resp_Enc]
-- ----------------------------
ALTER TABLE [dbo].[Resp_Enc] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Resp_Enc] ADD FOREIGN KEY ([IdRuta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[room_categories]
-- ----------------------------
ALTER TABLE [dbo].[room_categories] ADD FOREIGN KEY ([hotel_id]) REFERENCES [dbo].[hotels] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Rutas]
-- ----------------------------
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([idcrutas]) REFERENCES [dbo].[ClasRutas] ([IdCRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([idcrutas]) REFERENCES [dbo].[ClasRutas] ([IdCRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([vehiculo]) REFERENCES [dbo].[Vehiculos] ([IdVehiculos]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Rutas] ADD FOREIGN KEY ([Vendedor]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[SecRutas]
-- ----------------------------
ALTER TABLE [dbo].[SecRutas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SecRutas] ADD FOREIGN KEY ([Rutad]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Stock]
-- ----------------------------
ALTER TABLE [dbo].[Stock] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Stock] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Stock] ADD FOREIGN KEY ([Ruta]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[StockHistorico]
-- ----------------------------
ALTER TABLE [dbo].[StockHistorico] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[StockHistorico] ADD FOREIGN KEY ([Articulo]) REFERENCES [dbo].[Productos] ([Clave]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[StockPedidos]
-- ----------------------------
ALTER TABLE [dbo].[StockPedidos] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[TD_Comision]
-- ----------------------------
ALTER TABLE [dbo].[TD_Comision] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TD_Comision] ADD FOREIGN KEY ([ID_Comision]) REFERENCES [dbo].[TH_Comision] ([ID_Comision]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Td_StockAlmacen]
-- ----------------------------
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([Comp1]) REFERENCES [dbo].[ComponentesArt] ([Complemento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([Comp2]) REFERENCES [dbo].[ComponentesArt] ([Complemento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([Comp3]) REFERENCES [dbo].[ComponentesArt] ([Complemento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([Comp4]) REFERENCES [dbo].[ComponentesArt] ([Complemento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([Comp5]) REFERENCES [dbo].[ComponentesArt] ([Complemento]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Td_StockAlmacen] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[TH_Comision]
-- ----------------------------
ALTER TABLE [dbo].[TH_Comision] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[TH_SecVisitas]
-- ----------------------------
ALTER TABLE [dbo].[TH_SecVisitas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TH_SecVisitas] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[TH_SecVisitas] ADD FOREIGN KEY ([IdVendedor]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Th_StockAlmacen]
-- ----------------------------
ALTER TABLE [dbo].[Th_StockAlmacen] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Usuarios]
-- ----------------------------
ALTER TABLE [dbo].[Usuarios] ADD FOREIGN KEY ([empresamadre_id]) REFERENCES [dbo].[empresasmadre] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Usuarios] ADD FOREIGN KEY ([empresa_id]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Vehiculos]
-- ----------------------------
ALTER TABLE [dbo].[Vehiculos] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Vendedores]
-- ----------------------------
ALTER TABLE [dbo].[Vendedores] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Venta]
-- ----------------------------
ALTER TABLE [dbo].[Venta] ADD FOREIGN KEY ([FormaPag]) REFERENCES [dbo].[FormasPag] ([IdFpag]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Venta] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Venta] ADD FOREIGN KEY ([RutaId]) REFERENCES [dbo].[Rutas] ([IdRutas]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Venta] ADD FOREIGN KEY ([VendedorId]) REFERENCES [dbo].[Vendedores] ([IdVendedor]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

-- ----------------------------
-- Foreign Key structure for table [dbo].[Visitas]
-- ----------------------------
ALTER TABLE [dbo].[Visitas] ADD FOREIGN KEY ([DiaO], [ruta]) REFERENCES [dbo].[DiasO] ([DiaO], [RutaId]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO
ALTER TABLE [dbo].[Visitas] ADD FOREIGN KEY ([IdEmpresa]) REFERENCES [dbo].[Empresas] ([IdEmpresa]) ON DELETE CASCADE ON UPDATE CASCADE
GO