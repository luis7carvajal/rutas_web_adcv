class List < ActiveRecord::Base
  self.table_name = 'ListaP' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :comp_si_es_por_rango_de_precios, ->(lista_id) { where(id: lista_id).first } # para verificar si es lista de precio normal o por rango de precios. se debe colocar .first porque asi no devuelve una coleccion sino un solo objeto
  has_many :detallelp, class_name: "Deta", foreign_key: "ListaId"
  accepts_nested_attributes_for :detallelp

  validates :Lista, presence: true
  validates :FechaIni, presence: true
  validates :FechaFin, presence: true

  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |list|
        csv << list.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      list = find_by_id(row["ID"]) || new
      list.attributes = {id: row["ID"], Lista: row["Nombre"], Tipo: row["Tipo"], FechaIni: row["Fecha Inicial"], FechaFin: row["Fecha Final"], IdEmpresa: empresa}
      list.detallelp_attributes = [{Articulo: row["Articulo"], PrecioMin: row["Mínimo"], PrecioMax: row["Máximo"], IdEmpresa: empresa}]

      if list.save
        # stuff to do on successful save
       else
         list.errors.full_messages.each do |message|
           @errors << "Error fila #{i}, columna #{message}"
         end
       end


    end
    @errors #  <- need to return the @errors array
  end

  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

end
