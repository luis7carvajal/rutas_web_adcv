require 'test_helper'

class ListacomboControllerTest < ActionController::TestCase
  setup do
    @listcombo = listacombo(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:listacombo)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create listcombo" do
    assert_difference('Listcombo.count') do
      post :create, listcombo: { Activa: @listcombo.Activa, Caduca: @listcombo.Caduca, Clave: @listcombo.Clave, Descripcion: @listcombo.Descripcion, FechaF: @listcombo.FechaF, FechaI: @listcombo.FechaI, IdEmpresa: @listcombo.IdEmpresa, Monto: @listcombo.Monto }
    end

    assert_redirected_to listcombo_path(assigns(:listcombo))
  end

  test "should show listcombo" do
    get :show, id: @listcombo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @listcombo
    assert_response :success
  end

  test "should update listcombo" do
    patch :update, id: @listcombo, listcombo: { Activa: @listcombo.Activa, Caduca: @listcombo.Caduca, Clave: @listcombo.Clave, Descripcion: @listcombo.Descripcion, FechaF: @listcombo.FechaF, FechaI: @listcombo.FechaI, IdEmpresa: @listcombo.IdEmpresa, Monto: @listcombo.Monto }
    assert_redirected_to listcombo_path(assigns(:listcombo))
  end

  test "should destroy listcombo" do
    assert_difference('Listcombo.count', -1) do
      delete :destroy, id: @listcombo
    end

    assert_redirected_to listacombo_path
  end
end
