$(document).on("ready page:load", function() {

//los selectbox vendedores con chosen no les funcionaba la validacion porque la libreria los ocultaba, se debe colocar lo siguiente
$.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });

$(".activox").validate({
debug: true,
rules: {
"activo[CB]": {required: true, remote:'activos/activos_check_cb' }
}
});

$(".ruta-validado").validate({
debug: true,
rules: {
"ruta[Ruta]": {required: true, remote:'rutas/rutas_check' },
"ruta[Vendedor]": { required: true } //para validar que el selectbox no este vacio
}
});

$(".sabor-validado").validate({
debug: true,
rules: {
"sabor[Clave]": {required: true, remote:'sabores/sabor_check' }
}
});


$(".vendedor-validado").validate({
debug: true,
rules: {
"vendedor[Clave]": {required: true, remote:'vendedores/vendedores_check' },
"vendedor[PdaPw]": {required: true, remote:'vendedores/vendedores_pdapw_check' }
}
});

$(".vehiculo-validado").validate({
debug: true,
rules: {
"vehiculo[Clave]": {required: true, remote:'vehiculos/vehiculos_check' }
}
});

$(".cliente-validado").validate({
debug: true,
rules: {
"cliente[IdCli]": {required: true, remote:'clientes/clientes_check' }
}
});

$(".producto-validado").validate({
debug: true,
rules: {
"producto[Clave]": {required: true, remote:'productos/productos_check' },
"producto[CodBarras]": {required: true, remote:'productos/productos_codbarras_check' }
}
});

$(".promocion-validado").validate({
debug: true,
rules: {
"listaprom[Lista]": {required: true, remote:'listapromo/promociones_check' }
}
});

$(".mvmerma-validado").validate({
debug: true,
rules: {
"mvmerma[Clave]": {required: true, remote:'mvomerma/mvomerma_check' }
}
});

$(".formapag-validado").validate({
debug: true,
rules: {
"formapag[Clave]": {required: true, remote:'formaspag/formaspag_check' }
}
});

$(".motivonoventa-validado").validate({
debug: true,
rules: {
"motivonoventa[Clave]": {required: true, remote:'motivosnoventa/motivosnoventa_check' }
}
});

$(".mvodevolucion-validado").validate({
debug: true,
rules: {
"mvodevolucion[Clave]": {required: true, remote:'mvodev/mvodev_check' }
}
});

$(".usuario-validado").validate({
debug: true,
rules: {
"usuario[usuario]": {required: true, remote:'usuarios/usuarios_check' },
"usuario[email]": {required: true, remote:'usuarios/usuarios_check2' }
}
});

$(".new_usuario").validate({
  rules: {
    "usuario[password]": "required",
    "usuario[password_confirmation]": {
      equalTo: "#usuario_password"
    }
  }
});

$(".asig_clientes_entre_vendedores").validate(
{
    rules: {
      "vendedor": "required",
      "vendedor2": "required",
      "vendedor": {
        notEqualTo: "#vendedor2"
      },
      "vendedor2": {
        notEqualTo: "#vendedor"
      }
    }
});

$(".empresamadre-validado").validate({
debug: true,
rules: {
"empresamadre[id]": {required: true, remote:'empresasmadre/empresasmadre_check' },
"empresamadre[Url]": {required: true, remote:'empresasmadre/url_check' }
}
});

$(".empresa-validado").validate({
debug: true,
rules: {
"empresa[IdEmpresa]": {required: true, remote:'/empresa/empresas_check' }
}
});

$(".banco-validado").validate({
debug: true,
rules: {
"catbanco[Clave]": {required: true, remote:'/catbancos/banco_check' }
}
});

$(".catgrupo-validado").validate({
debug: true,
rules: {
"catgrupo[Clave]": {required: true, remote:'/catgrupos/catgrupo_check' }
}
});

$(".catmarca-validado").validate({
debug: true,
rules: {
"catmarca[Clave]": {required: true, remote:'/catmarcas/catmarcas_check' }
}
});


$(".catunidadmed-validado").validate({
debug: true,
rules: {
"catunidadmed[Clave]": {required: true, remote:'/catunidadmedida/catunidadmed_check' }
}
});

$(".incidencia-validado").validate({
debug: true,
rules: {
"incidencia[Clave]": {required: true, remote:'/incidencias/incidencia_check' }
}
});

$(".listcombo-validado").validate({
debug: true,
rules: {
"listcombo[Clave]": {required: true, remote:'/listacombo/listcombo_check' }
}
});

});
