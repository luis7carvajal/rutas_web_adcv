require 'test_helper'

class CteimagenControllerTest < ActionController::TestCase
  setup do
    @cteimage = cteimagen(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cteimagen)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cteimage" do
    assert_difference('Cteimage.count') do
      post :create, cteimage: { IdCli: @cteimage.IdCli, IdEmpresa: @cteimage.IdEmpresa }
    end

    assert_redirected_to cteimage_path(assigns(:cteimage))
  end

  test "should show cteimage" do
    get :show, id: @cteimage
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cteimage
    assert_response :success
  end

  test "should update cteimage" do
    patch :update, id: @cteimage, cteimage: { IdCli: @cteimage.IdCli, IdEmpresa: @cteimage.IdEmpresa }
    assert_redirected_to cteimage_path(assigns(:cteimage))
  end

  test "should destroy cteimage" do
    assert_difference('Cteimage.count', -1) do
      delete :destroy, id: @cteimage
    end

    assert_redirected_to cteimagen_path
  end
end
