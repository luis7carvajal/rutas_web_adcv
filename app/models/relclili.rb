class Relclili < ActiveRecord::Base
  self.table_name = 'RelCliLis' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  self.primary_key = "Id"
  #para buscar registros donde listad sea igual al parametro id que se para por el url, para su funcionamiento, se debe de colocar tambien en el controlador @relclilis = Relclili.clienteslistad(params[:id])
  scope :clienteslistad, -> (id_param) { where(listad: id_param) }
  scope :clienteslistap, -> (id_param) { where(listap: id_param) }
  scope :clientespromociones, -> (id_param) { where(ListaPromo: id_param) }
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }

  belongs_to :listapromomast, class_name:"Listapromomast", foreign_key: "ListaPromo"

  belongs_to :cliente, class_name:"Cliente", foreign_key: "CodCliente"


  def self.busqueda_descuento_para_pos(params)
    query = select('DetalleLD.Factor')
          .joins('inner join ListaD on RelCliLis.IdEmpresa=ListaD.IdEmpresa and RelCliLis.ListaD=ListaD.id
                  left join DetalleLD on ListaD.idempresa=DetalleLD.idempresa and ListaD.id=DetalleLD.ListaId')
          .where("(RelCliLis.CodCliente = :cliente) AND (DetalleLD.Articulo = :producto) AND (RelCliLis.IdEmpresa = :idempresa)",{cliente: params[:Cliente], producto: params[:Clave_producto], idempresa: params[:sucursal]})
    query
  end



end
