class RelprogruposController < ApplicationController
  before_action :set_relprogrupo, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  # GET /relprogrupos
  # GET /relprogrupos.json

  def productos_grupo
    contadores
    @grupo = Catgrupo.find_by_Clave(params[:id])
    @pathDeBusqueda = productos_grupo_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search_productos_in = Relprogrupo.productosquetiene(params[:id]).por_empresa(current_usuario.empresa_id).search(search_params)
    @search_productos_in.sorts = 'Clave' if @search_productos_in.sorts.empty?
    @relprogrupos = @search_productos_in.result().page(params[:productos_in]).per(10)

    @search_productos_out = Producto.activos.without_grupo(params[:id]).producto_sin_grupo_en_sucursal(current_usuario.empresa_id).search(search_params)
    @search_productos_out.sorts = 'Clave' if @search_productos_out.sorts.empty?
    @productos = @search_productos_out.result().page(params[:productos_out]).per(15)


    @relprogrupo = Relprogrupo.new

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def contadores
    @cantidad_productos_in = Relprogrupo.productosquetiene(params[:id]).por_empresa(current_usuario.empresa_id).count
    @cantidad_productos_out = Producto.activos.without_grupo(params[:id]).producto_sin_grupo_en_sucursal(current_usuario.empresa_id).count
  end

  # GET /relprogrupos/1
  # GET /relprogrupos/1.json
  def show
  end

  # GET /relprogrupos/new
  def new
    @relprogrupo = Relprogrupo.new
  end

  # GET /relprogrupos/1/edit
  def edit
  end

  # POST /relprogrupos
  # POST /relprogrupos.json

  def create
    @relprogrupo = Relprogrupo.new(relprogrupo_params)
    respond_to do |format|
      #verifica si existe el grupo
      @producto_asignado = Relprogrupo.find_by(ProductoId: @relprogrupo.ProductoId, IdEmpresa: current_usuario.empresa_id)
      if @producto_asignado == nil
        if @relprogrupo.save
          format.html { redirect_to @relprogrupo, notice: 'Relprogrupo was successfully created.' }
          format.json { render :show, status: :created, location: @relprogrupo }
          format.js {flash.now[:notice] = 'El grupo se ha creado de forma exitosa.'} #ajax
        else
          format.html { render :new }
          format.json { render json: @relprogrupo.errors, status: :unprocessable_entity }
          format.js {flash.now[:alert] = 'Error al crear el grupo.'} #ajax
        end
      else
        format.js {flash.now[:alert] = 'El producto ya fue añadido.'} #ajax
      end
      params[:id] = @relprogrupo.IdGrupo #<!--PARA QUE NO SE PIERDA EL ID DE LA LISTA CUANDO PASE POR EL EDITAR DEBIDO A QUE ESTARIA TOMANDO EL ID DEL REGISTRO Y NO EL DE LA LISTA-->
      contadores
    end
  end

  # PATCH/PUT /relprogrupos/1
  # PATCH/PUT /relprogrupos/1.json
  def update
    respond_to do |format|
      if @relprogrupo.update(relprogrupo_params)
        format.html { redirect_to @relprogrupo, notice: 'Relprogrupo was successfully updated.' }
        format.json { render :show, status: :ok, location: @relprogrupo }
      else
        format.html { render :edit }
        format.json { render json: @relprogrupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /relprogrupos/1
  # DELETE /relprogrupos/1.json
  def destroy
    @producto = Producto.find_by_Clave(@relprogrupo.ProductoId)
    @relprogrupo.destroy
    params[:id] = @relprogrupo.IdGrupo #<!--PARA QUE NO SE PIERDA EL ID DE LA LISTA CUANDO PASE POR EL EDITAR DEBIDO A QUE ESTARIA TOMANDO EL ID DEL REGISTRO Y NO EL DE LA LISTA-->
    contadores
    respond_to do |format|
      format.html { redirect_to relprogrupos_url, notice: 'Relprogrupo was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El grupo se ha borrado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relprogrupo
      @relprogrupo = Relprogrupo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relprogrupo_params
      params.require(:relprogrupo).permit(:IdEmpresa, :IdGrupo, :ProductoId, :Id)
    end
end
