class Bitacoratiempo < ActiveRecord::Base
  self.primary_key = "Id"
  self.table_name = 'BitacoraTiempos' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "Codigo"
  belongs_to :vendedor, class_name:"Vendedor", foreign_key: "IdVendedor"
  scope :visitas, -> { where(Visita: true) }
  scope :visitas_programadas, -> { where(Visita: true, Programado: true) }


  def self.total_clientes(params)
    query = joins("inner join clientes ON bitacoratiempos.Codigo=clientes.IdCli inner join rutas ON bitacoratiempos.RutaId = rutas.IdRutas")
            .where("(bitacoratiempos.RutaId = :rutaId or :rutaId = '') AND (bitacoratiempos.IdEmpresa = :idempresa) AND (bitacoratiempos.Visita = 1) ",{rutaId: params[:search], idempresa: params[:search0]})
            .where('bitacoratiempos.HI BETWEEN ? AND ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
    query = query.where('bitacoratiempos.DiaO >= ?', params[:diaO]) if params[:diaO].present?
    query
  end

#  def self.total_clientes(params)
#    query =  joins("left outer join clientes on bitacoratiempos.Codigo=clientes.IdCli")
#            .where("(bitacoratiempos.RutaId = :rutaId or :rutaId = '') AND (bitacoratiempos.IdEmpresa = :idempresa) AND (bitacoratiempos.Visita = 1) ",{rutaId: params[:search], idempresa: params[:search0]})
#            .distinct
#    query = query.where('bitacoratiempos.DiaO >= ?', params[:diaO]) if params[:diaO].present?
#    query
#  end

  #  SELECT DISTINCT COUNT(*) FROM BitacoraTiempos INNER JOIN Clientes ON BitacoraTiempos.Codigo = Clientes.IdCli
  #   WHERE(BitacoraTiempos.RutaId = 218) and BitacoraTiempos.HI between '20160402' and '20160410'


  def self.prom_tiempo_visita(params)
    query =  joins("left outer join clientes on bitacoratiempos.Codigo=clientes.IdCli")
            .where("(bitacoratiempos.RutaId = :rutaId or :rutaId = '') AND (bitacoratiempos.IdEmpresa = :idempresa) AND (bitacoratiempos.TS != '0') AND (bitacoratiempos.TS >= '00:00')",{rutaId: params[:search], idempresa: params[:search0]})
            .distinct
    query = query.where('bitacoratiempos.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query
  end

  def self.search_gps_venta(params)
    query = select("[bitacoratiempos].Codigo,[bitacoratiempos].RutaId,[bitacoratiempos].Descripcion,[bitacoratiempos].HI As Fecha,[bitacoratiempos].latitude,[bitacoratiempos].longitude,[clientes].NombreCorto AS c_nombre,[clientes].Direccion AS c_direccion,
    ISNULL((select SUM(TOTAL) from venta where RutaId=[bitacoratiempos].RutaId AND DiaO=[bitacoratiempos].DiaO AND CodCliente=[bitacoratiempos].Codigo AND Cancelada = 'false' AND TOTAL>0),0) AS TOTAL")
           .joins('left join clientes ON bitacoratiempos.Codigo=clientes.IdCli')
           .group(:Codigo,:Descripcion,:HI,:latitude,:longitude,:NombreCorto,:Direccion,:RutaId,:Id,:DiaO)
           .where("(bitacoratiempos.RutaId = :rutaId or :rutaId = '') AND (bitacoratiempos.DiaO = :diaO or :diaO = '') AND (bitacoratiempos.IdVendedor = :vendedor_id or :vendedor_id = '') AND (bitacoratiempos.IdEmpresa = :idempresa)",{rutaId: params[:search], diaO: params[:diaO], vendedor_id: params[:vendedor_id], idempresa: params[:search6]})
           .order('bitacoratiempos.HI')
           .distinct
    query
  end


  def self.visitas_pendientes(params)
   query =  joins("INNER JOIN rutas on bitacoratiempos.RutaId=rutas.IdRutas")
            .where("(bitacoratiempos.visita = 1) AND (bitacoratiempos.programado = 1) AND (bitacoratiempos.IdEmpresa = :idempresa)",{idempresa: params[:search0]})
            .distinct
    query = query.where('bitacoratiempos.HI between ? and ?', (Time.current.beginning_of_day-1.day), (Time.current.end_of_day))
    query
  end


  def self.reporte_bitacoratiempos(params)
    query = select("BitacoraTiempos.Codigo, BitacoraTiempos.DiaO, BitacoraTiempos.Descripcion, ISNULL(Clientes.Nombre,'') AS Nombre , ISNULL(Clientes.NombreCorto,'') AS NombreCorto, BitacoraTiempos.HI AS Fecha,
     BitacoraTiempos.HI AS HoraInicio, BitacoraTiempos.HF AS HoraFin,  BitacoraTiempos.HT AS TiempoTraslado, BitacoraTiempos.TS AS TiempoServicio,
                    BitacoraTiempos.Visita, BitacoraTiempos.Programado, Rutas.Ruta as RutaId, BitacoraTiempos.Cerrado, BitacoraTiempos.IdV, bitacoraTiempos.Tip, ISNULL(BitacoraTiempos.pila, 0) AS pila, BitacoraTiempos.latitude, BitacoraTiempos.longitude, BitacoraTiempos.IdVendedor")
           .joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas LEFT OUTER JOIN  Clientes ON BitacoraTiempos.Codigo = Clientes.IdCli')
           .where("(RutaId = :rutaId or :rutaId = '') AND (BitacoraTiempos.IdEmpresa = :idempresa) AND (BitacoraTiempos.DiaO = :diao) AND (BitacoraTiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{rutaId: params[:search], idempresa: params[:search6], diao: params[:diaO], vendedor_id: params[:vendedor_id]})
           .order('BitacoraTiempos.HI')
    query
  end

  def self.reporte_bitacoratiempos_visitas_counter(params)
    query =
           joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas LEFT OUTER JOIN  Clientes ON BitacoraTiempos.Codigo = Clientes.IdCli')
           .distinct
           .where('(RutaId= ?) AND (HI between ? and ?) AND (BitacoraTiempos.IdEmpresa= ?)',(params[:search]), (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"),(params[:search6]))
           .where("(RutaId = :rutaId or :rutaId = '') AND (BitacoraTiempos.IdEmpresa = :idempresa) AND (BitacoraTiempos.DiaO = :diao) AND (BitacoraTiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{rutaId: params[:search], idempresa: params[:search6], diao: params[:diaO], vendedor_id: params[:vendedor_id]})
           .order('BitacoraTiempos.HI')
    query
  end

  def self.rep_bitiempos_programadas_realizadas(params)
    query = select("Codigo")
            .distinct
            .joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas')
            .where("(visita=1) AND (programado=1) AND (RutaId = :rutaId or :rutaId = '') AND (BitacoraTiempos.IdEmpresa = :idempresa) AND (BitacoraTiempos.DiaO = :diao) AND (BitacoraTiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{rutaId: params[:search], idempresa: params[:search6], diao: params[:diaO], vendedor_id: params[:vendedor_id]})
  end

  def self.rep_bitiempos_realizadas_NO_repetidas(params)
    query = joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas')
            .where("(RutaId = :rutaId or :rutaId = '') AND (BitacoraTiempos.IdEmpresa = :idempresa) AND (BitacoraTiempos.DiaO = :diao)  AND (BitacoraTiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{rutaId: params[:search], idempresa: params[:search6], diao: params[:diaO], vendedor_id: params[:vendedor_id]})
            .distinct

  end


  def self.rep_bitiempos_NO_programadas(params)
    query = joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas')
            .where("(BitacoraTiempos.visita=1) AND  (BitacoraTiempos.RutaId = :rutaId) AND (BitacoraTiempos.DiaO = :diao) AND (BitacoraTiempos.IdVendedor = :vendedor_id or :vendedor_id = '')
            AND (BitacoraTiempos.IdEmpresa= :idempresa) AND BitacoraTiempos.Codigo NOT IN  (SELECT CodCli FROM TH_SecVisitas AS TH_SecVisitas_1 WHERE RutaId = :rutaId AND  Fecha BETWEEN :fechainicial AND :fechafinal)",{rutaId: params[:search], fechainicial: (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6], diao: params[:diaO], vendedor_id: params[:vendedor_id]})

  end



end
