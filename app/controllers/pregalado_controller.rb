class PregaladoController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index, :producto_obsequio]

  def modulo
    if action_name == "index"
      @Descripcion_Modulo = "Reporte de producto promoción"
    elsif action_name == "producto_obsequio"
      @Descripcion_Modulo = "Reporte de producto regalo"
    end
  end

  def index
    @controlador = 'pregalado'
    @accion = 'index'
    elementos_de_busqueda
    @pathDeBusqueda = pregalado_index_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable

    respond_to do |format|
      format.html
      format.js
    end
  end

  def busqueda
    elementos_de_busqueda
    if params[:Preventa] == 'P'
      @pregalado = Pregalad.rep_producto_promocion_preventa(params)
    else
      @pregalado = Pregalad.rep_producto_promocion(params)
    end

    respond_to do |format|
    format.js
    end
  end

  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchClienteB = Cliente.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchClienteB.sorts = 'IdCli' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    @searchProductoB = Producto.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchProductoB.sorts = 'Clave' if @searchProductoB.sorts.empty?
    @productos_Selector = @searchProductoB.result().page(params[:productos])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
  end

  def producto_obsequio
    @controlador = 'pregalado'
    @accion = 'producto_obsequio'
    elementos_de_busqueda
    @pathDeBusqueda = pregalado_producto_obsequio_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
  end



  def busqueda_producto_obsequio
  #@pregalado = Pregalad.limit(10)
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    if params[:Pedidos] == 'P'
      @pregalado = Pregalad.producto_obsequio_pedido(params)
    else
      @pregalado = Pregalad.busqueda_producto_obsequio(params)
    end

    respond_to do |format|
    format.js
    end
  end
end
