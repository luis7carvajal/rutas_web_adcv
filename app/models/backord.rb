class Backord < ActiveRecord::Base
  self.primary_key = "ID"

  def self.proceso_pedidos_liberados_backorder(idempresa)
    query = select("[clientes].IdCli AS Cliente, [clientes].Nombre AS Responsable, [clientes].NombreCorto AS NombreComercial, [productos].Clave AS ClaveProducto, [productos].Producto AS Producto, [backorder].PEDIDO AS Pedido, CASE  WHEN  [backorder].STATUS=1 THEN 'PENDIENTE'  WHEN [backorder].STATUS=2 THEN 'LIBERADO' WHEN [backorder].STATUS=3 THEN 'EN TRANSITO' WHEN [backorder].STATUS=4 THEN 'ENTREGADO' END AS Status,
                  [backorder].FOLIO_BO AS Folio_BO, [detalle_bo].PZA AS Pza, SUM([detalle_bo].IMPORTE) AS Importe, SUM([detalle_bo].IVA) AS IVA, SUM([detalle_bo].IMPORTE) + SUM([detalle_bo].IVA) AS Total, [backorder].Motivo AS Motivo, [backorder].ID, [backorder].CANCELADA, SUM(V_CantidadxPedido.Cajas) AS Cajas, SUM(V_CantidadxPedido.Pzas) AS Pzas, [backorder].RUTA AS Ruta, [backorder].FECHA_PEDIDO AS Fecha_Pedido, [backorder].FECHA_ENTREGA AS Fecha_Entrega")
           .joins('inner join detalle_bo ON backorder.PEDIDO = detalle_bo.PEDIDO AND
                 backorder.RUTA = detalle_bo.RUTA AND backorder.IDEMPRESA = detalle_bo.IDEMPRESA inner join productos ON detalle_bo.ARTICULO = Productos.Clave  inner join
                 clientes ON backorder.CODCLIENTE = clientes.IdCli inner join V_CantidadxPedido ON Backorder.PEDIDO = V_CantidadxPedido.Docto')
           .group("clientes.IdCli, clientes.Nombre, clientes.NombreCorto, productos.Clave, productos.Producto, backorder.Motivo, backorder.PEDIDO, backorder.FOLIO_BO, detalle_bo.PZA, backorder.ID, backorder.Status, backorder.CANCELADA, V_CantidadxPedido.Pzas, backorder.RUTA, backorder.FECHA_PEDIDO, backorder.FECHA_ENTREGA")
           .where('(backorder.CANCELADA != 1) AND backorder.IDEMPRESA = ?', idempresa)

    query
  end
end
