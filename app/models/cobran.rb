class Cobran < ActiveRecord::Base
  scope :por_ruta, -> (id_param) { where(RutaId: id_param) }
  scope :aquellos_con_saldo_pendiente, -> { where(Status: "1") }
  scope :aquellos_con_pagos, -> { where(Status: "1") }
  #scope :activos_sin_clientes, -> { joins('left outer join relactivos on activos.IdActivos=relactivos.Activo').select('activos.*,relactivos.Activo').where('relactivos.Activo is null') }
  scope :por_ruta_mediante_relcliruta, -> (ruta_param) { joins('inner join relclirutas on cobranza.Cliente=relclirutas.IdCliente').where('relclirutas.IdRuta = ?', ruta_param) }

	scope :sucursal, ->(sucursal) { where("cobranza.IdEmpresa LIKE ?", "#{sucursal}%") }

  scope :saldo_vencido, ->{ where("cobranza.FechaVence <= ?", Time.now.end_of_day) }
  scope :saldo_corriente, ->{ where("cobranza.FechaVence > ?", Time.now.end_of_day) }


  belongs_to :cliente, class_name:"Cliente", foreign_key: "Cliente"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  has_many :detallecob, class_name: "Detalleco", foreign_key: "IdCobranza"


  def self.ctasxcobrar_busqueda_general(params)
    query = select("cobranza.id,cobranza.Documento,cobranza.Saldo,cobranza.FechaReg as FR,cobranza.FechaVence as FV,cobranza.TipoDoc,cobranza.Cliente,cobranza.RutaId,Empresas.Sucursal AS Sucursal, Empresas.IdEmpresa, ISNULL((SUM(d.Abono)),0) AS Abono, (cobranza.Saldo - ISNULL((SUM(d.Abono)),0)) AS Saldo_actual")
           .joins('inner join Clientes C on cobranza.Cliente=C.IdCli inner join Rutas on cobranza.RutaId=Rutas.IdRutas inner join Vendedores on Rutas.Vendedor=Vendedores.IdVendedor inner join Relclirutas on cobranza.Cliente=Relclirutas.IdCliente and cobranza.rutaid=Relclirutas.idruta INNER JOIN Empresas ON Empresas.IdEmpresa = cobranza.IdEmpresa
            left outer join DetalleCob d on cobranza.id=d.IdCobranza and cobranza.rutaid=d.rutaid')
           .where("((cobranza.RutaId = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (cobranza.Status = 1)  AND (cobranza.IdEmpresa = :idempresa))",{rutaId: params[:search], idempresa: params[:search6]})
           .group('cobranza.id', 'cobranza.Documento', 'cobranza.Saldo', :FechaReg, :FechaVence, :TipoDoc, 'cobranza.RutaId', 'cobranza.Cliente','Empresas.Sucursal','Empresas.IdEmpresa')
           .distinct
    query = query.where("(cobranza.Id LIKE :Id or :Id = '') And (Rutas.Ruta LIKE :Ruta or :Ruta = '') And (Vendedores.Nombre LIKE :Vendedor or :Vendedor = '') And
     (C.IdCli LIKE :IdCli or :IdCli = '') And (C.Nombre LIKE :Cliente_Nombre or :Cliente_Nombre = '') And (C.NombreCorto LIKE :Cliente_NombreCorto or :Cliente_NombreCorto = '') And (cobranza.Documento LIKE :Documento or :Documento = '') And (cobranza.TipoDoc LIKE :TipoDoc or :TipoDoc = '')",
     {Id: "%#{params[:Id_param]}%", Ruta: "%#{params[:Ruta_param]}%", Vendedor: "%#{params[:Vendedor_param]}%", IdCli: "%#{params[:IdCli_param]}%", Cliente_Nombre: "%#{params[:Cliente_Nombre_param]}%",
      Cliente_NombreCorto: "%#{params[:Cliente_NombreCorto_param]}%", Documento: "%#{params[:Documento_param]}%", TipoDoc: "%#{params[:TipoDoc_param]}%"})
      query = query.where('cobranza.FechaReg between ? and ?', (params[:FechaReg_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaReg_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaReg_param].present?
      query = query.where('cobranza.FechaVence between ? and ?', (params[:FechaVence_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaVence_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaVence_param].present?

    if params[:Tipo_Fecha] == "Fecha Registro"
      query = query.where('cobranza.FechaReg between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    else
      query = query.where('cobranza.FechaVence between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    end
    query
  end



end
