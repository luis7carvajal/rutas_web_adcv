class MapaController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Reporte de GPS"
  end

  def index
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'mapa'
    @accion = 'index'
    @pathDeBusqueda = mapa_index_path # esto es usando para colocar el path dentro de las tablas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    # make name the default sort column
    @searchDiaOpB.sorts = 'Fecha desc' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def busqueda_gps_venta
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @ventas = Bitacoratiempo.search_gps_venta(params)
    @secuencia = 0
    @hash = Gmaps4rails.build_markers(@ventas) do |venta, marker|
      marker.lat venta.latitude
      marker.lng venta.longitude
      marker.infowindow venta.c_nombre #imprime el nombre del cliente
    #  marker.infowindow venta.c_nombre
      if venta.Codigo == "A18253"#inicio del dia
        marker.picture({
          "url": "/assets/inicio.png",
          "width" =>  32,
          "height" => 32
        })
        @venta=0
        @venta+=1 #se inicializa en cero y se incrementa en 1 para que cuando pase el inicio de dia, el siguiente debe ser el icono de primera visita del dia independientemente si tuvo venta o no

      elsif @venta == 1
        marker.picture({
          "url": "/assets/primera_visita.png",
          "width" =>  32,
          "height" => 32
        })
        @venta+=1 #se incrementa para que ya no haya otra venta inicial hasta que se repita el ciclo

      elsif venta.TOTAL > 0 #cuando la visita tuvo venta
        marker.picture({
          "url": "/assets/visita_con_venta.png",
          "width" =>  32,
          "height" => 32
        })

      elsif venta.TOTAL == 0 #cuando la visita no tuvo venta
        marker.picture({
          "url": "/assets/visita_sin_venta.png",
          "width" =>  32,
          "height" => 32
        })

      end

    end
    respond_to do |format|
    format.js
    end
  end

end
