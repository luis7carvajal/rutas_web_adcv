require 'test_helper'

class RelvendrutasControllerTest < ActionController::TestCase
  setup do
    @relvendruta = relvendrutas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:relvendrutas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create relvendruta" do
    assert_difference('Relvendruta.count') do
      post :create, relvendruta: { Fecha: @relvendruta.Fecha, IdEmpresa: @relvendruta.IdEmpresa, IdRuta: @relvendruta.IdRuta, IdVendedor: @relvendruta.IdVendedor }
    end

    assert_redirected_to relvendruta_path(assigns(:relvendruta))
  end

  test "should show relvendruta" do
    get :show, id: @relvendruta
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @relvendruta
    assert_response :success
  end

  test "should update relvendruta" do
    patch :update, id: @relvendruta, relvendruta: { Fecha: @relvendruta.Fecha, IdEmpresa: @relvendruta.IdEmpresa, IdRuta: @relvendruta.IdRuta, IdVendedor: @relvendruta.IdVendedor }
    assert_redirected_to relvendruta_path(assigns(:relvendruta))
  end

  test "should destroy relvendruta" do
    assert_difference('Relvendruta.count', -1) do
      delete :destroy, id: @relvendruta
    end

    assert_redirected_to relvendrutas_path
  end
end
