class CatbancosController < ApplicationController
  before_action :set_catbanco, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de bancos"
  end

  def index
    @search = Catbanco.activos.search(search_params)
    @search.sorts = 'Clave' if @search.sorts.empty?
    @catbancos = @search.result().page(params[:page])
    @cantidad_catbancos_activos = Catbanco.activos.count

    @catbanco = Catbanco.new


  end

  # GET /catbancos
  # GET /catbancos.json


  def inactivos
    @catbancos = Catbanco.inactivos
    @cantidad_catbancos_inactivos = Catbanco.inactivos.count
  end

  def banco_check
    @bancox = Catbanco.coprobar_existencia(params[:catbanco][:Clave]).first

    respond_to do |format|
    format.json { render :json => !@bancox }
    end
  end

  # GET /catbancos/1
  # GET /catbancos/1.json
  def show
  end

  # GET /catbancos/new
  def new
    @catbanco = Catbanco.new
  end

  # GET /catbancos/1/edit
  def edit
  end

  # POST /catbancos
  # POST /catbancos.json
  def create
    if @puede_crear != true
      return
    end
    @catbanco = Catbanco.new(catbanco_params)
    respond_to do |format|
      if @catbanco.save
        format.html { redirect_to @catbanco, notice: 'Catbanco was successfully created.' }
        format.json { render :show, status: :created, location: @catbanco }
        format.js {flash.now[:notice] = 'El banco se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @catbanco.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el banco.'} #ajax
      end
    end
  end

  # PATCH/PUT /catbancos/1
  # PATCH/PUT /catbancos/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @catbanco.update(catbanco_params)
        format.html { redirect_to @catbanco, notice: 'Catbanco was successfully updated.' }
        format.json { render :show, status: :ok, location: @catbanco }
        format.js {flash.now[:notice] = 'El banco se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @catbanco.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el banco.'} #ajax
      end
    end
  end

  # DELETE /catbancos/1
  # DELETE /catbancos/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to catbancos_url, notice: 'Catbanco was successfully destroyed.' }
      format.json { head :no_content }
      if @catbanco.Status == true
        @catbanco.update(Status:false)
        format.js {flash.now[:notice] = 'El banco se ha borrado de forma exitosa.'} #ajax
      else
        @catbanco.update(Status:true)
        format.js {flash.now[:notice] = 'El banco se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catbanco
      @catbanco = Catbanco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def catbanco_params
      params.require(:catbanco).permit(:Clave, :Descripcion, :Status, :NCuenta, :CLABE, :IdEmpresa)
    end
end
