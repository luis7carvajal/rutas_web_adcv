class CatmarcasController < ApplicationController
  before_action :set_catmarca, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de marcas"
  end

  # GET /catmarcas
  # GET /catmarcas.json
  def index

    @search = Catmarca.activos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @catmarcas = @search.result().page(params[:page])
    @cantidad_catmarcas_activos = Catmarca.activos.count

    @catmarca = Catmarca.new

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catmarcas.to_csv}
      format.xls
    end
  end

  def inactivos

    @search = Catmarca.inactivos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @catmarcas = @search.result().page(params[:page])
    @cantidad_catmarcas_inactivos = Catmarca.inactivos.count


    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catmarcas.to_csv}
      format.xls
    end
  end

  # GET /catmarcas/1
  # GET /catmarcas/1.json
  def show
  end

  def catmarcas_check
    @catmarcax = Catmarca.find_by_Clave(params[:catmarca][:Clave])
    respond_to do |format|
    format.json { render :json => !@catmarcax }
    end
  end

  # GET /catmarcas/new
  def new
    @catmarca = Catmarca.new
  end

  # GET /catmarcas/1/edit
  def edit
  end

  # POST /catmarcas
  # POST /catmarcas.json
  def create
    if @puede_crear != true
      return
    end
    @catmarca = Catmarca.new(catmarca_params)
    respond_to do |format|
      if @catmarca.save
        format.html { redirect_to @catmarca, notice: 'Catmarca was successfully created.' }
        format.json { render :show, status: :created, location: @catmarca }
        format.js {flash.now[:notice] = 'La marca se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @catmarca.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la marca.'} #ajax
      end
    end
  end

  # PATCH/PUT /catmarcas/1
  # PATCH/PUT /catmarcas/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @catmarca.update(catmarca_params)
        format.html { redirect_to @catmarca, notice: 'Catmarca was successfully updated.' }
        format.json { render :show, status: :ok, location: @catmarca }
        format.js {flash.now[:notice] = 'La marca se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @catmarca.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la marca.'} #ajax
      end
    end
  end

  # DELETE /catmarcas/1
  # DELETE /catmarcas/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to catmarcas_url, notice: 'Catmarca was successfully destroyed.' }
      format.json { head :no_content }
      if @catmarca.Status == true
        @catmarca.update(Status:false)
        format.js {flash.now[:notice] = 'La marca se ha borrado de forma exitosa.'} #ajax
      else
        @catmarca.update(Status:true)
        format.js {flash.now[:notice] = 'La marca se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catmarca
      @catmarca = Catmarca.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def catmarca_params
      params.require(:catmarca).permit(:Clave, :Descripcion, :Status, :IdEmpresa, :TipoMarca)
    end
end
