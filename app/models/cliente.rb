class Cliente < ActiveRecord::Base
  ActiveRecord::ConnectionAdapters::SQLServerAdapter.use_output_inserted = false
  self.primary_key = "IdCli"
  self.table_name = 'Clientes' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :primeros, ->{ order("IdCli ASC").limit(20) }
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  scope :comprobar_existencia, ->(paramc) { where(IdCli: paramc)} # model
  scope :clp, -> { where(IdCli: "10601942")} # model
  scope :credito, -> { where(Credito: true)} # model
  scope :contado, -> { where(Credito: false)} # model

  scope :datos_cliente, ->(param_id) { find_by(IdCli: param_id)} # para que no devuelva una coleccion, sino un solo registro

  scope :without_price, ->{Cliente.includes(:relclili).references(:relclili).where("relclilis.id IS NULL OR clientes.IdCli not in (select distinct CodCliente from relclilis where ListaP IS NOT NULL)")}
  #scope :without_price, -> (id_params) {Cliente.includes(:relclili).references(:relclili).where("relclilis.id IS NULL OR clientes.IdCli not in (select distinct CodCliente from relclilis where ListaP = ?)", )}
  scope :without_discount, ->{Cliente.includes(:relclili).references(:relclili).where("relclilis.id IS NULL OR clientes.IdCli not in (select distinct CodCliente from relclilis where ListaD IS NOT NULL)")}

  scope :without_promo, ->{Cliente.includes(:relclili).references(:relclili).where("relclilis.id IS NULL OR clientes.IdCli not in (select distinct CodCliente from relclilis where ListaPromo IS NOT NULL)")}

  scope :without_mensaje, -> (id_params) {Cliente.includes(:relmens).references(:relmens).where("relmens.IDRow IS NULL OR clientes.IdCli not in (select distinct CodCliente from relmens where IdCliente = ?)", id_params)}


  has_one :listapromomast, through: :relclili, source: :listapromomast, foreign_key: "ListaPromo"

  has_many :solicitudservicio, -> {where("Status = ?", true) }, class_name: "Solicitudserv", foreign_key: "CodCliente"

    # client model
    has_many :rutas, through: :relclirutas, source: :ruta, foreign_key: "IdRuta"




  has_one :relclili, class_name: "Relclili", foreign_key: "CodCliente"
  accepts_nested_attributes_for :relclili

  has_many :relclirutas, class_name: "Relcliruta", foreign_key: "IdCliente"
  accepts_nested_attributes_for :relclirutas, reject_if: proc{|attributes| attributes['IdRuta'].blank?}


  has_many :relactivos, class_name: "Relactivo", foreign_key: "Cliente"

  has_many :relmens, class_name: "Relmen", foreign_key: "CodCliente"

  has_many :reldaycli, class_name: "Reldayc", foreign_key: "CodCli"
  accepts_nested_attributes_for :reldaycli, reject_if: proc{|attributes| attributes['RutaId'].blank? && attributes['idVendedor'].blank? && attributes['Lunes'].blank? && attributes['Martes'].blank? && attributes['Miercoles'].blank? && attributes['Jueves'].blank? && attributes['Viernes'].blank? && attributes['Sabado'].blank? && attributes['Domingo'].blank?}


  has_one :relclicla, class_name: "Relclicla", foreign_key: "IdCliente"
  accepts_nested_attributes_for :relclicla

  has_one :cobran, class_name: "Cobran", foreign_key: "Cliente"
  has_many :detallecob, class_name: "Detalleco", foreign_key: "Cliente"

  #Reportes
  has_many :venta, class_name:"Vent", foreign_key: "CodCliente"
  has_many :devoluciones, class_name:"Devol", foreign_key: "CodCliente"
  has_many :pregalado, class_name:"Pregalad", foreign_key: "Cliente"
  has_many :bitacoratiempos, class_name:"Bitacoratiempo", foreign_key: "Codigo"
  has_one :noventasingular, class_name: "Noventasingular", foreign_key: "Cliente"

  def self.datos_cliente(params)
      query = select("clientes.*, SUM(Z.Saldo)-IsNull(SUM(Z.Abono),0) Saldo_Actual")
              .joins('Left Join Vw_Cobranza Z On clientes.IdCli=Z.Cliente And Z.Status=1')
      query = query.where("clientes.IdCli = ? ", params[:cliente]) if params[:cliente].present?
      query = query.where("clientes.IdCli = ? ", params[:id]) if params[:id].present?
      query = query.where("clientes.IdEmpresa = ?", params[:IdEmpresa]) if params[:IdEmpresa].present?
      query.group(:IdCli,:ValidarGPS,:Merma_en_ruta,:Nombre,:NombreCorto,:Direccion,:Colonia,:Email,:Credito,:DiasCreedito,:LimiteCredito,:Saldo,:Referencia,:Telefono,:CP,:Status,:Tel2,:VisitaObligada,:FirmaObligada,:MotivoBajaId,:Horario,:idclasc,:IdEmpresa,:Longitud,:Latitud,:VigenciaComodato,:CorreoE,:Ciudad,:Estado,:RFC)
   end

  def self.clientes
     query = select("clientes.*, SUM(Z.Saldo)-IsNull(SUM(Z.Abono),0) Saldo_Actual")
             .joins('Left Join Vw_Cobranza Z On clientes.IdCli=Z.Cliente And Z.Status=1')
             .group("clientes.Saldo, clientes.Status, clientes.IdEmpresa")
             .group(:IdCli,:ValidarGPS,:Merma_en_ruta,:Nombre,:NombreCorto,:Direccion,:Colonia,:Email,:Credito,:DiasCreedito,:LimiteCredito,:Referencia,:Telefono,:CP,:Tel2,:VisitaObligada,:FirmaObligada,:MotivoBajaId,:Horario,:idclasc,:Longitud,:Latitud,:VigenciaComodato,:CorreoE,:Ciudad,:Estado,:RFC)
  end

  def self.clientes_para_POS(params)
    query = select("Clientes.IdCli,Clientes.Nombre ,Clientes.NombreCorto, ISNULL(Credito,0) Credito")
            .joins('INNER JOIN RelClirutas ON Clientes.IdCli = RelClirutas.IdCliente AND Clientes.IdEmpresa = RelClirutas.IdEmpresa')
            .where("(relclirutas.IdRuta = :IdRuta) AND (relclirutas.IdEmpresa = :IdEmpresa) ",{IdRuta: params[:IdRuta], IdEmpresa: params[:sucursal]})
  end

  def self.limite_credito(params)
    query = select("ISNULL(LimiteCredito,0) Limite_Credito")
            .where("(IdCli = :Cliente)",{Cliente: params[:Cliente]})
  end

  def self.busqueda_general
   query = select("clientes.IdCli,clientes.Nombre,clientes.NombreCorto,clientes.Direccion,clientes.Colonia,clientes.Email,
                  clientes.Credito,clientes.DiasCreedito,clientes.LimiteCredito,clientes.Saldo, listapromomaster.ListaMaster as promocion")
   .joins('LEFT JOIN relclilis On clientes.IdCli=relclilis.CodCliente LEFT JOIN listapromomaster On relclilis.ListaPromo=listapromomaster.Id')
   .group(:IdCli,:Nombre,:NombreCorto,:Direccion,:Colonia,:Email,:Credito,:DiasCreedito,:LimiteCredito,:Saldo,:ListaMaster)

  end

  def self.exportar_excel_clientes
    query = select("clientes.IdCli, clientes.Nombre, clientes.NombreCorto, clientes.Direccion, clientes.CP, clientes.Colonia, clientes.Latitud, clientes.Longitud, clientes.Referencia, clientes.Status, clientes.Telefono, clientes.Tel2, clientes.Email, clientes.Horario, clientes.Credito, clientes.LimiteCredito, clientes.DiasCreedito, clientes.Saldo, clientes.VisitaObligada, clientes.FirmaObligada,
                    relcliclas.Clas1, relcliclas.Clas2, relcliclas.Clas3, relcliclas.Clas4, relcliclas.Clas5, listapromomaster.ListaMaster as promocion,
                   rutas.Ruta, vendedores.Clave as Vendedor, reldaycli.Lunes, reldaycli.Martes, reldaycli.Miercoles,reldaycli.Jueves, reldaycli.Viernes, reldaycli.Sabado, reldaycli.Domingo ")
         .joins('LEFT JOIN relclilis On clientes.IdCli=relclilis.CodCliente LEFT JOIN listapromomaster On relclilis.ListaPromo=listapromomaster.Id LEFT OUTER JOIN Reldaycli ON Clientes.IdCli = Reldaycli.CodCli LEFT OUTER JOIN Rutas ON Reldaycli.RutaId = Rutas.IdRutas LEFT OUTER JOIN Vendedores ON Reldaycli.idVendedor = Vendedores.IdVendedor
         LEFT JOIN Relcliclas ON Clientes.IdCli = Relcliclas.IdCliente')
      #   .joins('INNER JOIN Clientes ON Venta.CodCliente = Clientes.IdCli AND Venta.IdEmpresa = Clientes.IdEmpresa INNER JOIN FormasPag ON Venta.FormaPag = FormasPag.IdFpag LEFT OUTER JOIN DetalleVet ON Venta.Documento = DetalleVet.Docto AND Venta.RutaId = DetalleVet.RutaId')
    query
  end

  def self.clientes_fuera_de_ruta(params)
    query = select('clientes.*,relclirutas.IdCliente')
           .joins('left outer join relclirutas on clientes.IdCli=relclirutas.IdCliente')
           .where('( relclirutas.IdRuta IS NULL OR relclirutas.IdRuta != ? ) AND clientes.Status = ? AND clientes.IdEmpresa = ? AND clientes.IdCli not in (select distinct IdCliente from relclirutas where IdRuta = ?) ', params[:id], true, params[:IdEmpresa], params[:id])
           .distinct
  end

  def self.clientes_fuera_de_ruta2_contador(params)
    query = joins('left outer join relclirutas on clientes.IdCli=relclirutas.IdCliente')
           .where('( relclirutas.IdRuta IS NULL OR relclirutas.IdRuta != ? ) AND clientes.Status = ? AND clientes.IdEmpresa = ? AND clientes.IdCli not in (select distinct IdCliente from relclirutas where IdRuta = ?) ', params[:id], true, params[:IdEmpresa], params[:id])
           .distinct
  end

  def self.clientes_en_lista_de_precios(params)
    query = joins('inner join relclilis on clientes.IdCli=relclilis.CodCliente')
           .where('(relclilis.ListaP IS NOT NULL) AND clientes.Status = ? AND clientes.IdEmpresa = ?', true, params[:search6])
           .distinct
  end




    def self.to_csv(options = {})#exportar
      CSV.generate(options) do |csv|
        csv << column_names
        all.each do |cliente|
          csv << cliente.attributes.values_at(*column_names)
        end
      end
    end



    def self.import(file,empresa)#importar
      @errors = []
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]
        cliente = find_by_IdCli(row["Clave"]) || new
        cliente.attributes = {IdCli: row["Clave"], Nombre: row["Encargado"], NombreCorto: row["Nombre Comercial"], Direccion: row["Calle y N°"], CP: row["C.P"], Colonia: row["Colonia"], Latitud: row["Latitud"], Longitud: row["Longitud"], Referencia: row["Referencia"], Status: row["Activo"], Telefono: row["Teléfono"], Tel2: row["Tel. Celular"], Email: row["Email"], Horario: row["Horario"], Credito: row["Credito"], LimiteCredito: row["Limite Credito"], DiasCreedito: row["Días Credito"], Saldo: row["Saldo inicial"], VisitaObligada: row["Visita Obligada"], FirmaObligada: row["Firma Obligada"], IdEmpresa: empresa}

        @ruta = Ruta.find_by_Ruta_and_IdEmpresa(row["Ruta(numero de ruta)"], empresa)
        @vendedor = Vendedor.find_by_Clave_and_IdEmpresa(row["Vendedor(clave)"], empresa)

        if cliente.relclicla != nil
          cliente.relclicla.attributes = {IdCliente: row["Clave"], Clas1: row["Clase 1"], Clas2: row["Clase 2"], Clas3: row["Clase 3"], Clas4: row["Clase 4"], Clas5: row["Clase 5"], IdEmpresa: empresa}
        else
          cliente.relclicla_attributes = {IdCliente: row["Clave"], Clas1: row["Clase 1"], Clas2: row["Clase 2"], Clas3: row["Clase 3"], Clas4: row["Clase 4"], Clas5: row["Clase 5"], IdEmpresa: empresa}
        end

        if cliente.relclili != nil
          cliente.relclili.attributes = {CodCliente: row["Clave"], ListaP: row["Lista de Precio"], ListaD: row["Lista de Descuento"], ListaPromo: row["Lista de Promociones"], IdEmpresa: empresa}
        else
          cliente.relclili_attributes = {CodCliente: row["Clave"], ListaP: row["Lista de Precio"], ListaD: row["Lista de Descuento"], ListaPromo: row["Lista de Promociones"], IdEmpresa: empresa}
        end

        @rutaxi = Relcliruta.find_by_IdCliente_and_IdRuta(row["Clave"], @ruta.try(:IdRutas))
        if @rutaxi == nil
          cliente.relclirutas_attributes = [{IdRuta: @ruta.try(:IdRutas), IdEmpresa: empresa}]
        end

        @Vend = Relvendruta.find_by(IdVendedor: @vendedor.try(:IdVendedor), IdRuta: @ruta.try(:IdRutas))
        if @Vend == nil
          Relvendruta.create(IdVendedor: @vendedor.try(:IdVendedor), IdRuta: @ruta.try(:IdRutas), IdEmpresa: empresa, Fecha: Time.now.strftime("%d-%m-%Y %T"))
        end


          @exxtiste = Reldayc.exists?(:CodCli => cliente.IdCli, :idVendedor => @vendedor.try(:IdVendedor), :RutaId => @ruta.try(:IdRutas))
          if @exxtiste == false
            cliente.reldaycli_attributes = [{RutaId: @ruta.try(:IdRutas), idVendedor: @vendedor.try(:IdVendedor), Lunes: row["Lunes"], Martes: row["Martes"], Miercoles: row["Miércoles"], Jueves: row["Jueves"], Viernes: row["Viernes"], Sabado: row["Sábado"], Domingo: row["Domingo"], IdEmpresa: empresa}]
          end



        if cliente.save
          # stuff to do on successful save
         else
           cliente.errors.full_messages.each do |message|
             @errors << "Error fila #{i}, columna #{message}"
           end
         end


      end
      @errors #  <- need to return the @errors array
    end









    def self.open_spreadsheet(file)#importar
      case File.extname(file.original_filename)
       when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
       #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
       when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
       #else raise "Unknown file type: #{file.original_filename}"
       else raise "El formato debe ser .xlsx ó .csv"


       end
    end




end
