class UsuariosController < ApplicationController
  before_action :set_usuario, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]


  def modulo
    @Descripcion_Modulo = "Catálogo de usuarios"
  end

  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuario = Usuario.new
    @sucursales = Empresa.sucursales(current_usuario.empresamadre_id)
    @cantidad_usuarios_activos =  Usuario.por_empresa(current_usuario.empresa_id).count

    @search_usuarios = Usuario.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search_usuarios.sorts = 'IdUsuario desc' if @search_usuarios.sorts.empty?
    @usuarios = @search_usuarios.result().page(params[:usuarios]).per(15)

    @usuariosExport = Usuario.por_empresa(current_usuario.empresa_id)

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @usuariosExport.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def busqueda_usuario_perfil
    lista_de_perfiles
    @lista_perfiles.each do |perfil|
      if Perfil.find_by(Descripcion: perfil[:d], IdUser: params[:IdUsuario]) == nil
        Perfil.create(Modulo: perfil[:m], Descripcion: perfil[:d], IdUser: params[:IdUsuario])
      end
    end

    params[:IdEmpresa] = current_usuario.empresa_id
    @usuarioP = Usuario.find_by(IdUsuario: params[:IdUsuario])
  #  2.times { @usuarioP.perfiles.build }
    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def usuario_sucursales
    @usuario = Usuario.datos(current_usuario.IdUsuario)
    @sucursales = Empresa.sucursales(current_usuario.empresamadre_id)
    @empresa = Empresamadre.datos(current_usuario.empresamadre_id)
  end

  def usuarios_check
    @usuariox = Usuario.comprobar_existencia(params[:usuario][:usuario]).first
    respond_to do |format|
    format.json { render :json => !@usuariox }
    end
  end

  def usuarios_check2
    @usuario_Ex = Usuario.comprobar_existencia_Email(params[:usuario][:email]).first
    respond_to do |format|
    format.json { render :json => !@usuario_Ex }
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    if @puede_crear != true
      return
    end
    @sucursales = Empresa.sucursales(current_usuario.empresamadre_id)
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
        format.js {flash.now[:notice] = 'El usuario se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el usuario.'} #ajax
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    @sucursales = Empresa.sucursales(current_usuario.empresamadre_id)

    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to root_path}
        format.json { render :show, status: :ok, location: @usuario }
        format.js {flash.now[:notice] = 'El usuario se ha actualizado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el usuario.'} #ajax
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El usuario se ha borrado de forma exitosa.'} #ajax
    end
  end

  def lista_de_perfiles
    @lista_perfiles = [{m:"Usuarios",:d=>"Catálogo de usuarios"},{m:"Empresas",:d=>"Catálogo de empresas"},{m:"Rutas",:d=>"Catálogo de rutas"},{m:"Empleados",:d=>"Catálogo de empleados"},{m:"Vehículos",:d=>"Catálogo de vehículos"},{m:"Clientes",:d=>"Catálogo de clientes"},{m:"Productos",:d=>"Catálogo de productos"},{m:"Promociones",:d=>"Catálogo de promociones"},
      {m:"Combo",:d=>"Catálogo de combo"},{m:"Lista de precios",:d=>"Catálogo de lista de precios"},{m:"Lista de descuentos",:d=>"Catálogo de lista de descuentos"},{m:"Motivos de merma",:d=>"Catálogo de motivos de merma"},{m:"Formas de pago",:d=>"Catálogo de formas de pago"},{m:"Motivos de no venta",:d=>"Catálogo motivos de no venta"},{m:"Motivos devolución",:d=>"Catálogo de motivos devolución"},
      {m:"Incidencias",:d=>"Catálogo de incidencias"},{m:"Sabores",:d=>"Catálogo de sabores"},{m:"Cuotas",:d=>"Catálogo de Cuotas"},{m:"Comisiones",:d=>"Catálogo de comisiones"},{m:"Bancos",:d=>"Catálogo de bancos"},{m:"Grupos",:d=>"Catálogo de grupos"},{m:"Marcas",:d=>"Catálogo de marcas"},
      {m:"Unidad de medidas",:d=>"Catálogo de unidad de medidas"},{m:"Activos",:d=>"Catálogo de activos"},{m:"Ticket",:d=>"Catálogo de ticket"},{m:"Mensajes",:d=>"Catálogo de mensajes"},{m:"Encuestas",:d=>"Catálogo de encuestas"}, {m:"Generar ruta",:d=>"Proceso de generar ruta"},
      {m:"Carga de stock",:d=>"Proceso de carga de stock"}, {m:"Carga stock almacén",:d=>"Proceso de carga stock almacén"}, {m:"Backorder",:d=>"Proceso de backorder"}, {m:"Cuentas x cobrar",:d=>"Proceso de cuentas x cobrar"},
      {m:"GPS",:d=>"Reporte de GPS"}, {m:"Cobranza",:d=>"Reporte de cobranza"}, {m:"Comisiones",:d=>"Reporte de comisiones"}, {m:"Ventas",:d=>"Reporte de ventas"}, {m:"Devoluciones",:d=>"Reporte de devoluciones"}, {m:"Producto promoción",:d=>"Reporte de producto promoción"},
      {m:"Producto regalo",:d=>"Reporte de producto regalo"}, {m:"Visita sin venta",:d=>"Reporte de visita sin venta"}, {m:"Producto negado",:d=>"Reporte de producto negado"}, {m:"Recargas",:d=>"Reporte de recargas"}, {m:"Envases",:d=>"Reporte de envases"}, {m:"Consumos",:d=>"Reporte de consumos"}, {m:"Inventario sobrante",:d=>"Reporte de inventario sobrante"}, {m:"Inventario total",:d=>"Reporte de inventario total"},
      {m:"Bitácora tiempos",:d=>"Reporte de bitácora tiempos"}, {m:"Tripulación",:d=>"Reporte de tripulación"}, {m:"Cantidad productos por pedido",:d=>"Reporte de cantidad productos por pedido"}, {m:"Monitoreo de precios",:d=>"Reporte de monitoreo de precios"}, {m:"Liquidación",:d=>"Reporte de liquidación"}, {m:"Reporte consolidado de carga",:d=>"Reporte consolidado de carga"},
      {m:"Reporte consolidado piezas",:d=>"Reporte de consolidado piezas"}, {m:"Consolidado pedido del día siguiente",:d=>"Reporte consolidado pedido del día siguiente"}, {m:"Reporte consolidado de envases",:d=>"Reporte consolidado de envases"}]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:email, {  empresa_ids: [] }, :usuario, :password, :password_confirmation, :Nombre, :Supervisor,:empresa_id, :empresamadre_id, :cover, perfiles_por_Catalogo_attributes: [:id, :Altas, :Bajas, :Modi, :Listar, :usaPW], perfiles_por_Proceso_attributes: [:id, :Altas, :Bajas, :Modi, :Listar, :usaPW], perfiles_por_Reporte_attributes: [:id, :Altas, :Bajas, :Modi, :Listar, :usaPW], usuario_sucursales_attributes: [:id,  IdEmpresa: [] ])
    end

end
