class VentaController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:venta]

  def modulo
    @Descripcion_Modulo = "Reporte de ventas"
  end

  def venta
    elementos_de_busqueda
  #  @ventas = Vent.busqueda_general(params)
    @Sucursales = Empresa.all

    #@total_cajas = Vent.busqueda_total_cajas(params).sum("sum_Cja")
    #@total_piezas = Vent.busqueda_general(params).sum("sum_Pza")

    @pathDeBusqueda = venta_venta_path # esto es usando para colocar el path dentro de las tablas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable

    if params[:Entregas] == "1"
      @search_entregas = Pedidoliberado.busqueda_ventas_entregas(params).search(search_params)
      @search_entregas.sorts = 'Fecha_Entregado desc' if @search_entregas.sorts.empty?
      @entregas = @search_entregas.result().page(params[:entregas]).per(15)

      @total_efectivo = Pedidoliberado.busqueda_ventas_entregas(params).tipo_contado.map(&:TOTAL).compact.sum
      @efectivo = Pedidoliberado.busqueda_ventas_entregas(params).tipo_contado.pago_en_Efectivo.map(&:TOTAL).compact.sum
      @otros_depositos = Pedidoliberado.busqueda_ventas_entregas(params).pago_no_sea_Efectivo.map(&:TOTAL).compact.sum
      @total_credito = Pedidoliberado.busqueda_ventas_entregas(params).tipo_credito.map(&:TOTAL).compact.sum
      @total_contado = Pedidoliberado.busqueda_ventas_entregas(params).tipo_contado.map(&:TOTAL).compact.sum
      @total_venta = Pedidoliberado.busqueda_ventas_entregas(params).map(&:TOTAL).compact.sum
    else
      @search = Vent.busqueda_general(params).search(search_params)
      @search.sorts = 'Fecha desc' if @search.sorts.empty?
      @ventas = @search.result().page(params[:ventas]).per(15)

      @ventas_export = Vent.busqueda_general(params)
      @detalles_export = Detalleve.detalle_venta(params)
      @promociones_export = Pregalad.promocion_de_venta(params)

      @total_efectivo = Vent.busqueda_general(params).tipo_contado.map(&:TOTAL).compact.sum
      @efectivo = Vent.busqueda_general(params).tipo_contado.pago_en_Efectivo.map(&:TOTAL).compact.sum
      @otros_depositos = Vent.busqueda_general(params).pago_no_sea_Efectivo.map(&:TOTAL).compact.sum
      @total_credito = Vent.busqueda_general(params).tipo_credito.map(&:TOTAL).compact.sum
      @total_contado = Vent.busqueda_general(params).tipo_contado.map(&:TOTAL).compact.sum
      @total_venta = Vent.busqueda_general(params).sin_obsequios.map(&:TOTAL).compact.sum



    end

    @controlador = 'venta'
    @accion = 'venta'

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def detalle_venta
    @detalles = Detalleve.detalle_venta(params)
    @promociones = Pregalad.promocion_de_venta(params)

    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def detalle_entrega
    @detalles = Detallepedidolib.detalle_entrega(params)
    @promociones = Pregalad.promocion_de_venta_entregas(params)

    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def elementos_de_busqueda
    if params[:search6].nil?
      params[:search6] =  current_usuario.empresa_id
    end
    @pathDeBusqueda = venta_venta_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchClienteB = Cliente.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchClienteB.sorts = 'IdCli' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    @searchProductoB = Producto.activos.search(search_params)
    # make name the default sort column
    @searchProductoB.sorts = 'Clave' if @searchProductoB.sorts.empty?
    @productos_Selector = @searchProductoB.result().page(params[:productos])

    params[:ruta] = Ruta.find_by(Ruta:"#{params[:ruta_autocompletar]}", IdEmpresa: current_usuario.empresa_id)
    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])

  end

  def entrega_export
    @entregas_export = Pedidoliberado.busqueda_ventas_entregas(params)
    @detalles_export = Detallepedidolib.detalle_entrega(params)
    @promociones_export = Pregalad.promocion_de_venta_entregas(params)

    respond_to do |format|
      format.xls
    end
  end

  def pdf
    params[:search] = ""
    params[:search3] = ""
    params[:search2] = ""

    @venta = Vent.busqueda_general(params).find_by_Id(params[:V])
    @detalles = Detalleve.detalle_venta(params)

    respond_to do |format|
        format.html
        format.pdf do
          pdf = VentaPdf.new(@venta, @detalles)
          send_data pdf.render, filename: "Venta##{@venta.try(:Documento)}.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
    end
  end

  def pdf_entrega
    params[:search] = ""
    params[:search3] = ""
    params[:search2] = ""

    @venta = Vent.busqueda_general(params).find_by_Id(params[:V])
    @detalles = Detalleve.detalle_venta(params)

    respond_to do |format|
        format.html
        format.pdf do
          pdf = VentaPdf.new(@venta, @detalles)
          send_data pdf.render, filename: "Venta##{@venta.try(:Documento)}.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
    end
  end


end
