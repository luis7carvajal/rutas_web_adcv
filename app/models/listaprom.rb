class Listaprom < ActiveRecord::Base
  self.table_name = 'ListaPromo' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :activos, -> { where(Activa: true) }
  scope :inactivos, -> { where(Activa: false) }
  scope :comprobar_existencia, ->(paramc) { where(Lista: paramc)} # model

  has_many :detallepromo, -> { order 'Nivel asc' }, class_name: "Detalleprom", foreign_key: "PromoId"
  has_one :producto_base, -> { where(Tipo: false) }, class_name: "Detalleprom", foreign_key: "PromoId"

  has_one :detallelpromast, class_name: "Detallelpromast", foreign_key: "IdPromo"
  has_many :relclilis, class_name: "Relclili", foreign_key: "ListaPromo"

  accepts_nested_attributes_for :detallepromo, reject_if: proc { |attributes| attributes['Cantidad'].blank? and attributes['Monto'].blank? and attributes['Volumen'].blank? }, allow_destroy: true

  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model

  def self.promociones_out(params)
  query =  select('Listapromo.*')
          .where('Listapromo.Id not in (select distinct IdPromo from detallelpromaster where IdLm = ?) ', params[:IdPromo])
          .distinct
  end

  def self.promociones(params)
    query = where("(listapromo.id LIKE :id or :id = '') AND (Lista LIKE :Clave or :Clave = '') AND (Descripcion LIKE :Descripcion or :Descripcion = '')",{id: "%#{params[:id_param]}%", Clave: "%#{params[:Clave_param]}%", Descripcion: "%#{params[:Descripcion_param]}%"}) #Campos adicionales para la busqueda de todos los registros, el like es para buscar los que coincidan y para que funcione se deben colocar ambos signos de porcentaje
    #query = query.where("(FechaI LIKE :FechaInicial or :FechaInicial = '')",{FechaInicial: "%#{(params[:FechaI_param].try(:to_date)).try(:strftime,"%Y-%m-%d")}%"}) #Campos adicionales para la busqueda de todos los registros, el like es para buscar los que coincidan y para que funcione se deben colocar ambos signos de porcentaje
    query
  end


  #scope :with_tipoprom, ->{ select('listapromo.*,dp.Tipo,dp.TipoProm as TipoProm').joins('LEFT JOIN detallepromo dp on listapromo.id=dp.promoid').where('dp.Id IS NULL OR dp.Tipo = ?', false) }
  #Scopes y querys de ejemplos
  #scope :mostplayed, ->(player) { select('details.*, count(heros.id) AS hero_count').joins(:hero).where('player_id = ?', player.id).group('hero_id').order('hero_count DESC').limit(3) }

  #  def self.with_tipoprom
  #   joins(:detallepromo)
  #    .select('detallepromo.*, listapromo.*')
  #    .where('detallepromo.Id IS NULL OR detallepromo.Tipo = ?',false)
  #  end

end
