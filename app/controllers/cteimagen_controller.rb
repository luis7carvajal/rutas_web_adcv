class CteimagenController < ApplicationController
  before_action :set_cteimage, only: [:show, :edit, :update, :destroy]

  # GET /cteimagen
  # GET /cteimagen.json
  def index
    @cteimagen = Cteimage.all
  end

  # GET /cteimagen/1
  # GET /cteimagen/1.json
  def show
  end

  # GET /cteimagen/new
  def new
    @cteimage = Cteimage.new
  end

  # GET /cteimagen/1/edit
  def edit
  end

  # POST /cteimagen
  # POST /cteimagen.json
  def create
    @cteimage = Cteimage.new(cteimage_params)

    respond_to do |format|
      if @cteimage.save
        format.html { redirect_to @cteimage, notice: 'Cteimage was successfully created.' }
        format.json { render :show, status: :created, location: @cteimage }
      else
        format.html { render :new }
        format.json { render json: @cteimage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cteimagen/1
  # PATCH/PUT /cteimagen/1.json
  def update
    respond_to do |format|
      if @cteimage.update(cteimage_params)
        format.html { redirect_to @cteimage, notice: 'Cteimage was successfully updated.' }
        format.json { render :show, status: :ok, location: @cteimage }
      else
        format.html { render :edit }
        format.json { render json: @cteimage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cteimagen/1
  # DELETE /cteimagen/1.json
  def destroy
    @cteimage.destroy
    respond_to do |format|
      format.html { redirect_to cteimagen_url, notice: 'Cteimage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cteimage
      @cteimage = Cteimage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cteimage_params
      params.require(:cteimage).permit(:IdCli, :IdEmpresa)
    end
end
