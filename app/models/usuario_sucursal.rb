class UsuarioSucursal < ActiveRecord::Base
  belongs_to :usuario, foreign_key: "IdUsuario"
  belongs_to :empresa, foreign_key: "IdEmpresa"
end
