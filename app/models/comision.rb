class Comision < ActiveRecord::Base
  self.primary_key = "ID_Comision"
  self.table_name = "TH_Comision"
  scope :activos, -> { where(Status: "A") }
  scope :inactivos, -> { where(Status: "I") }

  belongs_to :producto, class_name:"Comision"
  has_many :td_comision, class_name: "Tdcomision", foreign_key: "ID_Comision"
  accepts_nested_attributes_for :td_comision
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model

  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |comision|
        csv << comision.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      if row["Status"] == "Activo"
         @Status = "A"
       else
         @Status = "I"
      end
      comision = (find_by_ID_Comision(row["Id"]))  || new
      comision.attributes = {ID_Producto: row["Clave del producto"], Comentarios: row["Descripción"], Status: @Status, IdEmpresa: empresa}



      #comision.td_comision = Tdcomision.find_by_ID_Detalle(row["ID_Detalle"])  || new
      comision.td_comision_attributes = [{RangoIni: row["RangoIni"], RangoFin: row["RangoFin"], Angente: row["Agente(Comision)"], Ayudante: row["Ayudante(Comision)"], IdEmpresa: empresa}]




      if comision.save
        # stuff to do on successful save
      else
        comision.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end

  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

end
