class ListacomboController < ApplicationController
  before_action :set_listcombo, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de combo"
  end

  # GET /listacombo
  # GET /listacombo.json
  def index
    contadores
    @pathDeBusqueda = listacombo_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Listcombo.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'id' if @search.sorts.empty?
    @listacombo = @search.result().page(params[:page]).per(15)

    @listcombo = Listcombo.new

    respond_to do |format|
      format.html
      format.js
    end

  end

  def listcombo_check
    @listcombox = Listcombo.comprobar_existencia(params[:listcombo][:Clave]).first
    respond_to do |format|
    format.json { render :json => !@listcombox }
    end
  end

  def contadores
    @cantidad_listcombo_activos = Listcombo.activos.por_empresa(current_usuario.empresa_id).count
    @cantidad_listcombo_inactivos = Listcombo.inactivos.por_empresa(current_usuario.empresa_id).count
  end

  def datos_listcombo
    respond_to do |format|
      format.js
    end
  end

  def inactivos
    contadores
    @pathDeBusqueda = listacombo_inactivos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Listcombo.inactivos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'id' if @search.sorts.empty?
    @listacombo = @search.result().page(params[:listcombo]).per(15)


    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /listacombo/1
  # GET /listacombo/1.json
  def show
  end

  # GET /listacombo/new
  def new
    @listcombo = Listcombo.new
  end

  # GET /listacombo/1/edit
  def edit
    @pathDeBusqueda = edit_listcombo_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @searchProductoB = Producto.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchProductoB.sorts = 'Clave' if @searchProductoB.sorts.empty?
    @productos_Selector = @searchProductoB.result().page(params[:productos])

    @listcombo.detallecombo.build
    @productos = Producto.all
    @cantidad_de_listcombo = Detacombo.productos_del_combo_count(params[:id]).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /listacombo
  # POST /listacombo.json
  def create
    if @puede_crear != true
      return
    end
    @listcombo = Listcombo.new(listcombo_params)
    respond_to do |format|
      if @listcombo.save
        contadores
        format.html { redirect_to @listcombo, notice: 'Listcombo was successfully created.' }
        format.json { render :show, status: :created, location: @listcombo }
        format.js {flash.now[:notice] = 'El combo se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @listcombo.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el combo.'} #ajax
      end
    end
  end

  # PATCH/PUT /listacombo/1
  # PATCH/PUT /listacombo/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @listcombo.update(listcombo_params)
        contadores
        format.html { redirect_to edit_listcombo_path(@listcombo), notice: 'El combo se actualizó de forma exitosa.' }
        format.json { render :show, status: :ok, location: @listcombo }
        format.js {flash.now[:notice] = 'El combo se ha modificado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @listcombo.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al modificar el combo.'} #ajax
      end
    end
  end

  # DELETE /listacombo/1
  # DELETE /listacombo/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to listacombo_url, notice: 'Listcombo was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El combo se ha eliminado de forma exitosa.'} #ajax
      if @listcombo.Activa == true
        @listcombo.update(Activa:false)
        format.js {flash.now[:notice] = 'El combo se ha borrado de forma exitosa.'} #ajax
      else
        @listcombo.update(Activa:true)
        format.js {flash.now[:notice] = 'El combo se ha habilitado de forma exitosa.'} #ajax
      end
      contadores
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listcombo
      @listcombo = Listcombo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listcombo_params
      params.require(:listcombo).permit(:Clave, :Descripcion, :Caduca, :FechaI, :FechaF, :Activa, :Monto, :IdEmpresa, detallecombo_attributes: [:id, :Articulo, :Cantidad, :TipMed, :IdEmpresa, :_destroy ])
    end
end
