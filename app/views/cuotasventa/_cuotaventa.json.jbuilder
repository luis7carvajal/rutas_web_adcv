json.extract! cuotaventa, :id, :year, :Ene, :Feb, :Mar, :Abr, :May, :Jun, :Jul, :Ago, :Sep, :Oct, :Nov, :Dic, :IdEmpresa, :created_at, :updated_at
json.url cuotaventa_url(cuotaventa, format: :json)