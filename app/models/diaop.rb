class Diaop < ActiveRecord::Base
  self.primary_key = "Id"
  self.table_name = 'DiasO' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :por_ruta, ->(ruta_id) { select("DiasO.*, (SELECT COALESCE(Nombre, '--') FROM Vendedores WHERE IdVendedor = (SELECT TOP 1 IdVendedor FROM BitacoraTiempos WHERE RutaId = DiasO.RutaId AND IdEmpresa = DiasO.IdEmpresa AND Diao = DiasO.DiaO)) AS Vendedor").where(RutaId: ruta_id) }

  def self.comprobar_si_existe_el_dia_para_el_detalle(params)
    query = select("MAX(DiaO) AS DiaO")
           .where("(RutaId = :rutaId) AND (Fecha = :fecha_a_consultar) AND (IdEmpresa = :idempresa)",{rutaId: params[:ruta_D], fecha_a_consultar: Time.now.strftime("%Y-%m-%d"), idempresa: params[:search6]})
           .group(:ID)
    query
  end

  def self.dia_de_remplazo_para_el_detalle(params)
    query = select("ISNULL(MAX(DiaO),0) AS DiaO")
           .where("(RutaId = :rutaId) AND (IdEmpresa = :idempresa)",{rutaId: params[:ruta_D], idempresa: params[:search6]})
           .group(:ID)
    query
  end

  def self.selector_dia_operativo(params)
    query = select("DiasO.Id, DiasO.DiaO, DiasO.Fecha, DiasO.RutaId, DiasO.VProg, DiasO.Ve, DiasO.IdEmpresa,Vendedores.idvendedor,Vendedores.Nombre AS Vendedor")
            .joins('JOIN BitacoraTiempos ON DiasO.DiaO = BitacoraTiempos.DiaO AND DiasO.RutaId = BitacoraTiempos.RutaId AND DiasO.IdEmpresa = BitacoraTiempos.IdEmpresa  INNER JOIN
                    Vendedores ON BitacoraTiempos.IdVendedor = Vendedores.IdVendedor')
            .where("(DiasO.RutaId = :rutaId) AND (DiasO.IdEmpresa = :idempresa)",{rutaId: params[:ruta], idempresa: params[:search6]})
            .group(:Fecha)
            .group('DiasO.Id, DiasO.DiaO, DiasO.RutaId, DiasO.VProg, DiasO.Ve, DiasO.IdEmpresa,Vendedores.idvendedor,Vendedores.Nombre')
  end



  def self.DiaO_para_POS(params)
    query = select("DiaO, Fecha")
           .where("(RutaId = :rutaId) AND (Fecha = :Fecha) AND (IdEmpresa = :idempresa)",{rutaId: params[:IdRuta],Fecha: (params[:Fecha]).strftime('%Y-%m-%d'), idempresa: params[:sucursal]})
    query
  end

end
