class UsuarioMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usuario_mailer.password_reset.subject
  #
  def password_reset
    @greeting = "Hi"

  #  mail to: "to@example.org"
    mail to: "luis7carvajal@gmail.com"
  end

  def forgot_password(user)
    @user = user
    @greeting = "Hi"

    #mail to: user.email, :subject => 'Reset password instructions'
    mail to: "luis7carvajal@gmail.com", :subject => 'Reset password instructions'
  end

end
