class Detalleve < ActiveRecord::Base
  self.primary_key = 'ID'
  scope :por_ruta, -> (param) { where(RutaId: param) }
  scope :por_documento, -> (param) { where(Docto: param) }
  scope :por_tipo, -> { where(TipoVta: "Contado") }
  scope :cajas, -> {where("DetalleVet.Tipo = 0")}
  scope :piezas, -> {where("DetalleVet.Tipo = 1")}
  scope :tipo_todas, -> {where("Venta.TipoVta <> 'Obsequio'")}
  scope :tipo_obsequio, -> {where("Venta.TipoVta = 'Obsequio'")}

  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  belongs_to :producto, class_name:"Producto", foreign_key: "Articulo"


  def self.detalle_venta(params)
    query = where("(DetalleVet.RutaId = :RutaId or :RutaId = '' or :RutaId IS NULL)",{RutaId: params[:rutaId]})
    query = query.where('DetalleVet.IdEmpresa = ?', params[:search6]) if params[:search6].present?
    query = query.where("(DetalleVet.Docto = :Docto or :Docto = '' or :Docto IS NULL)",{Docto: params[:docto]})

    query
   end

  def self.total_ventas_contado(params)
      query = select('detallevet.IVA, detallevet.Importe')
           .joins('left outer join venta ON venta.Documento=detallevet.Docto and venta.RutaId=detallevet.RutaId')
           .where("(venta.RutaId = :rutaId or :rutaId = '') AND (venta.TipoVta = 'Contado') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
           .distinct
      query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
      query
   end

   def self.total_ventas_credito(params)
       query = select('detallevet.IVA, detallevet.Importe')
            .joins('left outer join venta ON venta.Documento=detallevet.Docto and venta.RutaId=detallevet.RutaId')
            .where("(venta.RutaId = :rutaId or :rutaId = '') AND (venta.TipoVta = 'Credito') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
            .distinct
       query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
       query
    end

    def self.total_ventas_contadoCredito(params)
        query = select('detallevet.IVA, detallevet.Importe')
             .joins('left outer join venta ON venta.Documento=detallevet.Docto and venta.RutaId=detallevet.RutaId')
             .where("(venta.RutaId = :rutaId or :rutaId = '') AND (venta.TipoVta = 'Contado' or venta.TipoVta = 'Credito') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
             .distinct
        query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
        query
     end


   def self.rep_bitiempos_TotalesVenta(params)
     query = joins('INNER  JOIN Venta ON  DetalleVet.Docto=Venta.Documento AND  DetalleVet.RutaId=venta.RutaId AND DetalleVet.IdEmpresa = Venta.IdEmpresa')
     query = query.where('Venta.IdEmpresa = ?', params[:search6])
     query = query.where('Venta.RutaId = ?', params[:search])
     query = query.where('Venta.DiaO = ?', params[:diaO])
     query = query.where('Venta.Cancelada = 0')
     query = query.where('Venta.VendedorId = ?',params[:vendedor_id]) 
     query
   end

   def self.rep_bitiempos_TotalesObsequio_Piezas(params)
     query =  select("ISNULL(Pza,0) Piezas")
             .joins('INNER  JOIN Venta ON  DetalleVet.Docto=Venta.Documento AND  DetalleVet.RutaId=venta.RutaId AND DetalleVet.IdEmpresa = Venta.IdEmpresa')
             .where("(DetalleVet.Tipo='1') AND (venta.TipoVta='Obsequio') AND (Venta.Cancelada = 0) AND (venta.RutaId = :rutaId or :rutaId = '') AND (Venta.VendedorId = :VendedorId or :VendedorId = '') AND (Venta.IdEmpresa = :idempresa) AND (Venta.DiaO = :diao)",{rutaId: params[:search], VendedorId: params[:VendedorId], idempresa: params[:search6], diao: params[:diaO]})
     query
   end


end
