class Detalleco < ActiveRecord::Base
  self.primary_key = "Id"
  scope :por_ruta, -> (id_param) { where(Ruta: id_param) }

  belongs_to :cobran, class_name:"Cobran", foreign_key: "IdCobranza"
  belongs_to :formapag, class_name:"Formapag", foreign_key: "FormaP"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "Cliente"

#Consulta enviada por ana, no sé si se usará.
  def self.busqueda_cobranza(params)
      query = select("Rutas.Ruta, DetalleCob.Id, DetalleCob.Cliente AS Clave, C.Nombre, C.NombreCorto,  Vendedores.Clave AS ClaveVendedor, Vendedores.Nombre AS NombreVendedor, DetalleCob.Documento AS Folio, SUM(DetalleCob.Abono) AS Total,FormasPag.Forma, DetalleCob.Fecha AS FechaCob, Empresas.Sucursal AS Sucursal")
             .distinct
             .joins("INNER JOIN Clientes AS C ON DetalleCob.Cliente = C.IdCli INNER JOIN Rutas ON DetalleCob.RutaId = Rutas.IdRutas INNER JOIN
             BitacoraTiempos AS B ON DetalleCob.RutaId = B.RutaId AND DetalleCob.DiaO = B.DiaO AND B.Codigo = 'A18253' INNER JOIN Vendedores ON B.IdVendedor = Vendedores.IdVendedor INNER JOIN FormasPag ON DetalleCob.FormaP = FormasPag.IdFpag INNER JOIN Empresas ON Empresas.IdEmpresa = DetalleCob.IdEmpresa")
             .group("Rutas.Ruta, C.Nombre, Vendedores.IdVendedor, Vendedores.Clave, Vendedores.Nombre, C.NombreCorto, DetalleCob.IdEmpresa, FormasPag.Forma, DetalleCob.IdEmpresa, Empresas.Sucursal, DetalleCob.Id")
             .group(:Cliente, :Documento, :Fecha)
             .where("ISNULL(Cancelada, 0) = 0")
      query = query.where("DetalleCob.RutaId = ?", params[:search]) if params[:search].present?
      query = query.where("DetalleCob.Cliente = ?", params[:search2]) if params[:search2].present?
      query = query.where('DetalleCob.Fecha BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query = query.where('DetalleCob.DiaO = ?', params[:diaO]) if params[:diaO].present?
      query = query.where("(B.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
      query = query.where('DetalleCob.IdEmpresa = ?', params[:Sucursal_param]) if params[:Sucursal_param].present?
      query.order("FechaCob DESC")
  end

  def self.busqueda_cobranza_total(params)
      query = select("Abono")
            .joins("INNER JOIN Clientes AS C ON DetalleCob.Cliente = C.IdCli INNER JOIN Rutas ON DetalleCob.RutaId = Rutas.IdRutas INNER JOIN
             BitacoraTiempos AS B ON DetalleCob.RutaId = B.RutaId AND DetalleCob.DiaO = B.DiaO AND B.Codigo = 'A18253' INNER JOIN Vendedores ON B.IdVendedor = Vendedores.IdVendedor INNER JOIN FormasPag ON DetalleCob.FormaP = FormasPag.IdFpag")
            .where("ISNULL(Cancelada, 0) = 0")
      query = query.where("DetalleCob.RutaId = ?", params[:search]) if params[:search].present?
      query = query.where("DetalleCob.Cliente = ?", params[:search2]) if params[:search2].present?
      query = query.where("DetalleCob.IdEmpresa = ?", params[:search6]) if params[:search6].present?
      query = query.where('DetalleCob.Fecha BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query = query.where('DetalleCob.DiaO = ?', params[:diaO]) if params[:diaO].present?
      query = query.where("(B.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
      query
  end

  def self.por_cobranza(params)
    query = where('IdCobranza = ?', params[:IdCobranza])
  end


  def self.exportar_excel_reporte_cobranza(params)
    query = select("Rutas.Ruta, DetalleCob.Cliente AS Clave, C.Nombre, C.NombreCorto,  Vendedores.Clave AS ClaveVendedor, Vendedores.Nombre AS NombreVendedor, DetalleCob.Documento AS Folio, SUM(DetalleCob.Abono) AS Total,FormasPag.Forma, FORMAT(DetalleCob.Fecha, 'dd-MM-yyyy') AS FechaCob")
           .distinct
           .joins("INNER JOIN Clientes AS C ON DetalleCob.Cliente = C.IdCli INNER JOIN Rutas ON DetalleCob.RutaId = Rutas.IdRutas INNER JOIN
           BitacoraTiempos AS B ON DetalleCob.RutaId = B.RutaId AND DetalleCob.DiaO = B.DiaO AND B.Codigo = 'A18253' INNER JOIN Vendedores ON B.IdVendedor = Vendedores.IdVendedor INNER JOIN FormasPag ON DetalleCob.FormaP = FormasPag.IdFpag")
           .group("Rutas.Ruta, DetalleCob.Cliente, C.Nombre, DetalleCob.Documento, Vendedores.IdVendedor, Vendedores.Clave, Vendedores.Nombre, C.NombreCorto, DetalleCob.Fecha, FormasPag.Forma")
           .where("ISNULL(Cancelada, 0) = 0")
    query = query.where("DetalleCob.RutaId = ?", params[:search]) if params[:search].present?
    query = query.where("DetalleCob.Cliente = ?", params[:search2]) if params[:search2].present?
    query = query.where("DetalleCob.IdEmpresa = ?", params[:search6]) if params[:search6].present? && params[:search].present?
    query = query.where('DetalleCob.Fecha BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
           .order('Folio')
    query
  end


end
