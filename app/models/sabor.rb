class Sabor < ActiveRecord::Base
  self.primary_key = "Id"
  self.table_name = 'Sabores'
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }


end
