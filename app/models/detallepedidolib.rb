class Detallepedidolib < ActiveRecord::Base
  self.primary_key = "ID"
  scope :cajas, -> {where("Detallepedidoliberado.TIPO = 0")}
  scope :piezas, -> {where("Detallepedidoliberado.TIPO = 1")}
  scope :tipo_todas, -> {where("Pedidosliberados.TIPO <> 'Obsequio'")}
  scope :tipo_obsequio, -> {where("Pedidosliberados.TIPO = 'Obsequio'")}



  def self.detalle_pdf_visor_pedidos(params)
    query = select("detallepedidoliberado.Pedido,detallepedidoliberado.Ruta,detallepedidoliberado.articulo,detallepedidoliberado.PZA_LIBERADAS,case When detallepedidoliberado.Tipo='1' Then detallepedidoliberado.PZA_LIBERADAS Else detallepedidoliberado.PZA_LIBERADAS*X.PzaXCja End AS CANTIDAD")
           .joins('INNER Join ProductosXPzas X On detallepedidoliberado.ARTICULO=X.Producto Join Productos A On A.Clave=detallepedidoliberado.ARTICULO')
           .where('(detallepedidoliberado.Pedido= ?) AND (detallepedidoliberado.Ruta= ?) AND (detallepedidoliberado.IdEmpresa= ?)',(params[:P]),(params[:R]), (params[:IdEmpresa]))
    query
  end

   def self.detalle_entrega(params)
    query = select("R.Ruta, detallepedidoliberado.PEDIDO, P.Clave as Clave_Producto, P.Producto, P.IVA as IVA_Producto, detallepedidoliberado.Tipo, detallepedidoliberado.PZA, detallepedidoliberado.PRECIO, detallepedidoliberado.IVA, detallepedidoliberado.IMPORTE, detallepedidoliberado.DESCUENTO")
   	query = query.joins('INNER Join Productos P On Detallepedidoliberado.ARTICULO=P.Clave INNER Join Rutas R On R.IdRutas=detallepedidoliberado.Ruta')
    query = query.where("(Detallepedidoliberado.Ruta = :RutaId or :RutaId = '' or :RutaId IS NULL)",{RutaId: params[:rutaId]})
    query = query.where('Detallepedidoliberado.IDEMPRESA = ?', params[:search6]) if params[:search6].present?
    query = query.where("(Detallepedidoliberado.PEDIDO = :PEDIDO or :PEDIDO = '' or :PEDIDO IS NULL)",{PEDIDO: params[:Pedido]})
    query = query.distinct

    query
   end

    def self.rep_bitiempos_TotalesEntregas(params)
     query = joins('INNER  JOIN Pedidosliberados ON  Detallepedidoliberado.PEDIDO=Pedidosliberados.PEDIDO AND  Detallepedidoliberado.RUTA=pedidosliberados.RUTA AND Detallepedidoliberado.IDEMPRESA = Pedidosliberados.IDEMPRESA')
     query = query.where('Pedidosliberados.IDEMPRESA = ?', params[:search6])
     query = query.where('Pedidosliberados.RUTA = ?', params[:search])
     query = query.where('Pedidosliberados.DIAO_ENTREGA = ?', params[:diaO])
     query = query.where('Pedidosliberados.CANCELADA = 0')
     query = query.where('Pedidosliberados.AGENTE_ENTREGA = ?',params[:vendedor_id])
     query = query.where("Pedidosliberados.STATUS = 5")
     query
   end


end
