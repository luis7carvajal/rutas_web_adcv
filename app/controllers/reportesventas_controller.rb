class ReportesventasController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:Visita_sin_venta]

  def modulo
    if action_name == "Visita_sin_venta"
      @Descripcion_Modulo = "Reporte de visita sin venta"
    end
  end

  def Visita_sin_venta
    elementos_de_busqueda
    @pathDeBusqueda = reportesventas_Visita_sin_venta_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @controlador = 'reportesventas'
    @accion = 'Visita_sin_venta'
    respond_to do |format|
      format.html
      format.js
    end
  end

  def elementos_de_busqueda
    elementos_de_busqueda_ruta_autocompletar
    params[:search6] = current_usuario.empresa_id
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
    @vendedores = Relvendruta.por_ruta(params[:ruta])
  end

  def busqueda_noventas
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @noventas = Noventasingular.busqueda_general(params)
    respond_to do |format|
      format.js
    end
  end

end
