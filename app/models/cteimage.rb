class Cteimage < ActiveRecord::Base
  has_attached_file :Foto, styles: { thumb: "400x300", minithumb: "80x55" }
  validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/#validacion para evitar ataques, y solo esperar un tipo de archivo en content type, como pdf, ejecutables, imagenes etc. con el tipo de archivo especificado, el usuario puede subir varios tipos de imagenes
end
