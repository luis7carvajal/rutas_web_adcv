class Detallepedido < ActiveRecord::Base
  scope :cajas, -> {where("Detallepedidos.Tipo = 0")}
  scope :piezas, -> {where("Detallepedidos.Tipo = 1")}
  scope :tipo_todas, -> {where("Pedidos.Tipo <> 'Obsequio'")}
  scope :tipo_obsequio, -> {where("Pedidos.Tipo = 'Obsequio'")}

	def self.rep_bitiempos_TotalesEntregas_Pedidos(params)
     query = joins('INNER JOIN Pedidos ON Detallepedidos.Docto=Pedidos.Pedido AND Detallepedidos.RutaId=pedidos.Ruta AND Detallepedidos.IdEmpresa = Pedidos.IdEmpresa')
     query = query.where('Pedidos.IdEmpresa = ?', params[:search6])
     query = query.where('Pedidos.Ruta = ?', params[:search])
     query = query.where('Pedidos.DiaO = ?', params[:diaO])
     query = query.where('Pedidos.Cancelado = 0')
     query = query.where('Pedidos.IdVendedor = ?',params[:vendedor_id])
     query
   end
end
