class Relvendruta < ActiveRecord::Base
  self.primary_key = "Id"
  self.table_name = 'RelVendrutas' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)

  scope :vendedores_que_tiene, -> (id_param) { where(IdRuta: id_param) }
  belongs_to :vendedor, class_name:"Vendedor", foreign_key: "IdVendedor"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "IdRuta"
  scope :por_ruta, ->(paramRuta) { where(IdRuta: paramRuta)} # model
  validates :IdVendedor, presence: true
  validates :IdRuta, presence: true
  validates :Fecha, presence: true

  def self.total_vendedores_Ruta(params)
    select("relvendrutas.IdRuta")
     .where("(relvendrutas.IdRuta = :rutaId or :rutaId = '') AND (relvendrutas.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
  end

  def self.acceso_pos_vendedor(params)
    query = select("V.IdVendedor,V.Nombre, R.Ruta, R.IdRutas")
           .joins('INNER JOIN vendedores V ON relvendrutas.IdVendedor = V.IdVendedor INNER JOIN rutas R ON relvendrutas.IdRuta = R.IdRutas')
           .where("(relvendrutas.IdEmpresa = :idempresa) AND (R.Ruta = :Ruta) AND (V.PdaPW = :PdaPW) ",{Ruta: params[:Ruta], PdaPW: params[:PdaPW], idempresa: params[:sucursal]})
  end

end
