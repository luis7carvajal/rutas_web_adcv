class Deta < ActiveRecord::Base
  belongs_to :producto, class_name:"Producto", foreign_key: "Articulo"
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :productosquetiene, -> (id_param) { where(ListaId: id_param) }

  belongs_to :list, class_name:"List", foreign_key: "ListaId"

  validates :Articulo, presence: true
  validates :PrecioMin, presence: true

end
