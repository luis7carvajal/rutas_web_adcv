require 'test_helper'

class CuotasventaControllerTest < ActionController::TestCase
  setup do
    @cuotaventa = cuotasventa(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cuotasventa)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cuotaventa" do
    assert_difference('Cuotaventa.count') do
      post :create, cuotaventa: { Abr: @cuotaventa.Abr, Ago: @cuotaventa.Ago, Dic: @cuotaventa.Dic, Ene: @cuotaventa.Ene, Feb: @cuotaventa.Feb, IdEmpresa: @cuotaventa.IdEmpresa, Jul: @cuotaventa.Jul, Jun: @cuotaventa.Jun, Mar: @cuotaventa.Mar, May: @cuotaventa.May, Nov: @cuotaventa.Nov, Oct: @cuotaventa.Oct, Sep: @cuotaventa.Sep, year: @cuotaventa.year }
    end

    assert_redirected_to cuotaventa_path(assigns(:cuotaventa))
  end

  test "should show cuotaventa" do
    get :show, id: @cuotaventa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cuotaventa
    assert_response :success
  end

  test "should update cuotaventa" do
    patch :update, id: @cuotaventa, cuotaventa: { Abr: @cuotaventa.Abr, Ago: @cuotaventa.Ago, Dic: @cuotaventa.Dic, Ene: @cuotaventa.Ene, Feb: @cuotaventa.Feb, IdEmpresa: @cuotaventa.IdEmpresa, Jul: @cuotaventa.Jul, Jun: @cuotaventa.Jun, Mar: @cuotaventa.Mar, May: @cuotaventa.May, Nov: @cuotaventa.Nov, Oct: @cuotaventa.Oct, Sep: @cuotaventa.Sep, year: @cuotaventa.year }
    assert_redirected_to cuotaventa_path(assigns(:cuotaventa))
  end

  test "should destroy cuotaventa" do
    assert_difference('Cuotaventa.count', -1) do
      delete :destroy, id: @cuotaventa
    end

    assert_redirected_to cuotasventa_path
  end
end
