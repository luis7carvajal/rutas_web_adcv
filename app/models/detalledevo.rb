class Detalledevo < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  belongs_to :producto, class_name:"Producto", foreign_key: "SKU"



   def self.rep_bitiempos_devoluciones(params)
   	query = joins('JOIN Devoluciones ON DetalleDevol.Docto=Devoluciones.Docto AND Devoluciones.Ruta=DetalleDevol.RutaId AND Devoluciones.IdEmpresa = DetalleDevol.IdEmpresa')
   	query = query.where('Devoluciones.Cancelada = 0')
   	query = query.where('Devoluciones.IdEmpresa = ?', params[:search6])
   	query = query.where('Devoluciones.Ruta = ?', params[:search])
   	query = query.where('Devoluciones.DiaO = ?', params[:diaO])
   	query = query.where("Detalledevol.Motivo <> 'Comodato'")
    query
   end

   def self.detalle_devolucion
    query = select("detalledevol.*, (SELECT MvoDev FROM MvoDev WHERE MvoDev.Clave=detalledevol.Motivo) AS motivo_descripcion")
   end
end
