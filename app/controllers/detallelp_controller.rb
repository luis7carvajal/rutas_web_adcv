class DetallelpController < ApplicationController
  before_action :set_deta, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /detallelp
  # GET /detallelp.json
  def index
    datos_relcliruta

  end

  def datos_relcliruta
    @searchp = Producto.activos.without_price(params[:id]).search(search_params)
    @searchp.sorts = 'Clave' if @searchp.sorts.empty?
    @productos = @searchp.result().page(params[:productos]).per(15)

    @search = Deta.productosquetiene(params[:id]).search(search_paramsx)
    @search.sorts = 'Articulo' if @search.sorts.empty?
    @detallelp = @search.result().page(params[:detallelp]).per(10)

    @deta = Deta.new
    @list = List.comp_si_es_por_rango_de_precios(params[:id])

  end

  # GET /detallelp/1
  # GET /detallelp/1.json
  def show
  end

  # GET /detallelp/new
  def new
    @deta = Deta.new
  end

  # GET /detallelp/1/edit
  def edit
  end

  # POST /detallelp
  # POST /detallelp.json
  def create
    @deta = Deta.new(deta_params)
    @list = List.comp_si_es_por_rango_de_precios(@deta.ListaId)
    @producto_seleccionado = Producto.find_by_Clave(params[:producto_siguiente]) if params[:producto_siguiente].present?

    respond_to do |format|
      if @deta.save
        if @list.Tipo == TRUE
          @deta.update(PrecioMax: deta_params[:PrecioMin])
        end
        format.html { redirect_to @deta, notice: 'Deta was successfully created.' }
        format.json { render :show, status: :created, location: @deta }
        format.js {flash.now[:notice] = 'El producto se ha agregado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @deta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al agregar el producto.'} #ajax
      end
    end
    @detalle = @deta
    @deta = Deta.new(deta_params)

  end

  # PATCH/PUT /detallelp/1
  # PATCH/PUT /detallelp/1.json
  def update
    respond_to do |format|
      if @deta.update(deta_params)
        @list = List.comp_si_es_por_rango_de_precios(@deta.ListaId)
        if @list.Tipo == TRUE
          @deta.update(PrecioMax: deta_params[:PrecioMin])
        end
        format.html { redirect_to @deta, notice: 'Deta was successfully updated.' }
        format.json { render :show, status: :ok, location: @deta }
        format.js {flash.now[:notice] = 'Se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @deta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar.'} #ajax
      end
    end
  end

  # DELETE /detallelp/1
  # DELETE /detallelp/1.json
  def destroy
    @deta.destroy
    @list = List.comp_si_es_por_rango_de_precios(@deta.ListaId)
    params[:id]=@list.id
    @producto = Producto.find_by_Clave(@deta.Articulo)
    respond_to do |format|
      format.html { redirect_to detallelp_url, notice: 'Deta was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El producto se ha quitado de forma exitosa.'} #ajax
    end
  end


  def lp_productos_agregar_seleccionar_todos_masivo
    params[:IdEmpresa] = current_usuario.empresa_id

    @searchp = Producto.activos.without_price(params[:id]).search(search_params)
    @todos_los_productos_out_seleccionados = @searchp.result()

    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def lp_productos_agregar_de_forma_masiva
    params[:lista_productos_out].each do |(c,producto_clave)|
      @producto_seleccionado = Producto.find_by_Clave(producto_clave)
    end

    @deta = Deta.new
    @list = List.comp_si_es_por_rango_de_precios(params[:id_lista])
    #datos_relcliruta
    #contadores
    respond_to do |format|
        format.js
    end
  end

  def lp_productos_eliminar_seleccionar_todos_masivo
    params[:search6] = current_usuario.empresa_id

    @search = Deta.productosquetiene(params[:id]).search(search_paramsx)
    @todos_los_productos_in_seleccionados = @search.result()

    respond_to do |format|
      format.js {flash.now[:notice] = 'Los productos se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def lp_productos_eliminar_de_forma_masiva
    params[:lista_productos_in].each do |(c,deta_id)|
      @deta = Deta.find_by(id: deta_id)
      @deta.destroy
    end

    datos_relcliruta
    respond_to do |format|
        format.js {flash.now[:notice] = 'Todos los productos seleccionados se han desvinculado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deta
      @deta = Deta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deta_params
      params.require(:deta).permit(:ListaId, :Articulo, :PrecioMin, :PrecioMax, :IdEmpresa)
    end
end
