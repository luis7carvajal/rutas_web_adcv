class Vendedor < ActiveRecord::Base
  self.primary_key = 'IdVendedor'
  self.table_name = 'Vendedores' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  scope :comprobar_existencia, ->(paramc) { where(Clave: paramc)} # model
  before_save :valores_por_default

  scope :vendedores, -> { where(tipo: "Vendedor") }
  #scope :vendedores, -> { where("tipo = ? OR tipo IS NULL", 'Vendedor') } ya no es necesario debido a que actualice todos los registros nulos
  @presence = "No puede estar en blanco"

  validates :Clave, presence: {message: @presence}
  validates :Nombre, presence: {message: @presence}  #presence true validara que el elemento no se pueda guardar vacio y uniquenes para que no se repita en la base de datos
  validates :Direccion, presence: {message: @presence}
  validates :NumLicencia, presence: {message: @presence}
  validates :VenceLic, presence: {message: @presence}
  validates :Tipo, presence: {message: @presence}

  scope :ayudantes, -> { where(Tipo: "Ayudante") }
  has_one :ruta, class_name: "Ruta", foreign_key: "Vendedor"
  has_many :venta, class_name: "Vent", foreign_key: "VendedorId"
  has_many :reldaycli, class_name: "Reldayc", foreign_key: "idVendedor"

  has_one :asistente1, class_name: "Ruta", foreign_key: "id_ayudante1"
  has_one :asistente2, class_name: "Ruta", foreign_key: "id_ayudante2"
  has_many :bitacoratiempos, class_name: "Bitacoratiempo", foreign_key: "IdVendedor"

  scope :vendedores_sin_ruta, -> { joins('left outer join rutas on vendedores.IdVendedor=rutas.Vendedor').select('vendedores.*,rutas.Vendedor').where('rutas.Vendedor is null') }
  #scope que busca aquellos registros que no esten asociados a rutas

  scope :ordered, -> { order(Nombre: :asc) }


  def self.vendedores_fuera_de_ruta(params)
    query = select('vendedores.*')
           .joins('left outer join relvendrutas on vendedores.IdVendedor=relvendrutas.IdVendedor')
           .where('( relvendrutas.IdRuta IS NULL OR relvendrutas.IdRuta != ? ) AND vendedores.Status = ? AND vendedores.IdEmpresa = ? AND vendedores.IdVendedor not in (select distinct IdVendedor from relvendrutas where IdRuta = ?) ', params[:id], true, params[:IdEmpresa], params[:id])
           .distinct
  end

  def self.vendedores_fuera_de_ruta2_contador(params)
    query = joins('left outer join relvendrutas on vendedores.IdVendedor=relvendrutas.IdVendedor')
           .where('( relvendrutas.IdRuta IS NULL OR relvendrutas.IdRuta != ? ) AND vendedores.Status = ? AND vendedores.IdEmpresa = ? AND vendedores.IdVendedor not in (select distinct IdVendedor from relvendrutas where IdRuta = ?) ', params[:id], true, params[:IdEmpresa], params[:id])
           .distinct
  end



  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |vendedor|
        csv << vendedor.attributes.values_at(*column_names)
      end
    end
  end



  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      vendedor = (find_by_IdVendedor(row["Id"]) and find_by_Clave(row["Clave"]))  || new
      vendedor.attributes = {Clave: row["Clave"], Nombre: row["Nombre"], Direccion: row["Calle y N°"], Telefono: row["Teléfono"], NumLicencia: row["Numero de Licencia"], VenceLic: row["Vence Licencia"], PdaPw: row["PdaPw"], Movil: row["Móvil"], Tipo: row["Categoría"], CP: row["C.P"], Colonia: row["Colonia"], Latitud: row["Latitud"], Longitud: row["Longitud"], Referencia: row["Referencia"], Status: row["Status"], IdEmpresa: empresa}

      if vendedor.save
        # stuff to do on successful save
      else
        vendedor.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end


  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

  def valores_por_default
  	self.MetaDiaria ||= 0
    self.MetaMes ||= 0
    self.Telefono ||= ""
    self.PdaPw ||= ""
    self.Movil ||= ""

  end

  end
