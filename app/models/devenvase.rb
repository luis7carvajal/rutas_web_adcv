class Devenvase < ActiveRecord::Base
  self.primary_key = 'ID'

  #  .where("DevEnvases.Tipo='Venta' AND DevEnvases.RutaId='726' AND DevEnvases.DiaO='94'")

  #  .where("DevEnvases.Tipo='Venta' AND DevEnvases.RutaId='726' AND DevEnvases.DiaO='86'")
  #  .where("DevEnvases.Tipo='Venta' AND DevEnvases.RutaId='726' AND DevEnvases.DiaO='86'")


  def self.busqueda_rep_envases(params)
    query = select("DevEnvases.ID,DevEnvases.CodCli AS Clave,C.Nombre as Responsable, C.NombreCorto as Nombre_Comercial,DevEnvases.Articulo,P.Producto,P1.Producto Envase,SUM(DevEnvases.Cantidad) Entregados,SUM(DevEnvases.Devuelto) Devueltos,SUM(DevEnvases.Cantidad)-SUM(DevEnvases.Devuelto) Saldo ")
           .distinct
           .joins('INNER JOIN Clientes C ON DevEnvases.CodCli=C.IdCli Inner Join Productos P ON DevEnvases.Articulo=P.Clave Inner Join Productos P1 ON DevEnvases.Envase=P1.Clave')
           .where("(DevEnvases.Tipo = :Tipo_Env or :Tipo_Env = '') AND (DevEnvases.RutaId = :rutaId or :rutaId = '') AND (DevEnvases.DiaO = :diaO or :diaO = '') AND (DevEnvases.IdEmpresa = :idempresa)",{Tipo_Env: params[:Tipo_Env], rutaId: params[:search], diaO: params[:diaO], idempresa: params[:search6]})
           .group('DevEnvases.ID,DevEnvases.CodCli,C.Nombre,DevEnvases.Articulo,DevEnvases.Tipo,P.Producto,P1.Producto,C.NombreCorto')

    query = query.where("(DevEnvases.CodCli LIKE :Clave or :Clave = '') AND (C.Nombre LIKE :Nombre or :Nombre = '')  AND (C.NombreCorto LIKE :Nombre_Comercial or :Nombre_Comercial = '') AND (DevEnvases.Articulo LIKE :Articulo or :Articulo = '') AND (P.Producto LIKE :Producto or :Producto = '') AND (P1.Producto LIKE :Envase or :Envase = '')",
    {Clave: "%#{params[:Clave_param]}%", Nombre: "%#{params[:Nombre_param]}%", Nombre_Comercial: "%#{params[:Nombre_Comercial_param]}%", Articulo: "%#{params[:Articulo_param]}%", Producto: "%#{params[:Producto_param]}%",Envase: "%#{params[:Envase_param]}%"})
         ##Campos adicionales para la busqueda de todos los registros, el like es para buscar los que coincidan y para que funcione se deben colocar ambos signos de porcentaje
    query
  end

  def self.busqueda_rep_envases_devueltos(params)
    query = select("DevEnvases.ID,DevEnvases.CodCli AS Clave,C.Nombre as Responsable, C.NombreCorto as Nombre_Comercial,DevEnvases.Articulo,P.Producto,P1.Producto Envase,SUM(DevEnvases.Cantidad) Entregados,SUM(DevEnvases.Devuelto) Devueltos,SUM(DevEnvases.Cantidad)-SUM(DevEnvases.Devuelto) Saldo ")
           .distinct
           .joins('INNER JOIN Clientes C ON DevEnvases.CodCli=C.IdCli Inner Join Productos P ON DevEnvases.Articulo=P.Clave Inner Join Productos P1 ON DevEnvases.Envase=P1.Clave')
           .where("(DevEnvases.RutaId = :rutaId or :rutaId = '') AND (DevEnvases.DiaO = :diaO or :diaO = '') AND (DevEnvases.IdEmpresa = :idempresa)",{Tipo_Env: params[:Tipo_Env], rutaId: params[:search], diaO: params[:diaO], idempresa: params[:search6]})
           .group('DevEnvases.ID,DevEnvases.CodCli,C.Nombre,DevEnvases.Articulo,P.Producto,P1.Producto,C.NombreCorto')

    query = query.where("(DevEnvases.CodCli LIKE :Clave or :Clave = '') AND (C.Nombre LIKE :Nombre or :Nombre = '')  AND (C.NombreCorto LIKE :Nombre_Comercial or :Nombre_Comercial = '') AND (DevEnvases.Articulo LIKE :Articulo or :Articulo = '') AND (P.Producto LIKE :Producto or :Producto = '') AND (P1.Producto LIKE :Envase or :Envase = '')",
    {Clave: "%#{params[:Devueltos_Clave_param]}%", Nombre: "%#{params[:Devueltos_Nombre_param]}%", Nombre_Comercial: "%#{params[:Devueltos_Nombre_Comercial_param]}%", Articulo: "%#{params[:Devueltos_Articulo_param]}%", Producto: "%#{params[:Devueltos_Producto_param]}%",Envase: "%#{params[:Devueltos_Envase_param]}%"})
         ##Campos adicionales para la busqueda de todos los registros, el like es para buscar los que coincidan y para que funcione se deben colocar ambos signos de porcentaje
    query
  end





end
