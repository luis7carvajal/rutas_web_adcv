class ClientesController < ApplicationController
  before_action :set_cliente, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de clientes"
  end

  # GET /clientes
  # GET /clientes.json


  def index
    @pathDeBusqueda = clientes_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Cliente.clientes.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdCli' if @search.sorts.empty?
    @clientes = @search.result().page(params[:page]).per(15)

    @cantidad_clientes_activos = Cliente.activos.por_empresa(current_usuario.empresa_id).count
    @cantidad_clientes_activos_credito = Cliente.activos.por_empresa(current_usuario.empresa_id).credito.count
    @cantidad_clientes_activos_contado = Cliente.activos.por_empresa(current_usuario.empresa_id).contado.count

    @clientesExp = Cliente.exportar_excel_clientes.activos.por_empresa(current_usuario.empresa_id)
    p "*************************"
    p @clientesExp.to_sql
    p "*************************"
    @cliente = Cliente.new
    @cliente.build_relclicla
    @cp = Codigopostal.searchCP(params[:search]).first

    @clas1 = Clascliente.clas1_scope
    @clas2 = Clascliente.clas2_scope
    @clas3 = Clascliente.clas3_scope
    @clas4 = Clascliente.clas4_scope
    @clas5 = Clascliente.clas5_scope

    respond_to do |format|
      format.html
      format.csv { send_data @clientesExp.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
      format.js
    end
  end

  def inactivos
    @pathDeBusqueda = clientes_inactivos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Cliente.clientes.inactivos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdCli' if @search.sorts.empty?
    @clientes = @search.result().page(params[:page]).per(15)

    @cantidad_clientes_inactivos = Cliente.inactivos.por_empresa(current_usuario.empresa_id).count

    @clientesExp = Cliente.inactivos.por_empresa(current_usuario.empresa_id)
    @cp = Codigopostal.searchCP(params[:search]).first

    @clas1 = Clascliente.clas1_scope
    @clas2 = Clascliente.clas2_scope
    @clas3 = Clascliente.clas3_scope
    @clas4 = Clascliente.clas4_scope
    @clas5 = Clascliente.clas5_scope

    respond_to do |format|
      format.html
      format.csv { send_data @clientesExp.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
      format.js
    end
  end

  def datos_cliente
    params[:IdEmpresa] = current_usuario.empresa_id
    @cliente = Cliente.datos_cliente(params).last

    @cliente.build_relclicla
    @clas1 = Clascliente.clas1_scope
    @clas2 = Clascliente.clas2_scope
    @clas3 = Clascliente.clas3_scope
    @clas4 = Clascliente.clas4_scope
    @clas5 = Clascliente.clas5_scope
    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def import
    empresa = current_usuario.empresa_id
    @errors = Cliente.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to clientes_path, notice: "clientes importados."
    end
  end

  def clientes_check
    @clientex = Cliente.comprobar_existencia(params[:cliente][:IdCli]).first
    respond_to do |format|
    format.json { render :json => !@clientex }
    end
  end

  def busqueda_cp
    @cp = Codigopostal.search(params[:search]).first
    respond_to do |format|
    format.js
    end
  end


  # GET /clientes/1
  # GET /clientes/1.json
  def show
  end

  # GET /clientes/new
  def new
    @cliente = Cliente.new
  end

  # GET /clientes/1/edit
  def edit
  end

  # POST /clientes
  # POST /clientes.json
  def create
    if @puede_crear != true
      return
    end
    @clas1 = Clascliente.clas1_scope
    @clas2 = Clascliente.clas2_scope
    @clas3 = Clascliente.clas3_scope
    @clas4 = Clascliente.clas4_scope
    @clas5 = Clascliente.clas5_scope


    @cliente = Cliente.new(cliente_params)

    respond_to do |format|
      if @cliente.save
        format.html { redirect_to @cliente, notice: 'Cliente was successfully created.' }
        format.json { render :show, status: :created, location: @cliente }
        format.js {flash.now[:notice] = 'El cliente se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el cliente.'} #ajax
      end
    end
  end

  # PATCH/PUT /clientes/1
  # PATCH/PUT /clientes/1.json
  def update
    if @puede_editar != true
      return
    end
    @clas1 = Clascliente.clas1_scope
    @clas2 = Clascliente.clas2_scope
    @clas3 = Clascliente.clas3_scope
    @clas4 = Clascliente.clas4_scope
    @clas5 = Clascliente.clas5_scope

    respond_to do |format|
      if @cliente.update(cliente_params)
        format.html { redirect_to @cliente, notice: 'Cliente was successfully updated.' }
        format.json { render :show, status: :ok, location: @cliente }
        format.js {flash.now[:notice] = 'El cliente se ha actualizado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el cliente.'} #ajax

      end
    end
  end

  # DELETE /clientes/1
  # DELETE /clientes/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    Relcliruta.where(:IdCliente => @cliente.IdCli).destroy_all
    Reldayc.where(:CodCli => @cliente.IdCli).destroy_all
    Relclili.where(:CodCliente => @cliente.IdCli).destroy_all

    respond_to do |format|
      format.html { redirect_to clientes_url, notice: 'Cliente was successfully destroyed.' }
      format.json { head :no_content }
      if @cliente.Status == true
        @cliente.update(Status:false)
        format.js {flash.now[:notice] = 'El cliente se ha borrado de forma exitosa.'} #ajax
      else
        @cliente.update(Status:true)
        format.js {flash.now[:notice] = 'El cliente se ha habilitado de forma exitosa.'} #ajax
      end

    end
  end

  def imprimir_de_forma_masiva
    params[:lista].each do |(c,pedido_id)|
      @pedido = Pedido.find_by(IdPedido: pedido_id)
      if @pedido.Status == 2
        @pedido.update(Status: 3)
        Pedidoliberado.where(PEDIDO: @pedido.Pedido, RUTA: @pedido.Ruta, COD_CLIENTE: @pedido.CodCliente, IdEmpresa: current_usuario.empresa_id).update_all(STATUS: @pedido.Status)
      end
    end
    respond_to do |format|
      format.js {render :js => "window.location.href='"+visor_de_pedidos_path+"'"}
    end
  end

  def impresion_clientes_pdf
    @tiket = Tiket.find_by_IdEmpresa(current_usuario.empresa_id)
    @empresa = current_usuario.empresamadre.Empresa
    respond_to do |format|
        format.pdf do
          pdf = ImpresionClientesPdf.new(@empresa,@tiket,params[:lista])
          send_data pdf.render, filename: "impresion_clientes.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cliente
      @cliente = Cliente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cliente_params
      params.require(:cliente).permit(:IdCli, :ValidarGPS, :Merma_en_ruta, :Nombre, :NombreCorto, :Latitud, :Longitud, :Direccion, :Referencia, :Telefono, :CP, :Ciudad, :Estado, :RFC, :Credito, :LimiteCredito, :Status, :DiasCreedito, :Colonia, :Tel2, :Email, :VisitaObligada, :FirmaObligada, :MotivoBajaId, :Saldo, :Horario, :idclasc, :IdEmpresa, relclicla_attributes: [:id, :IdCliente, :Clas1, :Clas2, :Clas3, :Clas4, :Clas5, :IdEmpresa], relclili_attributes: [:id, :CodCliente, :ListaP, :ListaD, :ListaPromo, :IdEmpresa], relcliruta_attributes: [:id, :IdCliente, :IdRuta, :Fecha, :IdEmpresa], reldaycli_attributes: [:Id, :RutaId, :CodCli, :Lunes, :Martes, :Miercoles, :Jueves, :Viernes, :Sabado, :Domingo, :IdEmpresa])
    end
end
