class ThStockalmacenController < ApplicationController
  before_action :set_stockalmacen, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Proceso de carga stock almacén"
  end
  # GET /th_stockalmacen
  # GET /th_stockalmacen.json
  def index
    params[:search6] =  current_usuario.empresa_id # "value2"
    @productos = Producto.proceso_pedidos(params)

    @productos.each do |producto|
     Stockalmacen.create(Articulo: producto.Clave, Stock: 0, IdEmpresa:current_usuario.empresa_id)
    end

    @search = Stockalmacen.productos_de_un_stock_almacen(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'producto_Producto desc' if @search.sorts.empty?
    @th_stockalmacen = @search.result().page(params[:page]).per(15)

    @StockalmacenE = Stockalmacen.productos_de_un_stock_almacen(params)
    total_piezas_stock_almacen

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @StockalmacenE.to_csv}
      format.xls
    end

  end




  def import
    empresa = current_usuario.empresa_id
    @errors = Stockalmacen.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to th_stockalmacen_path, notice: "Pedido modificado."
    end
  end


  # GET /th_stockalmacen/1
  # GET /th_stockalmacen/1.json
  def show
  end

  # GET /th_stockalmacen/new
  def new
    @stockalmacen = Stockalmacen.new
  end

  # GET /th_stockalmacen/1/edit
  def edit
  end

  # POST /th_stockalmacen
  # POST /th_stockalmacen.json
  def create
    @stockalmacen = Stockalmacen.new(stockalmacen_params)

    respond_to do |format|
      if @stockalmacen.save
        format.html { redirect_to @stockalmacen, notice: 'Stockalmacen was successfully created.' }
        format.json { render :show, status: :created, location: @stockalmacen }
      else
        format.html { render :new }
        format.json { render json: @stockalmacen.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /th_stockalmacen/1
  # PATCH/PUT /th_stockalmacen/1.json
  def update
    params[:search6] = @stockalmacen.IdEmpresa

    respond_to do |format|
      if @stockalmacen.update(stockalmacen_params)
        @pzas = (@stockalmacen.producto.productosxpza.PzaXCja * (params[:f_cajas]).to_f) + (params[:f_piezas]).to_f # .to_f para pasar los parametros de string a integer
        @stockalmacen = Stockalmacen.productos_de_un_stock_almacen(params).find_by(Id: @stockalmacen.Id)
        @stockalmacen.update(Stock:@pzas)
        total_piezas_stock_almacen
        format.html { redirect_to @stockalmacen, notice: 'Stockalmacen was successfully updated.' }
        format.json { render :show, status: :ok, location: @stockalmacen }
        format.js {flash.now[:notice] = 'El Stock se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @stockalmacen.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el stock.'} #ajax
      end
    end
  end

  def total_piezas_stock_almacen
    @total_piezas_stock_almacen = Stockalmacen.productos_de_un_stock_almacen(params).sum("Stock")
    @total_cajas = Stockalmacen.productos_de_un_stock_almacen(params).es_producto.map(&:Cajas).compact.sum
    @total_piezas = Stockalmacen.productos_de_un_stock_almacen(params).es_producto.map(&:Piezas).compact.sum
    @total_cajas_envase = Stockalmacen.productos_de_un_stock_almacen(params).es_envase.map(&:Cajas).compact.sum
    @total_piezas_envase = Stockalmacen.productos_de_un_stock_almacen(params).es_envase.map(&:Piezas).compact.sum

  end

  # DELETE /th_stockalmacen/1
  # DELETE /th_stockalmacen/1.json
  def destroy
    @stockalmacen.destroy
    respond_to do |format|
      format.html { redirect_to th_stockalmacen_url, notice: 'Stockalmacen was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stockalmacen
      @stockalmacen = Stockalmacen.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stockalmacen_params
      params.require(:stockalmacen).permit(:IdEmpresa, :Articulo, :Stock)
    end
end
