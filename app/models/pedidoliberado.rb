class Pedidoliberado < ActiveRecord::Base
  self.primary_key = "ID"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "COD_CLIENTE"
  belongs_to :formapag, class_name: "Formapag", foreign_key: "FormaPag"
  scope :tipo_contado, -> { where(TIPO: "Contado") }
  scope :tipo_credito, -> { where(TIPO: "Credito") }
  scope :tipo_todas, -> { where("TIPO != 'Obsequio'") }
  scope :pago_en_Efectivo, -> { joins(:formapag).where(formaspag: {Forma: "Efectivo"})}
  scope :pago_no_sea_Efectivo, -> { joins(:formapag).where.not(formaspag: {Forma: "Efectivo"}) }
  scope :sin_obsequios, -> { where.not(TipoVta: "Obsequio") }

  def self.entregas_dashboard2(params)
   query = where("pedidosliberados.cancelada = 0")
   query = query.where('(pedidosliberados.Fecha_entrega between ? and ?) AND (idempresa = ?)', (Time.current.beginning_of_day-1.day), (Time.current.end_of_day), (params[:search0]))
   query
  end

  def self.pedidos_liberados(params)
    query = select('Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103) AS fecha, count(*) AS NoPedidos')
            .where("Cancelada = ? and Fecha_liberacion is not null", 0)
            .group("month(Fecha_pedido),Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103)")
            .order("month(Fecha_pedido)")
    query = query.where('(Fecha_pedido between ? and ?) AND (IdEmpresa = ?)', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T'), (params[:search0])) if params[:search1].present? and params[:search2].present?
    query
  end


  def self.pedidos_entregados(params)

    query = select('Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103) AS fecha, count(*) AS NoPedidos')
            .where("Cancelada = ? and Fecha_Entrega is not null", 0)
            .group("month(Fecha_pedido),Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103)")
            .order("month(Fecha_pedido)")
    query = query.where('(Fecha_pedido between ? and ?) AND (IdEmpresa = ?)', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T'), (params[:search0])) if params[:search1].present? and params[:search2].present?

    query


  end

  def self.reportes_monto_entregas(params)
      query = select("COALESCE(SUM(DetallePedidoliberado.IMPORTE), 0) AS Totals")
      query = query.joins("INNER JOIN DetallePedidoliberado ON DetallePedidoliberado.RUTA = pedidosliberados.Ruta AND DetallePedidoliberado.PEDIDO = pedidosliberados.Pedido AND DetallePedidoliberado.IdEmpresa = pedidosliberados.IdEmpresa")
      query = query.where("pedidosliberados.IdEmpresa = ?", params[:search6]) if params[:search6].present?
      query = query.where("pedidosliberados.Ruta = ?", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidosliberados.Status = 5 AND pedidosliberados.cancelada = 0")
      query = query.where('pedidosliberados.FECHA_ENTREGA between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query = query.where("pedidosliberados.DIAO_ENTREGA = ?", params[:diaO]) if params[:diaO].present?
      query
  end

  def self.pedidos_cancelados(params)

    query = select('Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103) AS fecha, count(*) AS NoPedidos')
            .where("Cancelada = ?", 1)
            .group("month(Fecha_pedido),Convert(varchar(10),CONVERT(date,Fecha_pedido,106),103)")
            .order("month(Fecha_pedido)")
    query = query.where('(Fecha_pedido between ? and ?) AND (IdEmpresa = ?)', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T'), (params[:search0])) if params[:search1].present? and params[:search2].present?

    query


  end



  def self.proceso_pedidos_lib
    query = select("[clientes].IdCli AS Cliente, [clientes].Nombre AS Responsable, [clientes].NombreCorto AS NombreComercial, [productos].Clave AS ClaveProducto, [productos].Producto AS Producto, [pedidosliberados].PEDIDO AS Pedido, [pedidosliberados].ID, [pedidosliberados].CANCELADA, [pedidosliberados].STATUS AS Status, [detallepedidoliberado].PZA_LIBERADAS AS Pza_liberadas,
                    [detallepedidoliberado].PZA AS Pza, SUM([detallepedidoliberado].IMPORTE) AS IMPORTE, SUM([detallepedidoliberado].IVA) AS IVA, SUM([detallepedidoliberado].IMPORTE) + SUM([detallepedidoliberado].IVA) AS TOTAL")
            .joins('inner join clientes ON pedidosliberados.COD_CLIENTE = clientes.IdCli AND pedidosliberados.IDEMPRESA = clientes.IdEmpresa inner join
                    detallepedidoliberado ON pedidosliberados.PEDIDO = detallepedidoliberado.PEDIDO AND pedidosliberados.RUTA = detallepedidoliberado.RUTA AND
                    pedidosliberados.IDEMPRESA = detallepedidoliberado.IDEMPRESA inner join
                    productos ON detallepedidoliberado.Articulo = Productos.Clave')
            .group("clientes.IdCli, clientes.Nombre, clientes.NombreCorto, productos.Clave, productos.Producto, pedidosliberados.PEDIDO, detallepedidoliberado.PZA_LIBERADAS, detallepedidoliberado.PZA, pedidosliberados.ID, pedidosliberados.Status, pedidosliberados.CANCELADA")
            .where('(pedidosliberados.CANCELADA != 1)')
    query
  end


  def self.rep_liq_con_contado(params)
    query = joins('inner join detallepedidoliberado DP on pedidosliberados.PEDIDO=DP.PEDIDO and pedidosliberados.RUTA=DP.RUTA and  pedidosliberados.IDEMPRESA=DP.idempresa')
            .where("pedidosliberados.cancelada = ?", 0)
            .where("pedidosliberados.status = ?", 5)
            .where("pedidosliberados.Tipo = 'Contado'")
            .where('pedidosliberados.Ruta = ?', params[:search])
            .where('pedidosliberados.DiaO_entrega = ?', params[:diaO])
  end

  def self.rep_liq_con_credito(params)
    query = joins('inner join detallepedidoliberado DP on pedidosliberados.PEDIDO=DP.PEDIDO and pedidosliberados.RUTA=DP.RUTA and  pedidosliberados.IDEMPRESA=DP.idempresa')
            .where("pedidosliberados.cancelada = ?", 0)
            .where("pedidosliberados.status = ?", 5)
            .where("pedidosliberados.Tipo = 'Credito'")
            .where('pedidosliberados.Ruta = ?', params[:search])
            .where('pedidosliberados.DiaO_entrega = ?', params[:diaO])
  end

  def self.rep_liq_ObsequioCajas(params)
    query = joins('INNER JOIN DETALLEPEDIDOLIBERADO DV ON DV.Ruta = [PEDIDOSLIBERADOS].Ruta AND DV.PEDIDO = [PEDIDOSLIBERADOS].PEDIDO AND DV.IdEmpresa = [PEDIDOSLIBERADOS].IdEmpresa')
            .joins('INNER JOIN ProductosXPzas PxP ON PxP.IdEmpresa = [PEDIDOSLIBERADOS].IdEmpresa AND PxP.Producto = DV.Articulo INNER JOIN Productos P ON P.Clave = PxP.Producto')
            .where("DV.Tipo = 0")
            .where("pedidosliberados.cancelada = ?", 0)
            .where("pedidosliberados.status = ?", 5)
            .where('pedidosliberados.DiaO_entrega = ?', params[:diaO])
            .where('pedidosliberados.Ruta = ?', params[:search])
            .where('pedidosliberados.IdEmpresa = ?', params[:search6])
  end


  def self.rep_liq_ObsequioPiezas(params)
    query = joins('INNER JOIN DETALLEPEDIDOLIBERADO DV ON DV.Ruta = [PEDIDOSLIBERADOS].Ruta AND DV.PEDIDO = [PEDIDOSLIBERADOS].PEDIDO AND DV.IdEmpresa = [PEDIDOSLIBERADOS].IdEmpresa')
            .joins('INNER JOIN ProductosXPzas PxP ON PxP.IdEmpresa = [PEDIDOSLIBERADOS].IdEmpresa AND PxP.Producto = DV.Articulo INNER JOIN Productos P ON P.Clave = PxP.Producto')
            .where("DV.Tipo = 1")
            .where("pedidosliberados.cancelada = ?", 0)
            .where("pedidosliberados.status = ?", 5)
            .where('pedidosliberados.DiaO_entrega = ?', params[:diaO])
            .where('pedidosliberados.Ruta = ?', params[:search])
            .where('pedidosliberados.IdEmpresa = ?', params[:search6])
  end

  def self.rep_liq_Descuento(params)
    query = joins('PEDIDOSLIBERADOS INNER JOIN DETALLEPEDIDOLIBERADO DV ON DV.Ruta = [PEDIDOSLIBERADOS].Ruta AND DV.IdEmpresa = [PEDIDOSLIBERADOS].IdEmpresa AND DV.pedido = [PEDIDOSLIBERADOS].pedido INNER JOIN Productos P ON P.Clave = DV.Articulo')
            .where("pedidosliberados.cancelada = ?", 0)
            .where("pedidosliberados.status = ?", 5)
            .where('pedidosliberados.DiaO_entrega = ?', params[:diaO])
            .where('pedidosliberados.Ruta = ?', params[:search])
            .where('pedidosliberados.IdEmpresa = ?', params[:search6])
  end


  def self.busqueda_ventas_entregas(params)
    query = select("[pedidosliberados].ID,[pedidosliberados].FECHA_ENTREGA AS Fecha_Entregado, [rutas].Ruta, [rutas].IdRutas, [pedidosliberados].COD_CLIENTE, [clientes].Nombre as Responsable, [clientes].NombreCorto as NombreComercial, [pedidosliberados].PEDIDO, [pedidosliberados].TIPO, [fp].Forma,[pedidosliberados].SubTotal,[pedidosliberados].IVA, SUM(d.DESCUENTO) AS sum_DescMon, [pedidosliberados].TOTAL, ISNULL((SELECT DISTINCT SUM(d.Pza) FROM (SELECT DISTINCT Articulo, Precio, Pza, Kg, DESCUENTO, Tipo, PEDIDO, Importe, IVA, IEPS,
                    Ruta FROM DETALLEPEDIDOLIBERADO) detallepedidoliberado WHERE detallepedidoliberado.RUTA=pedidosliberados.Ruta AND detallepedidoliberado.PEDIDO=pedidosliberados.PEDIDO AND Tipo=0),0) AS sum_Cja,ISNULL((SELECT DISTINCT SUM(d.Pza) FROM (SELECT DISTINCT Articulo, Precio, Pza, Kg, DESCUENTO, Tipo, PEDIDO, Importe, IVA, IEPS, Ruta FROM DETALLEPEDIDOLIBERADO) detallepedidoliberado WHERE detallepedidoliberado.RUTA=pedidosliberados.Ruta AND detallepedidoliberado.PEDIDO=pedidosliberados.PEDIDO AND Tipo=1),0) AS sum_Pza, [pedidosliberados].Cancelada, COALESCE([Ve].Nombre, '') AS Vendedor,  COALESCE([Ayudante1].Nombre, '') AS Ayudante1, COALESCE([Ayudante2].Nombre, '') AS Ayudante2, empresas.Sucursal")
         .joins("left outer join DETALLEPEDIDOLIBERADO d ON pedidosliberados.PEDIDO=d.PEDIDO and pedidosliberados.Ruta=d.RUTA LEFT JOIN rutas ON pedidosliberados.RUTA = rutas.IdRutas LEFT JOIN clientes ON pedidosliberados.COD_CLIENTE = clientes.IdCli LEFT JOIN formaspag fp ON pedidosliberados.formapag = fp.IdFpag LEFT JOIN Vendedores Ayudante1 ON Ayudante1.IdVendedor = pedidosliberados.ID_Ayudante1 AND Ayudante1.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ayudante2 ON Ayudante2.IdVendedor = pedidosliberados.ID_Ayudante2 AND Ayudante2.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ve ON Ve.IdVendedor = [pedidosliberados].IDVENDEDOR INNER JOIN Empresas ON Empresas.IdEmpresa = [pedidosliberados].IdEmpresa")
         .where("(pedidosliberados.RUTA = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (pedidosliberados.STATUS = 5) AND (pedidosliberados.TIPO != 'Obsequio') AND (pedidosliberados.IdEmpresa = :sucursal or :sucursal = '')",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], sucursal: params[:search6]})
         .group("pedidosliberados.RUTA,  pedidosliberados.IVA, rutas.Ruta, rutas.IdRutas, clientes.Nombre, clientes.NombreCorto, fp.Forma, Empresas.Sucursal, pedidosliberados.IdEmpresa, pedidosliberados.FormaPag, pedidosliberados.IDVENDEDOR, pedidosliberados.COD_CLIENTE, pedidosliberados.PEDIDO, pedidosliberados.FECHA_ENTREGA, pedidosliberados.Tipo, pedidosliberados.SubTotal,pedidosliberados.TOTAL,pedidosliberados.Cancelada, Ayudante1.Nombre,Ayudante2.Nombre,Ve.Nombre,pedidosliberados.ID")
         .distinct
    query = query.where('pedidosliberados.DIAO_ENTREGA = ?  AND pedidosliberados.IDVENDEDOR = ?', params[:diaO], params[:vendedor_id]) if params[:diaO].present?
    query = query.where('pedidosliberados.FECHA_PEDIDO BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where("([pedidosliberados].COD_CLIENTE LIKE :Cliente or :Cliente = '') AND ([clientes].Nombre LIKE :Responsable or :Responsable = '') AND ([clientes].NombreCorto LIKE :NombreComercial or :NombreComercial = '') AND ([pedidosliberados].PEDIDO LIKE :PEDIDO or :PEDIDO = '') AND ([pedidosliberados].TIPO LIKE :TipoVta or :TipoVta = '') AND ([fp].Forma LIKE :Forma or :Forma = '')",
    {Cliente: "%#{params[:Cliente_param]}%", Responsable: "%#{params[:Responsable_param]}%", NombreComercial: "%#{params[:Nombre_Comercial_param]}%", PEDIDO: "%#{params[:Pedido_param]}%", TipoVta: "%#{params[:Tipo_param]}%", Forma: "%#{params[:Metodo_de_pago_param]}%"})
    query
  end



    def self.busqueda_general_para_totales(params)
    query = select("[pedidosliberados].ID, [pedidosliberados].TOTAL")
         .distinct
    query = query.where(Id: select('[pedidosliberados].ID')
         .joins("left outer join DETALLEPEDIDOLIBERADO d ON pedidosliberados.PEDIDO=d.PEDIDO and pedidosliberados.Ruta=d.RUTA LEFT JOIN rutas ON pedidosliberados.RUTA = rutas.IdRutas LEFT JOIN clientes ON pedidosliberados.COD_CLIENTE = clientes.IdCli LEFT JOIN formaspag ON pedidosliberados.formapag = formaspag.IdFpag LEFT JOIN Vendedores Ayudante1 ON Ayudante1.IdVendedor = pedidosliberados.ID_Ayudante1 AND Ayudante1.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ayudante2 ON Ayudante2.IdVendedor = pedidosliberados.ID_Ayudante2 AND Ayudante2.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ve ON Ve.IdVendedor = [pedidosliberados].IDVENDEDOR INNER JOIN Empresas ON Empresas.IdEmpresa = [pedidosliberados].IdEmpresa")
         .where("(pedidosliberados.RUTA = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (pedidosliberados.STATUS = 5) AND (pedidosliberados.TIPO != 'Obsequio') AND (pedidosliberados.IdEmpresa = :sucursal or :sucursal = '')",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], sucursal: params[:search6]}))
    query = query.where('pedidosliberados.DIAO_ENTREGA = ?  AND pedidosliberados.IDVENDEDOR = ?', params[:diaO], params[:vendedor_id]) if params[:diaO].present?
    query = query.where('pedidosliberados.FECHA_ENTREGA BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query
  end

  def self.rep_bitiempos_busqueda_general_para_totales(params)
    query = where("(pedidosliberados.STATUS = 5) AND (pedidosliberados.TIPO != 'Obsequio') AND (pedidosliberados.CANCELADA = 0)")
    query = query.where("RUTA = ?", params[:search])
    query = query.where("IDEMPRESA = ?", params[:search6])
    query = query.where("DIAO_ENTREGA = ?", params[:diaO])
    query = query.where('AGENTE_ENTREGA = ?',params[:vendedor_id])
  end



end
