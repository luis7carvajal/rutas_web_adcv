class Stoc < ActiveRecord::Base
  self.primary_key = "IdStock"
  self.table_name = 'Stock' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_ruta, -> (ruta) { where(Ruta: ruta) }
  scope :por_empresa, -> (empresa) { where(IdEmpresa: empresa) }
  scope :es_producto, -> {joins(:producto).where("P.Ban_Envase = 0") }
  scope :es_envase, -> {joins(:producto).where("P.Ban_Envase = 1") }

  belongs_to :producto, class_name:"Producto", foreign_key: "Articulo"

  def self.stock_consolidado(params, pedidos)
    query = select('DISTINCT [stock].Articulo Clave, P.Producto, ([stock].Stock / PP.PzaXCja) Cajas, ([stock].Stock % PP.PzaXCja) Piezas, [stock].Stock TotalPiezas')
            .joins('INNER JOIN V_CantidadxPedido CP ON [stock].IdEmpresa = CP.IdEmpresa AND [stock].Ruta = CP.RutaId AND [stock].Articulo = CP.Sku INNER JOIN ProductosXPzas PP ON PP.Producto = [stock].Articulo INNER JOIN Productos P ON P.IdEmpresa = [stock].IdEmpresa AND [stock].Articulo = P.Clave')
    query = query.where('CP.Docto IN (?)', pedidos)
    query = query.where('CP.IdEmpresa = ?', params[:search6]) if params[:search6]
    query = query.where('CP.RutaId = ?', params[:search]) if params[:search]
    query = query.where('[stock].Stock > 0')
    query
  end

  def self.stock_envase_producto(params)
    query = select('Stock.IdStock, Stock.Articulo, E.Envase,Stock.Stock/(X.PzaXCja/E.Cant_Eq) Stock,Stock.Ruta,Stock.IdEmpresa')
            .joins('Inner Join Productos P On Stock.Articulo=P.Clave And Stock.IdEmpresa=P.IdEmpresa Inner Join ProductoEnvase E On P.Clave=E.Producto 
                    Left Join ProductosXPzas X On P.Clave=X.Producto And X.IdEmpresa=P.IdEmpresa')
    query = query.where('Stock.IdEmpresa = ?', params[:search6])
    query = query.where('Stock.Ruta = ?', params[:search])
    query = query.where('P.Clave = ?', params[:Producto])
    query
  end

    def self.productos_de_un_stock(params)
      query = select("[stock].IdStock, [stock].Stock, P.Clave AS Clave_producto, P.Producto, [productosxpzas].PzaXCja AS PzaXCja, LOWER([catunidadmedida].UnidadMedida) AS UnidadMedida,
      CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN 0 ELSE ([stock].Stock / [productosxpzas].PzaXCja) END AS Cajas,
      CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN [stock].Stock ELSE ([stock].Stock % [productosxpzas].PzaXCja) END AS Piezas") #LOWER para colocar el valor en minusculas
              .joins('inner join productos P ON stock.Articulo = P.Clave inner join productosxpzas ON P.Clave = productosxpzas.Producto inner join catunidadmedida ON P.UniMed = catunidadmedida.Clave')
              .where('stock.Ruta = ? AND stock.IdEmpresa = ? AND P.Status = ?', (params[:search]), (params[:search6]), "A")
              #.where("P.clave in (select distinct Articulo from detallelp where IdEmpresa = :idempresa)",{idempresa: params[:search6]})
      unless params[:Clave_param].blank?
        query = query.where("P.Clave like ? ", "%#{params[:Clave_param]}%")
      end
      unless params[:Producto_param].blank?
        query = query.where("P.Producto like ? ", "%#{params[:Producto_param]}%")
      end
      query
    end


    def self.to_csv(options = {})#exportar
      CSV.generate(options) do |csv|
        csv << column_names
        all.each do |stoc|
          csv << stoc.attributes.values_at(*column_names)
        end
      end
    end

    def self.import(file,empresa)#importar
      @errors = []
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
        row = Hash[[header, spreadsheet.row(i)].transpose]

        @pzas = ((Productosxpza.find_by_Producto(row["Clave del producto"]).PzaXCja * (row["StockCajas"]).to_f) + (row["StockPiezas"]).to_f)

#        stoc = (find_by_IdStock(row["Id del Stock"]) and find_by_Articulo(row["Clave del producto"]) and find_by_Ruta(row["Ruta"]))
        stoc = (find_by_IdStock(row["Id del Stock"]))
        stoc.attributes = {Stock: @pzas}


        if stoc.save
          # stuff to do on successful save
        else
          stoc.errors.full_messages.each do |message|
            @errors << "Error fila #{i}, columna #{message}"
          end
        end

      end
      @errors #  <- need to return the @errors array
    end


    def self.open_spreadsheet(file)#importar
      case File.extname(file.original_filename)
       when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
       #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
       when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
       #else raise "Unknown file type: #{file.original_filename}"
       else raise "El formato debe ser .xlsx ó .csv"
      end
    end
  def self.add_stock_from_pedido(params)
    sql = "SELECT  P.Pedido
            FROM [Pedidos] P
            INNER JOIN PEDIDOSLIBERADOS PL
                  ON PL.Pedido = P.Pedido
                  AND PL.Ruta = P.Ruta
                  AND PL.COD_CLIENTE = P.CodCliente
                  AND PL.IDEMPRESA = P.IdEmpresa
                  AND PL.IDVENDEDOR = P.idVendedor
            WHERE P.Status = 3
                  AND P.IdEmpresa = #{params[:search6]}
                  AND P.Ruta = #{params[:search]}
                  AND (PL.CargaStock IS NULL OR PL.CargaStock = 0)"
    pedidos_array = ActiveRecord::Base.connection.select_values(sql)

    unless pedidos_array.empty?
      # Si llega a dar fallos hay que quitar where in array y hacerlo manual como el de clientes
      sqlProductos = "SELECT C.SKU, SUM((C.Cajas * PP.PzaXCja) + C.Pzas) AS Pzas
                      FROM V_CantidadxPedido C
                      LEFT JOIN ProductosXPzas PP ON PP.Producto = C.SKU
                      WHERE C.RutaId = #{params[:search]}
                      AND C.IdEmpresa = #{params[:search6]}
                      AND C.Docto IN (#{pedidos_array.join(',')})
                      GROUP BY C.SKU
                      UNION
                      SELECT
                        PRegalado.SKU,
                        COALESCE(SUM(PRegalado.Cant * ProductosXPzas.PzaXCja), 0) AS Pzas
                      FROM PRegalado
                      LEFT JOIN ProductosXPzas ON PRegalado.SKU = ProductosXPzas.Producto
                      WHERE PRegalado.IdEmpresa = #{params[:search6]}
                      AND PRegalado.RutaId = #{params[:search]}
                      AND PRegalado.Docto IN (#{pedidos_array.join(',')})
                      AND PRegalado.Tipo = 'P'
                      AND PRegalado.TipMed = 'Caja'
                      GROUP BY PRegalado.SKU
                      UNION
                      SELECT PRegalado.SKU,
                        COALESCE(SUM(PRegalado.Cant), 0) AS Pzas
                      FROM PRegalado
                      LEFT JOIN ProductosXPzas ON PRegalado.SKU = ProductosXPzas.Producto
                      WHERE PRegalado.IdEmpresa = #{params[:search6]}
                      AND PRegalado.RutaId = #{params[:search]}
                      AND PRegalado.Docto IN (#{pedidos_array.join(',')})
                      AND PRegalado.Tipo = 'P'
                      AND PRegalado.TipMed = 'Pzas'
                      GROUP BY PRegalado.SKU

                      "
      articulos_array = ActiveRecord::Base.connection.select_rows(sqlProductos)
      articulos_array.each do |articulo|
        existe_articulo = ActiveRecord::Base.connection.execute("SELECT IdStock FROM Stock WHERE Ruta = #{params[:search]} AND IdEmpresa = #{params[:search6]} AND Articulo = '#{articulo[0]}'").to_i
        if existe_articulo > 0
          ActiveRecord::Base.connection.execute("UPDATE Stock SET Stock = Stock + #{articulo[1]} WHERE Articulo = '#{articulo[0]}' AND Ruta = #{params[:search]} AND IdEmpresa = #{params[:search6]}")
        else
          ActiveRecord::Base.connection.execute("INSERT INTO Stock (Articulo, Stock, Ruta, IdEmpresa) VALUES ('#{articulo[0]}', #{articulo[1]}, #{params[:search]}, #{params[:search6]})")
        end
      end
      ActiveRecord::Base.connection.execute("UPDATE PEDIDOSLIBERADOS SET CargaStock = 1 WHERE RUTA = #{params[:search]} AND PEDIDO IN (#{pedidos_array.join(',')}) AND IDEMPRESA = #{params[:search6]}")
    end
  end
end
