class InventarioController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:producto_negado, :inventario_total, :inventario_sobrante]

  def modulo
    if action_name == "producto_negado"
      @Descripcion_Modulo = "Reporte de producto negado"
    elsif action_name == "inventario_total"
      @Descripcion_Modulo = "Reporte de inventario total"
    elsif action_name == "inventario_sobrante"
      @Descripcion_Modulo = "Reporte de inventario sobrante"
    end
  end

  def producto_negado
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'inventario'
    @accion = 'producto_negado'
    @pathDeBusqueda = inventario_producto_negado_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda

    respond_to do |format|
      format.html
      format.js
    end
  end

  def busqueda_producto_negado
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @producto_negado = Productoneg.busqueda_general(params)

    respond_to do |format|
    format.js
    end
  end

  def inventario_total
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'inventario'
    @accion = 'inventario_total'
    @pathDeBusqueda = inventario_inventario_total_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
      #  @pathDeBusqueda = consumos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda
    total_piezas_inventario_total
    @search = Producto.reporte_inventario_total(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'Producto' if @search.sorts.empty?
    @productos = @search.result().page(params[:productos]).per(15)
    @productos_export = @search.result()
    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def total_piezas_inventario_total
    @total_cajas = Producto.reporte_inventario_total(params).productos.map(&:Cajas).compact.sum
    @total_piezas = Producto.reporte_inventario_total(params).productos.map(&:Piezas).compact.sum
    @total_cajas_envase = Producto.reporte_inventario_total(params).envases.map(&:Cajas).compact.sum
    @total_piezas_envase = Producto.reporte_inventario_total(params).envases.map(&:Piezas).compact.sum
  end

  def inventario_sobrante
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'inventario'
    @accion = 'inventario_sobrante'
    @pathDeBusqueda = inventario_inventario_sobrante_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
      #  @pathDeBusqueda = consumos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda
    @total_cajas = Producto.reporte_inventario_sobrante(params).map(&:Cajas).compact.sum
    @total_piezas = Producto.reporte_inventario_sobrante(params).map(&:Piezas).compact.sum
    total_piezas_inventario_sobrante

    @search = Producto.reporte_inventario_sobrante(params).search(search_params)
    @search.sorts = 'Producto' if @search.sorts.empty?
    @productos = @search.result().page(params[:productos]).per(15)
    @productos_export = @search.result()

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end
  def total_piezas_inventario_sobrante
    @total_cajas = Producto.reporte_inventario_sobrante(params).productos.map(&:Cajas).compact.sum
    @total_piezas = Producto.reporte_inventario_sobrante(params).productos.map(&:Piezas).compact.sum
    @total_cajas_envase = Producto.reporte_inventario_sobrante(params).envases.map(&:Cajas).compact.sum
    @total_piezas_envase = Producto.reporte_inventario_sobrante(params).envases.map(&:Piezas).compact.sum
  end

  def inventario_inicial
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'inventario'
    @accion = 'inventario_inicial'
    @pathDeBusqueda = inventario_inventario_inicial_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
      #  @pathDeBusqueda = consumos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda

    total_piezas_inventario_inicial

    @search = Producto.reporte_inventario_inicial(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'Producto' if @search.sorts.empty?
    @productos = @search.result().page(params[:productos]).per(15)
    @productos_export = @search.result()
    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end

  def total_piezas_inventario_inicial
    @total_cajas = Producto.reporte_inventario_inicial(params).productos.map(&:Cajas).compact.sum
    @total_piezas = Producto.reporte_inventario_inicial(params).productos.map(&:Piezas).compact.sum
    @total_cajas_envase = Producto.reporte_inventario_inicial(params).envases.map(&:Cajas).compact.sum
    @total_piezas_envase = Producto.reporte_inventario_inicial(params).envases.map(&:Piezas).compact.sum
  end

  def inventario_dia_siguiente
    @controlador = 'inventario'
    @accion = 'inventario_dia_siguiente'
    @pathDeBusqueda = inventario_inventario_dia_siguiente_path

    elementos_de_busqueda
    @total_cajas = Producto.reporte_inventario_dia_siguiente(params).map(&:Cajas).compact.sum
    @total_piezas = Producto.reporte_inventario_dia_siguiente(params).map(&:Piezas).compact.sum

    @search = Producto.reporte_inventario_dia_siguiente(params).search(search_params)
    @search.sorts = 'Producto' if @search.sorts.empty?
    @productos = @search.result().page(params[:productos]).per(15)

    @productos_export = @search.result()

    respond_to do |format|
      format.html
      format.js
      format.xls
    end
  end


  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
  end



end
