class Listapromomast < ActiveRecord::Base
  self.table_name = 'ListaPromoMaster' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  self.primary_key = 'Id'
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }

  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :nombre_grupo, ->(param_id) { find_by(Id: param_id)} # para que no devuelva una coleccion, sino un solo registro

  has_many :relclilis, class_name: "Relclili", foreign_key: "ListaPromo"
#  has_one :detallelpromast, class_name: "Detallelpromast", foreign_key: "IdLm"
  has_many :detallelpromaster, class_name: "Detallelpromast", foreign_key: "IdLm"
  has_many :clientes, through: :relclilis, source: :cliente, foreign_key: "CodCliente"






end
