class Reldayc < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "CodCli"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"

  belongs_to :relcliruta, class_name:"Relcliruta", foreign_key: "Id"
  belongs_to :vendedor, class_name:"Vendedor", foreign_key: "idVendedor"

  scope :ruta, -> (id_param) { where(RutaId: id_param) }
  scope :por_empresa, -> (id_param) { where(IdEmpresa: id_param) }
  scope :por_vendedor, -> (vendedor_param) { where(IdVendedor: vendedor_param) }
  scope :surtidos, -> {where("RelDayCli.CodCli IN (SELECT DISTINCT CodCliente FROM Pedidos WHERE Status = 3 AND Ruta = RelDayCli.RutaId AND IdEmpresa = RelDayCli.IdEmpresa)")}
  scope :clientes_diferentes, -> {select("DISTINCT(RelDayCli.CodCli), RelDayCli.CodCli.Id, RelDayCli.Lunes, RelDayCli.Martes, RelDayCli.Miercoles, RelDayCli.Jueves, RelDayCli.Viernes, RelDayCli.Sabado, RelDayCli.Domingo, RelDayCli.RutaId, RelDayCli.IdEmpresa, RelDayCli.idVendedor")}
  scope :dia_sin_clientes_entregas, -> {where('Lunes IS NULL AND Martes IS NULL AND Miercoles IS NULL AND Jueves IS NULL AND Viernes IS NULL AND Sabado IS NULL AND Domingo IS NULL')}
  scope :ordered_by_title, -> { reorder(Lunes: :asc) }
  scope :excluir_clientes_surtidos, -> {where("RelDayCli.CodCli NOT IN (SELECT DISTINCT CodCliente FROM Pedidos WHERE Status = 3 AND Ruta = RelDayCli.RutaId AND IdEmpresa = RelDayCli.IdEmpresa)")}
  scope :excluir_clientes_dia,  -> (dia) {where("[reldaycli].CodCli NOT IN (SELECT DISTINCT R.CodCli FROM RelDayCli R WHERE R.RutaId = [reldaycli].RutaId AND R.idVendedor = [reldaycli].idVendedor AND R.IdEmpresa = [reldaycli].IdEmpresa AND #{dia} > 0)")}
  scope :base, -> {where ("Lunes IS NULL AND Martes IS NULL AND Miercoles IS NULL AND Jueves IS NULL AND Viernes IS NULL AND Sabado IS NULL AND Domingo IS NULL")}

  validates :idVendedor, presence: true
  validates :CodCli, presence: true


  def self.cantidad_visitas_programadas_dashboard
    query = joins('INNER JOIN rutas ON reldaycli.RutaId=rutas.IdRutas INNER JOIN catgrupos ON rutas.TipoRuta = catgrupos.Clave')
         .where("dbo.catgrupos.Descripcion='Preventa'")
    query
   end

  scope :dia_con_clientes, -> (id_param22) {
    if (id_param22) == "Lunes"
      where("Lunes > ?", '0')

    elsif (id_param22) == "Martes"
      where("Martes > ?", '0')

    elsif (id_param22) == "Miercoles"
      where("Miercoles > ?", '0')

    elsif (id_param22) == "Jueves"
      where("Jueves > ?", '0')

    elsif (id_param22) == "Viernes"
      where("Viernes > ?", '0')

    elsif (id_param22) == "Sabado"
      where("Sabado > ?", '0')

    elsif (id_param22) == "Domingo"
      where("Domingo > ?", '0')

    end }

  scope :dia_con_clientes_dashboard, -> (id_param22) {
    if (id_param22) ==  "Monday"
      where("Lunes > ?", '0')

    elsif (id_param22) == "Tuesday"
      where("Martes > ?", '0')

    elsif (id_param22) ==  "Wednesday"
      where("Miercoles > ?", '0')

    elsif (id_param22) == "Thursday"
      where("Jueves > ?", '0')

    elsif (id_param22) == "Friday"
      where("Viernes > ?", '0')

    elsif (id_param22) ==  "Saturday"
      where("Sabado > ?", '0')

    elsif (id_param22) ==  "Sunday"
      where("Domingo > ?", '0')

    end }



    scope :dia_sin_clientes, -> (id_param23) {
      if (id_param23) == "Lunes"
        where("Lunes IS NULL")

      elsif (id_param23) == "Martes"
        where("Martes IS NULL")

      elsif (id_param23) == "Miercoles"
        where("Miercoles IS NULL")

      elsif (id_param23) == "Jueves"
        where("Jueves IS NULL")

      elsif (id_param23) == "Viernes"
        where("Viernes IS NULL")

      elsif (id_param23) == "Sabado"
        where("Sabado IS NULL")

      elsif (id_param23) == "Domingo"
        where("Domingo IS NULL")

      end }


  def self.busqueda_segundaria(params)
    query = joins('INNER JOIN Clientes C ON Reldaycli.CodCli=C.IdCli')
            .where("(C.IdCli LIKE :IdCli or :IdCli = '' or :IdCli IS NULL) AND (C.Nombre LIKE :Nombre or :Nombre = '' or :Nombre IS NULL) AND (C.NombreCorto LIKE :NombreCorto or :NombreCorto = '' or :NombreCorto IS NULL) AND (C.Direccion LIKE :Direccion or :Direccion = '' or :Direccion IS NULL) AND (C.Colonia LIKE :Colonia or :Colonia = '' or :Colonia IS NULL) AND (C.Horario = :Horario_c or :Horario_c = '' or :Horario_c IS NULL)",
    {IdCli: "%#{params[:cliente_IdCli_cont]}%", Nombre: "%#{params[:cliente_Nombre_cont]}%", NombreCorto: "%#{params[:cliente_NombreCorto_cont]}%", Direccion: "%#{params[:cliente_Direccion_cont]}%", Colonia: "%#{params[:cliente_Colonia_cont]}%", Horario_c: "#{params[:cliente_Horario_cont]}"})
    query
  end

end
