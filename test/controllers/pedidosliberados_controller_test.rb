require 'test_helper'

class PedidosliberadosControllerTest < ActionController::TestCase
  setup do
    @pedidoliberado = pedidosliberados(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pedidosliberados)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pedidoliberado" do
    assert_difference('Pedidoliberado.count') do
      post :create, pedidoliberado: { CANCELADA: @pedidoliberado.CANCELADA, COD_CLIENTE: @pedidoliberado.COD_CLIENTE, FECHA_ENTREGA: @pedidoliberado.FECHA_ENTREGA, FECHA_LIBERACION: @pedidoliberado.FECHA_LIBERACION, FECHA_PEDIDO: @pedidoliberado.FECHA_PEDIDO, FOLIO_BO: @pedidoliberado.FOLIO_BO, IDEMPRESA: @pedidoliberado.IDEMPRESA, IDVENDEDOR: @pedidoliberado.IDVENDEDOR, ID_AYUDANTE1: @pedidoliberado.ID_AYUDANTE1, ID_AYUDANTE2: @pedidoliberado.ID_AYUDANTE2, IEPS: @pedidoliberado.IEPS, INCIDENCIA: @pedidoliberado.INCIDENCIA, IVA: @pedidoliberado.IVA, KG: @pedidoliberado.KG, PEDIDO: @pedidoliberado.PEDIDO, RUTA: @pedidoliberado.RUTA, RutaEnt: @pedidoliberado.RutaEnt, STATUS: @pedidoliberado.STATUS, SUBTOTAL: @pedidoliberado.SUBTOTAL, TIPO: @pedidoliberado.TIPO, TOTAL: @pedidoliberado.TOTAL }
    end

    assert_redirected_to pedidoliberado_path(assigns(:pedidoliberado))
  end

  test "should show pedidoliberado" do
    get :show, id: @pedidoliberado
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pedidoliberado
    assert_response :success
  end

  test "should update pedidoliberado" do
    patch :update, id: @pedidoliberado, pedidoliberado: { CANCELADA: @pedidoliberado.CANCELADA, COD_CLIENTE: @pedidoliberado.COD_CLIENTE, FECHA_ENTREGA: @pedidoliberado.FECHA_ENTREGA, FECHA_LIBERACION: @pedidoliberado.FECHA_LIBERACION, FECHA_PEDIDO: @pedidoliberado.FECHA_PEDIDO, FOLIO_BO: @pedidoliberado.FOLIO_BO, IDEMPRESA: @pedidoliberado.IDEMPRESA, IDVENDEDOR: @pedidoliberado.IDVENDEDOR, ID_AYUDANTE1: @pedidoliberado.ID_AYUDANTE1, ID_AYUDANTE2: @pedidoliberado.ID_AYUDANTE2, IEPS: @pedidoliberado.IEPS, INCIDENCIA: @pedidoliberado.INCIDENCIA, IVA: @pedidoliberado.IVA, KG: @pedidoliberado.KG, PEDIDO: @pedidoliberado.PEDIDO, RUTA: @pedidoliberado.RUTA, RutaEnt: @pedidoliberado.RutaEnt, STATUS: @pedidoliberado.STATUS, SUBTOTAL: @pedidoliberado.SUBTOTAL, TIPO: @pedidoliberado.TIPO, TOTAL: @pedidoliberado.TOTAL }
    assert_redirected_to pedidoliberado_path(assigns(:pedidoliberado))
  end

  test "should destroy pedidoliberado" do
    assert_difference('Pedidoliberado.count', -1) do
      delete :destroy, id: @pedidoliberado
    end

    assert_redirected_to pedidosliberados_path
  end
end
