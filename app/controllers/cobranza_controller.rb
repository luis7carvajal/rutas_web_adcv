class CobranzaController < ApplicationController
  before_action :set_cobran, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index, :index_cobranza_bitacora]

  def modulo
    if action_name == "index"
      @Descripcion_Modulo = "Proceso de cuentas x cobrar"
    elsif action_name == "index_cobranza_bitacora"
      @Descripcion_Modulo = "Reporte de cobranza"
    end
  end
  # GET /cobranza
  # GET /cobranza.json
  def index
    @controlador = 'cobranza'
    @accion = 'index'
    elementos_de_busqueda
    registros_cobranza
    @cobranzas_export = Detalleco.exportar_excel_reporte_cobranza(params)
  end

  def busqueda_ctasxcobrar
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    registros_cobranza

    respond_to do |format|
      format.csv { send_data @pedidosExport.to_csv}
      format.xls
      format.js
    end
  end

#Esto es lo que se va a buscar dependiendo de la busqueda
  def registros_cobranza#Debo hacerlo de esta forma porque si lo coloco en la busqueda entonces a la hora de pasar de pagina con el kaminari me muestra un error porque me redirige a una "pagina" llamada busqueda_ctasxcobrar que no existe debido que asi se llama el metodo de busqueda
    #@Sucursales = Empresa.all
    #ADEMAS DE ESTO DEBO ESPECIFICAR EN EL PARCIAL EN LA PARTE DE LA PAGINACION A QUE CONTROLADOR Y METODO VA A RESPONDER
    @search = Cobran.ctasxcobrar_busqueda_general(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'FechaReg desc' if @search.sorts.empty?
    @cobranza = @search.result().page(params[:cxc])
    @detalleco = Detalleco.new

    @cobranza_export = @search.result()

    @saldo_vencido = Cobran.ctasxcobrar_busqueda_general(params).saldo_vencido.map(&:Saldo_actual).compact.sum
    @saldo_corriente = Cobran.ctasxcobrar_busqueda_general(params).saldo_corriente.map(&:Saldo_actual).compact.sum
    @saldo_total = @saldo_vencido + @saldo_corriente

    @rutas = Ruta.por_empresa(current_usuario.empresa_id)
    @detallecob = Detalleco.all.order(Id: :desc)
    @Formaspago = Formapag.activos
  end

  def elementos_de_busqueda
    @Sucursales = Empresa.all
    @rutas = Ruta.por_empresa(current_usuario.empresa_id)
    params[:search6] = current_usuario.empresa_id
    @pathDeBusqueda = cobranza_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchClienteB = Cliente.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchClienteB.sorts = 'IdCli' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
  end

  def abonos_cxc
    @cobran = Cobran.find_by_id(params[:cobranza_id])
    @Formaspago = Formapag.activos
    @detalleco = Detalleco.new
    @detallecob = Detalleco.where(IdCobranza: params[:cobranza_id]).order(Id: :desc)

    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end


  def index_cobranza_bitacora
    @controlador = 'cobranza'
    @accion = 'index_cobranza_bitacora'
    elementos_de_busqueda
    registros_bit_cobranza
    @clientes = Cliente.por_empresa(current_usuario.empresa_id).activos
  end

  # GET /cobranza/1
  # GET /cobranza/1.json
  def show
  end

  def obtener_ruta
    respond_to do |format|
      format.html { redirect_to ctasxcobrar_path(cobran_params[:RutaId]) } #envio de parametros que se usaran para enviar el la ruta
    end
  end




  def busqueda_cobranza
    @Sucursales = Empresa.all
    params[:search6] = current_usuario.empresa_id

    elementos_de_busqueda_ruta_autocompletar
    registros_bit_cobranza

#   @Formaspago = Formapag.activos
#   @cobranbusc = Cobran.new
#   @detalleco = Detalleco.new
#   @clientes = Cliente.por_empresa(current_usuario.empresa_id).activos


#   @detallevet = Detalleve.all
#   @venta = Vent.all

    respond_to do |format|
      format.js
    end
  end

  def registros_bit_cobranza
    params[:search6] = current_usuario.empresa_id

    @search = Detalleco.busqueda_cobranza(params).search(search_params)
    # make name the default sort column
    @cobranza_bitacora = @search.result().page(params[:cobranza]).per(15)
    @TotalEfectivo = Detalleco.busqueda_cobranza_total(params).sum('Abono')
  end

  def abonos_bit_cobranza
    params[:search6] = current_usuario.empresa_id

    @detalleco = Detalleco.por_cobranza(params)

    respond_to do |format|
      format.js
    end
  end




  # GET /cobranza/new
  def new
    @cobran = Cobran.new
  end

  # GET /cobranza/1/edit
  def edit
  end

  # POST /cobranza
  # POST /cobranza.json
  def create
    @cobran = Cobran.new(cobran_params)

    respond_to do |format|
      if @cobran.save
        format.html { redirect_to @cobran, notice: 'Cobran was successfully created.' }
        format.json { render :show, status: :created, location: @cobran }
        format.js {flash.now[:notice] = 'La cobranza se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @cobran.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la cobranza.'} #ajax
      end
    end
  end

  # PATCH/PUT /cobranza/1
  # PATCH/PUT /cobranza/1.json
  def update
    respond_to do |format|
      if @cobran.update(cobran_params)
        format.html { redirect_to @cobran, notice: 'Cobran was successfully updated.' }
        format.json { render :show, status: :ok, location: @cobran }
        format.js {flash.now[:notice] = 'La cobranza se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @cobran.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la cobranza.'} #ajax
      end
    end
  end

  # DELETE /cobranza/1
  # DELETE /cobranza/1.json
  def destroy
    @cobran.destroy
    respond_to do |format|
      format.html { redirect_to cobranza_url, notice: 'Cobran was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'La cobranza se ha borrado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cobran
      @cobran = Cobran.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cobran_params
      params.require(:cobran).permit(:Cliente, :Documento, :Saldo, :Status, :RutaId, :UltPago, :FechaPag, :FechaVence, :FolioInterno, :TipoDoc, :DiaO, :IdEmpresa)
    end
end
