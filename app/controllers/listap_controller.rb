class ListapController < ApplicationController
  before_action :set_list, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de lista de precios"
  end

  # GET /listap
  # GET /listap.json
  def index
    @search = List.por_empresa(current_usuario.empresa_id).search(search_params)
    @search.sorts = 'Lista desc' if @search.sorts.empty?
    @listap = @search.result().page(params[:page])

    @listap2 = Deta.por_empresa(current_usuario.empresa_id)
    @list = List.new
    @cantidad_listap_activos = List.por_empresa(current_usuario.empresa_id).count
    respond_to do |format|
      format.html
      format.xlsx
      format.csv { send_data @listap.to_csv}
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end
  
  def import
    empresa = current_usuario.empresa_id
    @errors = List.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to listap_path, notice: "listas importadas."
    end
  end

  # GET /listap/1
  # GET /listap/1.json
  def show
  end

  # GET /listap/new
  def new
    @list = List.new
  end

  # GET /listap/1/edit
  def edit
  end

  # POST /listap
  # POST /listap.json
  def create
    if @puede_crear != true
      return
    end
    @list = List.new(list_params)
    respond_to do |format|
      if @list.save
        format.html { redirect_to @list, notice: 'List was successfully created.' }
        format.json { render :show, status: :created, location: @list }
        format.js {flash.now[:notice] = 'La lista se ha creado de forma exitosa.'} #ajax

      else
        format.html { render :new }
        format.json { render json: @list.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la lista.'} #ajax

      end
    end
  end

  # PATCH/PUT /listap/1
  # PATCH/PUT /listap/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to @list, notice: 'List was successfully updated.' }
        format.json { render :show, status: :ok, location: @list }
        format.js {flash.now[:notice] = 'La lista se ha actualizado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @list.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la lista.'} #ajax

      end
    end
  end

  # DELETE /listap/1
  # DELETE /listap/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    @list.destroy
    respond_to do |format|
      format.html { redirect_to listap_url, notice: 'List was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'La lista se ha borrado de forma exitosa.'} #ajax

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_list
      @list = List.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def list_params
      params.require(:list).permit(:Lista, :Tipo, :FechaIni, :FechaFin, :IdEmpresa, detallelp_attributes: [:id, :Articulo, :PrecioMin, :PrecioMax, :IdEmpresa])
    end
end
