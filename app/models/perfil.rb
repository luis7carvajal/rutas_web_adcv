class Perfil < ActiveRecord::Base
  self.primary_key = "IdPerfiles"
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  scope :por_Catalogos, ->(usuario) {where("Descripcion LIKE ? AND IdUser = ?", "Catálogo%", usuario) }
  scope :por_Procesos, ->(usuario) {where("Descripcion LIKE ? AND IdUser = ?", "Proceso%", usuario) }
  scope :por_Reportes, ->(usuario) {where("Descripcion LIKE ? AND IdUser = ?", "Reporte%", usuario) }

  belongs_to :usuario, class_name:"Usuario", foreign_key: "IdUsuario"
end
