class Vehiculo < ActiveRecord::Base
  self.primary_key = 'IdVehiculos'
  self.table_name = 'Vehiculos' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: "1") }
  scope :inactivos, -> { where(Status: "0") }
  scope :comprobar_existencia, ->(paramc) { where(Clave: paramc)} # model
  scope :ordered, -> { order(Placas: :asc) }

  has_one :ruta, class_name: "Ruta", foreign_key: "vehiculo"
  belongs_to :catmarca, class_name:"Catmarca", foreign_key: "Marcas"


  validates :Clave, presence: true
  validates :Descripcion, presence: true
  validates :Marcas, presence: true
  validates :Modelo, presence: true
  validates :Modelo_year, presence: true
  validates :Placas, presence: true
  validates :Kilometraje, presence: true
  validates :NumeroEco, presence: true
  validates :KilometrajeSem, presence: true



  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |vehiculo|
        csv << vehiculo.attributes.values_at(*column_names)
      end
    end
  end



  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      @marca = Catmarca.find_by_Descripcion(row["Marca"])

      vehiculo = (find_by_IdVehiculos(row["Id"]) and find_by_Clave(row["Clave"]))  || new
      vehiculo.attributes = {Clave: row["Clave"], Marcas: @marca.try(:Clave), Modelo: row["Modelo(Tipo)"], Modelo_year: row["Modelo(Año)"], Placas: row["Placa"], Descripcion: row["Descripción"], Kilometraje: row["Kilometraje"], NumeroEco: row["N° Economico"], Aseguradora: row["Aseguradora"], Poliza: row["Poliza"], FechaVencSeguro: row["Fecha Vence Seguro"], MesVerifica: row["Fecha de Verificación"], KilometrajeSem: row["KMs Ult Servicio"], Status: row["Status"], IdEmpresa: empresa}

      if vehiculo.save
        # stuff to do on successful save
      else
        vehiculo.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

end
