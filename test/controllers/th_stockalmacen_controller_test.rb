require 'test_helper'

class ThStockalmacenControllerTest < ActionController::TestCase
  setup do
    @stockalmacen = th_stockalmacen(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:th_stockalmacen)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stockalmacen" do
    assert_difference('Stockalmacen.count') do
      post :create, stockalmacen: { Articulo: @stockalmacen.Articulo, IdEmpresa: @stockalmacen.IdEmpresa, Stock: @stockalmacen.Stock }
    end

    assert_redirected_to stockalmacen_path(assigns(:stockalmacen))
  end

  test "should show stockalmacen" do
    get :show, id: @stockalmacen
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stockalmacen
    assert_response :success
  end

  test "should update stockalmacen" do
    patch :update, id: @stockalmacen, stockalmacen: { Articulo: @stockalmacen.Articulo, IdEmpresa: @stockalmacen.IdEmpresa, Stock: @stockalmacen.Stock }
    assert_redirected_to stockalmacen_path(assigns(:stockalmacen))
  end

  test "should destroy stockalmacen" do
    assert_difference('Stockalmacen.count', -1) do
      delete :destroy, id: @stockalmacen
    end

    assert_redirected_to th_stockalmacen_path
  end
end
