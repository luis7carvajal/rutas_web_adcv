class RelclirutasController < ApplicationController
  before_action :set_relcliruta, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /relclirutas
  # GET /relclirutas.json
  def ruta_clientes

    datos_relcliruta
    contadores
    @ruta = Ruta.datos_ruta(params[:id])

    respond_to do |format|
      format.html
      format.xls #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end

  end

  def datos_relcliruta
    params[:IdEmpresa] = current_usuario.empresa_id

    @relcliruta = Relcliruta.new
    @relcliruta.build_reldayc
    @reldayc = Reldayc.new

    @searchc = Cliente.clientes_fuera_de_ruta(params).search(search_params)
    @searchc.sorts = 'id' if @searchc.sorts.empty?
    @clientes = @searchc.result().page(params[:clientes]).per(10)

    @search = Relcliruta.clientes_que_tiene(params[:id]).search(search_paramsx)
    @search.sorts = 'CodCliente' if @search.sorts.empty?
    @relclirutas = @search.result().page(params[:relclirutas]).per(10)
    @clientes_de_la_ruta_para_exportar = Relcliruta.clientes_que_tiene(params[:id])

  end

  def contadores
    params[:IdEmpresa] = current_usuario.empresa_id
    @searchc2 = Cliente.clientes_fuera_de_ruta2_contador(params).search(search_params)
    @cantidad_clientes_disponibles = @searchc2.result().count

    @search2 = Relcliruta.clientes_que_tiene(params[:id]).search(search_paramsx)
    @cantidad_clientes_relacionados_a_ruta = @search2.result().count
  end

  def borrar_clientes_ruta
    Relcliruta.where(:IdRuta => params[:IdRuta]).destroy_all

    params[:id] = params[:IdRuta]
    datos_relcliruta
    contadores
    respond_to do |format|
      format.js {flash.now[:notice] = 'Los clientes han sido eliminados.'} #ajax
    end

  end

  # GET /relclirutas/1
  # GET /relclirutas/1.json
  def show
  end

  # GET /relclirutas/new
  def new
    @relcliruta = Relcliruta.new
  end

  # GET /relclirutas/1/edit
  def edit
  end

  # POST /relclirutas
  # POST /relclirutas.json
  def create
    @relcliruta = Relcliruta.new(relcliruta_params)
    respond_to do |format|
      if @relcliruta.save
        params[:id] = @relcliruta.IdRuta
        contadores
        format.html { redirect_to @relcliruta, notice: 'Relcliruta was successfully created.' }
        format.json { render :show, status: :created, location: @relcliruta }
        format.js {flash.now[:notice] = 'El cliente se ha agregado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @relcliruta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al agregar el cliente.'} #ajax
      end
    end
  end

  # PATCH/PUT /relclirutas/1
  # PATCH/PUT /relclirutas/1.json
  def update
    respond_to do |format|
      if @relcliruta.update(relcliruta_params)
        format.html { redirect_to @relcliruta, notice: 'Relcliruta was successfully updated.' }
        format.json { render :show, status: :ok, location: @relcliruta }
      else
        format.html { render :edit }
        format.json { render json: @relcliruta.errors, status: :unprocessable_entity }
      end
    end
  end


  def relcliruta_agregar_seleccionar_todos_masivo
    params[:IdEmpresa] = current_usuario.empresa_id

    @searchc = Cliente.clientes_fuera_de_ruta(params).search(search_params)
    @todos_los_clientes_out_seleccionados = @searchc.result()


    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def relcliruta_agregar_de_forma_masiva
    params[:lista_clientes_out].each do |(c,cliente_id)|
      Relcliruta.create(IdRuta: params[:id],IdCliente: cliente_id, IdEmpresa: current_usuario.empresa_id, Fecha: Time.now.strftime("%d-%m-%Y"))
    end

    datos_relcliruta
    contadores
    respond_to do |format|
        format.js {flash.now[:notice] = 'Todos los clientes se han agregado de forma exitosa.'} #ajax
    end
  end

  def relcliruta_eliminar_seleccionar_todos_masivo
    params[:search6] = current_usuario.empresa_id

    @search = Relcliruta.clientes_que_tiene(params[:id]).search(search_paramsx)
    @todos_los_clientes_in_seleccionados = @search.result()

    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def relcliruta_eliminar_de_forma_masiva
    params[:lista_clientes_in].each do |(c,relcliruta_id)|
      @relcliruta = Relcliruta.find_by(Id: relcliruta_id)
      Reldayc.where(CodCli: @relcliruta.IdCliente, RutaId: params[:id]).delete_all #Eliminar su registro en el generador de rutas
      @relcliruta.destroy
    end

    datos_relcliruta
    contadores
    respond_to do |format|
        format.js {flash.now[:notice] = 'Todos los clientes seleccionados se han desvinculado de forma exitosa.'} #ajax
    end
  end



  # DELETE /relclirutas/1
  # DELETE /relclirutas/1.json
  def destroy
    @cliente = Cliente.find_by(IdCli: @relcliruta.IdCliente) #PARA RENDERIZAR EL CLIENTE
    Reldayc.where(CodCli: @relcliruta.IdCliente, RutaId: @relcliruta.IdRuta).delete_all #Eliminar su registro en el generador de rutas
    params[:id] = @relcliruta.IdRuta #<!--PARA QUE NO SE PIERDA EL ID DE LA LISTA CUANDO PASE POR EL EDITAR DEBIDO A QUE ESTARIA TOMANDO EL ID DEL REGISTRO Y NO EL DE LA LISTA-->
    @relcliruta.destroy
    @relcliruta.reldayc.destroy if @relcliruta.reldayc.present?

    respond_to do |format|
      contadores
      format.html { redirect_to relclirutas_url, notice: 'Relcliruta was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El cliente se ha borrado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relcliruta
      @relcliruta = Relcliruta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relcliruta_params
      params.require(:relcliruta).permit(:IdCliente, :IdRuta, :Fecha, :IdEmpresa, reldayc_attributes: [:RutaId, :CodCli, :IdEmpresa ])
    end
end
