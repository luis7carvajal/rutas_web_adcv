class Pregalad < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "Cliente"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  belongs_to :producto, class_name:"Producto", foreign_key: "SKU"
  scope :cajas, -> {where("pregalado.Tipmed = 'Caja'")}
  scope :piezas, -> {where("pregalado.Tipmed = 'Pzas'")}

  def self.promocion_de_venta(params)
    query = select("Rutas.Ruta, PRegalado.Docto Folio, PRegalado.SKU, Productos.Producto, CASE WHEN PRegalado.Tipmed = 'Caja' THEN 'Cajas' ELSE 'Piezas' END AS unidad, PRegalado.Cant")
    query = query.joins('INNER JOIN Rutas ON Rutas.IdRutas = PRegalado.RutaId AND Rutas.IdEmpresa = PRegalado.IdEmpresa INNER JOIN Productos ON Productos.Clave = PRegalado.SKU AND Productos.IdEmpresa = PRegalado.IdEmpresa')
    query = query.where('PRegalado.Tipo = \'V\'')
    query = query.where('PRegalado.Docto = ?', params[:docto]) if params[:docto].present?
    query = query.where('PRegalado.RutaId = ?', params[:rutaId]) if params[:rutaId].present?
    query = query.where('PRegalado.IdEmpresa = ?', params[:search6]) if params[:search6].present?
    query
  end

  def self.promocion_de_venta_entregas(params)
    query = select("Rutas.Ruta, PRegalado.Docto Folio, PRegalado.SKU, Productos.Producto, CASE WHEN PRegalado.Tipmed = 'Caja' THEN 'Cajas' ELSE 'Piezas' END AS unidad, PRegalado.Cant")
    query = query.joins('INNER JOIN Rutas ON Rutas.IdRutas = PRegalado.RutaId AND Rutas.IdEmpresa = PRegalado.IdEmpresa INNER JOIN Productos ON Productos.Clave = PRegalado.SKU AND Productos.IdEmpresa = PRegalado.IdEmpresa')
    query = query.where('PRegalado.Tipo != \'V\'')
    query = query.where('PRegalado.Docto = ?', params[:Pedido]) if params[:Pedido].present?
    query = query.where('PRegalado.RutaId = ?', params[:rutaId]) if params[:rutaId].present?
    query = query.where('PRegalado.IdEmpresa = ?', params[:search6]) if params[:search6].present?
    query
  end

  def self.rep_bitacora(params)
    query = joins('INNER JOIN Venta AS V ON pregalado.Docto = V.Documento AND pregalado.DiaO = V.DiaO AND V.RutaId = pregalado.RutaId AND V.IdEmpresa = pregalado.IdEmpresa')
    query = query.where('V.RutaId = ?', params[:search])
    query = query.where('V.IdEmpresa = ?', params[:search6])
    query = query.where('V.DiaO = ?', params[:diaO])
    query = query.where("pregalado.Tipo = 'V' AND V.Cancelada = 0 AND V.TipoVta!='Obsequio'")
    query = query.where('V.VendedorId = ?',params[:vendedor_id])
    query
  end

  def self.rep_bitacora_entregas(params)
    query = joins('INNER JOIN Pedidosliberados AS PL ON pregalado.Docto = PL.PEDIDO AND pregalado.DiaO = PL.DIAO_ENTREGA AND PL.RUTA = pregalado.RutaId AND PL.IDEMPRESA = pregalado.IdEmpresa')
    query = query.where('PL.RUTA = ?', params[:search])
    query = query.where('PL.IDEMPRESA = ?', params[:search6])
    query = query.where('PL.DIAO_ENTREGA = ?', params[:diaO])
    query = query.where("pregalado.Tipo = 'E' AND PL.CANCELADA = 0 AND PL.TIPO!='Obsequio'")
    query = query.where('PL.IDVENDEDOR = ?',params[:vendedor_id])
    query = query.where("PL.STATUS = 5")
    query
  end

  def self.rep_bitacora_pedidos(params)
    query = joins('INNER JOIN Pedidos AS P ON pregalado.Docto = P.Pedido AND pregalado.DiaO = P.DiaO AND P.Ruta = pregalado.RutaId AND P.IdEmpresa = pregalado.IdEmpresa')
    query = query.where('P.Ruta = ?', params[:search])
    query = query.where('P.IdEmpresa = ?', params[:search6])
    query = query.where('P.DiaO = ?', params[:diaO])
    query = query.where("pregalado.Tipo = 'P' AND P.Cancelado = 0 AND P.Tipo!='Obsequio'")
    query = query.where('P.IdVendedor = ?',params[:vendedor_id])
    query
  end

  def self.rep_producto_promocion(params)
    query =   select("Rutas.Ruta, pregalado.Cliente,C.Nombre as Responsable,C.NombreCorto as [Nombre_Comercial],V.Fecha, P.Clave as SKU,P.Producto, CASE WHEN ProductosXPzas.pzaxcja='1' then 0 else case  when pregalado.TipMed='Caja' then ISNULL(SUM(pregalado.Cant),0)  ELSE ISNULL(SUM(pregalado.Cant), 0)/ProductosXPzas.PzaXCja END END AS Cajas,  CASE WHEN ProductosXPzas.pzaxcja='1' then ISNULL(SUM(pregalado.Cant),0) ELSE CASE  WHEN  pregalado.TipMed='Pzas' then ISNULL(SUM(pregalado.Cant)%ProductosXPzas.PzaXCja,0) ELSE 0 END END AS Pzas,pregalado.Docto")
              .distinct
              .joins("INNER JOIN Rutas ON Rutas.idRutas = pregalado.RutaId INNER JOIN Productos AS P ON pregalado.SKU = P.Clave INNER JOIN Venta AS V ON pregalado.Docto = V.Documento AND pregalado.DiaO = V.DiaO AND V.RutaId = pregalado.RutaId INNER JOIN Clientes AS C ON V.CodCliente = C.IdCli INNER JOIN ProductosXPzas ON pregalado.SKU = ProductosXPzas.Producto")
              .where("(P.Ban_Envase=0) AND (V.Cancelada='0') AND (TipoVta!='Obsequio') AND (pregalado.Tipo='V') AND (pregalado.RutaId = :rutaId or :rutaId = '') AND (pregalado.SKU = :articulo or :articulo = '')  AND (pregalado.Cliente = :codcliente or :codcliente = '') AND (pregalado.IdEmpresa = :idempresa)",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], idempresa: params[:search6]})
    query = query.where('V.Fecha >= ? AND V.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where('pregalado.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query = query.where('V.VendedorId = ?',params[:vendedor_id]) if params[:vendedor_id].present?
    query = query.group("Rutas.Ruta, V.Fecha , P.Clave,P.Producto,pregalado.TipMed,pregalado.DocTo,pregalado.Cliente,ProductosXPzas.PzaXCja , C.Nombre, C.NombreCorto")
    query
  end

  def self.rep_producto_promocion_preventa(params)
    query =   select("Rutas.Ruta, pregalado.Cliente,C.Nombre as Responsable,C.NombreCorto as [Nombre_Comercial],pedidos.Fecha, P.Clave as SKU,P.Producto, CASE WHEN ProductosXPzas.pzaxcja='1' then 0 else case  when pregalado.TipMed='Caja' then ISNULL(SUM(pregalado.Cant),0)  ELSE ISNULL(SUM(pregalado.Cant), 0)/ProductosXPzas.PzaXCja END END AS Cajas,  CASE WHEN ProductosXPzas.pzaxcja='1' then ISNULL(SUM(pregalado.Cant),0) ELSE CASE  WHEN  pregalado.TipMed='Pzas' then ISNULL(SUM(pregalado.Cant)%ProductosXPzas.PzaXCja,0) ELSE 0 END END AS Pzas,pregalado.Docto")
              .distinct
              .joins("INNER JOIN Rutas ON Rutas.idRutas = pregalado.RutaId INNER JOIN Productos AS P ON pregalado.SKU = P.Clave left outer join pedidos on pregalado.Docto=pedidos.pedido and pregalado.RutaId=pedidos.Ruta AND pregalado.IdEmpresa = pedidos.IdEmpresa INNER JOIN Clientes AS C ON pedidos.CodCliente = C.IdCli INNER JOIN ProductosXPzas ON pregalado.SKU = ProductosXPzas.Producto")
              .where("(P.Ban_Envase=0) AND (pedidos.Cancelado='0') AND (pedidos.Tipo!='Obsequio') AND (pregalado.Tipo='P')")
    query = query.where("pregalado.RutaId = ?", params[:search])  if params[:search].present?
    query = query.where("pregalado.SKU = ?", params[:search3]) if params[:search3].present?
    query = query.where("pregalado.Cliente = ?", params[:search2]) if params[:search2].present?
    query = query.where("pregalado.IdEmpresa = ?", params[:search6]) if params[:search6].present?
  #  query = query.where('pedidos.Fecha >= ? AND pedidos.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where("pedidos.Status = ?", params[:Status]) if params[:Status].present?
    query = query.where('pregalado.Docto = ?', params[:pedido]) if params[:pedido].present?
    query = query.where('pregalado.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query = query.where('pedidos.idVendedor = ?',params[:vendedor_id]) if params[:vendedor_id].present?
    query = query.where("(P.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (P.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
    query = query.group("Rutas.Ruta, pedidos.Fecha , P.Clave,P.Producto,pregalado.TipMed,pregalado.DocTo,pregalado.Cliente,ProductosXPzas.PzaXCja , C.Nombre, C.NombreCorto")
    query
  end


  def self.total_existencias(params)
    if params[:commit].present?
      query =   select("isnull(SUM(V_CantidadxPedidoPromoxGrupo.piezas)/V_CantidadxPedidoPromoxGrupo.PzaXCja, 0) AS Cajas_Total, isnull(SUM(V_CantidadxPedidoPromoxGrupo.piezas)%V_CantidadxPedidoPromoxGrupo.PzaXCja, 0) AS Piezas_Total")
                .joins("INNER JOIN Productos AS P ON pregalado.SKU = P.Clave left outer join pedidos on pregalado.Docto=pedidos.pedido and pregalado.RutaId=pedidos.Ruta inner join V_CantidadxPedidoPromoxGrupo on pregalado.IdEmpresa=V_CantidadxPedidoPromoxGrupo.IdEmpresa and
                        PRegalado.RutaId=V_CantidadxPedidoPromoxGrupo.RutaId and PRegalado.SKU=V_CantidadxPedidoPromoxGrupo.SKU and PRegalado.Docto=V_CantidadxPedidoPromoxGrupo.docto LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente AND Clientes.IdEmpresa = Pedidos.IdEmpresa ")
                .where("(P.Ban_Envase=0) AND (pedidos.Cancelado='0') AND (pedidos.Tipo!='Obsequio') AND (pregalado.Tipo='P')")
      query = query.where("pregalado.RutaId = ?", params[:rutaIdB])  if params[:rutaIdB].present?
      query = query.where("pregalado.IdEmpresa = ?", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where('pedidos.Fecha >= ? AND pedidos.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query = query.where("pedidos.Pedido like :pedido", {:pedido => "%#{params[:Pedido_param]}%"}) if params[:Pedido_param].present?
      query = query.where("pedidos.Tipo like :tipo", {:tipo => "%#{params[:Tipo_param]}%"}) if params[:Tipo_param].present?
      query = query.where("clientes.IdCli like :cliente", {:cliente => "%#{params[:Cliente_IdCli_param]}%"}) if params[:Cliente_IdCli_param].present?
      query = query.where("clientes.NombreCorto like :nombre", {:nombre => "%#{params[:Cliente_NombreComercial_param]}%"}) if params[:Cliente_NombreComercial_param].present?
      query = query.where('pedidos.Fecha between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?
      query = query.where('pedidos.FechaEntrega between ? and ?', (params[:FechaEntrega_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaEntrega_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaEntrega_param].present?
      query = query.where("(P.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (P.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
      query = query.group("V_CantidadxPedidoPromoxGrupo.PzaXCja")
      query
    else
      query = select("0 AS Cajas_Total, 0 AS Piezas_Total").limit(1)
    end
    query
  end


  def self.producto_obsequio_pedido(params)
   query =   select("pregalado.Cliente, Clientes.Nombre as  Responsable, Clientes.NombreCorto as [NombreComercial], PE.Fecha, CASE WHEN PE.Cancelado=1 then 'Si' else 'No' end as  Cancelada, P.Clave AS SKU, P.Producto,CASE WHEN pregalado.TipMed='Caja' then (ISNULL(SUM(pregalado.Cant), 0)*ProductosXPzas.PzaXCja)/ProductosXPzas.PzaXCja ELSE (ISNULL(SUM(pregalado.Cant), 0)/ProductosXPzas.PzaXCja) END AS Cajas,CASE WHEN pregalado.TipMed='Pzas' then (ISNULL(SUM(pregalado.Cant), 0))%ProductosXPzas.PzaXCja ELSE (ISNULL(SUM(pregalado.Cant), 0)*ProductosXPzas.PzaXCja)%ProductosXPzas.PzaXCja END AS Pzas, pregalado.Docto")
                .distinct
                .joins("INNER JOIN Productos AS P ON pregalado.SKU = P.Clave INNER JOIN pedidos AS PE ON pregalado.Docto = PE.Pedido AND pregalado.DiaO = PE.DiaO AND PE.Ruta = pregalado.RutaId and pregalado.cliente=PE.Codcliente and pregalado.IdEmpresa=PE.IdEmpresa INNER JOIN Clientes ON pregalado.Cliente = Clientes.IdCli INNER JOIN ProductosXPzas ON pregalado.SKU = ProductosXPzas.Producto")
                .where("(pregalado.Tipo='P') AND (pregalado.RutaId = :rutaId or :rutaId = '') AND (pregalado.SKU = :articulo or :articulo = '')  AND (pregalado.Cliente = :codcliente or :codcliente = '') AND (pregalado.IdEmpresa = :idempresa)",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], idempresa: params[:search6]})
    query = query.where('PE.Fecha Between ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where('pregalado.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query = query.where('pedidos.idVendedor = ?',params[:vendedor_id]) if params[:vendedor_id].present?
    query = query.group("PE.Fecha , P.Clave,P.Producto,pregalado.TipMed,pregalado.DocTo,pregalado.Cliente,ProductosXPzas.PzaXCja , Clientes.Nombre, Clientes.NombreCorto,PE.Cancelado")
    query
  end

  def self.busqueda_general(params)
   query =   select("pregalado.*, venta.Fecha AS fecha_venta")
              .joins("left outer join venta on pregalado.Docto=venta.Documento and pregalado.RutaId=venta.RutaId")
                      .where("(TipoVta!='Obsequio') AND (pregalado.Tipo='V') AND (pregalado.RutaId = :rutaId or :rutaId = '') AND (pregalado.SKU = :articulo or :articulo = '')  AND (pregalado.Cliente = :codcliente or :codcliente = '') AND (pregalado.IdEmpresa = :idempresa)",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], idempresa: params[:search6]})
                      .distinct
  query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
  query
  end


  def self.busqueda_producto_obsequio(params)
   query =   select("pregalado.Cliente, Clientes.Nombre as  Responsable, Clientes.NombreCorto as NombreComercial, V.Fecha, CASE  WHEN   V.Cancelada=1 then 'Si' else 'No' end as  Cancelada, P.Clave AS SKU, P.Producto,CASE WHEN pregalado.TipMed='Caja' then (ISNULL(SUM(pregalado.Cant), 0)*ProductosXPzas.PzaXCja)/ProductosXPzas.PzaXCja ELSE (ISNULL(SUM(pregalado.Cant), 0)/ProductosXPzas.PzaXCja) END AS Cajas,CASE WHEN pregalado.TipMed='Pzas' then (ISNULL(SUM(pregalado.Cant), 0))%ProductosXPzas.PzaXCja ELSE (ISNULL(SUM(pregalado.Cant), 0)*ProductosXPzas.PzaXCja)%ProductosXPzas.PzaXCja END AS Pzas, pregalado.Docto")
                .distinct
                .joins("INNER JOIN Productos AS P ON pregalado.SKU = P.Clave INNER JOIN Venta AS V ON pregalado.Docto = V.Documento AND pregalado.DiaO = V.DiaO AND V.RutaId = pregalado.RutaId INNER JOIN Clientes ON pregalado.Cliente = Clientes.IdCli INNER JOIN ProductosXPzas ON pregalado.SKU = ProductosXPzas.Producto")
                .where("(TipoVta='Obsequio') AND (pregalado.RutaId = :rutaId or :rutaId = '') AND (pregalado.SKU = :articulo or :articulo = '')  AND (pregalado.Cliente = :codcliente or :codcliente = '') AND (pregalado.IdEmpresa = :idempresa)",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], idempresa: params[:search6]})
    query = query.where('V.Fecha Between ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where('pregalado.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query = query.where('V.VendedorId = ?',params[:vendedor_id]) if params[:vendedor_id].present?
    query = query.group("V.Fecha , P.Clave,P.Producto,pregalado.TipMed,pregalado.DocTo,pregalado.Cliente,ProductosXPzas.PzaXCja , Clientes.Nombre, Clientes.NombreCorto,V.Cancelada")
    query
  end






end
