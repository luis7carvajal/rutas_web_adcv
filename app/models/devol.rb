class Devol < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "CodCliente"
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model

  def self.busqueda_general(params)
   query = select('[devoluciones].Id,[devoluciones].CodCliente,[devoluciones].Devol,[devoluciones].Fecha,[devoluciones].Status,[devoluciones].Ruta,
                    [devoluciones].Vendedor,[devoluciones].Items,[devoluciones].Docto,[devoluciones].Cancelada,[devoluciones].IdEmpresa, [devoluciones].Subtotal,
                    [devoluciones].Total,[devoluciones].IVA, (sum(CASE WHEN isnull(DetalleDevol.Tipo,0)=0 then ISNULL(DetalleDevol.Pza,0) else 0 end)) as Cajas,
                    (sum(CASE WHEN isnull(DetalleDevol.Tipo,0)=1 then ISNULL(DetalleDevol.Pza,0) else 0 end)) as Piezas')
              .joins("Left outer join detalledevol on devoluciones.Docto=detalledevol.Docto and devoluciones.Ruta = detalledevol.RutaId INNER JOIN ProductosXPzas ON DetalleDevol.SKU=ProductosXPzas.Producto")
              .group(:Id,:CodCliente,:Devol,:Fecha,:Status,:Ruta,:Vendedor,:Items,:KG,:Docto,:Cancelada, :IdEmpresa, :Subtotal, :Total, :IVA)
              .where("(devoluciones.Ruta = :rutaId or :rutaId = '') AND (detalledevol.SKU = :articulo or :articulo = '')  AND (devoluciones.CodCliente = :codcliente or :codcliente = '') AND (devoluciones.DiaO = :diao or :diao = '')",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], diao: params[:diaO]})
              .where("(devoluciones.Vendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
              query = query.where('devoluciones.Fecha >= ? AND devoluciones.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
              query = query.distinct
  query
  end


end
