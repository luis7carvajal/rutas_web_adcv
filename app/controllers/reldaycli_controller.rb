class ReldaycliController < ApplicationController
  before_action :set_reldayc, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  protect_from_forgery except: :proc_transf_cli_vend

  def modulo
    @Descripcion_Modulo = "Proceso de generar ruta"
  end
  # GET /reldaycli
  # GET /reldaycli.json
  def index
    elementos_de_busqueda
    @reldayc = Reldayc.new

    @total_clientes_ruta = @reldaycliOutForCount.busqueda_segundaria(params[:q]).count if params[:q].present?
    @total_clientes = @reldaycliInForCount.busqueda_segundaria(params[:x]).count if params[:x].present?

    @total_pedidos = 0

    respond_to do |format| #RANSANK junto con la accion index
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def busqueda_vendedores_por_ruta #Actualiza las sucursales concorde a las empresas en el acceso
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @vendedores = Relvendruta.por_ruta(params[:ruta])
    respond_to do |format|
      format.js
    end
  end

  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id

    elementos_de_busqueda_ruta_autocompletar

    if params[:Entregas].present? and params[:vendedor].present? and !params[:reldayc_in].present? and !params[:reldayc_out].present?
      Stored_procedure.insert_update_delete_calculate("exec SPAD_DiasEntregaxVendedor '#{params[:Dia_de_semana]}','#{Time.now.strftime("%Y-%m-%d")}','#{params[:search]}','#{params[:vendedor]}','#{current_usuario.empresa_id}'")
    end
    @pathDeBusqueda = reldaycli_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable

    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @search_reldayc_out = Reldayc.ruta(params[:search]).por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
    #if params[:Entregas].present? me pudieron que lo comentara
    #  @search_reldayc_out = @search_reldayc_out.excluir_clientes_surtidos
    #else
      @search_reldayc_out = @search_reldayc_out.excluir_clientes_dia(params[:Dia_de_semana]) if params[:Dia_de_semana].present?
    #end
    @reldaycliOutForCount = @search_reldayc_out
    @search_reldayc_out = @search_reldayc_out.search(search_params)
    # make name the default sort column
    @search_reldayc_out.sorts = 'Id' if @search_reldayc_out.sorts.empty?
    @reldaycliglobal = @search_reldayc_out.result().page(params[:reldayc_out]).per(10)

    @search_reldayc_in = Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
    #unless params[:Entregas].present? me pudieron que lo comentara
      @search_reldayc_in = @search_reldayc_in.dia_con_clientes(params[:Dia_de_semana])
    #else
    #  @search_reldayc_in = @search_reldayc_in.base.surtidos
    #end

    @reldaycliInForCount = @search_reldayc_in
    #@secuencia = @search_reldayc_in.order(:Lunes).last
    @search_reldayc_in = @search_reldayc_in.search(search_paramsx)
    # make name the default sort column
    @search_reldayc_in.sorts = 'Id' if @search_reldayc_in.sorts.empty?
    @reldaycli = @search_reldayc_in.result().page(params[:reldayc_in]).per(10)

    @reldaycli_export = Reldayc.ruta(params[:search]).dia_con_clientes(params[:Dia_de_semana]).ordered_by_title.por_vendedor(params[:vendedor])

    session[:dia] = (params[:Dia_de_semana])#pasando el parametro a una variable de sesion para pasar el dia al update y evitar que se pierda el dia al actualizar
    @dia = session[:dia]
    @total_clientes = @reldaycliInForCount.count
    @total_clientes_ruta = @reldaycliOutForCount.count

  end



  def busqueda_generacion_ruta
    elementos_de_busqueda
    proceso_creacion_registros_por_vendedor

    @ruta = Ruta.datos_ruta(params[:search])
    if params[:Entregas].present?
      @total_pedidos = Pedido.total_pedidos_grutas(params, current_usuario.empresa_id).count
    else
      @total_pedidos = 0
    end

    #lo agregó el equipo de Efraín, me pidieron comentarlo
    #if params[:Entregas].present?
    #  add_stock_ruta_pedido
    #end
  end

  #lo agregó el equipo de Efraín, me pidieron comentarlo
  # def add_stock_ruta_pedido
  #  params[:search6] = current_usuario.empresa_id
  #  @Stock = Stoc.add_stock_from_pedido(params)
  #end

  def proceso_creacion_registros_por_vendedor
    #Buscar aquellos relcliruta cuyo cliente no lo tenga el vendedor que estoy buscando
    if params[:vendedor] != nil
      @relcliruta = Relcliruta.proceso_creacion_reldaycli_para_vendedor(params)
      @relcliruta.each do |relcliruta|
        Stored_procedure.insert_update_delete_calculate("exec SPAD_CrearRegistroPorVendedorRuta '#{relcliruta.IdCliente}','#{relcliruta.IdRuta}','#{params[:vendedor]}','#{current_usuario.empresa_id}'")
      end
    end
    Stored_procedure.insert_update_delete_calculate("exec SPAD_BorraRelDayCliRepetidos '#{params[:search]}','#{params[:vendedor]}','#{current_usuario.empresa_id}'")

  end

  def borrar_clientes_ruta_al_dia
    #no debe borrarse el reldaycli sino actualizarse debido a que el registro se crea al agregar un cliente a la ruta.

      if (params[:Dia_de_semana] == "Lunes") || (params[:Dia_de_semana] == "Martes") || (params[:Dia_de_semana] == "Miercoles") || (params[:Dia_de_semana] == "Jueves") || (params[:Dia_de_semana] == "Viernes") || (params[:Dia_de_semana] == "Sabado") || (params[:Dia_de_semana] == "Domingo")
        Reldayc.where(:RutaId => params[:RutaId], :idVendedor => params[:vendedor]).update_all(:"#{params[:Dia_de_semana]}" => nil )
      end
  #  params[:id] = params[:IdRuta]
  #  datos_relcliruta
    respond_to do |format|
      format.js {flash.now[:notice] = 'Los clientes han sido eliminados.'} #ajax
    end
  end

  # GET /reldaycli/1
  # GET /reldaycli/1.json
  def show
  end

  # GET /reldaycli/new
  def new
    @reldayc = Reldayc.new
  end

  # GET /reldaycli/1/edit
  def edit
  end

  # POST /reldaycli
  # POST /reldaycli.json
  def create
    @reldayc = Reldayc.new(reldayc_params)

    respond_to do |format|
      if @reldayc.save
        format.html { redirect_to @reldayc, notice: 'Reldayc was successfully created.' }
        format.json { render :show, status: :created, location: @reldayc }
        format.js {flash.now[:notice] = 'El cliente se ha agregado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @reldayc.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al agregar el cliente.'} #ajax
      end
    end
  end

  # PATCH/PUT /reldaycli/1
  # PATCH/PUT /reldaycli/1.json
  def update
     @dia = session[:dia]
    #params[:reldayc][:Lunes] = 15
    #@secuencia = @search_reldayc_in.order(:Lunes).last
    #params[:id][:Lunes] = @secuencia
    #para la secuencia
    if params[:reldayc][@dia] == "1" and params[:actualizado_por_el_usuario].nil?
      @registro = Reldayc.find_by(id:params[:id])
      @secuencia = Reldayc.ruta(@registro.RutaId).por_vendedor(@registro.idVendedor).por_empresa(current_usuario.empresa_id).dia_con_clientes(@dia).order(@dia).last.try(@dia)
      params[:reldayc][@dia] = @secuencia + 1 if @secuencia.present?
    end
    respond_to do |format|
      if @reldayc.update(reldayc_params)
        format.html { redirect_to @reldayc, notice: 'Reldayc was successfully updated.' }
        format.json { render :show, status: :ok, location: @reldayc }
        format.js {flash.now[:notice] = 'Proceso exitoso.'}
      else
        format.html { render :edit }
        format.json { render json: @reldayc.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error en el proceso.'}
      end
    end
    params[:search] = @reldayc.RutaId
    params[:Dia_de_semana] = session[:dia]
  end

  def generar_ruta_agregar_seleccionar_todos_masivo
    params[:search6] = current_usuario.empresa_id

    @todos_los_clientes_out_seleccionados = Reldayc.ruta(params[:search]).por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
    @todos_los_clientes_out_seleccionados = @todos_los_clientes_out_seleccionados.excluir_clientes_dia(params[:Dia_de_semana]) if params[:Dia_de_semana].present?
    @todos_los_clientes_out_seleccionados = @todos_los_clientes_out_seleccionados.busqueda_segundaria(params)

    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def generar_ruta_agregar_de_forma_masiva
    @dia = params[:dia_semana]
    @numero_asignado = 1
    params[:lista_clientes_out].each do |(c,reldayc_id)|
      @reldayc = Reldayc.find_by(id:reldayc_id)
      @secuencia = Reldayc.ruta(@reldayc.RutaId).por_vendedor(@reldayc.idVendedor).por_empresa(current_usuario.empresa_id).dia_con_clientes(@dia).order(@dia).last.try(@dia)
      @numero_asignado = @secuencia + 1 if @secuencia.present?
      @reldayc.update(:"#{@dia}" => @numero_asignado)
    end

    params[:search] = @reldayc.RutaId
    params[:vendedor] = @reldayc.idVendedor
    params[:Dia_de_semana] = params[:dia_semana]

    elementos_de_busqueda
    respond_to do |format|
        format.html { redirect_to @reldayc, notice: 'Reldayc was successfully updated.' }
        format.json { render :show, status: :ok, location: @reldayc }
        format.js {flash.now[:notice] = 'Todos los clientes se han agregado de forma exitosa.'} #ajax
    end
    #params[:search] = @reldayc.RutaId
    #params[:Dia_de_semana] = session[:dia]
  end

  def generar_ruta_eliminar_seleccionar_todos_masivo
    params[:search6] = current_usuario.empresa_id

    @todos_los_clientes_in_seleccionados = Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
    @todos_los_clientes_in_seleccionados = @todos_los_clientes_in_seleccionados.dia_con_clientes(params[:Dia_de_semana])
    @todos_los_clientes_in_seleccionados = @todos_los_clientes_in_seleccionados.busqueda_segundaria(params)

    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
    end
  end

  def generar_ruta_eliminar_de_forma_masiva
    @dia = params[:dia_semana]
    params[:lista_clientes_in].each do |(c,reldayc_id)|
      @reldayc = Reldayc.find_by(id:reldayc_id)
      @reldayc.update(:"#{@dia}" => nil)
    end

    params[:search] = @reldayc.RutaId
    params[:vendedor] = @reldayc.idVendedor
    params[:Dia_de_semana] = params[:dia_semana]

    elementos_de_busqueda
    respond_to do |format|
        format.html { redirect_to @reldayc, notice: 'Reldayc was successfully updated.' }
        format.json { render :show, status: :ok, location: @reldayc }
        format.js {flash.now[:notice] = 'Todos los clientes seleccionados se han desvinculado de forma exitosa.'} #ajax
    end
  end

  # DELETE /reldaycli/1
  # DELETE /reldaycli/1.json
  def destroy
    @reldayc.destroy
    respond_to do |format|
      format.html { redirect_to reldaycli_url, notice: 'Reldayc was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El cliente se ha borrado de forma exitosa.'} #ajax
    end
  end

  def asig_clientes_entre_vendedores
    elementos_de_busqueda2
    @pathDeBusqueda = asig_clientes_entre_vendedores_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @nombre_vendedor = Vendedor.find_by_IdVendedor(params[:vendedor]).try(:Nombre)
    @nombre_vendedor2 = Vendedor.find_by_IdVendedor(params[:vendedor2]).try(:Nombre)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def elementos_de_busqueda2
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar

    @search_cliente_v = Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
    @search_cliente_v = @search_cliente_v.dia_con_clientes(params[:Dia_de_semana])
    @clientes_v_ForCount = @search_cliente_v
    @search_cliente_v = @search_cliente_v.search(search_params)
    @search_cliente_v.sorts = 'Id' if @search_cliente_v.sorts.empty?
    @clientes_v = @search_cliente_v.result().page(params[:cliente_v]).per(10)

    @search_cliente_v2 = Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor2]).por_empresa(current_usuario.empresa_id)
    @search_cliente_v2 = @search_cliente_v2.dia_con_clientes(params[:Dia_de_semana])
    @clientes_v2_ForCount = @search_cliente_v2
    @search_cliente_v2 = @search_cliente_v2.search(search_paramsx)
    @search_cliente_v2.sorts = 'Id' if @search_cliente_v2.sorts.empty?
    @clientes_v2 = @search_cliente_v2.result().page(params[:cliente_v2]).per(10)

    #session[:dia] = (params[:Dia_de_semana])#pasando el parametro a una variable de sesion para pasar el dia al update y evitar que se pierda el dia al actualizar
    #@dia = session[:dia]
    @total_clientes_v = @clientes_v_ForCount.count
    @total_clientes_v2 = @clientes_v2_ForCount.count
    @vendedor = Vendedor.find_by_IdVendedor(params[:vendedor])
    @vendedor2 = Vendedor.find_by_IdVendedor(params[:vendedor2])

  end

  def proc_transf_cli_vend
    if params[:vendedor] != params[:vendedor2]
      @dia = params[:Dia_de_semana]
      @secuencia = 0
      #Remover los que tenía previamente asignado
      @clientes_v2= Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor2]).por_empresa(current_usuario.empresa_id)
      @clientes_v2 = @clientes_v2.dia_con_clientes(params[:Dia_de_semana])
      @clientes_v2.update_all(:"#{@dia}" => nil)

      #Asignar nuevos clientes
      @clientes_v = Reldayc.ruta(params[:search]).ordered_by_title.por_vendedor(params[:vendedor]).por_empresa(current_usuario.empresa_id)
      @clientes_v = @clientes_v.dia_con_clientes(params[:Dia_de_semana])

      #asignación de clientes
      @clientes_v.each do |cliente|
        @secuencia = @secuencia + 1
        @cliente_existente = Reldayc.find_by(:RutaId => params[:search], :CodCli => cliente.CodCli, :idVendedor => params[:vendedor2], :IdEmpresa => current_usuario.empresa_id)
        if @cliente_existente.present?
          @cliente_existente.update(:"#{@dia}" => @secuencia)
        else
          Reldayc.create(:"#{@dia}" => @secuencia, :RutaId => params[:search], :CodCli => cliente.CodCli, :idVendedor => params[:vendedor2], :IdEmpresa => current_usuario.empresa_id)
        end
      end


    end

    elementos_de_busqueda2
    respond_to do |format|
        format.js {flash.now[:notice] =
          if params[:vendedor] != params[:vendedor2]
            'Los clientes se han transferido de forma exitosa.'
          else
            'No se pudieron transferir los clientes: Debe seleccionar vendedores diferentes.'
          end
        }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reldayc
      @reldayc = Reldayc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reldayc_params
      params.require(:reldayc).permit(:CodCli, :Lunes, :Martes, :Miercoles, :Jueves, :Viernes, :Sabado, :Domingo, :RutaId, :IdEmpresa)
    end
end
