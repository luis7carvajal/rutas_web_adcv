class PosController < ApplicationController
  before_action :set_pos, only: [:destroy]
  before_action :authenticate_usuario!
  def modulo
    @Descripcion_Modulo = "Proceso POS"
  end

  def verificar_credenciales_pos
    params[:sucursal] = current_usuario.empresa_id
    @vendedor = Relvendruta.acceso_pos_vendedor(params).last

    respond_to do |format|
      if @vendedor != nil
        format.js {render :js => "window.location.href='"+pos_index_path(params[:Ruta],params[:PdaPW])+"'"}
      else
        format.js {flash.now[:alert] = 'Datos incorrectos'}
      end
    end

  end

  def index
    @controlador = 'pos'
    @accion = 'index'



    @pathDeBusqueda = pos_index_path # esto es usando para colocar el path dentro de las tablas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    params[:sucursal] = current_usuario.empresa_id
    @vendedor = Relvendruta.acceso_pos_vendedor(params).last
    params[:IdRuta] = @vendedor.IdRutas
    params[:IdVendedor] = @vendedor.IdVendedor

    params[:Fecha] = DateTime.now.in_time_zone("Mexico City")
    #params[:productox] = "asdasdasdasdasdasdasdasdasdad!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #params[:clientex] = "asdasdasdasdasdasdasdasdasdad!!!!!!!!!!!!!!!!!!!!!!!!!!"

    @searchClienteB = Cliente.activos.clientes_para_POS(params).search(search_params)
    @searchClienteB.sorts = 'NombreCorto' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    #para que solo actualice Continuid cuando se ingrese a la pag y no cuando se haga paginacion
    if params[:DiaO].nil?
      @DiaC = Continuid.DiaO_para_POS(params).last
      @DiaC.update(UDiaO: @DiaC.UDiaO+1, FolVta: @DiaC.FolVta+1)
    end

    @DiaO = Diaop.DiaO_para_POS(params).last
    if @DiaO.nil?
      @DiaO = Diaop.create(DiaO: @DiaC.UDiaO, Fecha:params[:Fecha], RutaId: params[:IdRuta], IdEmpresa:current_usuario.empresa_id)
    end
    params[:DiaO] = @DiaO.try(:DiaO)

    @Formaspago = Formapag.activos

    @searchProductoB = Producto.productos_Selector_para_POS(params).search(search_params)
    @searchProductoB.sorts = 'Clave' if @searchProductoB.sorts.empty?
    @productos_Selector = @searchProductoB.result().page(params[:productos])


    params[:documento] = @DiaC.try(:FolVta)
    @productos = Productoparapos.productos_para_POS(params)
    @cantidad_productos = Productoparapos.productos_para_POS_cantidad(params).count
    @suma_total = Productoparapos.productos_para_POS(params).sum("productosparapos.Total")

    #para la busqueda del descuento del producto cuando se ingresa el mismo !!!!!!!!!!!!!!!!!!!!!!!!!!*//*************
    #if params[:Clave_producto] != nil
    #   @producto = Producto.producto_para_POS_descuento(params).last
    #end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def producto_para_POS
    params[:sucursal] = current_usuario.empresa_id
    params[:documento] = params[:Folio_Vta]
    @SaldoAct = Vwcobran.saldo_pendiente_cliente(params).map(&:SaldoAct).compact.sum
    @cliente = Cliente.limite_credito(params).last
    @Saldodisponible = @cliente.Limite_Credito - @SaldoAct
    @total = Productoparapos.productos_para_POS(params).map(&:Total).compact.sum.to_d + params[:Total].to_f.round(2)

    @producto = Producto.productos_Selector_para_POS(params).find_by(Clave:params[:Clave_producto])
    @Factor = Relclili.busqueda_descuento_para_pos(params).last.try(:Factor).to_f

    params[:PU] = (@producto.VBase / ((@producto.IVA.to_f/100)+1)).round(2)
    if (params[:UM] == "Cajas")
        params[:LT_KG] = (@producto.PzaXCja*params[:Cantidad].to_i)*@producto.Equivalente
        params[:SubTotal] = params[:PU]*params[:Cantidad].to_i
        params[:Tipo_UM] = 0
    elsif (params[:UM] == "Piezas")
        params[:LT_KG] = params[:Cantidad].to_i*@producto.Equivalente
        params[:SubTotal] = ((params[:PU]/@producto.PzaXCja)*params[:Cantidad].to_i).round(2)
        params[:Tipo_UM] = 1
    end
    params[:Descuento] = (params[:SubTotal].to_f * (@Factor/100))

    params[:IVA] = (params[:SubTotal] - params[:Descuento]) * (@producto.IVA.to_f / 100).round(2)
    params[:Total] = (params[:SubTotal] - params[:Descuento] + params[:IVA])

    if (params[:Condicion_de_venta] != "Crédito") || (params[:Condicion_de_venta] == "Crédito" && @Saldodisponible > @total)
      proceso_creacion_producto_para_POS
    end

    respond_to do |format|
      if (params[:Condicion_de_venta] != "Crédito") || (params[:Condicion_de_venta] == "Crédito" && @Saldodisponible > @total)
       format.js {flash.now[:notice] = 'El producto se ha agregado de forma exitosa.'}
       #format.js {flash.now[:notice] = "#{@Saldodisponible}"}
      else
       format.js {flash.now[:notice] = 'Excede el Limite de Crédito.'}
      end
    end

  end

  def proceso_creacion_producto_para_POS
    @producto = Productoparapos.create(RutaId: params[:IdRuta], DiaO: params[:DiaO],
      Documento: params[:Folio_Vta],Vendedor: params[:IdVendedor],Cliente: params[:Cliente],
      TipoVta: params[:Condicion_de_venta],FormaPag: params[:Condicion_de_pago],LT_KG: params[:LT_KG],
      DocSalida: params[:DocSalida],Producto: params[:Clave_producto],Cantidad: params[:Cantidad],
      Tipo: params[:Tipo_UM],Precio: params[:PU],Descuento: params[:Descuento], Descuento_Porcentaje: @Factor,
      IVA: params[:IVA],IEPS: @producto.IEPS,SubTotal: params[:SubTotal],Total: params[:Total],IdEmpresa: current_usuario.empresa_id)

      params[:sucursal] = current_usuario.empresa_id
      params[:IdRuta] = @producto.RutaId
      params[:IdVendedor] = @producto.Vendedor

      @cantidad_productos = Productoparapos.productos_para_POS_cantidad(params).count
      @suma_total = Productoparapos.productos_para_POS(params).sum("productosparapos.Total")

      params[:Ps_id] = @producto.id
      @producto = Productoparapos.productos_para_POS(params).last
  end

  def procesar_venta
    params[:sucursal] = current_usuario.empresa_id
    params[:Fecha] = DateTime.now.in_time_zone("Mexico City")
    @productos = Productoparapos.productos_para_POS(params)
    #@producto = @productos.last
    @cantidad_productos = Productoparapos.productos_para_POS_cantidad(params).count
    @total = Productoparapos.productos_para_POS(params).map(&:Total).compact.sum
    params[:Cliente] = @productos.last.Cliente

    @SaldoAct = Vwcobran.saldo_pendiente_cliente(params).map(&:SaldoAct).compact.sum
    @cliente = Cliente.limite_credito(params).last
    @Saldodisponible = @cliente.Limite_Credito - @SaldoAct



    if (@productos.last.TipoVta != "Crédito") || (@productos.last.TipoVta == "Crédito" && @Saldodisponible > @total)
      @productos.each do |producto|
        if producto.Tipo == 0 # cero es caja
          @pzas = producto.Cantidad*producto.PzaXCja
        else
          @pzas = producto.Cantidad
        end
        Detalleve.create(Articulo: producto.Clave, Descripcion: producto.Producto, Precio: producto.Precio, Pza: @pzas, Kg: producto.LT_KG, DescPorc: producto.Descuento_Porcentaje, DescMon: producto.Descuento, Tipo: producto.Tipo, Docto: producto.Documento, Importe: producto.SubTotal, IVA: producto.IVA_calculado, IEPS: producto.IEPS, RutaId: producto.RutaId,IdEmpresa:current_usuario.empresa_id)
      end

      @venta= Vent.create(RutaId: @productos.last.RutaId, VendedorId: @productos.last.Vendedor, CodCliente: @productos.last.Cliente, Documento: @productos.last.Documento, Fecha: params[:Fecha], TipoVta: @productos.last.TipoVta,
                  SubTotal: @productos.sum("SubTotal"), IVA: @productos.sum("IVA"), IEPS: @productos.sum("IEPS"), TOTAL: @productos.sum("TOTAL"), EnLetra: "en letra", Items: @cantidad_productos, FormaPag: @productos.last.FormaPag, DocSalida: @productos.last.DocSalida, DiaO: @productos.last.DiaO, Cancelada: 0,
                  Kg: 0, IdEmpresa:current_usuario.empresa_id)

      if @productos.last.TipoVta == "Crédito"
        @venta.update(DiasCred: @productos.last.cliente.DiasCreedito, CreditoDispo: @Saldodisponible, Saldo: 0, Fvence: params[:Fecha] + @productos.last.cliente.DiasCreedito.days)
        Cobran.create(Cliente: @productos.last.Cliente, Documento: @productos.last.Documento, Saldo: @total, Status: 1, RutaId: @productos.last.RutaId, UltPago: 0, FechaReg: params[:Fecha], FechaVence: params[:Fecha] + @productos.last.cliente.DiasCreedito.days, FolioInterno: 0, TipoDoc: @productos.last.DocSalida, DiaO: @productos.last.DiaO, IdEmpresa:current_usuario.empresa_id)
        @productos.last.cliente.update(Saldo: (@productos.last.cliente.try(:Saldo) + @total) )
      end

      params[:Ruta] = Ruta.find_by(IdRutas:@productos.last.RutaId).Ruta
      @productos = Productoparapos.productos_para_POS(params).delete_all
    end


    respond_to do |format|
      if (@venta.TipoVta != "Crédito") || (@venta.TipoVta == "Crédito" && @Saldodisponible > @total)
        format.js {render :js => "window.location.href='"+pos_index_path(params[:Ruta],params[:PdaPW])+"'"}
        #format.js {render :js => "window.location.href='"+root_path+"'"}
      else
       format.js {flash.now[:notice] = 'Excede el Limite de Crédito.'}
      end
    end

  end

  def pos_vendedor_descuentoxproducto
    params[:sucursal] = current_usuario.empresa_id
    @Factor = Relclili.busqueda_descuento_para_pos(params).last.try(:Factor).to_f
    @Descuento = (params[:SubTotal].to_f * (@Factor/100))
    @IVA_calculado = ((params[:SubTotal].to_f - @Descuento) * (params[:iva].to_f / 100)).round(2)
    @Total = (params[:SubTotal].to_f - @Descuento + @IVA_calculado)

    @Total = '%.2f' % @Total
    @SubTotal = '%.2f' % params[:SubTotal]
    @Descuento = '%.2f' % @Descuento


    respond_to do |format|
      format.js
    end
  end

  def destroy
    @pos.destroy

      params[:sucursal] = current_usuario.empresa_id
      params[:IdRuta] = @pos.RutaId
      params[:IdVendedor] = @pos.Vendedor
      params[:documento] = @pos.Documento
      params[:DiaO] = @pos.DiaO

      @cantidad_productos = Productoparapos.productos_para_POS_cantidad(params).count
      @suma_total = Productoparapos.productos_para_POS(params).sum("productosparapos.Total")

    respond_to do |format|
      format.js {flash.now[:notice] = 'El producto se ha eliminado de forma exitosa.'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pos
      @pos = Productoparapos.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pos_params
      params.require(:mensaje).permit(:Clave, :EnBaseA, :Descripcion, :Mensaje, :FechaInicio, :FechaFinal, :Estado, :IdEmpresa)
    end
end
