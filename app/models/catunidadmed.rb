class Catunidadmed < ActiveRecord::Base
  self.primary_key = "Clave"
  self.table_name = 'CatUnidadMedida' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  has_many :productos, class_name: "Producto", foreign_key: "UniMed"


  def self.busqueda_pieza
    query = where("LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas'")
  end

end
