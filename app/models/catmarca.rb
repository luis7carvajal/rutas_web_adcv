class Catmarca < ActiveRecord::Base
  self.primary_key = "Clave"
  self.table_name = 'CatMarcas' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :producto_marca, -> { where(TipoMarca: "P") }
  scope :vehiculo_marca, -> { where(TipoMarca: "V") }
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }

  has_many :productos, class_name: "Producto", foreign_key: "ClaveMarca"
  has_many :vehiculos, class_name: "Vehiculo", foreign_key: "Marcas"

end
