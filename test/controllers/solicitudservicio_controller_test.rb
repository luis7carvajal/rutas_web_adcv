require 'test_helper'

class SolicitudservicioControllerTest < ActionController::TestCase
  setup do
    @solicitudserv = solicitudservicio(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:solicitudservicio)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create solicitudserv" do
    assert_difference('Solicitudserv.count') do
      post :create, solicitudserv: { Cancelado: @solicitudserv.Cancelado, CodCliente: @solicitudserv.CodCliente, DescripcionServicio: @solicitudserv.DescripcionServicio, Fecha: @solicitudserv.Fecha, FechaServicio: @solicitudserv.FechaServicio, FolServicio: @solicitudserv.FolServicio, IdEmpresa: @solicitudserv.IdEmpresa, Observaciones: @solicitudserv.Observaciones, Prioridad: @solicitudserv.Prioridad }
    end

    assert_redirected_to solicitudserv_path(assigns(:solicitudserv))
  end

  test "should show solicitudserv" do
    get :show, id: @solicitudserv
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @solicitudserv
    assert_response :success
  end

  test "should update solicitudserv" do
    patch :update, id: @solicitudserv, solicitudserv: { Cancelado: @solicitudserv.Cancelado, CodCliente: @solicitudserv.CodCliente, DescripcionServicio: @solicitudserv.DescripcionServicio, Fecha: @solicitudserv.Fecha, FechaServicio: @solicitudserv.FechaServicio, FolServicio: @solicitudserv.FolServicio, IdEmpresa: @solicitudserv.IdEmpresa, Observaciones: @solicitudserv.Observaciones, Prioridad: @solicitudserv.Prioridad }
    assert_redirected_to solicitudserv_path(assigns(:solicitudserv))
  end

  test "should destroy solicitudserv" do
    assert_difference('Solicitudserv.count', -1) do
      delete :destroy, id: @solicitudserv
    end

    assert_redirected_to solicitudservicio_path
  end
end
