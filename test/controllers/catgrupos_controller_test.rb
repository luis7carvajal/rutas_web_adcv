require 'test_helper'

class CatgruposControllerTest < ActionController::TestCase
  setup do
    @catgrupo = catgrupos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:catgrupos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create catgrupo" do
    assert_difference('Catgrupo.count') do
      post :create, catgrupo: { Clave: @catgrupo.Clave, Descripcion: @catgrupo.Descripcion, IdEmpresa: @catgrupo.IdEmpresa, Status: @catgrupo.Status, TipoGrupo: @catgrupo.TipoGrupo }
    end

    assert_redirected_to catgrupo_path(assigns(:catgrupo))
  end

  test "should show catgrupo" do
    get :show, id: @catgrupo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @catgrupo
    assert_response :success
  end

  test "should update catgrupo" do
    patch :update, id: @catgrupo, catgrupo: { Clave: @catgrupo.Clave, Descripcion: @catgrupo.Descripcion, IdEmpresa: @catgrupo.IdEmpresa, Status: @catgrupo.Status, TipoGrupo: @catgrupo.TipoGrupo }
    assert_redirected_to catgrupo_path(assigns(:catgrupo))
  end

  test "should destroy catgrupo" do
    assert_difference('Catgrupo.count', -1) do
      delete :destroy, id: @catgrupo
    end

    assert_redirected_to catgrupos_path
  end
end
