class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource_or_scope)
    visor_de_pedidos_path
  end

  def search_params #gema ransak
    params[:q]
  end

  def search_paramsx #gema ransak
    params[:x]
  end

  def clear_search_index #gema ransak
    if params[:search_cancel]
      params.delete(:search_cancel)
      if(!search_params.nil?)
        search_params.each do |key, param|
          search_params[key] = nil
        end
      end
    end
  end

  def elementos_de_busqueda_ruta_autocompletar
    if params[:ruta_autocompletar].present?
      @ruta_parametros = Ruta.activos.find_by(Ruta:"#{params[:ruta_autocompletar]}", IdEmpresa: current_usuario.empresa_id)
      params[:ruta] = @ruta_parametros.id
      params[:search] = @ruta_parametros.id
      params[:rutaIdB] = @ruta_parametros.id
    end
  end

  def permiso_listar
    modulo
    @puede_listar = current_usuario.try(:perfiles).find_by_Descripcion(@Descripcion_Modulo).try(:Listar)
    if @puede_listar != true
      redirect_to root_path, alert: "No tienes permisos para ver el módulo."
    end
  end

  def permiso_create
    modulo
      #para solventar un error al crear registros que las tablas tienen el trigger
      if ActiveRecord::Base.connection.class.name.match(/SQLServerAdapter/)
         ActiveRecord::ConnectionAdapters::SQLServerAdapter.use_output_inserted = false
      end
    
    @puede_crear = current_usuario.try(:perfiles).find_by_Descripcion(@Descripcion_Modulo).try(:Altas)
    if @puede_crear != true
      respond_to do |format|
        format.js {flash.now[:alert] = 'No tienes permisos para realizar esta acción.'}
      end
    end
  end

  def permiso_update
    modulo
    @puede_editar = current_usuario.try(:perfiles).find_by_Descripcion(@Descripcion_Modulo).try(:Modi)
    if @puede_editar != true
      respond_to do |format|
        format.js { flash.now[:alert] = "No tienes permisos para realizar esta acción."}
      end
    end
  end

  def permiso_destroy
    modulo
    @puede_destroy = current_usuario.try(:perfiles).find_by_Descripcion(@Descripcion_Modulo).try(:Bajas)
    if @puede_destroy != true
      respond_to do |format|
        format.js { flash.now[:alert] = "No tienes permisos para realizar esta acción."}
      end
    end
  end
end
