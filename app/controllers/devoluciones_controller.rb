class DevolucionesController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Reporte de devoluciones"
  end

  def index
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'devoluciones'
    @accion = 'index'
    elementos_de_busqueda
  end

  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id
    @pathDeBusqueda = devoluciones_index_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchClienteB = Cliente.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchClienteB.sorts = 'IdCli' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    @searchProductoB = Producto.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchProductoB.sorts = 'Clave' if @searchProductoB.sorts.empty?
    @productos_Selector = @searchProductoB.result().page(params[:productos])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])

  end

  def busqueda_devoluciones
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
   @devoluciones = Devol.busqueda_general(params)
   @detalledevol = Detalledevo.detalle_devolucion

    respond_to do |format|
    format.js
    end
  end

end
