class CatgruposController < ApplicationController
  before_action :set_catgrupo, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de grupos"
  end

  # GET /catgrupos
  # GET /catgrupos.json
  def index
    @search = Catgrupo.activos.productos_por_Sucursal(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdGrupo' if @search.sorts.empty?
    @catgrupos = @search.result().page(params[:page])
    @catgrupo= Catgrupo.new
    @cantidad_grupos_activos = Catgrupo.activos.productos_por_Sucursal(current_usuario.empresa_id).count

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catgrupos.to_csv}
      format.xls
    end
  end

  def inactivos
    @search = Catgrupo.inactivos.productos_por_Sucursal(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdGrupo' if @search.sorts.empty?
    @catgrupos = @search.result().page(params[:page])
    @cantidad_grupos_inactivos = Catgrupo.inactivos.productos_por_Sucursal(current_usuario.empresa_id).count

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @catgrupos.to_csv}
      format.xls
    end
  end

  def catgrupo_check
    @catgrupox = Catgrupo.find_by_Clave(params[:catgrupo][:Clave])
    respond_to do |format|
    format.json { render :json => !@catgrupox }
    end
  end

  # GET /catgrupos/1
  # GET /catgrupos/1.json
  def show
  end

  # GET /catgrupos/new
  def new
    @catgrupo = Catgrupo.new
  end

  # GET /catgrupos/1/edit
  def edit
  end

  # POST /catgrupos
  # POST /catgrupos.json
  def create
    if @puede_crear != true
      return
    end
    @catgrupo = Catgrupo.new(catgrupo_params)
    respond_to do |format|
      if @catgrupo.save
        format.html { redirect_to @catgrupo, notice: 'Catgrupo was successfully created.' }
        format.json { render :show, status: :created, location: @catgrupo }
        format.js {flash.now[:notice] = 'El grupo se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @catgrupo.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el grupo.'} #ajax
      end
    end
  end

  # PATCH/PUT /catgrupos/1
  # PATCH/PUT /catgrupos/1.json
  def update
    if @puede_editar != true
      return
    end
    #esto para evitar que se modifique el TipoGrupo si el grupo llega a estar relacionado a otro registro
    @tipo_catgrupo = Catgrupo.find_by_Clave(params[:id]).TipoGrupo
    if @tipo_catgrupo != catgrupo_params[:TipoGrupo]
      if @tipo_catgrupo == "T"
        @existe = Ruta.find_by_Sector(params[:id])
      elsif @tipo_catgrupo == "R"
        @existe = Ruta.find_by_TipoRuta(params[:id])
      elsif @tipo_catgrupo == "P"
        @existe = Relprogrupo.find_by_IdGrupo(params[:id])
      end
    end
    #que actualice solo si el registro no ha cambiado su tipo o si no está relacionado a otro registro
    if @existe == nil
      respond_to do |format|
        if @catgrupo.update(catgrupo_params)
          format.html { redirect_to @catgrupo, notice: 'Catgrupo was successfully updated.' }
          format.json { render :show, status: :ok, location: @catgrupo }
          format.js {flash.now[:notice] = 'El grupo se ha actualizado de forma exitosa.'} #ajax
        else
          format.html { render :edit }
          format.json { render json: @catgrupo.errors, status: :unprocessable_entity }
          format.js {flash.now[:alert] = 'Error al actualizar el grupo.'} #ajax
        end
      end
    else
      respond_to do |format|
          format.js {flash.now[:alert] = 'Error al actualizar, no es posible cambiar el tipo de grupo cuando el grupo está relacionado a otro registro.'} #ajax
      end
    end

  end

  # DELETE /catgrupos/1
  # DELETE /catgrupos/1.json

  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to catgrupo_url, notice: 'catgrupo was successfully destroyed.' }
      format.json { head :no_content }
      if @catgrupo.Status == true
        @catgrupo.update(Status:false)
        format.js {flash.now[:notice] = 'El grupo se ha borrado de forma exitosa.'} #ajax
      else
        @catgrupo.update(Status:true)
        format.js {flash.now[:notice] = 'El grupo se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_catgrupo
      @catgrupo = Catgrupo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def catgrupo_params
      params.require(:catgrupo).permit(:Clave, :Descripcion, :Status, :IdEmpresa, :TipoGrupo)
    end
end
