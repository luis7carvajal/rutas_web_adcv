class SaboresController < ApplicationController
  before_action :set_sabor, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de sabores"
  end

  # GET /sabores
  # GET /sabores.json
  def index
    @search = Sabor.activos.search(search_params)
    @search.sorts = 'Descripcion' if @search.sorts.empty?
    @sabores = @search.result().page(params[:page]).per(15)
    @sabor = Sabor.new
    @cantidad_sabores_activos = Sabor.activos.count

    respond_to do |format|
      format.html
      format.js 
      format.csv { send_data @sabores.to_csv}
      format.xls
    end
  end

  # GET /sabores
  # GET /sabores.json
  def inactivos
    @search = Sabor.inactivos.search(search_params)
    @search.sorts = 'Descripcion' if @search.sorts.empty?
    @sabores = @search.result().page(params[:page]).per(15)
    @sabor = Sabor.new
    @cantidad_sabores_inactivos = Sabor.inactivos.count

    respond_to do |format|
      format.html
      format.js 
      format.csv { send_data @sabores.to_csv}
      format.xls
    end
  end

  def sabor_check
    @saborx = Sabor.find_by_Clave(params[:sabor][:Clave])
    respond_to do |format|
    format.json { render :json => !@saborx }
    end
  end

  # GET /sabores/1
  # GET /sabores/1.json
  def show
  end

  # GET /sabores/new
  def new
    @sabor = Sabor.new
  end

  # GET /sabores/1/edit
  def edit
  end

  # POST /sabores
  # POST /sabores.json
  def create
    if @puede_crear != true
      return
    end
    @sabor = Sabor.new(sabor_params)

    respond_to do |format|
      if @sabor.save
        format.html { redirect_to @sabor, notice: 'Sabor was successfully created.' }
        format.json { render :show, status: :created, location: @sabor }
        format.js {flash.now[:notice] = 'El sabor se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @sabor.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el sabor.'} #ajax
      end
    end
  end

  # PATCH/PUT /sabores/1
  # PATCH/PUT /sabores/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @sabor.update(sabor_params)
        format.html { redirect_to @sabor, notice: 'Sabor was successfully updated.' }
        format.json { render :show, status: :ok, location: @sabor }
        format.js {flash.now[:notice] = 'El sabor se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @sabor.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el sabor.'} #ajax
      end
    end
  end

  # DELETE /sabores/1
  # DELETE /sabores/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to sabores_url, notice: 'Sabor was successfully destroyed.' }
      format.json { head :no_content }
      if @sabor.Status == true
        @sabor.update(Status:false)
        format.js {flash.now[:notice] = 'El sabor se ha borrado de forma exitosa.'} #ajax
      else
        @sabor.update(Status:true)
        format.js {flash.now[:notice] = 'El sabor se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sabor
      @sabor = Sabor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sabor_params
      params.require(:sabor).permit(:Clave, :Descripcion, :Status)
    end
end
