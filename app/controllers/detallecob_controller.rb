class DetallecobController < ApplicationController
  before_action :set_detalleco, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /detallecob
  # GET /detallecob.json
  def index
    @detalleco = Detalleco.new
    @rutas = Ruta.por_empresa(current_usuario.empresa_id)
  end

  # GET /detallecob/1
  # GET /detallecob/1.json
  def show
  end

  def obtener_ruta
    respond_to do |format|
      format.html { redirect_to cuentasxcobrar_path(detalleco_params[:RutaId]) } #envio de parametros que se usaran para enviar el la ruta
    end
  end

  def cuentasxcobrar
    @detalleco = Detalleco.new
    @rutas = Ruta.por_empresa(current_usuario.empresa_id)
    @cobranza = Cobran.por_ruta(params[:id])

  end


  # GET /detallecob/new
  def new
    @detalleco = Detalleco.new
  end

  # GET /detallecob/1/edit
  def edit
  end

  # POST /detallecob
  # POST /detallecob.json
  def create
    @cobranza = Cobran.find_by_id(params[:detalleco][:IdCobranza])
    params[:detalleco][:SaldoAnt] = @cobranza.Saldo - @cobranza.detallecob.sum(:Abono)
    @detalleco = Detalleco.new(detalleco_params)
    respond_to do |format|
      if @detalleco.save
        if (@cobranza.Saldo - @cobranza.detallecob.sum(:Abono)) <= 0
          @cobranza.update(Status:2)
        end

        dia_operativo_detalle
        format.html { redirect_to @detalleco, notice: 'Detalleco was successfully created.' }
        format.json { render :show, status: :created, location: @detalleco }
        format.js {flash.now[:notice] = 'El abono se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @detalleco.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el abono.'} #ajax
      end
    end
  end

  def dia_operativo_detalle
    #@detalleco.update(Saldo:(@detalleco.SaldoAnt - @detalleco.Abono))
    params[:ruta_D]= @detalleco.RutaId
    params[:search6] = current_usuario.empresa_id
    @existe_el_dia = Diaop.comprobar_si_existe_el_dia_para_el_detalle(params)
    if @existe_el_dia.last != nil
      @detalleco.update(Saldo:(@detalleco.SaldoAnt - @detalleco.Abono),DiaO:@existe_el_dia.last.try(:DiaO))
    else
      @dia_de_remplazo = Diaop.dia_de_remplazo_para_el_detalle(params)
      @detalleco.update(Saldo:(@detalleco.SaldoAnt - @detalleco.Abono),DiaO:@dia_de_remplazo.last.try(:DiaO))
    end

    Reloperacion.create(Id: 0, Folio: @detalleco.Documento, RutaId: @detalleco.RutaId, DiaO: @detalleco.DiaO, CodCli: @detalleco.Cliente, Tipo: "Cobranza", Total: @detalleco.Abono, Fecha: Time.now.strftime("%d-%m-%Y"), IdEmpresa: current_usuario.empresa_id)

  end

  # PATCH/PUT /detallecob/1
  # PATCH/PUT /detallecob/1.json
  def update
    respond_to do |format|
      if @detalleco.update(detalleco_params)
        format.html { redirect_to @detalleco, notice: 'Detalleco was successfully updated.' }
        format.json { render :show, status: :ok, location: @detalleco }
        format.js {flash.now[:notice] = 'El abono se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @detalleco.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el abono.'} #ajax
      end
    end
  end

  # DELETE /detallecob/1
  # DELETE /detallecob/1.json
  def destroy
    @detalleco.destroy
    respond_to do |format|
      format.html { redirect_to detallecob_url, notice: 'Detalleco was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El abono se ha eliminado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detalleco
      @detalleco = Detalleco.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detalleco_params
      params.require(:detalleco).permit(:IdCobranza, :Abono, :Fecha, :RutaId, :SaldoAnt, :Saldo, :FormaP, :DiaO, :Documento, :Cliente, :IdEmpresa, :Cancelada)
    end
end
