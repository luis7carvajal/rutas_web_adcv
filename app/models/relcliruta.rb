class Relcliruta < ActiveRecord::Base
  self.primary_key = "Id"
  scope :clientes_que_tiene, -> (id_param) { where(IdRuta: id_param) }
  scope :por_empresa, -> (id_param) {where(IdEmpresa: id_param)}
  scope :con_pedidos_surtidos, -> {where("IdCliente IN (SELECT DISTINCT CodCliente FROM Pedidos WHERE Status = 3 AND Ruta = RelClirutas.IdRuta AND IdEmpresa = RelClirutas.IdEmpresa)")}
  belongs_to :cliente, class_name:"Cliente", foreign_key: "IdCliente"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "IdRuta"

  has_one :reldayc, class_name: "Reldayc", foreign_key: "Id", dependent: :destroy
  accepts_nested_attributes_for :reldayc
  validates :IdRuta, presence: true
  validates :IdCliente, presence: true
  validates :IdEmpresa, presence: true
  validates_uniqueness_of :IdCliente, :scope => :IdRuta #validacion que un cliente no pueda estar en la misma ruta dos veces

  def self.total_clientes_Ruta(params)
    select("relclirutas.IdRuta")
     .where("(relclirutas.IdRuta = :rutaId or :rutaId = '') AND (relclirutas.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
  end

  def self.proceso_creacion_reldaycli_para_vendedor(params)
    query =  select('relclirutas.IdRuta, relclirutas.IdCliente')
             .where('(relclirutas.IdRuta = ?) ', params[:search])
             .where('relclirutas.IdCliente not in (select distinct CodCli from reldaycli where (reldaycli.IdVendedor = :vendedorId) AND (reldaycli.RutaId = :rutaId) AND (reldaycli.IdEmpresa = :empresa))',{rutaId: params[:search], vendedorId: params[:vendedor], empresa: params[:search6]})
             .distinct('relclirutas.IdCliente')
    query
  end


end
