class VentasController < ApplicationController
  before_action :authenticate_usuario!
  def index

  elementos_de_busqueda
  resultado_de_busqueda_general_graficos

  respond_to do |format|
  format.html
  format.js
  end

  end

  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id

    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])


  end

  def index3


    @chart = Fusioncharts::Chart.new({
        width: "600",
        height: "400",
        type: "mscolumn2d",
        renderAt: "chartContainer2",
        dataSource: {
            chart: {
            caption: "Comparison of Quarterly Revenue",
            subCaption: "Harry's SuperMart",
            xAxisname: "Quarter",
            yAxisName: "Amount ($)",
            numberPrefix: "$",
            theme: "fint",
            exportEnabled: "1",
            },
            categories: [{
                    category: [
                        { label: "Q1" },
                        { label: "Q2" },
                        { label: "Q3" },
                        { label: "Q4" }
                    ]
                }],
                dataset: [
                    {
                        seriesname: "Previous Year",
                        data: [
                            { value: "10000" },
                            { value: "11500" },
                            { value: "12500" },
                            { value: "15000" }
                        ]
                    },
                    {
                        seriesname: "Current Year",
                        data: [
                            { value: "25400" },
                            { value: "29800" },
                            { value: "21800" },
                            { value: "26800" }
                        ]
                    }
              ]
        }
    })

  end

  def index4


  end

  def busqueda_general_graficos


    resultado_de_busqueda_general_graficos

    respond_to do |format|
    format.js
    end
  end

  def resultado_de_busqueda_general_graficos
    params[:search6] = current_usuario.empresa_id
    params[:search0] =  current_usuario.empresa_id

    if (params[:ruta] != nil) and (params[:ruta] != "")
      params[:search] = Ruta.find_by(Ruta: params[:ruta], IdEmpresa: current_usuario.empresa_id).try(:IdRutas)
    else
      params[:search] = ""
    end

    if (params[:search1] == nil) or (params[:search1] == "")
      params[:search1] = Time.current.beginning_of_day-7.day
      params[:search2] =  Time.current.beginning_of_day
    end

    #Parametros por defecto



    @total_clientes = Bitacoratiempo.total_clientes(params).count
    @total_clientes_porcentaje = Relcliruta.total_clientes_Ruta(params).count

    #Sacar el promedio del tiempo
      # Get all values to find average
      values = Bitacoratiempo.prom_tiempo_visita(params).pluck(:TS)
      # Convert it to plain integers
      # anteriormente se uso la clase Time acá pero se eliminó y se hizo de esta forma manual ya que habían tiempos mayores a 23 horas con lo cual quedaban fuera del rango de la clase Time
      conv_values = values.map {|val| val.split(":")[0].to_i * 3600 + val.split(":")[1].to_i * 60 + val.split(":")[2].to_i }
      # Then count the average
      if conv_values.sum != 0
        avg = conv_values.sum / conv_values.length
      else
        avg = 0
      end
      # Optionally convert it to the string
      nice_string = Time.at(avg).utc.strftime("%H:%M")
      @prom_tiempo_visita = nice_string
    #fin del calculo del tiempo

    @total_ventas_contado = Detalleve.total_ventas_contado(params).sum("Importe+detallevet.IVA")
    @total_ventas_credito = Detalleve.total_ventas_credito(params).sum("Importe+detallevet.IVA")
    @total_ventas = Detalleve.total_ventas_contadoCredito(params).sum("Importe+detallevet.IVA")

    @best_seller = Producto.bestseller(params)
    @best_product = Producto.bestseller(params).first
      @contador = 6

    @worst_seller = Producto.worstseller(params)
    #Para la gráfica inicial
    @comparativo_ventas_Contado_VS_C = Vent.dashboard_VentasContado_VS_VentasCredito(params).tipo_contado
    @comparativo_ventas_C_VS_Credito = Vent.dashboard_VentasContado_VS_VentasCredito(params).tipo_credito

  end

  def index2
    params[:search1] = Time.current.beginning_of_day  # toma la fecha de ayer
    params[:search2] = Time.current.end_of_day+8.day  # toma la fecha de ayer

    params[:search0] =  current_usuario.empresa_id # "value2"

    resultado_de_busqueda_general_grafios_index2
  end

  def busqueda_general_graficos_index2

    resultado_de_busqueda_general_grafios_index2

    respond_to do |format|
    format.js
    end
  end

  def resultado_de_busqueda_general_grafios_index2
    params[:search6] = current_usuario.empresa_id

    params[:search2] = (params[:search1].to_date)+8.day  # toma la fecha de ayer
    params[:id_param22] = Date.today.strftime("%A") #obtiene el nombre del dia actual

    @cantidad_visitas = Reldayc.cantidad_visitas_programadas_dashboard.dia_con_clientes_dashboard(params[:id_param22]).count
    @visitas_pendientes = @cantidad_visitas - (Bitacoratiempo.visitas_pendientes(params).count)
    @pedidos = Pedido.pedido_dashboard2(params).count
    @entregas = Pedidoliberado.entregas_dashboard2(params).count
    @pedidos_levantados = Pedido.pedidos_levantados(params)
    @pedidos_liberados = Pedidoliberado.pedidos_liberados(params)
    @pedidos_entregados = Pedidoliberado.pedidos_entregados(params)
    @pedidos_cancelados = Pedidoliberado.pedidos_cancelados(params)
    @cantidad_pedidos = Pedido.cantidad_pedidos(params)
    @pedidosxtipo_venta = Pedido.pedidosxtipo_venta(params)
    @pedidosxdia = Pedido.pedidosxdia(params)
    @pedidosxclasif_cliente = Pedido.pedidosxclasif_cliente(params)



  end

  def busqueda_clientes_captados
    @rutas = Ruta.joins('left outer join relclirutas on rutas.IdRutas=relclirutas.IdRuta')
                     .where('relclirutas.IdRuta IS NOT NULL AND relclirutas.IdEmpresa = ? AND (relclirutas.Fecha >= ? AND relclirutas.Fecha <= ?)',current_usuario.empresa_id, (params[:search].to_date).strftime('%Y-%m-%d'),( params[:search2].to_date).strftime('%Y-%m-%d'))
                     .distinct

    respond_to do |format|
    format.js
    end
  end



end
