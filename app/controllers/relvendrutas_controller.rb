class RelvendrutasController < ApplicationController
  before_action :set_relvendruta, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /relvendrutas
  # GET /relvendrutas.json
  def ruta_vendedores

    datos_relvendruta
    contadores
    @ruta = Ruta.datos_ruta(params[:id])

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end

  end

  def datos_relvendruta
    params[:IdEmpresa] = current_usuario.empresa_id

    @relvendruta = Relvendruta.new

    @searchv = Vendedor.vendedores_fuera_de_ruta(params).search(search_params)
    @searchv.sorts = 'id' if @searchv.sorts.empty?
    @vendedores = @searchv.result().page(params[:vendedores]).per(15)

    @search = Relvendruta.vendedores_que_tiene(params[:id]).search(search_params)
    @search.sorts = 'IdVendedor' if @search.sorts.empty?
    @relvendrutas = @search.result().page(params[:relvendrutas]).per(10)
  end

  def contadores
    params[:IdEmpresa] = current_usuario.empresa_id
    @cantidad_vendedores_disponibles = Vendedor.vendedores_fuera_de_ruta2_contador(params).count
    @cantidad_vendedores_relacionados_a_ruta = Relvendruta.vendedores_que_tiene(params[:id]).count
  end

  def borrar_vendedores_ruta
    Relvendruta.where(:IdRuta => params[:IdRuta]).destroy_all #Elimina todos los vendedores de la ruta
    Reldayc.where(:RutaId => params[:IdRuta]).destroy_all #Elimina todos los reldaycli de la ruta

    params[:id] = params[:IdRuta]
    Ruta.find_by(:IdRutas => params[:IdRuta]).update(Vendedor: nil) #Le quita el vendedor a la ruta

    datos_relvendruta
    contadores
    respond_to do |format|
      format.js {flash.now[:notice] = 'Los vendedores han sido eliminados.'} #ajax
    end

  end



  # GET /relvendrutas/1
  # GET /relvendrutas/1.json
  def show
  end

  # GET /relvendrutas/new
  def new
    @relvendruta = Relvendruta.new
  end

  # GET /relvendrutas/1/edit
  def edit
  end

  #Si la ruta no tiene vendedor asignado, entonces lo asigna
  def asignacion_vendedor_en_TablaRutas_cuando_se_crea
    if @relvendruta.ruta.Vendedor == nil
      @relvendruta.ruta.update(Vendedor: @relvendruta.try(:IdVendedor)) #se actualiza
    end
  end

  # POST /relvendrutas
  # POST /relvendrutas.json
  def create
    @relvendruta = Relvendruta.new(relvendruta_params)

    respond_to do |format|
      if @relvendruta.save
        params[:id] = @relvendruta.IdRuta
        contadores
        asignacion_vendedor_en_TablaRutas_cuando_se_crea
        format.html { redirect_to @relvendruta, notice: 'Relvendruta was successfully created.' }
        format.json { render :show, status: :created, location: @relvendruta }
        format.js {flash.now[:notice] = 'El vendedor se ha agregado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @relvendruta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al agregar el vendedor.'} #ajax
      end
    end
  end

  # PATCH/PUT /relvendrutas/1
  # PATCH/PUT /relvendrutas/1.json
  def update
    respond_to do |format|
      if @relvendruta.update(relvendruta_params)
        format.html { redirect_to @relvendruta, notice: 'Relvendruta was successfully updated.' }
        format.json { render :show, status: :ok, location: @relvendruta }
      else
        format.html { render :edit }
        format.json { render json: @relvendruta.errors, status: :unprocessable_entity }
      end
    end
  end

  def reasignacion_vendedor_en_TablaRutas_cuando_se_elimina
    if @relvendruta.IdVendedor == @relvendruta.ruta.Vendedor #se compara si es el mismo de la ruta
       @Otro_vendedor_para_La_Ruta = Relvendruta.where(IdRuta: @relvendruta.IdRuta).where.not(IdVendedor: @relvendruta.IdVendedor).first #se busca otro
       @relvendruta.ruta.update(Vendedor: @Otro_vendedor_para_La_Ruta.try(:IdVendedor)) #se actualiza
    end
  end



  # DELETE /relvendrutas/1
  # DELETE /relvendrutas/1.json
  def destroy
    @vendedor = Vendedor.find_by(IdVendedor: @relvendruta.IdVendedor) #PARA RENDERIZAR EL Vendedor
    Reldayc.where(idVendedor: @relvendruta.IdVendedor, RutaId: @relvendruta.IdRuta).delete_all #Eliminar su registro en el generador de rutas
    reasignacion_vendedor_en_TablaRutas_cuando_se_elimina #en caso de que el vendedor sea el principal de la ruta se asigne otro
    @relvendruta.destroy
    params[:id] = @relvendruta.IdRuta #<!--PARA QUE NO SE PIERDA EL ID DE LA LISTA CUANDO PASE POR EL EDITAR DEBIDO A QUE ESTARIA TOMANDO EL ID DEL REGISTRO Y NO EL DE LA LISTA-->
    respond_to do |format|
      contadores
      format.html { redirect_to relvendrutas_url, notice: 'Relvendruta was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El vendedor se ha borrado de forma exitosa.'} #ajax
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relvendruta
      @relvendruta = Relvendruta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relvendruta_params
      params.require(:relvendruta).permit(:IdVendedor, :IdRuta, :IdEmpresa, :Fecha)
    end
end
