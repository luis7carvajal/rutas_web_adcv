class Producto < ActiveRecord::Base
  self.primary_key = "Clave"
  self.table_name = 'Productos' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
#  scope :productosquetiene, -> (id_param) { where(ListaId: id_param) }
  scope :envases, -> { where(Ban_Envase: true) }
  scope :productos, -> { where(Ban_Envase: false) }
  scope :activos, -> { where(Status: "A") }
  scope :inactivos, -> { where(Status: "I") }
  scope :datos_producto, ->(param_id) { find_by(Clave: param_id)} # para que no devuelva una coleccion, sino un solo registro
  scope :comprobar_existencia, ->(paramc) { where(Clave: paramc)} # model
  scope :without_mensaje, -> (id_params) {Producto.includes(:relmens).references(:relmens).where("relmens.IDRow IS NULL OR productos.Clave not in (select distinct CodProducto from relmens where IdProducto = ?)", id_params)}

  validates :Clave, presence: true
  validates :Producto, presence: true

#scope productos promociones
  scope :promociones_productos_por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model

  #scope :without_discount -> {Product.includes(:detalleId).where(:detalleId => { id: nil })}
  #scope :without_discount, -> (id_param) {Producto.includes(:detalleld).where("detalleld.ListaId <> ?", id_param)}
# scope :without_discount, -> (id_params) {Producto.includes(:detalleld).where.not(:detalleld => { listaid: id_params })}
#scope :without_discount, -> {Producto.includes(:detalleld).references(:detalleld).where("detalleld.id IS NULL")}

#buscame aquellos productos que no tengan un detalleld o detalle.listaid sea diferente del q paso x el url
  scope :without_discount, -> (id_params) {Producto.includes(:detalleld).references(:detalleld).where("detalleld.id IS NULL OR productos.Clave not in (select distinct Articulo from detalleld where ListaId = ?)", id_params)}
  scope :without_price, -> (id_params) {Producto.includes(:detallelp).references(:detallelp).where("detallelp.id IS NULL OR productos.Clave not in (select distinct Articulo from detallelp where ListaId = ?)", id_params)}

  #La lista de productos de promociones producto-promocion no debe repetirse
  scope :without_detallepromo, -> (id_params) {Producto.includes(:detallepromo).references(:detallepromo).where("detallepromo.id IS NULL OR productos.Clave not in (select distinct Articulo from detallepromo where PromoId = ? and Tipo = 'TRUE')", id_params).order("productos.Producto ASC")}

  scope :without_grupo, -> (id_params) {Producto.includes(:relprogrupos).references(:relprogrupos).where("relprogrupos.id IS NULL OR productos.Clave not in (select distinct ProductoId from relprogrupos where IdGrupo = ?)", id_params)}
  scope :producto_sin_grupo_en_sucursal, -> (id_empresa) {Producto.includes(:relprogrupos).references(:relprogrupos).where("relprogrupos.id IS NULL OR productos.Clave not in (select distinct ProductoId from relprogrupos where IdEmpresa = ?)",id_empresa)}




  scope :wut, -> {Producto.includes(:detalleld).where(:detalleld => { id: nil })}

  has_one :productosxpza, class_name: "Productosxpza", foreign_key: "Producto", dependent: :destroy
  accepts_nested_attributes_for :productosxpza

  has_one :stoc, class_name: "Stoc", foreign_key: "Articulo", dependent: :destroy
  has_one :stocalmacen, class_name: "Stocalmacen", foreign_key: "Articulo"
  #belongs_to :catgrupo, class_name:"Catgrupo", foreign_key: "Sector" #esta no se usará porque se cambio productos a empresas y los grupos por sucursal y se agrego un nuevo modulo relprogrupos
  has_many :catgrupos, through: :relprogrupos, source: :catgrupo, foreign_key: "ProductoId"

  belongs_to :catunidadmed, class_name:"Catunidadmed", foreign_key: "UniMed"
  belongs_to :unimedeq, class_name:"Catunidadmed", foreign_key: "UniMedEq"


  belongs_to :catmarca, class_name: "Catmarca", foreign_key: "ClaveMarca"



  has_many :productoenvase, class_name: "Productoenvas", foreign_key: "Producto", dependent: :destroy


  has_many :detalleld, class_name: "Detalle", foreign_key: "Articulo", dependent: :destroy
  has_many :detallelp, class_name: "Deta", foreign_key: "Articulo", dependent: :destroy

  has_many :relprogrupos, class_name: "Relprogrupo", foreign_key: "ProductoId", dependent: :destroy

  has_many :relmens, class_name: "Relmen", foreign_key: "CodProducto"

  has_one :comision, class_name: "Comision", foreign_key: "producto_id"

  has_many :detallepromo, class_name:"Detalleprom", foreign_key: "Articulo"
  has_many :detallecombo, class_name:"Detacombo", foreign_key: "Articulo"

  #Reportes
  has_many :detalledevol, class_name: "Detalledevo", foreign_key: "SKU"
  has_many :pregalado, class_name: "Pregalad", foreign_key: "SKU"
  has_one :productoneg, class_name: "Productoneg", foreign_key: "CodProd", dependent: :destroy
  has_many :recarga, class_name: "Recarg", foreign_key: "Articulo"


  has_attached_file :cover, styles: { thumb: "400x300", minithumb: "80x55" }
	#attached file q tiene un archivo adjunto, q es cover, el mismo campo agregado en la migracion. styles es para configuraciones, y especificamos las dos versiones de tamaños
	validates_attachment_content_type :cover, content_type: /\Aimage\/.*\Z/#validacion para evitar ataques, y solo esperar un tipo de archivo en content type, como pdf, ejecutables, imagenes etc. con el tipo de archivo especificado, el usuario puede subir varios tipos de imagenes



  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |producto|
        csv << producto.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      @sector = Catgrupo.find_by_Clave_and_IdEmpresa(row["Sector(Clave)"], empresa)
      @unidadmed = Catunidadmed.find_by_Abreviatura_and_IdEmpresa(row["Unidad Medida(Abreviatura)"], empresa)
      @marca = Catmarca.find_by_Descripcion_and_IdEmpresa(row["Marca(Descripción)"], empresa)
      @unidadmedEq = Catunidadmed.find_by_Abreviatura_and_IdEmpresa(row["Unidad Medida Eq(Abreviatura)"], empresa)
      if @sector.try(:TipoGrupo) == "P"
        @producto_en_el_sector = Relprogrupo.find_by_IdGrupo_and_ProductoId(@sector.id, row["Clave"])
      end

      producto = (find_by_Clave(row["Clave"]))  || new
      producto.attributes = {Clave: row["Clave"], Producto: row["Producto"], CodBarras: row["Codigo de Barras"], IVA: row["IVA"], Status: row["Status"], IEPS: row["IEPS"], VBase: row["Precio Base"], Equivalente: row["Valor Eq"], Ban_Envase: row["Es envase"], Granel: row["Granel"], UniMedEq: @unidadmedEq.try(:Clave), ClaveMarca: @marca.try(:Clave), UniMed: @unidadmed.try(:Clave), IdEmpresa: empresa}


      if producto.productosxpza != nil
        producto.productosxpza.attributes = {PzaXCja: row["PzaXCja"], Producto: row["Clave"], IdEmpresa: empresa}
      else
        producto.productosxpza_attributes = {PzaXCja: row["PzaXCja"], Producto: row["Clave"], IdEmpresa: empresa}
      end

      if producto.save
        if (@producto_en_el_sector.nil? && @sector != nil)
         Relprogrupo.create(IdGrupo: @sector.id, ProductoId: row["Clave"], IdEmpresa:empresa)
        end
      else
        producto.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end







  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

  def self.proceso_stock_busqueda_productos(params)
    query =  joins('left outer join stock on productos.Clave=stock.Articulo')
             .select('productos.Clave,productos.Producto,stock.Articulo')
             .where("(stock.ruta IS NULL OR stock.ruta != :rutaId ) AND (productos.Status = :status) AND productos.clave not in (select distinct articulo from stock where ruta = :rutaId and IdEmpresa = :idempresa)",{rutaId: params[:search], status: "A", idempresa: params[:search6]})
             .distinct
  end

  def self.productos_Selector_para_POS(params)
    query =  select('Stock.Articulo AS Clave,Cast(Stock.Stock As Int)/(Case When IsNull(ProductosXPzas.PzaXCja,0)=1 Then 1 Else ProductosXPzas.PzaXCja End) AS Stock,
                      Productos.Producto,Productos.IEPS,Productos.IVA,Productos.Equivalente, Productos.Id, Productos.CodBarras, Productos.Foto, stock.ruta, Productos.VBase,
                      ProductosXPzas.PzaXCja, case When IsNull(ProductosXPzas.PzaXCja,0)=1 Then 1 Else ProductosXPzas.PzaXCja End AS PzaXCja ')
             .joins('INNER JOIN Stock ON Productos.Clave = Stock.Articulo Left Join ProductosXPzas On Productos.Clave=ProductosXPzas.Producto')
             .where("(stock.ruta = :rutaId)",{rutaId: params[:IdRuta]})
  end

  def self.productos_para_POS2r(params)
    query =  select('Productos.Clave,Productos.IEPS,Productos.IVA,dld.Factor, rcl.CodCliente')
             .joins('Left outer join Detalleld dld ON Productos.Clave = dld.Articulo Left outer join Relclilis rcl On dld.ListaId=rcl.ListaD')
             .where("(Productos.Clave = :producto)",{producto: params[:productox]})
    query = query.where("(rcl.CodCliente = :cliente or Productos.Clave = :producto)",{cliente: params[:clientex], producto: params[:productox]})
  end

  def self.producto_para_POS_descuento(params)
    query_string = "Productos.Clave,Productos.IEPS,Productos.IVA,(SELECT TOP 1 Factor from Detalleld dld inner join Productos p ON dld.Articulo=Productos.Clave inner join Relclilis rcl On dld.ListaId=rcl.ListaD  where rcl.CodCliente = :cliente and dld.Articulo= :producto) as Factor"
    query = select(sanitize_sql_array([query_string, producto: params[:Clave_producto], cliente: params[:clientex]]))
    query = query.where("(Productos.Clave = :producto)",{producto: params[:Clave_producto]})
    query
  end


  def self.catalogo_productos(params)
    query_string = "Productos.*, isnull((select  catgrupos.descripcion from  relprogrupos inner  join catgrupos  on relprogrupos.idgrupo=catgrupos.clave where  catgrupos.TipoGrupo='P' and   relprogrupos.idempresa = :idempresa and  relprogrupos.productoid=Productos.Clave),'') as  Grupo, CatUnidadMedida.UnidadMedida"
    query = select(sanitize_sql_array([query_string, idempresa: params[:sucursal]]))
            .joins('LEFT OUTER JOIN CatUnidadMedida ON Productos.UniMed = CatUnidadMedida.Clave')
  end

  def self.bestseller(params)
      query =  joins("INNER JOIN DetalleVet ON Productos.Clave = DetalleVet.Articulo INNER JOIN Venta ON DetalleVet.Docto = Venta.Documento AND DetalleVet.RutaId = Venta.RutaId INNER JOIN ProductosXPzas ON Productos.Clave = ProductosXPzas.Producto")
            .select("DetalleVet.Articulo, SUM(CASE  WHEN  DetalleVet.Tipo=0 THEN DetalleVet.Pza * ProductosXPzas.PzaXCja ELSE DetalleVet.Pza END) AS count,Productos.Producto, Productos.Clave")
            .group("detallevet.Articulo, productos.Producto, productos.Clave").order("count DESC").limit(10)
            .where("(venta.RutaId = :rutaId or :rutaId = '') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
      query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
      query
   end

   def self.worstseller(params)
      query =  joins("INNER JOIN DetalleVet ON Productos.Clave = DetalleVet.Articulo INNER JOIN Venta ON DetalleVet.Docto = Venta.Documento AND DetalleVet.RutaId = Venta.RutaId INNER JOIN ProductosXPzas ON Productos.Clave = ProductosXPzas.Producto")
            .select("DetalleVet.Articulo, SUM(CASE  WHEN  DetalleVet.Tipo=0 THEN DetalleVet.Pza * ProductosXPzas.PzaXCja ELSE DetalleVet.Pza END) AS count,Productos.Producto, Productos.Clave")
            .group("detallevet.Articulo, productos.Producto, productos.Clave").order("count ASC").limit(5)
            .where("(venta.RutaId = :rutaId or :rutaId = '') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
      query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
      query
   end


   #Con el distinct para que no se repita no se debe colocar en el select campos como el vendedor id o el Id debido a que regresaria registros repetidos porque son campos que son diferentes
   def self.proceso_pedidos(params)

     query =  joins('left outer join th_stockalmacen on productos.Clave=th_stockalmacen.Articulo')
              .select('productos.Clave,productos.Producto,th_stockalmacen.Articulo')
              .where("(productos.Status = :status) AND productos.clave not in (select distinct articulo from th_stockalmacen where IdEmpresa = :idempresa) AND productos.clave in (select distinct Articulo from detallelp where IdEmpresa = :idempresa)",{status: "A", idempresa: params[:search6]})
              .distinct
     query

   end

  def self.reporte_inventario_dia_siguiente(params)
    unless params[:search].nil? && params[:fechaDiaO].nil?
      query_string = "R.Ruta, Productos.Clave, Productos.Producto Descripcion, COALESCE(CONVERT(varchar, PC.Fecha, 105), CONVERT(varchar, PS.Fecha, 105)) Fecha, COALESCE(PC.Cantidad, 0) Cajas, COALESCE(PS.Cantidad, 0) Piezas, :diaO DiaO"
      query = select(sanitize_sql_array([query_string, diaO: params[:diaO]]))
              .joins(sanitize_sql_array(["LEFT JOIN PedDiaSig PC ON PC.IdRuta = :rutaId AND PC.Diao = :diaO AND PC.IdEmpresa = :empresa AND PC.Articulo = Productos.Clave", rutaId: params[:search], diaO: params[:diaO], empresa: params[:search6]]))
              .joins(sanitize_sql_array(["LEFT JOIN PedDiaSigPzs PS ON PS.IdRuta = :rutaId AND PS.Diao = :diaO AND PS.IdEmpresa = :empresa AND PS.Articulo = Productos.Clave", rutaId: params[:search], diaO: params[:diaO], empresa: params[:search6]]))
              .joins("LEFT JOIN Rutas R ON R.IdRutas = COALESCE(PC.IdRuta, PS.IdRuta)")
              .where("PC.Fecha IS NOT NULL OR PS.Fecha IS NOT NULL")
      unless params[:Clave_param].blank?
        query = query.where("Productos.Clave LIKE ?", "%#{params[:Clave_param]}%")
      end
      unless params[:Descripcion_param].blank?
        query = query.where("Productos.Producto LIKE ?", "%#{params[:Descripcion_param]}%")
      end
    else
      query = select("Id").where("Id = ?", 0)
    end
    query
  end

  def self.reporte_inventario_total(params)
    now = DateTime.now.strftime("%d-%m-%Y").to_s
    unless params[:search].nil? || params[:fechaDiaO].nil?
      if now == params[:fechaDiaO]
        query_string = "Productos.Id, Productos.Clave, R.Ruta AS Ruta, S.Stock, PP.PzaXCja, LOWER([catunidadmedida].UnidadMedida) AS UnidadMedida,
        CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN 0 ELSE (S.Stock / PP.PzaXCja) END AS Cajas,
          CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN S.Stock ELSE (S.Stock % PP.PzaXCja) END AS Piezas,
          Productos.Producto Descripcion, :diaO DiaO, :fecha Fecha"
        query = select(sanitize_sql_array([query_string, diaO: params[:diaO], fecha: params[:fechaDiaO]]))
                .joins("INNER JOIN Stock S ON S.Articulo = Productos.Clave")
                .joins("INNER JOIN ProductosXPzas PP ON PP.Producto = Productos.Clave")
                .joins("INNER JOIN Rutas R ON R.IdRutas = S.Ruta")
                .joins("INNER JOIN catunidadmedida ON Productos.UniMed = catunidadmedida.Clave")
                .where("S.IdEmpresa = ?", params[:search6])
                .where("S.Ruta = ?", params[:search])
                .where("S.Stock > 0")
                .distinct()
      else
        query_string = "R.Ruta AS Ruta, S.Stock, PP.PzaXCja, LOWER([catunidadmedida].UnidadMedida) AS UnidadMedida,
          CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN 0 ELSE (S.Stock / PP.PzaXCja) END AS Cajas,
          CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN S.Stock ELSE (S.Stock % PP.PzaXCja) END AS Piezas,
          Productos.Clave, Productos.Producto Descripcion, FORMAT(S.Fecha,'dd-MM-yyyy') Fecha, :diaO DiaO"
        query = select(sanitize_sql_array([query_string, diaO: params[:diaO]]))
                .joins("LEFT JOIN StockHistorico S ON S.Articulo = Productos.Clave")
                .joins("LEFT JOIN ProductosXPzas PP ON PP.Producto = Productos.Clave")
                .joins("LEFT JOIN Rutas R ON R.IdRutas = S.RutaID")
                .joins("INNER JOIN catunidadmedida ON Productos.UniMed = catunidadmedida.Clave")
                .where("S.IdEmpresa = ?", params[:search6])
                .where("S.RutaID = ?", params[:search])
                .where("S.DiaO = ?", params[:diaO])
                .where("S.Stock > 0")
      end
    else
      query = select("*").where("Id = 0")
    end
    unless params[:Clave_param].blank?
      query = query.where("Productos.Clave LIKE ?", "%#{params[:Clave_param]}%")
    end
    unless params[:Descripcion_param].blank?
      query = query.where("Productos.Producto LIKE ?", "%#{params[:Descripcion_param]}%")
    end
    query
  end

  def self.reporte_inventario_inicial(params)
    query_string = "R.Ruta AS Ruta, S.Stock, PP.PzaXCja,
     CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN 0 ELSE (S.Stock / PP.PzaXCja) END AS Cajas,
     CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN S.Stock ELSE (S.Stock % PP.PzaXCja) END AS Piezas,
     Productos.Clave, Productos.Producto Descripcion, LOWER([catunidadmedida].UnidadMedida) AS UnidadMedida, FORMAT(S.Fecha,'dd-MM-yyyy') Fecha, :diaO DiaO"
    query = select(sanitize_sql_array([query_string, diaO: params[:diaO]]))
            .joins("LEFT JOIN StockHistorico S ON S.Articulo = Productos.Clave")
            .joins("LEFT JOIN ProductosXPzas PP ON PP.Producto = Productos.Clave")
            .joins("LEFT JOIN Rutas R ON R.IdRutas = S.RutaID")
            .joins("INNER JOIN catunidadmedida ON Productos.UniMed = catunidadmedida.Clave")
            .where("S.IdEmpresa = ?", params[:search6])
            .where("S.RutaID = ?", params[:search])
            .where("S.DiaO = ?", params[:diaO])
            .where("S.Stock > 0")
    unless params[:Clave_param].blank?
      query = query.where("Productos.Clave LIKE ?", "%#{params[:Clave_param]}%")
    end
    unless params[:Descripcion_param].blank?
      query = query.where("Productos.Producto LIKE ?", "%#{params[:Descripcion_param]}%")
    end
    query.distinct
  end


  def self.reporte_bitacoratiempos(params)
    query = select("BitacoraTiempos.Id")
           .distinct
           .joins('INNER JOIN Rutas ON BitacoraTiempos.RutaId = Rutas.IdRutas LEFT OUTER JOIN  Clientes ON BitacoraTiempos.Codigo = Clientes.IdCli')
           .where('(RutaId= ?) AND (HI between ? and ?) AND (BitacoraTiempos.IdEmpresa= ?)',(params[:search]), (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"),(params[:search6]))
           .order('BitacoraTiempos.HI')
    query
  end

#Buscara todos los productos de la empresa, con su unidad de medida y grupo, pero en grupos solo aquellos grupos que sean de la sucursal
  def self.catalogo_productos(params)
    query_string = "Productos.*, isnull((select  catgrupos.descripcion from  relprogrupos inner  join catgrupos  on relprogrupos.idgrupo=catgrupos.clave where  catgrupos.TipoGrupo='P' and   relprogrupos.idempresa = :idempresa and  relprogrupos.productoid=Productos.Clave),'') as  Grupo, CatUnidadMedida.UnidadMedida"
    query = select(sanitize_sql_array([query_string, idempresa: params[:sucursal]]))
            .joins('LEFT OUTER JOIN CatUnidadMedida ON Productos.UniMed = CatUnidadMedida.Clave')
  end


  def self.reporte_inventario_sobrante(params)
    query = select("R.Ruta, FORMAT(O.Fecha,'dd-MM-yyyy') Fecha, O.DiaO, Productos.Clave, Productos.Producto Descripcion, ROUND(VPP.Stock / PP.PzaXCja, 0) Cajas, (VPP.Stock % PP.PzaXCja) Piezas")
            .joins("INNER JOIN V_ProductoPaseado VPP ON Productos.Clave = VPP.CodProd")
            .joins("INNER JOIN ProductosXPzas PP ON PP.Producto = VPP.CodProd")
            .joins("LEFT JOIN DiasO O ON O.RutaId = VPP.RutaId AND O.DiaO = VPP.DiaO AND O.IdEmpresa = VPP.IdEmpresa")
            .joins("LEFT JOIN Rutas R ON R.IdRutas = VPP.RutaId")
            .where("(VPP.RutaId = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (O.DiaO = :diao or :diao = '') AND (VPP.Stock > 0) AND (VPP.IdEmpresa = :idempresa)",{rutaId: params[:search],diao: params[:diaO], idempresa: params[:search6]})
    unless params[:Clave_param].blank?
      query = query.where("Productos.Clave LIKE ?", "%#{params[:Clave_param]}%")
    end
    unless params[:Descripcion_param].blank?
      query = query.where("Productos.Producto LIKE ?", "%#{params[:Descripcion_param]}%")
    end
    query
  end


end
