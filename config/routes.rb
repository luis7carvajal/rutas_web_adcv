Rails.application.routes.draw do

  resources :solicitudservicio
  get '/formulario_crear_servicio', to: 'solicitudservicio#formulario_crear_servicio', as: 'formulario_crear_servicio'

  resources :sabores do
    get 'sabor_check', :on => :collection
  end
  get '/sabores_inactivos', to: 'sabores#inactivos', as: 'sabores_inactivos'

  get 'verificar_credenciales_pos', to: 'pos#verificar_credenciales_pos', as: 'verificar_credenciales_pos'
  get 'pos/:Ruta/:PdaPW', to: 'pos#index', as: 'pos_index'
  get 'producto_para_POS', to: 'pos#producto_para_POS', as: 'producto_para_POS'
  get '/pos_vendedor_descuentoxproducto', to: 'pos#pos_vendedor_descuentoxproducto', as: 'pos_vendedor_descuentoxproducto'
  get '/procesar_venta', to: 'pos#procesar_venta', as: 'procesar_venta'
  delete '/pos_terminated/:id', to: 'pos#destroy', as: 'pos_terminated'

  get '/lista_rutas_disponibles', to: 'rutas#lista_rutas_disponibles', as: 'lista_rutas_disponibles'

  resources :password_resets

  resources :relprogrupos
  get '/grupo/:id/productos', to: 'relprogrupos#productos_grupo', as: 'productos_grupo'
  get 'comisiones', to: 'stored_procedures#comisiones', as: 'stored_procedures_comisiones'
  get 'vendedor_ruta_liquidacion', to: 'stored_procedures#vendedor_ruta_liquidacion', as: 'vendedor_ruta_liquidacion'
  get '/imprimir_comision', to: 'stored_procedures#pdf', as: 'comision_pdf'
  get 'pedidos_por_ruta', to: 'stored_procedures#cantidad_productosxpedido', as: 'cantidad_productosxpedido'
  get '/pedidos_ruta_impresion_etiquetas_pdf', to: 'stored_procedures#pedidos_ruta_impresion_etiquetas_pdf', as: 'pedidos_ruta_impresion_etiquetas_pdf'
#  get '/pedidos_ruta_imprimir_etiquetas_seleccionar_todos_masivo', to: 'stored_procedures#pedidos_ruta_imprimir_etiquetas_seleccionar_todos_masivo', as: 'pedidos_ruta_imprimir_etiquetas_seleccionar_todos_masivo'
  get 'consolidado_de_pedidos', to: 'stored_procedures#cantidad_productosxpedido_consolidado', as: 'cantidad_productosxpedido_consolidado'
  get '/productos_x_pedido_consolidado_pdf', to: 'stored_procedures#productos_x_pedido_consolidado_pdf', as: 'productos_x_pedido_consolidado_pdf'
  get 'monitoreo_de_precios', to: 'stored_procedures#monitoreo_de_precios', as: 'monitoreo_de_precios'
  get 'liquidaciones', to: 'stored_procedures#liquidaciones', as: 'liquidaciones'
  get 'reporte_consolidado_de_carga', to: 'stored_procedures#reporte_consolidado_de_carga', as: 'reporte_consolidado_de_carga'
  get 'reporte_consolidado_piezas', to: 'stored_procedures#reporte_consolidado_piezas', as: 'reporte_consolidado_piezas'
  get 'consentrado_pedido_del_dia_siguiente', to: 'stored_procedures#consentrado_pedido_del_dia_siguiente', as: 'consentrado_pedido_del_dia_siguiente'
  get 'reporte_consolidado_de_envases', to: 'stored_procedures#reporte_consolidado_de_envases', as: 'reporte_consolidado_de_envases'
  get '/reporte_liquidacion_concentrado', to: 'stored_procedures#reporte_liquidacion_concentrado', as: 'reporte_liquidacion_concentrado'
  post '/reporte_liquidacion_concentrado', to: 'stored_procedures#reporte_liquidacion_concentrado_detalles', as: 'reporte_liquidacion_concentrado_detalles'
  get '/reporte_liquidacion_concentrado_exportar', to: 'stored_procedures#reporte_liquidacion_concentrado_detalles', as: 'reporte_liquidacion_concentrado_exportar'

  resources :relvendrutas
  get '/ruta/:id/vendedores', to: 'relvendrutas#ruta_vendedores', as: 'ruta_vendedores'
  get '/borrar_vendedores_ruta', to: 'relvendrutas#borrar_vendedores_ruta', as: 'borrar_vendedores_ruta'

  get 'bitacoratiempos', to: 'bitacoratiempos#index', as: 'bitacoratiempos'

  resources :listacombo do
    get 'listcombo_check', :on => :collection
  end
  get '/combos_inactivos', to: 'listacombo#inactivos', as: 'listacombo_inactivos'
  get '/datos_listcombo', to: 'cuotasventa#datos_listcombo', as: 'datos_listcombo'
  resources :cuotasventa
  get '/datos_cuotaventa', to: 'cuotasventa#datos_cuotaventa', as: 'datos_cuotaventa'

  get 'inventario/inventario_inicial'
  get 'inventario/inventario_dia_siguiente'
  get 'inventario/inventario_sobrante'
  get 'inventario/inventario_total'

  resources :pedidos

  post '/pedidos/facturacion', to: 'pedidos#facturacion', as: 'facturacion_electronica'
  post '/pedidos/cancelar', to: 'pedidos#cancelar'

  get '/detalles_visor_de_pedidos', to: 'pedidos#detalles_visor_de_pedidos', as: 'detalles_pedidos'
  get '/visor_de_pedidos', to: 'pedidos#visor_de_pedidos', as: 'visor_de_pedidos'
  get '/imprimir_pedido_surtido', to: 'pedidos#pdf', as: 'pedidos_pdf'
  get '/consolidar', to: 'pedidos#consolidacion', as: 'consolidacion'
  get '/eliminar_pedido_consolidado', to: 'pedidos#eliminar_pedidos_consolidados', as: 'eliminar_pedido_consolidado'
  get '/surtir_de_forma_masiva', to: 'pedidos#surtir_de_forma_masiva', as: 'surtir_de_forma_masiva'
  get '/seleccionar_todos_surtimiento_masivo', to: 'pedidos#seleccionar_todos_surtimiento_masivo', as: 'seleccionar_todos_surtimiento_masivo'

  resources :backorder
  resources :pedidosliberados
  get '/busqueda_stored', to: 'pedidosliberados#busqueda_stored', as: 'busqueda_stored'

  resources :ctiket
  patch '/listap/:id/desincronizado', to: 'relclilis#desincronizado'
  put '/listap/:id/desincronizado', to: 'relclilis#desincronizado'

  resources :th_stockalmacen do
    collection { post :import}#importar datos excel
  end

  resources :cteimagen
  resources :incidencias do
    get 'incidencia_check', :on => :collection
  end
  get '/incidencias_inactivas', to: 'incidencias#inactivos', as: 'incidencias_inactivos'

  get 'mapa/index'

  resources :catmarcas do
    get 'catmarcas_check', :on => :collection
  end
  get '/marcas_inactivas', to: 'catmarcas#inactivos', as: 'catmarcas_inactivos'

  resources :catunidadmedida do
    get 'catunidadmed_check', :on => :collection
  end
  get '/medidas_inactivas', to: 'catunidadmedida#inactivos', as: 'catunidadmedida_inactivos'

  resources :catgrupos do
    get 'catgrupo_check', :on => :collection
  end
  get '/grupos_inactivos', to: 'catgrupos#inactivos', as: 'catgrupos_inactivos'

  resources :catbancos do
    get 'banco_check', :on => :collection
  end

  get '/bancos_inactivos', to: 'catbancos#inactivos', as: 'catbancos_inactivos'

  resources :cp


  get 'ventas/dashboard_ejecutivo_ventas', to: 'ventas#index', as: 'ventas1'
  get '/busqueda_clientes_captados', to: 'ventas#busqueda_clientes_captados', as: 'busqueda_clientes_captados'
  get '/busqueda_general_graficos', to: 'ventas#busqueda_general_graficos', as: 'busqueda_general_graficos'

  get 'ventas/dashboard_preventa', to: 'ventas#index2', as: 'ventas2'
  get '/busqueda_general_graficos_index2', to: 'ventas#busqueda_general_graficos_index2', as: 'busqueda_general_graficos_index2'
  get 'ventas/index3', as: 'ventas3'
  get 'ventas/index4', as: 'ventas4'


  resources :detallecob
  #get '/CuentasxCobrar/:id', to: 'detallecob#cuentasxcobrar', as: 'cuentasxcobrar'
  #post '/CuentasxCobrar/datos', to: 'detallecob#obtener_ruta', as: 'cuentasxcobrar_obtener_ruta' #as es el nombre con el path que se le colocara

  resources :stock do
    collection { post :import}#importar datos excel
  end

  resources :relactivos
  get '/activos_cliente/:id', to: 'relactivos#activos_cliente', as: 'activos_cliente'

  resources :activos do
     get 'activos_check_cb', :on => :collection
  end

  get '/activos_inactivos', to: 'activos#inactivos', as: 'activos_inactivos'


  resources :reldaycli
  get '/generar_ruta/:id/:id2', to: 'reldaycli#generar_ruta', as: 'generar_ruta'
  get '/busqueda_generacion_ruta', to: 'reldaycli#busqueda_generacion_ruta'

  get '/busqueda_vendedores_por_ruta', to: 'reldaycli#busqueda_vendedores_por_ruta'

  post '/reldaycli/datos', to: 'reldaycli#obtener_ruta_y_dia', as: 'ryd' #as es el nombre con el path que se le colocara
  get '/borrar_clientes_ruta_al_dia', to: 'reldaycli#borrar_clientes_ruta_al_dia', as: 'borrar_clientes_ruta_al_dia'

  get '/generar_ruta_agregar_de_forma_masiva', to: 'reldaycli#generar_ruta_agregar_de_forma_masiva', as: 'generar_ruta_agregar_de_forma_masiva'
  get '/generar_ruta_agregar_seleccionar_todos_masivo', to: 'reldaycli#generar_ruta_agregar_seleccionar_todos_masivo', as: 'generar_ruta_agregar_seleccionar_todos_masivo'

  get '/generar_ruta_eliminar_de_forma_masiva', to: 'reldaycli#generar_ruta_eliminar_de_forma_masiva', as: 'generar_ruta_eliminar_de_forma_masiva'
  get '/generar_ruta_eliminar_seleccionar_todos_masivo', to: 'reldaycli#generar_ruta_eliminar_seleccionar_todos_masivo', as: 'generar_ruta_eliminar_seleccionar_todos_masivo'

  get '/asig_clientes_entre_vendedores', to: 'reldaycli#asig_clientes_entre_vendedores', as: 'asig_clientes_entre_vendedores'
  get '/proc_transf_cli_vend', to: 'reldaycli#proc_transf_cli_vend', as: 'proc_transf_cli_vend'

  resources :secrutas
  resources :relproclas
  resources :clasproductos
  resources :relcliclas
  #promociones
  resources :detallelpromaster
  get '/detallepromocion/:id', to: 'detallelpromaster#index', as: 'detallelpromasterid'
  resources :listapromomaster
  resources :clasclientes
  resources :listapromo do
    resources :detallepromo
    get 'promociones_check', :on => :collection
  end
  get '/promociones_inactivas', to: 'listapromo#inactivos', as: 'listapromo_inactivos'


  resources :room_categories
  resources :hotels
  resources :relruclas
  resources :clasrutas
  resources :continuidad
  resources :configrutasp

  resources :rutas do
    collection { post :import}#importar datos excel
    get 'rutas_check', :on => :collection
  end
  get '/rutas_inactivas', to: 'rutas#inactivos', as: 'rutas_inactivos'
  get '/datos_ruta', to: 'rutas#datos_ruta', as: 'datos_ruta'

  resources :relclirutas
  get '/ruta/:id/clientes', to: 'relclirutas#ruta_clientes', as: 'ruta_clientes'
  get '/borrar_clientes_ruta', to: 'relclirutas#borrar_clientes_ruta', as: 'borrar_clientes_ruta'

  get '/relcliruta_agregar_de_forma_masiva', to: 'relclirutas#relcliruta_agregar_de_forma_masiva', as: 'relcliruta_agregar_de_forma_masiva'
  get '/relcliruta_agregar_seleccionar_todos_masivo', to: 'relclirutas#relcliruta_agregar_seleccionar_todos_masivo', as: 'relcliruta_agregar_seleccionar_todos_masivo'
  get '/relcliruta_eliminar_de_forma_masiva', to: 'relclirutas#relcliruta_eliminar_de_forma_masiva', as: 'relcliruta_eliminar_de_forma_masiva'
  get '/relcliruta_eliminar_seleccionar_todos_masivo', to: 'relclirutas#relcliruta_eliminar_seleccionar_todos_masivo', as: 'relcliruta_eliminar_seleccionar_todos_masivo'


  resources :perfiles
  resources :mvodev do
    get 'mvodev_check', :on => :collection

  end
  get '/mvodevoluciones_inactivos', to: 'mvodev#inactivos', as: 'mvodev_inactivos'

  resources :encuestas
  get '/encuestas_inactivas', to: 'encuestas#inactivos', as: 'encuestas_inactivos'



  resources :relmens

  resources :mensajes do
    get 'mensajes_check', :on => :collection
  end
  get 'mensajes/_inactivos', to: 'mensajes#inactivos', as: 'mensajes_inactivos'

  resources :cuotas
  get '/cuotas_inactivos', to: 'cuotas#inactivos', as: 'cuotas_inactivos'

  resources :td_comision
  resources :detallelp
  resources :detallelp
  resources :listap do
    collection { post :import}#importar datos excel
  end
  resources :relclilis
  resources :detalleld
  resources :th_comision do
    collection { post :import}#importar datos excel
  end
  get '/th_comision_inactivos', to: 'th_comision#inactivos', as: 'th_comision_inactivos'
  resources :listad do
    collection { post :import}#importar datos excel
  end

  resources :mvomerma do
    get 'mvomerma_check', :on => :collection
  end
  get '/mvomerma_inactivos', to: 'mvomerma#inactivos', as: 'mvomerma_inactivos'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :motivosnoventa do
    get 'motivosnoventa_check', :on => :collection

  end
  get '/motivosnoventa_inactivos', to: 'motivosnoventa#inactivos', as: 'motivosnoventa_inactivos'
  get '/busqueda_noventas', to: 'reportesventas#busqueda_noventas', as: 'busqueda_noventas'

  resources :formaspag do
    get 'formaspag_check', :on => :collection
  end
  get '/formaspago_inactivos', to: 'formaspag#inactivos', as: 'formaspag_inactivos'

  resources :productosxpzas

  resources :productoenvase
  get '/Productos/:id/envase', to: 'productoenvase#index', as: 'producto_envase'

  resources :detalleld

#Lista de descuento
  get '/listald/:id/productos', to: 'detalleld#index', as: 'producto_detalle'
  get '/listald/:id/clientes', to: 'relclilis#index', as: 'cliente_detalle'

#Lista de precios
  get '/listap/:id/productos', to: 'detallelp#index', as: 'productos_listap'

  get '/lp_productos_agregar_de_forma_masiva', to: 'detallelp#lp_productos_agregar_de_forma_masiva', as: 'lp_productos_agregar_de_forma_masiva'
  get '/lp_productos_agregar_seleccionar_todos_masivo', to: 'detallelp#lp_productos_agregar_seleccionar_todos_masivo', as: 'lp_productos_agregar_seleccionar_todos_masivo'
  get '/lp_productos_eliminar_de_forma_masiva', to: 'detallelp#lp_productos_eliminar_de_forma_masiva', as: 'lp_productos_eliminar_de_forma_masiva'
  get '/lp_productos_eliminar_seleccionar_todos_masivo', to: 'detallelp#lp_productos_eliminar_seleccionar_todos_masivo', as: 'lp_productos_eliminar_seleccionar_todos_masivo'


  get '/listap/:id/clientes', to: 'relclilis#indexlistap', as: 'cliente_listap'

  get '/rcl_clientes_agregar_de_forma_masiva', to: 'relclilis#rcl_clientes_agregar_de_forma_masiva', as: 'rcl_clientes_agregar_de_forma_masiva'
  get '/rcl_clientes_agregar_seleccionar_todos_masivo', to: 'relclilis#rcl_clientes_agregar_seleccionar_todos_masivo', as: 'rcl_clientes_agregar_seleccionar_todos_masivo'
  get '/rcl_clientes_eliminar_de_forma_masiva', to: 'relclilis#rcl_clientes_eliminar_de_forma_masiva', as: 'rcl_clientes_eliminar_de_forma_masiva'
  get '/rcl_clientes_eliminar_seleccionar_todos_masivo', to: 'relclilis#rcl_clientes_eliminar_seleccionar_todos_masivo', as: 'rcl_clientes_eliminar_seleccionar_todos_masivo'

#Agregando Clientes al grupo de promociones
  get '/Promociones/:id/clientes', to: 'relclilis#clientes_promociones', as: 'clientes_promociones'

#detalle de la Comision
  get '/comision/:id/detalle', to: 'td_comision#index', as: 'comision_detalle'

  post "/usuario_nuevo", to: 'usuarios#create', as:'usuario_nuevo'

#Mensajes
  get '/Mensajes/:id/productos', to: 'relmens#index', as: 'mensaje_productos'
  get '/Mensajes/:id/clientes', to: 'relmens#clientes', as: 'mensaje_clientes'
  get '/Mensajes/:id/rutas', to: 'relmens#rutas', as: 'mensaje_rutas'



  devise_for :usuarios, controllers: {
    registrations: "usuarios/registrations",
    sessions: "usuarios/sessions",
    passwords: "usuarios/passwords"}
    resources :usuarios do
      get 'usuarios_check', :on => :collection
      get 'usuarios_check2', :on => :collection

    end
    devise_scope :usuario do
      get 'usuarios/sessions/busqueda_sucursales', as: 'busqueda_sucursales'#Actualiza el selectbox de la sucursal dependindo de la empresa en el login
      #get 'acceso/:id', to: 'usuarios/sessions#new' #sesion por empresa
      #get 'error_empresa', to: 'usuarios/sessions#error_empresa', as: 'error_empresa'
    end

    get '/usuarios_inactivos', to: 'usuarios#inactivos', as: 'usuarios_inactivos'
    get '/usuario_sucursales', to: 'usuarios#usuario_sucursales', as: 'usuario_sucursales'
    get '/busqueda_usuario_perfil', to: 'usuarios#busqueda_usuario_perfil', as: 'busqueda_usuario_perfil'

  resources :productos do
    collection { post :import}#importar datos excel
    get 'productos_check', :on => :collection
    get 'productos_codbarras_check', :on => :collection
  end
  get '/datos_producto', to: 'productos#datos_producto', as: 'datos_producto'
  post 'create_producto_generico', to: 'productos#create_producto_generico', as: 'create_producto_generico'

  get '/productos_inactivos', to: 'productos#inactivos', as: 'productos_inactivos'
  get '/busqueda_cp', to: 'clientes#busqueda_cp', as: 'busqueda_cp'

  resources :clientes do
    collection { post :import}#importar datos excel
    get 'clientes_check', :on => :collection
  end
  get '/impresion_clientes_pdf', to: 'clientes#impresion_clientes_pdf', as: 'impresion_clientes_pdf'

  get '/clientes_inactivos', to: 'clientes#inactivos', as: 'clientes_inactivos'
  get '/datos_cliente', to: 'clientes#datos_cliente', as: 'datos_cliente'

  resources :ayudantes do
    collection { post :import}#importar datos excel
  end

  resources :empresas do
    collection { post :import}#importar datos excel
  end
  get '/empresa/:id/sucursales', to: 'empresas#sucursales', as: 'sucursales'
  #Comprobar existencia de empresa
  get '/empresa/empresas_check', to: 'empresas#empresas_check', as: 'empresas_check_empresas'

  get '/empresas_inactivas', to: 'empresas#inactivos', as: 'empresasempresas_inactivos'

  resources :empresasmadre do
    collection { post :import}#importar datos excel
    get 'empresasmadre_check', :on => :collection
    get 'url_check', :on => :collection

  end
  get '/empresasmadre_inactivas', to: 'empresasmadre#inactivos', as: 'empresasmadre_inactivos'


  resources :vehiculos do
    collection { post :import}#importar datos excel
    get 'vehiculos_check', :on => :collection
  end
  get '/vehiculos_inactivos', to: 'vehiculos#inactivos', as: 'vehiculos_inactivos'

  resources :vendedores do
    collection { post :import}#importar datos excel
    get 'vendedores_check', :on => :collection
    get 'vendedores_pdapw_check', :on => :collection
  end

  get '/vendedores_inactivos', to: 'vendedores#inactivos', as: 'vendedores_inactivos'

  get 'welcome/index'

  #REPORTES
  get 'pregalado/producto_obsequio'
  get '/busqueda_producto_obsequio', to: 'pregalado#busqueda_producto_obsequio'

  get 'pregalado/index'
  get '/busqueda_producto_promocion', to: 'pregalado#busqueda', as: 'busqueda'

  get 'devoluciones/index'
  get '/busqueda_devoluciones', to: 'devoluciones#busqueda_devoluciones', as: 'busqueda_devoluciones'

  get 'venta/venta'
  get '/detalle_venta', to: 'venta#detalle_venta', as: 'detalle_venta'
  get '/detalle_entrega', to: 'venta#detalle_entrega', as: 'detalle_entrega'
  get '/imprimir_venta', to: 'venta#pdf', as: 'ventas_pdf'
  get '/imprimir_entrega', to: 'venta#pdf_entrega', as: 'entregas_pdf'
  get 'reportesventas/Visita_sin_venta'
  get '/busqueda_gps_venta', to: 'mapa#busqueda_gps_venta'
  get '/entrega_export', to: 'venta#entrega_export', as: 'entrega_export'


  resources :cobranza
  post '/ctasxcobrar/datos', to: 'cobranza#obtener_ruta', as: 'cobranza_obtener_ruta' #as es el nombre con el path que se le colocara
  get '/abonos_cxc', to: 'cobranza#abonos_cxc', as: 'abonos_cxc'

  get '/bitacora_cobranza/:id', to: 'cobranza#cobranza_bitacora', as: 'cobranza_bitacora'
  get '/bitacora_cobranza', to: 'cobranza#index_cobranza_bitacora', as: 'index_cobranza_bitacora'

  get '/abonos_bit_cobranza', to: 'cobranza#abonos_bit_cobranza', as: 'abonos_bit_cobranza'

  get '/busqueda_ctasxcobrar', to: 'cobranza#busqueda_ctasxcobrar'
  get 'bit_cobranza/busqueda_cobranza', to: 'cobranza#busqueda_cobranza'

  get 'inventario/producto_negado'
  get '/busqueda_producto_negado', to: 'inventario#busqueda_producto_negado'

  get 'reportes_ruta/recargas'
  get '/busqueda_recarga', to: 'reportes_ruta#busqueda_recarga'

  get '/tripulacion', to: 'reportes_ruta#index_tripulacion', as: 'tripulacion'

  get 'reportes_ruta/envases'
  get 'reportes_ruta/update_dia', as: 'update_dia'#Actualiza el selectbox de dia operativo conconrde a la ruta

  get 'reportes_ruta/consumos', to: 'reportes_ruta#consumos', as: 'consumos'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'pedidos#visor_de_pedidos'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
