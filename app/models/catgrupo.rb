class Catgrupo < ActiveRecord::Base
  self.primary_key = "Clave"
  self.table_name = 'CatGrupos' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :producto_sector, -> { where(TipoGrupo: "P") }
  scope :ruta_grupo, -> { where(TipoGrupo: "R") }
  scope :ruta_tipo, -> { where(TipoGrupo: "T") }
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }

  scope :productos_por_Sucursal, -> idEmpresa { where("(TipoGrupo = 'P' AND idEmpresa = ?) OR (TipoGrupo != 'P')", idEmpresa) } #Buscara todos los registros de los grupos, pero aquellos que son de tipo producto seran solo por sucursales


#  has_many :productos, class_name: "Producto", foreign_key: "Sector"
  has_many :productos, through: :relprogrupos, source: :producto, foreign_key: "IdGrupo"
  has_many :rutasector, class_name: "Ruta", foreign_key: "Sector"
  has_many :rutastiporuta, class_name: "Ruta", foreign_key: "TipoRuta"
  has_one :detalleprom, class_name: "Detalleprom", foreign_key: "Grupo"
  has_many :relprogrupos, class_name: "Relprogrupo", foreign_key: "IdGrupo"
end
