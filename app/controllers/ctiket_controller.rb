class CtiketController < ApplicationController
  before_action :set_tiket, only: [:show, :edit, :update, :destroy]
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de ticket"
  end

  # GET /ctiket
  # GET /ctiket.json
  def index
    @ctiket = Tiket.por_empresa(current_usuario.empresa_id)
    @existencia = Tiket.find_by_IdEmpresa(current_usuario.empresa_id)
    @tiket = Tiket.new
  end

  # GET /ctiket/1
  # GET /ctiket/1.json
  def show
  end

  # GET /ctiket/new
  def new
    @tiket = Tiket.new
  end

  # GET /ctiket/1/edit
  def edit
  end

  # POST /ctiket
  # POST /ctiket.json
  def create
    if @puede_crear != true
      return
    end
    @tiket = Tiket.new(tiket_params)
    respond_to do |format|
      if @tiket.save
        format.html { redirect_to @tiket, notice: 'Tiket was successfully created.' }
        format.json { render :show, status: :created, location: @tiket }
        format.js {flash.now[:notice] = 'El ticket se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @tiket.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el ticket.'} #ajax
      end
    end
  end

  # PATCH/PUT /ctiket/1
  # PATCH/PUT /ctiket/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @tiket.update(tiket_params)
        format.html { redirect_to @tiket, notice: 'Tiket was successfully updated.' }
        format.json { render :show, status: :ok, location: @tiket }
        format.js {flash.now[:notice] = 'El ticket se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @tiket.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el ticket.'} #ajax
      end
    end
  end

  # DELETE /ctiket/1
  # DELETE /ctiket/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    @tiket.destroy
    respond_to do |format|
      format.html { redirect_to ctiket_url, notice: 'Tiket was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El ticket se ha borrado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tiket
      @tiket = Tiket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tiket_params
      params.require(:tiket).permit(:Linea1, :Linea2, :Linea3, :Linea4, :Mensaje, :Tdv, :LOGO, :MLiq, :IdEmpresa)
    end
end
