class Solicitudserv < ActiveRecord::Base

    def self.solicitud_servicio(params)
      query = select("solicitudservicio.id,solicitudservicio.FolServicio, solicitudservicio.CodCliente, C.Nombre, solicitudservicio.DescripcionServicio, solicitudservicio.Prioridad, solicitudservicio.FechaServicio, dlp.PrecioMin, dlp.PrecioMax")
             .joins('inner join Clientes C on solicitudservicio.CodCliente=C.IdCli left outer join Detallelp dlp on solicitudservicio.FolServicio=dlp.Articulo')
             .where("((solicitudservicio.Cancelado = 0)  AND (solicitudservicio.IdEmpresa = :idempresa))",{idempresa: params[:search6]})
             .group('solicitudservicio.id', :FolServicio, 'solicitudservicio.CodCliente', 'C.Nombre', :DescripcionServicio, :Prioridad, :FechaServicio, 'dlp.PrecioMin', 'dlp.PrecioMax')
             .distinct
      query = query.where("(solicitudservicio.FolServicio LIKE :FolServicio or :FolServicio = '') And (solicitudservicio.CodCliente LIKE :CodCliente or :CodCliente = '') And (C.Nombre LIKE :Nombre or :Nombre = '') And
       (solicitudservicio.DescripcionServicio LIKE :DescripcionServicio or :DescripcionServicio = '') And (solicitudservicio.Prioridad LIKE :Prioridad or :Prioridad = '') And (dlp.PrecioMin LIKE :PrecioMin or :PrecioMin = '') And (dlp.PrecioMax LIKE :PrecioMax or :PrecioMax = '')",
       {FolServicio: "%#{params[:FolServicio_param]}%", CodCliente: "%#{params[:CodCliente_param]}%", Nombre: "%#{params[:Nombre_param]}%", DescripcionServicio: "%#{params[:DescripcionServicio_param]}%", Prioridad: "%#{params[:Prioridad_param]}%",
        PrecioMin: "%#{params[:PrecioMin_param]}%", PrecioMax: "%#{params[:PrecioMax_param]}%"})
      query = query.where('solicitudservicio.FechaServicio between ? and ?', (params[:FechaServicio_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaServicio_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaServicio_param].present?

      query
    end


    def self.solicitud_servicio2(params)
      query = select("solicitudservicio.id")
             .joins('inner join Clientes C on solicitudservicio.CodCliente=C.IdCli inner join Detallelp dlp on solicitudservicio.FolServicio=dlp.Articulo')
             .where("((solicitudservicio.Cancelado = 0)  AND (solicitudservicio.IdEmpresa = :idempresa))",{idempresa: params[:search6]})
             .group('solicitudservicio.id', :FolServicio, 'C.IdCli', 'C.Nombre', :DescripcionServicio, :Prioridad, :FechaServicio, 'dlp.PrecioMin', 'dlp.PrecioMax')
             .distinct
      query = query.where("(solicitudservicio.FolServicio LIKE :FolServicio or :FolServicio = '') And (C.IdCli LIKE :CodCliente or :CodCliente = '') And (C.Nombre LIKE :Nombre or :Nombre = '') And
       (solicitudservicio.DescripcionServicio LIKE :DescripcionServicio or :DescripcionServicio = '') And (solicitudservicio.Prioridad LIKE :Prioridad or :Prioridad = '') And (dlp.PrecioMin LIKE :PrecioMin or :PrecioMin = '') And (dlp.PrecioMax LIKE :PrecioMax or :PrecioMax = '')",
       {FolServicio: "%#{params[:FolServicio_param]}%", CodCliente: "%#{params[:CodCliente_param]}%", Nombre: "%#{params[:Nombre_param]}%", DescripcionServicio: "%#{params[:DescripcionServicio_param]}%", Prioridad: "%#{params[:Prioridad_param]}%",
        PrecioMin: "%#{params[:PrecioMin_param]}%", PrecioMax: "%#{params[:PrecioMax_param]}%"})
      query = query.where('solicitudservicio.FechaServicio between ? and ?', (params[:FechaServicio_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaServicio_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaServicio_param].present?

      query
    end



end
