class BitacoratiemposController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Reporte de bitácora tiempos"
  end

  def index
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'bitacoratiempos'
    @accion = 'index'

    @pathDeBusqueda = bitacoratiempos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    elementos_de_busqueda

    #Bitacora tiempos
    @search = Bitacoratiempo.reporte_bitacoratiempos(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @bitacoratiempos = @search.result().page(params[:bitacoratiempos]).per(15)

    @bitacoratiempos_Export = Bitacoratiempo.reporte_bitacoratiempos(params)

    #Clientes no visitados
    @search_clientesNoVisitados = Th_secvisita.clientes_no_visitados_R_Bitacora(params).search(search_params)
    @search_clientesNoVisitados.sorts = 'Id' if @search_clientesNoVisitados.sorts.empty?
    @clientes_no_visitados = @search_clientesNoVisitados.result().page(params[:clientes_no_visitados]).per(15)

    #Resumen
    #visitas realizadas
    @visitas_realizadas = Bitacoratiempo.reporte_bitacoratiempos_visitas_counter(params).visitas.count
    #programadas
    @visitas_programadas = Th_secvisita.reporte_bitacoratiempos_visitas_counter(params).count
    #programadas realizadas
    @programadas_realizadas = Bitacoratiempo.rep_bitiempos_programadas_realizadas(params).count
    #realizadas no repetidas
    @realizadas_no_repetidas = Bitacoratiempo.rep_bitiempos_realizadas_NO_repetidas(params).visitas.count
    #no programadas
    @realizadas_no_programadas = Bitacoratiempo.rep_bitiempos_NO_programadas(params).count
    #clientes no visitados
    @clientes_no_visitados_count = Th_secvisita.clientes_no_visitados_R_Bitacora_count(params).count
    #sin venta
    @sin_venta = Noventasingular.rep_bitiempos_SinVenta(params).count
    #prog_con_venta
    @prog_con_venta = Reloperacion.rep_bitiempos_prog_con_venta(params).count
    #no_prog_con_venta
    @no_prog_con_venta = Reloperacion.rep_bitiempos_no_prog_con_venta(params).count
    #clientes_con_venta
    @clientes_con_venta = @prog_con_venta + @no_prog_con_venta

  #***************************Apartado de entregas/pedidos*****************************

  #Totales
  @entregas_pedidos_total = Pedidoliberado.rep_bitiempos_busqueda_general_para_totales(params).tipo_todas.sum("TOTAL") + Pedido.rep_bitiempos_busqueda_general_para_totales(params).sin_obsequios.sum("Total")
  @entregas_pedidos_credito = Pedidoliberado.rep_bitiempos_busqueda_general_para_totales(params).tipo_credito.sum("TOTAL") + Pedido.rep_bitiempos_busqueda_general_para_totales(params).credito.sum("Total")
  @entregas_pedidos_contado = Pedidoliberado.rep_bitiempos_busqueda_general_para_totales(params).tipo_contado.sum("TOTAL") + Pedido.rep_bitiempos_busqueda_general_para_totales(params).contado.sum("Total")

  #Efectividad
  if @visitas_programadas != 0
    if @entregas_pedidos_total.to_i != 0
      @efectividad_entrega = ((@programadas_realizadas.to_f  / @visitas_programadas) * 100).round(2)
    else
      @efectividad_entrega = @entregas_pedidos_total
    end
  end

  #Devoluciones
  @devoluciones_entregas_pedidos = Detalledevo.rep_bitiempos_devoluciones(params).count

  #Totales Venta
  @TE_Cajas_entregas_pedidos = Detallepedidolib.rep_bitiempos_TotalesEntregas(params).cajas.tipo_todas.sum('PZA').to_i + Detallepedido.rep_bitiempos_TotalesEntregas_Pedidos(params).cajas.tipo_todas.sum('Pza').to_i
  @TE_Piezas_entregas_pedidos = Detallepedidolib.rep_bitiempos_TotalesEntregas(params).piezas.tipo_todas.sum('PZA').to_i + Detallepedido.rep_bitiempos_TotalesEntregas_Pedidos(params).piezas.tipo_todas.sum('PZA').to_i
  @TO_Cajas_entregas_pedidos = Detallepedidolib.rep_bitiempos_TotalesEntregas(params).cajas.tipo_obsequio.sum('PZA').to_i + Detallepedido.rep_bitiempos_TotalesEntregas_Pedidos(params).cajas.tipo_obsequio.sum('PZA').to_i
  @TO_Piezas_entregas_pedidos = Detallepedidolib.rep_bitiempos_TotalesEntregas(params).piezas.tipo_obsequio.sum('PZA').to_i + Detallepedido.rep_bitiempos_TotalesEntregas_Pedidos(params).piezas.tipo_obsequio.sum('PZA').to_i

  @TP_Cajas_entregas_pedidos = Pregalad.rep_bitacora_entregas(params).cajas.sum('Cant').to_i + Pregalad.rep_bitacora_pedidos(params).cajas.sum('Cant').to_i
  @TP_Piezas_entregas_pedidos = Pregalad.rep_bitacora_entregas(params).piezas.sum('Cant').to_i + Pregalad.rep_bitacora_pedidos(params).piezas.sum('Cant').to_i

  #***************************FIN Apartado de entregas*****************************



  #***************************Apartado de ventas*****************************

  #Totales
  @ventas_total = Vent.rep_bitiempos_busqueda_general_para_totales(params).tipo_todas.sum("TOTAL")
  @ventas_credito = Vent.rep_bitiempos_busqueda_general_para_totales(params).tipo_credito.sum("TOTAL")
  @ventas_contado = Vent.rep_bitiempos_busqueda_general_para_totales(params).tipo_contado.sum("TOTAL")

  #Efectividad
  if @visitas_programadas != 0
    if @ventas_total.to_i != 0
      @efectividad = ((@programadas_realizadas.to_f  / @visitas_programadas) * 100).round(2)
    else
      @efectividad = @ventas_total
    end
  end

  #Devoluciones
  @devoluciones = Detalledevo.rep_bitiempos_devoluciones(params).count

  #Totales Venta
  @TV_Cajas = Detalleve.rep_bitiempos_TotalesVenta(params).cajas.tipo_todas.sum('Pza')
  @TV_Piezas = Detalleve.rep_bitiempos_TotalesVenta(params).piezas.tipo_todas.sum('Pza')
  @TO_Cajas = Detalleve.rep_bitiempos_TotalesVenta(params).cajas.tipo_obsequio.sum('Pza')
  @TO_Piezas = Detalleve.rep_bitiempos_TotalesVenta(params).piezas.tipo_obsequio.sum('Pza')

  @TP_Cajas = Pregalad.rep_bitacora(params).cajas.sum('Cant')
  @TP_Piezas = Pregalad.rep_bitacora(params).piezas.sum('Cant')

  #***************************FIN Apartado de ventas*****************************

  #Apartado de Estadística

    if @clientes_con_venta != 0
      @ConsumoPromedioPorClientes = ((@entregas_pedidos_total+@ventas_total)/@clientes_con_venta)
    end

    if @programadas_realizadas != 0
      @EfectividadDeLaVenta = (@prog_con_venta.to_f/@programadas_realizadas.to_f) * 100
    end

    if @visitas_programadas != 0
      @EfectividadEnVisitas = (@programadas_realizadas.to_f/@visitas_programadas.to_f) * 100
    end

    respond_to do |format|
      format.html
      format.js
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def elementos_de_busqueda
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    @searchVendedorB = Vendedor.por_empresa(current_usuario.empresa_id).activos.search(search_params)
    # make name the default sort column
    @searchVendedorB.sorts = 'IdVendedor' if @searchVendedorB.sorts.empty?
    @vendedores_Selector = @searchVendedorB.result().page(params[:vendedors])

    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'DiaO DESC' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
  end


end
