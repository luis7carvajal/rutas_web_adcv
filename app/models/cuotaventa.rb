class Cuotaventa < ActiveRecord::Base
  self.primary_key = "year"
  self.table_name = 'Cuotasventa' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)

  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
end
