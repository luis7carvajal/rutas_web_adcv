class Reloperacion < ActiveRecord::Base
  self.primary_key = "Id"

  def self.rep_bitiempos_prog_con_venta(params)
  	query = select("CodCli")
  				 .distinct
  				 .joins('inner join bitacoratiempos ON reloperaciones.DiaO=bitacoratiempos.DiaO and reloperaciones.RutaId=bitacoratiempos.RutaId and reloperaciones.CodCli=bitacoratiempos.Codigo and reloperaciones.IdEmpresa=bitacoratiempos.IdEmpresa')
  				 .where("Tipo in ('Venta','Pedido','Entrega') And (reloperaciones.RutaId= :rutaId) And (bitacoratiempos.DiaO= :diao) And (Fecha BETWEEN :fechainicial AND :fechafinal)
  				 AND (reloperaciones.IdEmpresa= :idempresa) AND (bitacoratiempos.IdVendedor = :VendedorId or :VendedorId = '')  and  codcli IN  (SELECT CodCli FROM TH_SecVisitas AS TH_SecVisitas WHERE RutaId= :rutaId  AND (Fecha BETWEEN :fechainicial AND :fechafinal))",{rutaId: params[:search], diao: params[:diaO], VendedorId: params[:vendedor_id], fechainicial: (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
  				 .where("(bitacoratiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
  	query
  end

  def self.rep_bitiempos_no_prog_con_venta(params)
  	query = select("CodCli")
  				 .distinct
  				 .joins('inner join bitacoratiempos ON reloperaciones.DiaO=bitacoratiempos.DiaO and reloperaciones.RutaId=bitacoratiempos.RutaId and reloperaciones.CodCli=bitacoratiempos.Codigo and reloperaciones.IdEmpresa=bitacoratiempos.IdEmpresa')
  				 .where("Tipo in ('Venta','Pedido','Entrega') And (reloperaciones.RutaId= :rutaId) And (bitacoratiempos.DiaO= :diao) And (Fecha BETWEEN :fechainicial AND :fechafinal)
  				 AND (reloperaciones.IdEmpresa= :idempresa) AND (bitacoratiempos.IdVendedor = :VendedorId or :VendedorId = '')  and  codcli NOT IN  (SELECT CodCli FROM TH_SecVisitas AS TH_SecVisitas WHERE RutaId = :rutaId AND (Fecha BETWEEN :fechainicial AND :fechafinal))",{rutaId: params[:search], diao: params[:diaO], VendedorId: params[:vendedor_id], fechainicial: (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
  				 .where("(bitacoratiempos.IdVendedor = :vendedor_id or :vendedor_id = '')",{vendedor_id: params[:vendedor_id]})
  	query
  end

end
