class RutasController < ApplicationController
  before_action :set_ruta, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de rutas"
  end

  def lista_rutas_disponibles
    @rutas = Ruta.activos.order(:Ruta).where("Ruta like ? and IdEmpresa = ?", "%#{params[:term]}%", "#{current_usuario.empresa_id}")
    render json: @rutas.map(&:Ruta)
  end

  # GET /rutas
  # GET /rutas.json
  def index
    @pathDeBusqueda = rutas_path
    @search = Ruta.activos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdRutas' if @search.sorts.empty?
    @rutas = @search.result().page(params[:rutas])
    @cantidad_rutas_activos = Ruta.activos.por_empresa(current_usuario.empresa_id).count

    @rutasEXP = Ruta.activos.por_empresa(current_usuario.empresa_id)
    @ruta = Ruta.new
    @ruta.build_configruta #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    #pero en el caso de ser un has_many se debe usar @producto.productosxpza.build
    @ruta.build_continuid #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    @ruta.build_relrucla
    @catgrupo = Catgrupo.new
    datos_para_los_selectbox

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @rutasEXP.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def datos_para_los_selectbox
      @catgrupos_ruta_TipoRuta = Catgrupo.activos.ruta_tipo
      @catgrupos_ruta_Sector = Catgrupo.activos.ruta_grupo

      @vendedores = Vendedor.activos.por_empresa(current_usuario.empresa_id).ordered
    #  @vendedores = Vendedor.activos.por_empresa(current_usuario.empresa_id).vendedores.vendedores_sin_ruta.ordered
    #  @ayudantes = Vendedor.activos.por_empresa(current_usuario.empresa_id).ayudantes.vendedores_sin_ruta.ordered
      @vehiculos = Vehiculo.activos.por_empresa(current_usuario.empresa_id).ordered

      @clas1 = Clasruta.clas1_scope
      @clas2 = Clasruta.clas2_scope
      @clas3 = Clasruta.clas3_scope
      @clas4 = Clasruta.clas4_scope
      @clas5 = Clasruta.clas5_scope
  end

  def datos_ruta

    @ruta = Ruta.find_by_IdRutas(params[:IdRutas])

    #@ruta.build_configruta #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
                            #pero en el caso de ser un has_many se debe usar @producto.productosxpza.build
    #@ruta.build_relrucla
    #@ruta.build_continuid #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza

    datos_para_los_selectbox

    respond_to do |format|
      format.js
    end
  end

  def inactivos

    @pathDeBusqueda = rutas_inactivos_path
    @search = Ruta.inactivos.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'IdRutas' if @search.sorts.empty?
    @rutas = @search.result().page(params[:rutas])
    @cantidad_rutas_inactivos = Ruta.inactivos.por_empresa(current_usuario.empresa_id).count

    @rutasEXP = Ruta.inactivos.por_empresa(current_usuario.empresa_id)
    @ruta = Ruta.new
    @ruta.build_configruta #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    #pero en el caso de ser un has_many se debe usar @producto.productosxpza.build
    @ruta.build_continuid #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    @ruta.build_relrucla
    @catgrupo = Catgrupo.new
    datos_para_los_selectbox

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @rutasEXP.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end




  def rutas_check
    @rutax = Ruta.find_by(Ruta:params[:ruta][:Ruta], IdEmpresa: current_usuario.empresa_id)

    respond_to do |format|
    format.json { render :json => !@rutax }
    end
  end


  def import
    fecha_actual_mexico = DateTime.now.in_time_zone("Mexico City")
    empresa = current_usuario.empresa_id
    @errors = Ruta.import(params[:file], empresa, fecha_actual_mexico)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to rutas_path, notice: "Rutas importadas."
    end
  end
  # GET /rutas/1
  # GET /rutas/1.json
  def show
  end

  # GET /rutas/new
  def new
    @ruta = Ruta.new
  end

  # GET /rutas/1/edit
  def edit
  end

  # POST /rutas
  # POST /rutas.json
  def create
    datos_para_los_selectbox
    if @puede_crear != true
      return
    end
    @ruta = Ruta.new(ruta_params)
    respond_to do |format|
      if @ruta.save
        Relvendruta.create(IdRuta: @ruta.IdRutas, IdVendedor: @ruta.Vendedor, Fecha: Time.now.strftime("%d-%m-%Y %T"), IdEmpresa:current_usuario.empresa_id)
        proceso_creacion_productos_para_stock
        format.html { redirect_to @ruta, notice: 'Ruta was successfully created.' }
        format.json { render :show, status: :created, location: @ruta }
        format.js {flash.now[:notice] = 'La Ruta se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @ruta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la ruta.'} #ajax

      end
    end
  end

  def proceso_creacion_productos_para_stock
    params[:search6] =  current_usuario.empresa_id
    params[:search] = @ruta.IdRutas
    @productos = Producto.proceso_stock_busqueda_productos(params)

     @productos.each do |producto|
       Stoc.create(Articulo: producto.Clave, Ruta:params[:search], Stock: 0, IdEmpresa:current_usuario.empresa_id)
      end
  end

  # PATCH/PUT /rutas/1
  # PATCH/PUT /rutas/1.json
  def update
    datos_para_los_selectbox
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @ruta.update(ruta_params)
        format.html { redirect_to @ruta, notice: 'Ruta was successfully updated.' }
        format.json { render :show, status: :ok, location: @ruta }
        format.js {flash.now[:notice] = 'La ruta se ha actualizado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @ruta.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la ruta.'} #ajax

      end
    end
  end

#Busca si existe un vendedor asignado a la ruta, si no existe lo crea
  def crear_otrosRelvendruta_cuando_se_Actualice
    @relvendruta = Relvendruta.find_by(IdRuta: @ruta.IdRutas, IdVendedor: @ruta.Vendedor)
    if @relvendruta == nil
      Relvendruta.create(IdRuta: @ruta.IdRutas, IdVendedor: @ruta.Vendedor, Fecha: Time.now.strftime("%d-%m-%Y %T"), IdEmpresa:current_usuario.empresa_id)
    end
  end
  # DELETE /rutas/1
  # DELETE /rutas/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to vendedores_url, notice: 'La ruta was successfully destroyed.' }
      format.json { head :no_content }
      if @ruta.Activa == true
        @ruta.update(Activa:false)
        format.js {flash.now[:notice] = 'La ruta se ha borrado de forma exitosa.'} #ajax
      else
        @ruta.update(Activa:true)
        format.js {flash.now[:notice] = 'La ruta se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ruta
      @ruta = Ruta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ruta_params
      params.require(:ruta).permit(:Ruta, :Activa, :TipoRuta, :VenSinStock, :Oficina, :Sector, :Vendedor, :Tipo, :id_ayudante1, :id_ayudante2, :VendedorAyudante, :Foranea, :vehiculo, :idcrutas, :IdEmpresa, configruta_attributes: [:id, :RutaId, :ModelPrinter, :VelCom, :COM, :Server, :Puerto, :ServerGPS, :GPS, :PuertoG, :PagoContado, :CteNvo, :CveCteNvo, :IdEmpresa, :SugerirCant, :PromoEq], relrucla_attributes: [:id, :RutaId, :Clas1, :Clas2, :Clas3, :Clas4, :Clas5, :IdEmpresa], continuid_attributes: [:id, :RutaID, :DiaO, :FolVta, :FolPed, :FolDevol, :FolCob, :UDiaO, :CteNvo, :IdEmpresa],relvendrutas_attributes: [:id, :IdVendedor, :IdRuta, :IdEmpresa, :Fecha])
    end
end
