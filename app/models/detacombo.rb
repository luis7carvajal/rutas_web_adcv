class Detacombo < ActiveRecord::Base
  self.primary_key = 'Id'
  self.table_name = 'DetalleCombo' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :productos_del_combo_count, -> (id_param) { where(ComboId: id_param) }
  belongs_to :listcombo, class_name:"Listcombo", foreign_key: "ComboId"
  belongs_to :producto, class_name: "Producto", foreign_key: "Articulo"
  
end
