require 'test_helper'

class SaboresControllerTest < ActionController::TestCase
  setup do
    @sabor = sabores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sabores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sabor" do
    assert_difference('Sabor.count') do
      post :create, sabor: { Clave: @sabor.Clave, Descripcion: @sabor.Descripcion, Status: @sabor.Status }
    end

    assert_redirected_to sabor_path(assigns(:sabor))
  end

  test "should show sabor" do
    get :show, id: @sabor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sabor
    assert_response :success
  end

  test "should update sabor" do
    patch :update, id: @sabor, sabor: { Clave: @sabor.Clave, Descripcion: @sabor.Descripcion, Status: @sabor.Status }
    assert_redirected_to sabor_path(assigns(:sabor))
  end

  test "should destroy sabor" do
    assert_difference('Sabor.count', -1) do
      delete :destroy, id: @sabor
    end

    assert_redirected_to sabores_path
  end
end
