class Productoparapos < ActiveRecord::Base
  belongs_to :cliente, class_name:"Cliente", foreign_key: "Cliente"
	
  def self.productos_para_POS(params)
    query =  select('productosparapos.id AS id_productoparapos, P.Clave as Clave,P.Producto,productosparapos.LT_KG,productosparapos.IVA as IVA_calculado,productosparapos.Descuento,productosparapos.Descuento_Porcentaje,productosparapos.DocSalida,productosparapos.DiaO,productosparapos.IEPS,productosparapos.RutaId,productosparapos.Tipo,productosparapos.Documento,productosparapos.FormaPag,productosparapos.Vendedor,productosparapos.Cliente,productosparapos.TipoVta,productosparapos.SubTotal,productosparapos.Total,productosparapos.Cantidad,productosparapos.Precio, PXP.PzaXCja, case When IsNull(PXP.PzaXCja,0)=1 Then 1 Else PXP.PzaXCja End AS PzaXCja, C.DiasCreedito, C.LimiteCredito, C.Saldo')
             .joins('inner join Productos P ON P.Clave = productosparapos.Producto inner join Clientes C On productosparapos.Cliente=C.IdCli inner join Vendedores V On productosparapos.Vendedor=V.IdVendedor Left Join ProductosXPzas PXP On P.Clave=PXP.Producto ')
          if params[:Ps_id].present?
            query = query.where("(productosparapos.id = :id)",{id: params[:Ps_id]})
          else
            query = query.where("(productosparapos.RutaId = :rutaId) AND (productosparapos.Vendedor = :vendedor) AND (productosparapos.Documento = :documento) AND (productosparapos.DiaO = :DiaO) AND (productosparapos.IdEmpresa = :idempresa)",{rutaId: params[:IdRuta], vendedor: params[:IdVendedor], documento: params[:documento], DiaO: params[:DiaO], idempresa: params[:sucursal]})
          end
  end



  def self.productos_para_POS_cantidad(params)
    query =  joins('inner join Productos P ON P.Clave = productosparapos.Producto inner join Clientes C On productosparapos.Cliente=C.IdCli inner join Vendedores V On productosparapos.Vendedor=V.IdVendedor')
             .where("(productosparapos.RutaId = :rutaId) AND (productosparapos.Vendedor = :vendedor) AND (productosparapos.Documento = :documento) AND (productosparapos.DiaO = :DiaO) AND (productosparapos.IdEmpresa = :idempresa)",{rutaId: params[:IdRuta], vendedor: params[:IdVendedor], documento: params[:documento], DiaO: params[:DiaO], idempresa: params[:sucursal]})
  end

end
