class Stockalmacen < ActiveRecord::Base
  self.primary_key = "Id"
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :es_producto, -> {joins(:producto).where("P.Ban_Envase = 0") }
  scope :es_envase, -> {joins(:producto).where("P.Ban_Envase = 1") }
  
  belongs_to :producto, class_name:"Producto", foreign_key: "Articulo"

  def self.productos_de_un_stock_almacen(params)
    query = select("[Th_stockalmacen].Id, [Th_stockalmacen].Stock, [p].Clave AS Clave_producto, [p].Producto, [productosxpzas].PzaXCja AS PzaXCja, LOWER([catunidadmedida].UnidadMedida) AS UnidadMedida,
    CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN 0 ELSE ([Th_stockalmacen].Stock / [productosxpzas].PzaXCja) END AS Cajas,
    CASE  WHEN  LOWER([catunidadmedida].UnidadMedida)='pieza' OR LOWER([catunidadmedida].UnidadMedida)='piezas' THEN [Th_stockalmacen].Stock ELSE ([Th_stockalmacen].Stock % [productosxpzas].PzaXCja) END AS Piezas") #LOWER para colocar el valor en minusculas
            .joins('inner join productos p ON Th_stockalmacen.Articulo = p.Clave inner join productosxpzas ON p.Clave = productosxpzas.Producto inner join catunidadmedida ON p.UniMed = catunidadmedida.Clave')
            .where("Th_stockalmacen.IdEmpresa = :idempresa AND p.clave in (select distinct Articulo from detallelp where IdEmpresa = :idempresa) AND p.Status = 'A'",{idempresa: params[:search6]})

    query = query.where("(p.Clave LIKE :Clave or :Clave = '') AND (p.Producto LIKE :Producto or :Producto = '')",{Clave: "%#{params[:Clave_param]}%", Producto: "%#{params[:Producto_param]}%"}) #Campos adicionales para la busqueda de todos los registros, el like es para buscar los que coincidan y para que funcione se deben colocar ambos signos de porcentaje
    query
  end

  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |stockalmacen|
        csv << stockalmacen.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      @pzas = ((Productosxpza.find_by_Producto(row["Clave del producto"]).PzaXCja * (row["StockCajas"]).to_f) + (row["StockPiezas"]).to_f)

  #        stoc = (find_by_IdStock(row["Id del Stock"]) and find_by_Articulo(row["Clave del producto"]) and find_by_Ruta(row["Ruta"]))
      stockalmacen = (find_by_Id(row["Id del Pedido"]))
      stockalmacen.attributes = {Stock: @pzas}


      if stockalmacen.save
        # stuff to do on successful save
      else
        stockalmacen.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end


  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"
    end
  end

end
