class RelmensController < ApplicationController
  before_action :set_relmen, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /relmens
  # GET /relmens.json
  def index

    @relmen = Relmen.new
    @pathDeBusqueda = relmens_path

    @search_producto_out = Producto.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).search(search_params)
    # make name the default sort column
    @search_producto_out.sorts = 'Clave' if @search_producto_out.sorts.empty?
    @productos_out = @search_producto_out.result().page(params[:productos_out]).per(10)

    @search_producto_in = Relmen.productosquetiene(params[:id]).search(search_params)
    @search_producto_in.sorts = 'CodProducto' if @search_producto_in.sorts.empty?
    @productos_in = @search_producto_in.result().page(params[:producto_in]).per(10)
    contadores

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end




  end

  def clientes
    @relmen = Relmen.new
    @pathDeBusqueda = mensaje_clientes_path
    contadores

    @search_cliente_out = Cliente.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).search(search_params)
    # make name the default sort column
    @search_cliente_out.sorts = 'Nombre' if @search_cliente_out.sorts.empty?
    @clientes_out = @search_cliente_out.result().page(params[:clientes_out]).per(10)

    @search_cliente_in = Relmen.clientesquetiene(params[:id]).search(search_params)
    @search_cliente_in.sorts = 'Nombre' if @search_cliente_in.sorts.empty?
    @clientes_in = @search_cliente_in.result().page(params[:clientes_in]).per(10)


    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def rutas

    @relmen = Relmen.new
    @pathDeBusqueda = mensaje_rutas_path
    contadores

    @search_ruta_out = Ruta.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).search(search_params)
    # make name the default sort column
    @search_ruta_out.sorts = 'Ruta' if @search_ruta_out.sorts.empty?
    @rutas_out = @search_ruta_out.result().page(params[:rutas_out]).per(10)

    @search_ruta_in = Relmen.rutasquetiene(params[:id]).search(search_params)
    @search_ruta_in.sorts = 'Ruta' if @search_ruta_in.sorts.empty?
    @rutas_in = @search_ruta_in.result().page(params[:rutas_in]).per(10)


    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end

  end

  def contadores
    if @relmen.IdProducto != nil
      params[:id] = @relmen.try(:IdProducto)
    elsif @relmen.IdCliente != nil
      params[:id] = @relmen.try(:IdCliente)
    elsif @relmen.IdRuta != nil
      params[:id] = @relmen.try(:IdRuta)
    end
    @cantidad_productos_out = Producto.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).count
    @cantidad_productos_in = Relmen.productosquetiene(params[:id]).count

    @cantidad_clientes_out = Cliente.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).count
    @cantidad_clientes_in = Relmen.clientesquetiene(params[:id]).count

    @cantidad_rutas_out = Ruta.por_empresa(current_usuario.empresa_id).without_mensaje(params[:id]).count
    @cantidad_rutas_in = Relmen.rutasquetiene(params[:id]).count
  end

  # GET /relmens/1
  # GET /relmens/1.json
  def show
  end

  # GET /relmens/new
  def new
    @relmen = Relmen.new
  end

  # GET /relmens/1/edit
  def edit
  end



  # POST /relmens
  # POST /relmens.json
  def create
    @relmen = Relmen.new(relmen_params)
    respond_to do |format|
      if @relmen.save
        contadores
        format.html { redirect_to @relmen, notice: 'Relmen was successfully created.' }
        format.json { render :show, status: :created, location: @relmen }
        format.js {flash.now[:notice] = 'El registro se ha agregado de forma exitosa.'} #ajax

      else
        format.html { render :new }
        format.json { render json: @relmen.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al agregar el registro.'} #ajax

      end
    end
  end

  # PATCH/PUT /relmens/1
  # PATCH/PUT /relmens/1.json
  def update
    respond_to do |format|
      if @relmen.update(relmen_params)
        format.html { redirect_to @relmen, notice: 'Relmen was successfully updated.' }
        format.json { render :show, status: :ok, location: @relmen }
        format.js {flash.now[:notice] = 'La lista se ha actualizado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @relmen.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la lista.'} #ajax

      end
    end
  end

  # DELETE /relmens/1
  # DELETE /relmens/1.json
  def destroy
    @producto = Producto.find_by(Clave: @relmen.CodProducto)
    @cliente = Cliente.find_by(IdCli: @relmen.CodCliente)
    @ruta = Ruta.find_by(IdRutas: @relmen.CodRuta)
    @relmen.destroy
    contadores
    respond_to do |format|
      format.html { redirect_to relmens_url, notice: 'Relmen was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El registro se ha borrado de forma exitosa.'} #ajax

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relmen
      @relmen = Relmen.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relmen_params
      params.require(:relmen).permit(:MenId, :CodCliente, :IdCliente, :CodProducto, :IdProducto, :CodRuta, :IdRuta, :IdEmpresa)
    end
end
