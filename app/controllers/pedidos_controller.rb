require 'net/http'
class PedidosController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]
  before_action :clear_search_index, :only => [:index, :consolidacion]
  before_action :authenticate_usuario!

  def cancelar
    pedido = Pedido.find_by(IdPedido: params[:IdPedido])
    pedido_liberado = Pedidoliberado.where('PEDIDO = ?', pedido[:Pedido])
                                    .where('RUTA = ?', pedido[:Ruta])
                                    .where('IDEMPRESA = ?', pedido[:IdEmpresa])
                                    .where('IDVENDEDOR = ?', pedido[:Vendedor])
    response = pedido.update(Cancelado: 1) && pedido_liberado.first.update(CANCELADA: 1)
    respond_to do |format|
      format.json {render json: response}
    end
  end


  def visor_de_pedidos
    @controlador = 'pedidos'
    @accion = 'visor_de_pedidos'
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
      @items = session[:items]
      session[:items] = @items



    if (params[:search4] == nil and params[:search] == nil)
      Stored_procedure.insert_update_delete_calculate("exec SPAD_LiberarPedidos_Todos '#{params[:search6]}'")
    else
      Stored_procedure.insert_update_delete_calculate("exec SPAD_LiberarPedidos '#{(params[:search4].try(:to_date)).try(:strftime,"%Y-%m-%d")}','#{params[:search]}','#{params[:search6]}'")
    end

    params[:IdEmpresa] = current_usuario.empresa_id
    @monto_total = Pedido.visor_de_pedidos(params).contadoycredito.map(&:Total_calculado).compact.sum
    @monto_entregas = Pedido.visor_de_pedidos(params).entregados.map(&:Total_calculado).compact.sum
    @monto_credito = Pedido.visor_de_pedidos(params).credito.map(&:Total_calculado).compact.sum
    @monto_contado = Pedido.visor_de_pedidos(params).contado.map(&:Total_calculado).compact.sum
    @pedidos_total_existencia_cajas = Pedido.total_existencias(params).sin_obsequios.map(&:Cajas_Total).compact.sum
    @pedidos_total_existencia_piezas= Pedido.total_existencias(params).sin_obsequios.map(&:Piezas_Total).compact.sum
    @pedidos_obsequios_total_existencia_cajas = Pedido.total_existencias(params).con_obsequios.map(&:Cajas_Total).compact.sum
    @pedidos_obsequios_total_existencia_piezas= Pedido.total_existencias(params).con_obsequios.map(&:Piezas_Total).compact.sum
    @promocion_total_existencia_cajas = Pregalad.total_existencias(params).map(&:Cajas_Total).compact.sum
    @promocion_total_existencia_piezas = Pregalad.total_existencias(params).map(&:Piezas_Total).compact.sum
    @total_cajas_entregadas = Pedido.cajas_piezas_entregas(params).entregados.sin_obsequios.map(&:Cajas_Total).compact.sum #compact desecha los valores nulos del map
    @total_piezas_entregadas = Pedido.cajas_piezas_entregas(params).entregados.sin_obsequios.map(&:Piezas_Total).compact.sum


    @search = Pedido.visor_de_pedidos(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'Fecha desc' if @search.sorts.empty?
    @pedidos = @search.result().page(params[:pedidos])

    params_productos = {:Ruta => params[:Ruta], :IdEmpresa => params[:search6]}

    @pedidos_export = Pedido.visor_de_pedidos(params)
    @detalles_export = Pedido.detalle_visor_de_pedidos(params)
    @productos_promocion_export = Pregalad.rep_producto_promocion_preventa(params)

    params_promocion = {:search => params[:Ruta], :search6 => params[:search6]}
    @promocionesExport = Pregalad.rep_producto_promocion_preventa(params_promocion)

    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    #para estadisticas
    @total_pedidos = Pedido.visor_de_pedidos(params).sin_cancelados.count('Pedidos.Pedido')
    @total_clientes = Pedido.visor_pedidos_total_clientes(params).sin_cancelados.count

    respond_to do |format|
      format.html
      format.csv { send_data @pedidosExport.to_csv}
      format.xls #{ send_data @pedidosExport.to_csv(col_sep: "\t") }
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def facturacion
    params[:search6] = current_usuario.empresa_id
    cliente = Pedido.datos_cliente(params).first
    pedido = Pedido.find_by IdPedido: params[:IdPedido]
    productos_pedido = Pedido.detalle_visor_de_pedidos(params)
    i = 0
    body_request = {
      "SSK_Data[contrato]" => "ac2a2049-256b-4",
      "SSK_Data[password]" => "38004076-4c60-4",
      "SSK_Data[username]" => "aolivares@adventech-logistica.com",
      "codigoMoneda" => "MXN",
      "datosComprobante[CodeCliente]" => cliente.try(:IdCli),
      "datosComprobante[FechaVence]" => pedido.try(:FechaVe).strftime("%Y-%m-%dT%H:%M:%S"),
      "datosComprobante[Subtotal]" => pedido.try(:Subt).to_f.round(2),
      "datosComprobante[condicionesPago]" => "Contado",
      "datosComprobante[folio]" => pedido.try(:Pedido).to_s,
      "datosComprobante[formaPago]" => "01",
      "datosComprobante[horaEmision]" => DateTime.now.in_time_zone("Mexico City").strftime("%Y-%m-%dT%H:%M:%S"),
      "datosComprobante[metodoPago]" => "PUE",
      "datosComprobante[modoProduccion]" => "0",
      "datosComprobante[numeroReferencia]" => pedido.try(:Pedido).to_s + "A",
      "datosComprobante[serie]" => "A",
      "datosComprobante[tipoCambio]" => "0",
      "datosComprobante[tipoDeComprobante]" => "ingreso",
      "datosComprobante[total]" => pedido.try(:Total).to_f.round(2),
      "datosComprobante[usoCFDi]" => "G01",
      "datosEmisor[RegimenFiscal]" => "601",
      "datosEmisor[codigoPostalEmisor]" => "09070",
      "datosEmisor[rfcEmisor]" => "ALO040120HG8",
      "datosReceptor[dirEnvioReceptor]" => cliente.try(:Direccion),
      "datosReceptor[dirReceptor]" => cliente.try(:Direccion),
      "datosReceptor[extra1]" => cliente.try(:Email),
      "datosReceptor[extra2]" => "",
      "datosReceptor[nombreReceptor]" => cliente.try(:NombreCorto),
      "datosReceptor[rfcReceptor]" => 'XAXX010101000'
    }
    productos_pedido.each do |pro|
      importe_base = pro.try(:DetallePedidos_Precio).to_f.round(2) * pro.try(:Pza).to_f.round(2)
      the_product = {
        "productos[#{i}][Descuento]" => pro.try(:DetallePedidos_Descuento).to_f.round(2),
        "productos[#{i}][cantidad]" => pro.try(:Pza).to_i,
        "productos[#{i}][clave_ProdServ]" => pro.try(:Producto_Clave).to_s,
        "productos[#{i}][noIdentificacion]" => pro.try(:Producto_Clave).to_s,
        "productos[#{i}][clave_unit]" => "caja",
        "productos[#{i}][descripcion]" => pro.try(:Producto_Producto),
        "productos[#{i}][importe]" => importe_base,
        "productos[#{i}][valorUnitario]" => pro.try(:DetallePedidos_Precio).to_f.round(2),
        "productos[#{i}][impuestos][0][Base]" => importe_base.round(2),
        "productos[#{i}][impuestos][0][impuesto]" => "IVA",
        "productos[#{i}][impuestos][0][tasa]" => "0.16"
      }
      body_request = body_request.merge(the_product)
      i = i + 1
    end
    uri = URI.parse("https://app.cfdismart.com/api/Factory/Produce_CFDi")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded; charset=UTF-8"
    request["Connection"] = "keep-alive"
    request["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36"
    request.set_form_data(body_request)

    req_options = {
      use_ssl: uri.scheme == "https",
    }
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    respond_to do |format|
      format.json {render json: response.body}
    end
  end


  def detalles_visor_de_pedidos
    params[:IdEmpresa] = current_usuario.empresa_id
    @detalles = Pedido.detalle_visor_de_pedidos(params)
    params_promocion = {:search => params[:Ruta], :search6 => params[:IdEmpresa], :pedido => params[:Pedido], :Clave_param => params[:Clave_param], :Producto_param => params[:Producto_param]}
    @productos_promocion = Pregalad.rep_producto_promocion_preventa(params_promocion)
    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def surtir_de_forma_masiva
    params[:lista].each do |(c,pedido_id)|
      @pedido = Pedido.find_by(IdPedido: pedido_id)
      if @pedido.Status == 2
        @pedido.update(Status: 3)
        Pedidoliberado.where(PEDIDO: @pedido.Pedido, RUTA: @pedido.Ruta, COD_CLIENTE: @pedido.CodCliente, IdEmpresa: current_usuario.empresa_id).update_all(STATUS: @pedido.Status)
      end
    end

    respond_to do |format|
      format.js {render :js => "window.location.href='"+visor_de_pedidos_path+"'"}
    end
  end

  def seleccionar_todos_surtimiento_masivo
    params[:search6] = current_usuario.empresa_id
    @todos_los_pedidos_seleccionados = Pedido.visor_de_pedidos(params)

    respond_to do |format|
      format.js {flash.now[:notice] = 'Todos los pedidos se han seleccionado de forma exitosa.'} #ajax
    end
  end


  def update
    respond_to do |format|
      if @pedido.Status < 3 #el estatus del pedido debe ser menor a 3 para surtirlo
        if @pedido.update(pedido_params)
          Pedidoliberado.where(PEDIDO: @pedido.Pedido, RUTA: @pedido.Ruta, COD_CLIENTE: @pedido.CodCliente, IdEmpresa: current_usuario.empresa_id).update_all(STATUS: @pedido.Status)
          @pedido = Pedido.visor_de_pedidos(:search6 => current_usuario.empresa_id, :rutaIdB => "", :Status => "", :commit => "update").find_by(IdPedido: @pedido.IdPedido)
          format.html { redirect_to @pedido, notice: 'Pedido was successfully updated.' }
          format.json { render :show, status: :ok, location: @pedido }
          format.js {flash.now[:notice] = 'El pedido se ha actualizado de forma exitosa.'} #ajax
        else
          format.html { render :edit }
          format.json { render json: @pedido.errors, status: :unprocessable_entity }
          format.js {flash.now[:alert] = 'Error al crear el pedido.'} #ajax
        end
      else
        @pedido = Pedido.visor_de_pedidos(:search6 => current_usuario.empresa_id, :rutaIdB => "", :Status => "", :commit => "update").find_by(IdPedido: @pedido.IdPedido)
        format.js {flash.now[:alert] = 'El pedido ya fue surtido.'} #ajax
      end
    end
  end

  def consolidacion
    params[:search6] = current_usuario.empresa_id
    @pedidos = Pedido.obtener_ids(params).pluck(:Pedido)
    @pedidos_todos = Pedido.consolidado(params, @pedidos)
    @pedidos_consolidar = Kaminari.paginate_array(@pedidos_todos.to_a).page(params[:consolidar]).per(10)
    respond_to do |format|
      format.js
      format.xls
    end
  end

  def consolidacion2
      params[:search6] = current_usuario.empresa_id

      #@items = [ "a", "b", "c" ]
      #@items.push("d", "e", "f")
      @items = session[:items] #tomara el array de index para no perderlo
      if (@items == nil) # toma solo los parametros que estoy pasando
        @items = params[:items]
      elsif (params[:items] != nil) #añade los parametros que estoy pasando al arreglo
        params[:items].each do |course|
          @items.push("#{course}")
        end
      end

      params[:pedidos_para_consolidar] = @items
      if params[:subaction]== "Consolidar"
        @pedidos_listos_para_consolidar= Pedido.pedidos_listos_para_consolidar(params)
          @pedidos_listos_para_consolidar.each do |pedido|
            Hotel.create(name: pedido.Pedido)
          end
      end

      @pedidos_consolidar= Pedido.pedido_para_consolidar(params)
      @mostrar_ventana_pc = params[:ventana_modal]

      session[:items] = @items
      respond_to do |format|
        format.js {flash.now[:notice] = 'El pedido se ha actualizado de forma exitosa.'} #ajax
      end
    end



  def eliminar_pedidos_consolidados
    params[:search6] = current_usuario.empresa_id
    @items = session[:items] #tomara el array de index para no perderlo
    @items.delete(params[:Pedido]) #borra el registro del arreglo

    params[:pedidos_para_consolidar] = @items
    @pedidos_consolidar= Pedido.pedido_para_consolidar(params)

    respond_to do |format|
      format.js {flash.now[:notice] = 'El pedido ha consolidar se ha eliminado.'} #ajax
    end
  end

  def pdf
    params[:IdEmpresa] = current_usuario.empresa_id
    @pedido = Pedido.busqueda_por_pedido(params[:P]).busqueda_por_ruta(params[:R]).por_empresa(params[:IdEmpresa])
    @detalles = Detallepedidolib.detalle_pdf_visor_pedidos(params)
    respond_to do |format|
        format.html
        format.pdf do
          pdf = PedidoSurtidoPdf.new(@pedido, @detalles)
          send_data pdf.render, filename: "Pedido_Surtido##{@pedido.last.Pedido}.pdf",
                                type: "application/pdf",
                                disposition: "inline"
        end
    end
  end






  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedido
      @pedido = Pedido.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pedido_params
      params.require(:pedido).permit(:Status)
    end


end
