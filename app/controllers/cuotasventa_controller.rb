class CuotasventaController < ApplicationController
  before_action :set_cuotaventa, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]

  # GET /cuotasventa
  # GET /cuotasventa.json
  def index
    @search = Cuotaventa.search(search_params)
  #  @search = Cuotaventa.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @cuotasventa = @search.result().page(params[:page])
    @cantidad_cuotasventa_activos = Cuotaventa.all.count
    @rutas = Ruta.activos.por_empresa(current_usuario.empresa_id)
    @cuotaventa = Cuotaventa.new
    datos

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @cuotasventa.to_csv}
      format.xls
    end

  end

  def datos_cuotaventa
      @rutas = Ruta.activos.por_empresa(current_usuario.empresa_id)
      @cuotaventa = Cuotaventa.find_by_year(params[:year_cuotaventa])
      datos

    respond_to do |format|
      format.js
    end
  end

  def datos
    @years = [Time.current.year, Time.current.year-1, Time.current.year-2]

  end

  # GET /cuotasventa/1
  # GET /cuotasventa/1.json
  def show
  end

  # GET /cuotasventa/new
  def new
    @cuotaventa = Cuotaventa.new
  end

  # GET /cuotasventa/1/edit
  def edit
  end

  # POST /cuotasventa
  # POST /cuotasventa.json
  def create
    @cuotaventa = Cuotaventa.new(cuotaventa_params)

    respond_to do |format|
      if @cuotaventa.save
        format.html { redirect_to @cuotaventa, notice: 'Cuotaventa was successfully created.' }
        format.json { render :show, status: :created, location: @cuotaventa }
        format.js {flash.now[:notice] = 'La cuota se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @cuotaventa.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la cuota.'} #ajax
      end
    end
  end

  # PATCH/PUT /cuotasventa/1
  # PATCH/PUT /cuotasventa/1.json
  def update
    respond_to do |format|
      if @cuotaventa.update(cuotaventa_params)
        format.html { redirect_to @cuotaventa, notice: 'Cuotaventa was successfully updated.' }
        format.json { render :show, status: :ok, location: @cuotaventa }
        format.js {flash.now[:notice] = 'La cuota se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @cuotaventa.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la cuota.'} #ajax
      end
    end
  end

  # DELETE /cuotasventa/1
  # DELETE /cuotasventa/1.json
  def destroy
    @cuotaventa.destroy
    respond_to do |format|
      format.html { redirect_to cuotasventa_url, notice: 'Cuotaventa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cuotaventa
      @cuotaventa = Cuotaventa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cuotaventa_params
      params.require(:cuotaventa).permit(:RutaId, :year, :Ene, :Feb, :Mar, :Abr, :May, :Jun, :Jul, :Ago, :Sep, :Oct, :Nov, :Dic, :IdEmpresa)
    end
end
