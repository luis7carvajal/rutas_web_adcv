class CustomFailure < Devise::FailureApp
  def redirect_url
     new_usuario_session_url(:subdomain => 'secure')
  end

  # You need to override respond to eliminate recall
  def respond
    if http_auth?
      http_auth
    else
      root
    end
  end
end
