class ThComisionController < ApplicationController
  before_action :set_comision, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de comisiones"
  end

  # GET /th_comision
  # GET /th_comision.json
  def index
    @search = Comision.activos.where("IdEmpresa = ?", current_usuario.empresa_id).search(search_params)
    @th_comision = @search.result
    @comision = Comision.new
    @cantidad_th_comision_activos = Comision.activos.por_empresa(current_usuario.empresa_id).count
    respond_to do |format|
      format.html
      format.csv { send_data @th_comision.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def inactivos
    @th_comision = Comision.inactivos
    @cantidad_th_comision_inactivos = Comision.inactivos.count
    respond_to do |format|
      format.html
      format.csv { send_data @th_comision.to_csv}
      format.xls #{ send_data @empresas.to_csv(col_sep: "\t") }
    end
  end

  def import
    empresa = current_usuario.empresa_id
    @errors = Comision.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to th_comision_path, notice: "Comisiones importadas."
    end
  end

  # GET /th_comision/1
  # GET /th_comision/1.json
  def show
  end

  # GET /th_comision/new
  def new
    @comision = Comision.new
  end

  # GET /th_comision/1/edit
  def edit
  end

  def contadores
    @cantidad_th_comision_activos = Comision.activos.por_empresa(current_usuario.empresa_id).count
    @cantidad_th_comision_inactivos = Comision.inactivos.por_empresa(current_usuario.empresa_id).count
  end

  # POST /th_comision
  # POST /th_comision.json
  def create
    if @puede_crear != true
      return
    end
    @comision = Comision.new(comision_params)
    respond_to do |format|
      if @comision.save
        contadores
        format.html { redirect_to @comision, notice: 'Comision was successfully created.' }
        format.json { render :show, status: :created, location: @comision }
        format.js {flash.now[:notice] = 'La comisión se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @comision.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la comisión.'} #ajax
      end
    end
  end

  # PATCH/PUT /th_comision/1
  # PATCH/PUT /th_comision/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @comision.update(comision_params)
        contadores
        format.html { redirect_to @comision, notice: 'Comision was successfully updated.' }
        format.json { render :show, status: :ok, location: @comision }
        format.js {flash.now[:notice] = 'La comisión se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @comision.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la comisión.'} #ajax
      end
    end
  end

  # DELETE /th_comision/1
  # DELETE /th_comision/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to th_comision_url, notice: 'Comision was successfully destroyed.' }
      format.json { head :no_content }
      if @comision.Status == "A"
        @comision.update(Status:"I")
        contadores
        format.js {flash.now[:notice] = 'La comisión se ha borrado de forma exitosa.'} #ajax
      else
        @comision.update(Status:"A")
        contadores
        format.js {flash.now[:notice] = 'La comisión se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comision
      @comision = Comision.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comision_params
      params.require(:comision).permit(:Status, :Comentarios, :IdEmpresa, :ID_Producto, tdcomision_attributes: [:id, :RangoIni, :IdEmpresa])
    end
end
