class RelclilisController < ApplicationController
  before_action :set_relclili, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /relclilis
  # GET /relclilis.json
  def index

    @searchc = Cliente.activos.por_empresa(current_usuario.empresa_id).without_discount.search(search_params)
    @searchc.sorts = 'id' if @searchc.sorts.empty?
    @clientes = @searchc.result().page(params[:clientes]).per(15)

    @search = Relclili.clienteslistad(params[:id]).search(search_params)
    @search.sorts = 'CodCliente' if @search.sorts.empty?
    @relclilis = @search.result().page(params[:relclili]).per(10)

    @relclili = Relclili.new

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def indexlistap
    @relclili = Relclili.new
    datoslistap

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def datoslistap
    @searchc = Cliente.activos.por_empresa(current_usuario.empresa_id).without_price.search(search_params)
    @searchc.sorts = 'id' if @searchc.sorts.empty?
    @clientes = @searchc.result().page(params[:clientes]).per(10)

    @search = Relclili.clienteslistap(params[:id]).search(search_paramsx)
    @search.sorts = 'CodCliente' if @search.sorts.empty?
    @relclilis = @search.result().page(params[:relclilis]).per(10)

  end


    def rcl_clientes_agregar_seleccionar_todos_masivo
      params[:IdEmpresa] = current_usuario.empresa_id

      @searchc = Cliente.activos.por_empresa(current_usuario.empresa_id).without_price.search(search_params)
      @todos_los_clientes_out_seleccionados = @searchc.result()


      respond_to do |format|
        format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
      end
    end

    def rcl_clientes_agregar_de_forma_masiva
      params[:lista_clientes_out].each do |(c,cliente_id)|
        if Relclili.exists?(CodCliente: cliente_id)
          @relclili =  Relclili.find_by(CodCliente: cliente_id)
          @relclili.update(ListaP: params[:id]) if @relclili.present?
        else
          Relclili.create(ListaP: params[:id],CodCliente: cliente_id, IdEmpresa: current_usuario.empresa_id)
        end
      end

      datoslistap
      respond_to do |format|
          format.js {flash.now[:notice] = 'Todos los clientes se han agregado de forma exitosa.'} #ajax
      end
    end

    def rcl_clientes_eliminar_seleccionar_todos_masivo
      params[:search6] = current_usuario.empresa_id

      @search = Relclili.clienteslistap(params[:id]).search(search_paramsx)
      @todos_los_clientes_in_seleccionados = @search.result()

      respond_to do |format|
        format.js {flash.now[:notice] = 'Todos los clientes se han seleccionado de forma exitosa.'} #ajax
      end
    end

    def rcl_clientes_eliminar_de_forma_masiva
      params[:lista_clientes_in].each do |(c,relclili_id)|
        @relclili = Relclili.find_by(Id: relclili_id)
        @relclili.update(ListaP: nil)
      end

      datoslistap
      respond_to do |format|
          format.js {flash.now[:notice] = 'Todos los clientes seleccionados se han desvinculado de forma exitosa.'} #ajax
      end
    end


  def clientes_promociones

    @searchc = Cliente.activos.por_empresa(current_usuario.empresa_id).without_promo.search(search_params)
    @searchc.sorts = 'id' if @searchc.sorts.empty?
    @clientes = @searchc.result().page(params[:clientes]).per(15)

    @search = Relclili.clientespromociones(params[:id]).search(search_params)
    @search.sorts = 'CodCliente' if @search.sorts.empty?
    @relclilis = @search.result().page(params[:relclili]).per(10)




    @grupo = Listapromomast.nombre_grupo(params[:id])

    @relclili = Relclili.new

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  # GET /relclilis/1
  # GET /relclilis/1.json
  def show
  end

  # GET /relclilis/new
  def new
    @relclili = Relclili.new
  end

  # GET /relclilis/1/edit
  def edit
  end

  # POST /relclilis
  # POST /relclilis.json
  def create
    respuesta_tipo = 0
    if relclili_params[:ListaP] != nil
      @proceso = "ListaPrecio"
    elsif relclili_params[:ListaD] != nil
      @proceso = "ListaDescuento"
    elsif relclili_params[:ListaPromo] != nil
      @proceso = "ListaPromo"
    end
    relclili_params[:CodCliente].split(/,/).each do |cliente|
      if Relclili.exists?(CodCliente: cliente)
        @relclili =  Relclili.find_by(CodCliente: cliente)
        if relclili_params[:ListaP] != nil
          @relclili.update(ListaP: relclili_params[:ListaP]) if @relclili.present?

        elsif relclili_params[:ListaD] != nil
          @relclili.update(ListaD: relclili_params[:ListaD]) if @relclili.present?

        elsif relclili_params[:ListaPromo] != nil
          @relclili.update(ListaPromo: relclili_params[:ListaPromo]) if @relclili.present?
        end
        respuesta_tipo = 1
      else
        relclili_params_ = {"CodCliente" => cliente, "ListaP" => relclili_params[:ListaP], "IdEmpresa" => relclili_params[:IdEmpresa]}
        @relclili = Relclili.new(relclili_params_)
        if @relclili.save
          respuesta_tipo = 2
        else
          respuesta_tipo = 3
        end
      end
    end
    redirect_to :back
  end

  # PATCH/PUT /relclilis/1
  # PATCH/PUT /relclilis/1.json
  def update
    respond_to do |format|
      if @relclili.update(relclili_params)

        if relclili_params[:ListaP] != nil
          @proceso = "ListaPrecio"
        elsif relclili_params[:ListaD] != nil
          @proceso = "ListaDescuento"
        elsif relclili_params[:ListaPromo] != nil
          @proceso = "ListaPromo"
        end

        params[:id] = params[:parametro_ID] #<!--PARA QUE NO SE PIERDA EL ID DE LA LISTA CUANDO PASE POR EL EDITAR DEBIDO A QUE ESTARIA TOMANDO EL ID DEL REGISTRO Y NO EL DE LA LISTA-->
        @container = @relclili #PARA EVITAR QUE SE EDITE EL REGISTRO CUANDO SE RENDERIZE EN CLIENTES EN VEZ DE CREARSE
        @cliente = Cliente.find_by(IdCli: @relclili.CodCliente) #PARA RENDERIZAR EL CLIENTE
          @relclili = Relclili.new #PARA QUE NO DE ERROR EN LA RENDERIZACION DEBIDO A QUE EL .NEW ESTA EN EL INDEX

        format.html { redirect_to @relclili, notice: 'Relclili was successfully updated.' }
        format.json { render :show, status: :ok, location: @relclili }
        format.js {flash.now[:notice] = 'Se ha desvinculado de forma exitosa.'} #ajax

      else
        format.html { render :edit }
        format.json { render json: @relclili.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al desvincular.'} #ajax

      end
    end
  end



  # DELETE /relclilis/1
  # DELETE /relclilis/1.json
  def destroy
    @relclili.destroy
    respond_to do |format|
      format.html { redirect_to relclilis_url, notice: 'Relclili was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El cliente se ha borrado de forma exitosa.'} #ajax

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relclili
      @relclili = Relclili.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def relclili_params
      params.require(:relclili).permit(:CodCliente, :ListaP, :ListaD, :ListaPromo, :IdEmpresa)
    end
end
