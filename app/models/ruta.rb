class Ruta < ActiveRecord::Base
  self.primary_key = "IdRutas"
  self.table_name = 'Rutas' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :primeros, ->{ order("IdRutas ASC").limit(20) }
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Activa: true) }
  scope :inactivos, -> { where(Activa: false) }
  scope :datos_ruta, ->(param_id) { find_by(IdRutas: param_id)} # para que no devuelva una coleccion, sino un solo registro
  scope :comprobar_existencia, ->(paramc) { where(Ruta: paramc)} # model
  scope :without_mensaje, -> (id_params) {Ruta.includes(:relmens).references(:relmens).where("relmens.IDRow IS NULL OR rutas.IdRutas not in (select distinct CodRuta from relmens where IdRuta = ?)", id_params)}


  validates :Ruta, presence: true  #presence true validara que el elemento no se pueda guardar vacio y uniquenes para que no se repita en la base de datos
  validates :Oficina, presence: true
  validates :Sector, presence: true
  validates :TipoRuta, presence: true

  # route model
  has_many :clientes, through: :relclirutas, source: :cliente, foreign_key: "IdCliente"

  belongs_to :coche, class_name: "Vehiculo", foreign_key: "vehiculo"

#campos con 2 foreign_key en la tabla Catgrupo
  belongs_to :sector, class_name:"Catgrupo", foreign_key: "Sector"
  belongs_to :tiporuta, class_name:"Catgrupo", foreign_key: "TipoRuta"

  belongs_to :asistente1, class_name: "Vendedor", foreign_key: "id_ayudante1"
  belongs_to :asistente2, class_name: "Vendedor", foreign_key: "id_ayudante2"

  has_one :reldayc, class_name: "Reldayc", foreign_key: "RutaId"

  has_many :relmens, class_name: "Relmen", foreign_key: "CodRuta"
  belongs_to :vendedor, class_name:"Vendedor", foreign_key: "Vendedor"

  has_one :configruta, class_name: "Configruta", foreign_key: "RutaId"
  accepts_nested_attributes_for :configruta

  has_one :continuid, class_name: "Continuid", foreign_key: "RutaID"
  accepts_nested_attributes_for :continuid

  has_one :relrucla, class_name: "Relrucla", foreign_key: "RutaId"
  accepts_nested_attributes_for :relrucla

  has_one :cobran, class_name: "Cobran", foreign_key: "RutaId"
  has_many :relclirutas, class_name: "Relcliruta", foreign_key: "IdRuta"
  has_many :relvendrutas, class_name: "Relvendruta", foreign_key: "IdRuta"
  accepts_nested_attributes_for :relvendrutas

  #Reportes
  has_many :venta, class_name: "Venta", foreign_key: "RutaId"
  has_many :detallevet, class_name: "Detalleve", foreign_key: "RutaId"
  has_many :pregalado, class_name: "Pregalad", foreign_key: "RutaId"
  has_many :bitacoratiempos, class_name: "Bitacoratiempo", foreign_key: "RutaId"
  has_one :noventasingular, class_name: "Noventasingular", foreign_key: "RutaId"
  has_many :recarga, class_name: "Recarg", foreign_key: "IdRuta"


  def self.tripulacion(params)
    query = select("DISTINCT BitacoraTiempos.RutaId, Rutas.Ruta, BitacoraTiempos.DiaO, BitacoraTiempos.HI AS Fecha, BitacoraTiempos.IdVendedor, ISNULL(Vendedores.Clave, '') AS 'ClaveVendedor', ISNULL(Vendedores.Nombre, '') AS 'NombreVendedor', BitacoraTiempos.Id_Ayudante1, ISNULL(Ayudantes_1.Clave, '') AS 'ClaveAyudante1', ISNULL(Ayudantes_1.Nombre, '') AS 'NombreAyudante1', ISNULL(Ayudantes_1.Nombre, '') AS 'NombreAyudante1',  BitacoraTiempos.Id_Ayudante2, ISNULL(Ayudantes_2.Clave, '') AS 'ClaveAyudante2',ISNULL(Ayudantes_2.Nombre, '') AS 'NombreAyudante2', Medidores.OdometroInicial, Medidores.OdometroFinal, Medidores.TanqueInicial, Medidores.TanqueFinal, Medidores.LitrosCargados, Medidores.GastoLitros, Medidores.Rendimiento, Medidores.KmR, Vehiculos.Clave AS Vehiculo_clave, Vehiculos.Placas AS Placa")
           .joins('INNER JOIN BitacoraTiempos INNER JOIN Vendedores ON BitacoraTiempos.IdVendedor = Vendedores.IdVendedor AND BitacoraTiempos.IdEmpresa = Vendedores.IdEmpresa INNER JOIN DiasO ON BitacoraTiempos.DiaO = DiasO.DiaO AND BitacoraTiempos.RutaId = DiasO.RutaId AND BitacoraTiempos.IdEmpresa = DiasO.IdEmpresa ON Rutas.IdRutas = BitacoraTiempos.RutaId LEFT JOIN Medidores ON DiasO.DiaO = Medidores.DiaO AND DiasO.RutaId = Medidores.IdRuta AND DiasO.IdEmpresa = Medidores.IdEmpresa LEFT OUTER JOIN vendedores  as Ayudantes_1 ON BitacoraTiempos.Id_Ayudante1 = Ayudantes_1.IdVendedor LEFT OUTER JOIN vendedores AS Ayudantes_2 ON BitacoraTiempos.Id_Ayudante2 = Ayudantes_2.IdVendedor INNER JOIN Vehiculos ON Vehiculos.IdVehiculos = Medidores.IdVehiculo')
           .where("(bitacoratiempos.Codigo = :Codigo) AND (bitacoratiempos.RutaId = :rutaId or :rutaId = '') AND (bitacoratiempos.IdEmpresa = :idempresa)",{Codigo: 'A18253', rutaId: params[:search], idempresa: params[:search6]})
           .order('BitacoraTiempos.HI DESC, BitacoraTiempos.RutaId, BitacoraTiempos.DiaO')
    query = query.where('BitacoraTiempos.HI between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?

     query = query.where("(Rutas.Ruta LIKE :Ruta or :Ruta = '') AND (BitacoraTiempos.DiaO LIKE :DiaO or :DiaO = '') AND (Vendedores.Clave LIKE :ClaveVendedor or :ClaveVendedor = '') AND (Vendedores.Nombre LIKE :NombreVendedor or :NombreVendedor = '')", {Ruta: "%#{params[:Ruta_param]}%", DiaO: "%#{params[:DiaO_param]}%", ClaveVendedor: "%#{params[:ClaveVendedor_param]}%", NombreVendedor: "%#{params[:NombreVendedor_param]}%", ClaveAyudante1: "%#{params[:ClaveAyudante1_param]}%", NombreAyudante1: "%#{params[:NombreAyudante1_param]}%", ClaveAyudante2: "%#{params[:ClaveAyudante2_param]}%", NombreAyudante2: "%#{params[:NombreAyudante2_param]}%"})

     query = query.where('BitacoraTiempos.HI between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?

      query = query.where("(Medidores.OdometroInicial LIKE :OdometroInicial or :OdometroInicial = '') AND (Medidores.OdometroFinal LIKE :OdometroFinal or :OdometroFinal = '') AND (Medidores.LitrosCargados LIKE :LitrosCargados or :LitrosCargados = '')  AND (Medidores.GastoLitros LIKE :GastoLitros or :GastoLitros = '') AND (Medidores.Rendimiento LIKE :Rendimiento or :Rendimiento = '')  AND (Medidores.KmR LIKE :KmR or :KmR = '')",
      {OdometroInicial: "%#{params[:OdometroInicial_param]}%", OdometroFinal: "%#{params[:OdometroFinal_param]}%", LitrosCargados: "%#{params[:LitrosCargados_param]}%", GastoLitros: "%#{params[:GastoLitros_param]}%", Rendimiento: "%#{params[:Rendimiento_param]}%", KmR: "%#{params[:KmR_param]}%"})
    query
  end




  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |ruta|
        csv << ruta.attributes.values_at(*column_names)
      end
    end
  end


  def self.import(file,empresa, fecha_actual_mexico)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]

      @sector = Catgrupo.ruta_grupo.find_by_Descripcion(row["Sector"])
      @tiporuta = Catgrupo.ruta_tipo.find_by_Descripcion(row["Tipo Ruta"])

      ruta = (find_by_IdRutas(row["Id"]) and find_by_Ruta(row["Número de Ruta"]))  || new
      ruta.attributes = {Ruta: row["Número de Ruta"], TipoRuta: @tiporuta.try(:Clave), Oficina: row["Oficina"], Vendedor: row["Vendedor"], vehiculo: row["Vehiculo"], Sector: @sector.try(:Clave), Activa: row["Activa"], IdEmpresa: empresa}

        if ruta.continuid != nil
          ruta.continuid.attributes = {IdEmpresa: empresa}
        else
          ruta.continuid_attributes = {IdEmpresa: empresa}
        end

        if ruta.relrucla != nil
          ruta.relrucla.attributes = {IdEmpresa: empresa}
        else
          ruta.relrucla_attributes = {IdEmpresa: empresa}
        end


      if ruta.save
        @vendedor = Relvendruta.find_by(IdVendedor:row["Vendedor"],IdRuta:ruta.IdRutas)
        if @vendedor == nil
          Relvendruta.create(IdRuta: ruta.IdRutas, IdVendedor: row["Vendedor"], Fecha: fecha_actual_mexico, IdEmpresa: empresa )
        end
      else
        ruta.errors.full_messages.each do |message|
          @errors << "Error fila #{i}, columna #{message}"
        end
      end

    end
    @errors #  <- need to return the @errors array
  end

  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end


end
