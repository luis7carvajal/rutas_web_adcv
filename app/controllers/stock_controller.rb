class StockController < ApplicationController
  before_action :set_stoc, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Proceso de carga de stock"
  end
  # GET /stock
  # GET /stock.json
  def index
    params[:search6] =  current_usuario.empresa_id
    @pathDeBusqueda = stock_path # esto es usando para colocar el path dentro de las tablas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @controlador = 'stock'
    @accion = 'index'
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])

    proceso_creacion_productos_para_stock
    registros_stock
    total_piezas_stock


    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @stockE.to_csv}
      format.xls
    end

  end

  def proceso_creacion_productos_para_stock
    params[:search6] =  current_usuario.empresa_id
    @ruta_parametro = Ruta.find_by_IdRutas(params[:search])

    if @ruta_parametro != nil
      @productos = Producto.proceso_stock_busqueda_productos(params)

     @productos.each do |producto|
       Stoc.create(Articulo: producto.Clave, Ruta:params[:search], Stock: 0, IdEmpresa:current_usuario.empresa_id)
      end
    end
  # IMPORTANTE debido a que se usan dos busquedasm siendo la primera la busqueda principal y la segunda la busqueda de datos dentro de la tabla se deben colocar los parametros ruta y empresa en el formulario de busqueda de datos de la gema dentro de la tabla porque no toma los datos de la busqueda principal
    #registros_stock
    #@stockE = Stoc.por_ruta(params[:id])
    #session[:parametro] = (params[:id])#pasando el parametro a una variable de sesion para pasar el dia al update y evitar que se pierda el dia al actualizar

  end

  def registros_stock#Debo hacerlo de esta forma porque si lo coloco en la busqueda entonces a la hora de pasar de pagina con el kaminari me muestra un error porque me redirige a una "pagina" llamada busqueda_ctasxcobrar que no existe debido que asi se llama el metodo de busqueda
      params[:search6] =  current_usuario.empresa_id

    #ADEMAS DE ESTO DEBO ESPECIFICAR EN EL PARCIAL EN LA PARTE DE LA PAGINACION A QUE CONTROLADOR Y METODO VA A RESPONDER
    @search = Stoc.productos_de_un_stock(params).search(search_params)
    @search.sorts = 'producto_Producto ASC' if @search.sorts.empty?
    @stock = @search.result().page(params[:stock]).per(15)

    @stockE = Stoc.productos_de_un_stock(params)
  end

  def import
    empresa = current_usuario.empresa_id
    @errors = Stoc.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to stock_path, notice: "Stock modificado."
    end
  end

  # GET /stock/1
  # GET /stock/1.json
  def show
  end

  # GET /stock/new
  def new
    @stoc = Stoc.new
  end

  # GET /stock/1/edit
  def edit
  end

  # POST /stock
  # POST /stock.json
  def create
    @stoc = Stoc.new(stoc_params)

    respond_to do |format|
      if @stoc.save
        format.html { redirect_to @stoc, notice: 'Stoc was successfully created.' }
        format.json { render :show, status: :created, location: @stoc }
      else
        format.html { render :new }
        format.json { render json: @stoc.errors, status: :unprocessable_entity }
      end
    end
  end



  # PATCH/PUT /stock/1
  # PATCH/PUT /stock/1.json
  def update
    params[:search] = @stoc.Ruta
    params[:search6] = @stoc.IdEmpresa
    params[:Producto] = @stoc.Articulo

    respond_to do |format|
      if @stoc.update(stoc_params)
        #para pasar a piezas
        @pzas = (@stoc.producto.productosxpza.PzaXCja * (params[:f_cajas]).to_f) + (params[:f_piezas]).to_f # .to_f para pasar los parametros de string a integer
        @Stock_almacen = Stockalmacen.find_by(Articulo: @stoc.Articulo, IdEmpresa: @stoc.IdEmpresa)


        # debe haber disponibilidad del producto en stock almacen para almacenar en stock y debe descontarse
        @stoc = Stoc.productos_de_un_stock(params).find_by(IdStock: @stoc.IdStock)
        if (@Stock_almacen != nil) && (@Stock_almacen.try(:Stock) >= @pzas)
          @stoc.update(Stock:@pzas)

          # debe actualizar el envase si el producto tiene un envase
          @envase = Stoc.stock_envase_producto(params)
          @envase.each do |e|
           Stoc.find_by(Ruta: e.Ruta, Articulo: e.Envase, IdEmpresa: e.IdEmpresa).update(Stock: e.Stock)
          end

          total_piezas_stock
          @Stock_almacen.update(Stock:(@Stock_almacen.Stock - @pzas))
          format.js {flash.now[:notice] = 'El stock se ha actualizado de forma exitosa.'} #ajax
        else
          format.js {flash.now[:alert] = "No hay disponibilidad en stock almacén."} #ajax
        end
        format.html { redirect_to @stoc, notice: 'Stoc was successfully updated.' }
        format.json { render :show, status: :ok, location: @stoc }
      else
        format.html { render :edit }
        format.json { render json: @stoc.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el stock.'} #ajax
      end
    end
  end

  def total_piezas_stock
    @total_piezas_stock = Stoc.productos_de_un_stock(params).sum("Stock")
    @total_cajas = Stoc.productos_de_un_stock(params).es_producto.map(&:Cajas).compact.sum
    @total_piezas = Stoc.productos_de_un_stock(params).es_producto.map(&:Piezas).compact.sum
    @total_cajas_envase = Stoc.productos_de_un_stock(params).es_envase.map(&:Cajas).compact.sum
    @total_piezas_envase = Stoc.productos_de_un_stock(params).es_envase.map(&:Piezas).compact.sum
  end

  # DELETE /stock/1
  # DELETE /stock/1.json
  def destroy
    @stoc.destroy
    respond_to do |format|
      format.html { redirect_to stock_url, notice: 'Stoc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stoc
      @stoc = Stoc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stoc_params
      params.require(:stoc).permit(:Articulo, :Stock, :Ruta, :IdEmpresa)
    end
end
