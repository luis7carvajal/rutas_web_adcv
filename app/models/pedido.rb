class Pedido < ActiveRecord::Base
  self.primary_key = "IdPedido"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "Ruta"
  scope :busqueda_por_pedido, ->(paramp) { where(Pedido: paramp)} # model
  scope :busqueda_por_ruta, ->(paramr) { where(Ruta: paramr)} # model
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :entregados, -> { where(Status: 5) }
  scope :contado, -> { where(Tipo: "Contado") }
  scope :credito, -> { where(Tipo: "Credito") }
  scope :contadoycredito, -> { where("pedidos.Tipo = ? or pedidos.Tipo = ?", "Contado", "Credito") }
  scope :sin_obsequios, -> { where("pedidos.Tipo!='Obsequio'") }
  scope :con_obsequios, -> { where("pedidos.Tipo='Obsequio'") }
  scope :sin_cancelados, -> { where("pedidos.Cancelado='0'") }



  def self.consolidado(params, pedidos)
    pedidos = pedidos.join(',')
    sql = "SELECT SKU Clave,
                  (SELECT Producto FROM Productos WHERE Clave = consolidado.SKU AND IdEmpresa = #{params[:search6]}) Producto,
                  SUM(Cajas) Cajas,
                  SUM(Piezas) Piezas,
                  (SELECT PzaXCja FROM ProductosXPzas WHERE Producto = consolidado.SKU) PiezasXProducto
            FROM (
              SELECT  SKU, SUM(Cajas) Cajas, SUM(Pzas) Piezas
              FROM V_CantidadxPedido
              WHERE RutaId = #{params[:search]} AND IdEmpresa = #{params[:search6]}
              AND Docto IN (#{pedidos})
              GROUP BY SKU
              UNION
              SELECT SKU, SUM(Cant) Cajas, 0 Pzas
              FROM PRegalado
              WHERE Tipo = 'P' AND TipMed = 'Caja'
              AND RutaId = #{params[:search]} AND IdEmpresa = #{params[:search6]}
              AND Docto IN (#{pedidos})
              GROUP BY SKU
              UNION
              SELECT SKU, 0 Cajas, SUM(Cant) Pzas
              FROM PRegalado
              WHERE Tipo = 'P' AND TipMed = 'Pzas'
              AND RutaId = #{params[:search]} AND IdEmpresa = #{params[:search6]}
              AND Docto IN (#{pedidos})
              GROUP BY SKU
            ) consolidado
            GROUP BY SKU"
    query = ActiveRecord::Base.connection.select_all(sql)
  end

  def self.obtener_ids(params)
    query = select(:Pedido, :IdPedido)
    query = query.where('IdEmpresa = ?', params[:search6]) if params[:search6].present?
    query = query.where('Ruta = ?', params[:search]) if params[:search].present?
    query = query.where("Status = ? AND Cancelado = 0", params[:status]) if params[:status].present? and params[:status].to_i < 6
    query = query.where("Cancelado = 1") if params[:status].present? and params[:status].to_i.eql?(6)
    query = query.where('Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query.distinct
  end

  def self.datos_cliente(params)
    query = select("Clientes.NombreCorto, Clientes.Direccion, Clientes.Email, Clientes.RFC, Clientes.IdCli")
            .joins("LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente")
            .where("Pedidos.Cancelado = 0 AND Pedidos.IdPedido = ?", params[:IdPedido])
  end

  def self.pedido_dashboard2(params)
   query =  joins("inner join pedidosliberados on pedidos.Pedido=pedidosliberados.pedido and pedidos.Ruta=pedidosliberados.ruta and pedidos.CodCliente = pedidosliberados.cod_cliente and pedidos.IdEmpresa = pedidosliberados.idempresa")
            .where("pedidos.Cancelado = 0")
  query = query.where('(pedidos.Fecha between ? and ?) AND (pedidos.IdEmpresa = ?)', (Time.current.beginning_of_day-1.day), (Time.current.end_of_day), (params[:search0]))
  query
  end

  def self.pedidos_levantados(params)
    query = select('Convert(varchar(10),CONVERT(date,Fecha,106),103) AS fecha, count(*) AS NoPedidos')
            .where("Cancelado = ?", 0)
            .group("month(fecha),Convert(varchar(10),CONVERT(date,Fecha,106),103)")
            .order("month(fecha)")
    query = query.where('(Fecha between ? and ?) AND (IdEmpresa = ?)', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T'), (params[:search0])) if params[:search1].present? and params[:search2].present?
    query
  end

  def self.visor_de_pedidos(params)
    if params[:commit].present?
      query = select("Pedidos.Ruta AS Id_Ruta, Pedidos.Tipo,[rutas].Ruta AS Ruta_Ruta, Pedidos.CodCliente AS Cliente_IdCli, [clientes].NombreCorto AS Cliente_NombreComercial, [pedidos].Pedido, [pedidos].IdPedido, [pedidos].Fecha As Pedidos_Fecha, [pedidos].FechaEntrega As Pedidos_Fecha_Compromiso, isnull((SELECT SUM(Total) FROM DetallePedidos WHERE Docto = Pedidos.Pedido AND RutaId = Pedidos.Ruta AND IdEmpresa = Pedidos.IdEmpresa AND Pedidos.Cancelado = 0 AND Pedidos.Tipo != 'Obsequio'),0) Total, isnull((SELECT SUM(Total) FROM DetallePedidos WHERE Docto = Pedidos.Pedido AND RutaId = Pedidos.Ruta AND IdEmpresa = Pedidos.IdEmpresa AND Pedidos.Cancelado = 0 AND Pedidos.Tipo != 'Obsequio'),0) Total_calculado, CASE  WHEN [Pedidos].Status=1 AND [Pedidos].Cancelado = 0 THEN 'PENDIENTE'  WHEN [Pedidos].Status=2 AND [Pedidos].Cancelado = 0 THEN 'LIBERADO' WHEN [Pedidos].Status=3 AND [Pedidos].Cancelado = 0 THEN 'SURTIDO' WHEN [Pedidos].Status=4 AND [Pedidos].Cancelado = 0 THEN 'EN TRANSITO' WHEN [Pedidos].Status=5 AND [Pedidos].Cancelado = 0 THEN 'ENTREGADO' WHEN [Pedidos].Cancelado = 1 THEN 'CANCELADO' END AS Status_Pedido, CASE WHEN Pedidos.Tipo='Obsequio' THEN 0 ELSE
(SELECT SUM(Cajas) FROM V_CantidadxPedido WHERE Docto = Pedidos.Pedido AND idempresa = Pedidos.IdEmpresa AND Rutaid = Pedidos.Ruta) END AS Cajas, CASE WHEN Pedidos.Tipo='Obsequio' THEN 0 ELSE (SELECT SUM(Pzas) FROM V_CantidadxPedido WHERE Docto = Pedidos.Pedido AND idempresa = Pedidos.IdEmpresa AND Rutaid = Pedidos.Ruta) END AS Pzas, [Pedidosliberados].FECHA_PEDIDO_ENTREGADO AS Pedidos_Fecha_Entrega, Pedidos.Cancelado, Clientes.Credito Credito, 0 Stock, [clientes].Email")
              .select("CASE WHEN Pedidos.Tipo='Obsequio' THEN (SELECT SUM(Cajas) FROM V_CantidadxPedido WHERE Docto = Pedidos.Pedido AND idempresa = Pedidos.IdEmpresa AND Rutaid = Pedidos.Ruta) ELSE COALESCE((SELECT SUM(Cajas_Total) Promociones_Cajas FROM (SELECT CASE WHEN ProductosXPzas.PzaXCja='1' THEN 0 ELSE CASE  WHEN PRegalado.TipMed='Caja' THEN ISNULL(SUM(PRegalado.Cant),0) ELSE ISNULL(SUM(PRegalado.Cant), 0)/ProductosXPzas.PzaXCja   END END AS Cajas_Total FROM PRegalado LEFT JOIN ProductosXPzas ON pregalado.SKU = ProductosXPzas.Producto WHERE PRegalado.IdEmpresa = [Pedidos].IdEmpresa AND PRegalado.RutaId = [Pedidos].Ruta AND PRegalado.Docto = [Pedidos].Pedido AND PRegalado.Tipo = 'P' GROUP BY ProductosXPzas.PzaXCja, PRegalado.TipMed) Promociones), 0) END  AS Promociones_Cajas")
              .select("CASE WHEN Pedidos.Tipo='Obsequio' THEN (SELECT SUM(Pzas) FROM V_CantidadxPedido WHERE Docto = Pedidos.Pedido AND idempresa = Pedidos.IdEmpresa AND Rutaid = Pedidos.Ruta) ELSE COALESCE((SELECT SUM(Piezas_Total) Promociones_Pzas FROM (SELECT CASE WHEN ProductosXPzas.PzaXCja='1' THEN ISNULL(SUM(PRegalado.Cant),0) ELSE CASE WHEN  PRegalado.TipMed='Pzas' THEN ISNULL(SUM(PRegalado.Cant)%ProductosXPzas.PzaXCja,0) ELSE 0 END END AS Piezas_Total FROM PRegalado LEFT JOIN ProductosXPzas  ON pregalado.SKU = ProductosXPzas.Producto WHERE PRegalado.IdEmpresa = [Pedidos].IdEmpresa AND PRegalado.RutaId = [Pedidos].Ruta AND PRegalado.Docto = [Pedidos].Pedido AND PRegalado.Tipo = 'P' GROUP BY ProductosXPzas.PzaXCja, PRegalado.TipMed) Promociones), 0) END AS  Promociones_Pzas")
              .joins("LEFT JOIN detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave")
              .joins('LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente AND Clientes.IdEmpresa = Pedidos.IdEmpresa LEFT JOIN Rutas ON Rutas.IdRutas  = Pedidos.Ruta AND Rutas.IdEmpresa = Pedidos.IdEmpresa LEFT JOIN Pedidosliberados ON Pedidosliberados.PEDIDO = Pedidos.Pedido AND Pedidosliberados.IDEMPRESA = Pedidos.IdEmpresa AND Pedidosliberados.Ruta = Pedidos.Ruta')
      if params[:Tipo_Fecha] == "Fech.Entrega"
        query = query.joins('LEFT JOIN PedidosLiberados PL ON PL.PEDIDO = Pedidos.Pedido AND PL.RUTA = Pedidos.Ruta AND PL.IDEMPRESA = Pedidos.IdEmpresa')
        query = query.where('PedidosLiberados.FECHA_PEDIDO_ENTREGADO between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      else
        query = query.where('pedidos.Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      end
      query = query.where("pedidos.IdEmpresa = ? ", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Ruta = ? ", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where("pedidos.Pedido like :pedido", {:pedido => "%#{params[:Pedido_param]}%"}) if params[:Pedido_param].present?
      query = query.where("pedidos.Tipo like :tipo", {:tipo => "%#{params[:Tipo_param]}%"}) if params[:Tipo_param].present?
      query = query.where("Clientes.IdCli like :cliente", {:cliente => "%#{params[:Cliente_IdCli_param]}%"}) if params[:Cliente_IdCli_param].present?
      query = query.where("Clientes.NombreCorto like :nombre", {:nombre => "%#{params[:Cliente_NombreComercial_param]}%"}) if params[:Cliente_NombreComercial_param].present?
      query = query.where('pedidos.Fecha between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?
      query = query.where('pedidos.FechaEntrega between ? and ?', (params[:FechaEntrega_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaEntrega_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaEntrega_param].present?
      query = query.where("(productos.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (productos.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
      query = query.distinct
    else
      query = select("0").where("IdEmpresa = NULL")
    end
    query
  end

  def self.cajas_piezas_entregas(params)
    if params[:commit].present?
      query = select("isnull(SUM(V_CantidadxPedidoxGrupo.piezas)/V_CantidadxPedidoxGrupo.PzaXCja, 0) AS Cajas_Total,
                      isnull(SUM(V_CantidadxPedidoxGrupo.piezas)%V_CantidadxPedidoxGrupo.PzaXCja, 0) AS Piezas_Total")
              .joins('LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente AND Clientes.IdEmpresa = Pedidos.IdEmpresa LEFT JOIN Rutas ON Rutas.IdRutas  = Pedidos.Ruta AND Rutas.IdEmpresa = Pedidos.IdEmpresa
                      inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave
                      LEFT JOIN Pedidosliberados ON Pedidosliberados.PEDIDO = Pedidos.Pedido AND Pedidosliberados.IDEMPRESA = Pedidos.IdEmpresa AND Pedidosliberados.Ruta = Pedidos.Ruta inner join V_CantidadxPedidoxGrupo ON detallepedidos.SKU = V_CantidadxPedidoxGrupo.SKU
                      AND detallepedidos.Docto = V_CantidadxPedidoxGrupo.Docto AND V_CantidadxPedidoxGrupo.IdEmpresa=detallepedidos.IdEmpresa and V_CantidadxPedidoxGrupo.RutaId = detallepedidos.RutaId')
      if params[:Tipo_Fecha] == "Fech.Entrega"
        query = query.where('PedidosLiberados.FECHA_PEDIDO_ENTREGADO between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      else
        query = query.where('pedidos.Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      end
      query = query.where("pedidos.IdEmpresa = ? ", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Ruta = ? ", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where("pedidos.Pedido like :pedido", {:pedido => "%#{params[:Pedido_param]}%"}) if params[:Pedido_param].present?
      query = query.where("pedidos.Tipo like :tipo", {:tipo => "%#{params[:Tipo_param]}%"}) if params[:Tipo_param].present?
      query = query.where("Clientes.IdCli like :cliente", {:cliente => "%#{params[:Cliente_IdCli_param]}%"}) if params[:Cliente_IdCli_param].present?
      query = query.where("Clientes.NombreCorto like :nombre", {:nombre => "%#{params[:Cliente_NombreComercial_param]}%"}) if params[:Cliente_NombreComercial_param].present?
      query = query.where('pedidos.Fecha between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?
      query = query.where('pedidos.FechaEntrega between ? and ?', (params[:FechaEntrega_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaEntrega_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaEntrega_param].present?
      query = query.where("(productos.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (productos.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
      query = query.group("V_CantidadxPedidoxGrupo.PzaXCja")
    else
      query = select("0").where("IdEmpresa = NULL")
    end
    query
  end

  def self.visor_de_pedidos_total(params)
    if params[:commit].present?
      query = select("COALESCE(SUM(DetallePedidos.Total), 0) AS Totals")
      query = query.joins("INNER JOIN DetallePedidos ON DetallePedidos.RutaId = pedidos.Ruta AND DetallePedidos.Docto = pedidos.Pedido AND DetallePedidos.IdEmpresa = pedidos.IdEmpresa")
      query = query.where("pedidos.IdEmpresa = ?", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Ruta = ?", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where('pedidos.Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query
    else
      select("SUM(0) AS Totals")
    end
  end


  def self.visor_pedidos_total_clientes(params)
    if params[:commit].present?
      query = select("DISTINCT Pedidos.CodCliente")
              .joins('LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente AND Clientes.IdEmpresa = Pedidos.IdEmpresa LEFT JOIN Rutas ON Rutas.IdRutas  = Pedidos.Ruta AND Rutas.IdEmpresa = Pedidos.IdEmpresa LEFT JOIN Pedidosliberados ON Pedidosliberados.PEDIDO = Pedidos.Pedido AND Pedidosliberados.IDEMPRESA = Pedidos.IdEmpresa AND Pedidosliberados.Ruta = Pedidos.Ruta')
              .joins("inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave ")
      if params[:Tipo_Fecha] == "Fech.Entrega"
        query = query.joins('LEFT JOIN PedidosLiberados PL ON PL.PEDIDO = Pedidos.Pedido AND PL.RUTA = Pedidos.Ruta AND PL.IDEMPRESA = Pedidos.IdEmpresa')
        query = query.where('PedidosLiberados.FECHA_PEDIDO_ENTREGADO between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      else
        query = query.where('pedidos.Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      end
      query = query.where("pedidos.IdEmpresa = ? ", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Ruta = ? ", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where("pedidos.Pedido like :pedido", {:pedido => "%#{params[:Pedido_param]}%"}) if params[:Pedido_param].present?
      query = query.where("pedidos.Tipo like :tipo", {:tipo => "%#{params[:Tipo_param]}%"}) if params[:Tipo_param].present?
      query = query.where("Clientes.IdCli like :cliente", {:cliente => "%#{params[:Cliente_IdCli_param]}%"}) if params[:Cliente_IdCli_param].present?
      query = query.where("Clientes.NombreCorto like :nombre", {:nombre => "%#{params[:Cliente_NombreComercial_param]}%"}) if params[:Cliente_NombreComercial_param].present?
      query = query.where('pedidos.Fecha between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?
      query = query.where('pedidos.FechaEntrega between ? and ?', (params[:FechaEntrega_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaEntrega_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaEntrega_param].present?
      query = query.where("(productos.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (productos.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
      query = query.distinct
    else
      query = select("0").where("IdEmpresa = NULL")
    end
    query
  end






    def self.pedido_para_consolidar(params)
      query = select("[rutas].IdRutas AS Id_Ruta, [rutas].Ruta AS Ruta_Ruta, [clientes].IdCli AS Cliente_IdCli, [clientes].NombreCorto AS Cliente_NombreComercial, [pedidos].Pedido, [pedidos].IdPedido, [pedidos].Fecha As Pedidos_Fecha, [pedidos].FechaEntrega As Pedidos_Fecha_Compromiso, [pedidos].Total, CASE  WHEN  [pedidos].Status=1 THEN 'PENDIENTE'  WHEN [pedidos].Status=2 THEN 'LIBERADO' WHEN [pedidos].Status=3 THEN 'SURTIDO' WHEN [pedidos].Status=4 THEN 'EN TRANSITO' WHEN [pedidos].Status=5 THEN 'ENTREGADO' END AS Status_Pedido, SUM(V_CantidadxPedido.Cajas) AS Cajas, SUM(V_CantidadxPedido.Pzas) AS Pzas, [Pedidosliberados].FECHA_PEDIDO_ENTREGADO AS Pedidos_Fecha_Entrega")
              .joins('INNER JOIN Clientes ON Pedidos.CodCliente = Clientes.IdCli AND Pedidos.IdEmpresa = Clientes.IdEmpresa INNER JOIN Rutas ON Pedidos.Ruta = Rutas.IdRutas AND Pedidos.IdEmpresa = Rutas.IdEmpresa INNER JOIN V_CantidadxPedido ON Pedidos.Pedido = V_CantidadxPedido.Docto AND Pedidos.IdEmpresa = V_CantidadxPedido.idempresa AND Pedidos.Ruta = V_CantidadxPedido.Rutaid  INNER JOIN Pedidosliberados ON Pedidos.Pedido = Pedidosliberados.PEDIDO AND Pedidos.IdEmpresa = Pedidosliberados.IDEMPRESA AND Pedidos.Ruta = Pedidosliberados.Ruta')
              .where('(pedidos.IdPedido IN (?)) AND (pedidos.IdEmpresa = ?)', params[:pedidos_para_consolidar], params[:search6])
              .group("rutas.IdRutas, rutas.ruta, clientes.IdCli, clientes.NombreCorto, pedidos.Pedido, pedidos.IdPedido, pedidos.Fecha, pedidos.FechaEntrega, pedidos.Total, pedidos.Status, pedidos.IdPedido")
              .group(:FECHA_PEDIDO_ENTREGADO)
    end

    def self.pedidos_listos_para_consolidar(params)
      query = select("detallepedidos.SKU AS P_Clave, SUM(V_CantidadxPedido.Cajas) AS Cajas, SUM(V_CantidadxPedido.Pzas) AS Piezas")
              .joins('inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId join V_CantidadxPedido ON detallepedidos.SKU = V_CantidadxPedido.SKU AND detallepedidos.Docto = V_CantidadxPedido.Docto AND
              V_CantidadxPedido.IdEmpresa=detallepedidos.IdEmpresa  and  V_CantidadxPedido.RutaId = detallepedidos.RutaId')
              .where('(pedidos.IdPedido IN (?)) AND (pedidos.IdEmpresa = ?)', params[:pedidos_para_consolidar], params[:search6])
              .group("detallepedidos.SKU")
      query
    end

  def self.detalle_visor_de_pedidos(params)
    query = select("(SELECT Ruta FROM Rutas WHERE IdRutas = pedidos.Ruta) RutaPedido, [pedidos].Pedido, [pedidos].CodCliente, [pedidos].Fecha, [pedidos].FechaEntrega AS Fecha_Entrega, [productos].Clave AS Producto_Clave, [productos].Producto AS Producto_Producto, V_CantidadxPedido.Cajas AS Cajas, V_CantidadxPedido.Pzas AS Piezas, [detallepedidos].Precio AS DetallePedidos_Precio, CASE  WHEN  pedidos.tipo !='Obsequio' THEN [detallepedidos].Importe ELSE 0 END AS DetallePedidos_Importe, CASE  WHEN  pedidos.tipo !='Obsequio' THEN [detallepedidos].IVA ELSE 0 END AS DetallePedidos_IVA, CASE  WHEN  pedidos.tipo !='Obsequio' THEN [detallepedidos].DesctoV ELSE 0 END AS DetallePedidos_Descuento, CASE  WHEN  pedidos.tipo !='Obsequio' THEN [detallepedidos].Total ELSE 0 END AS DetallePedidos_Total, ISNULL([detallepedidos].Importe, 0) + ISNULL([detallepedidos].IVA, 0) - ISNULL([detallepedidos].DesctoV, 0) AS Total, CASE  WHEN  [pedidos].Status=1 THEN 'PENDIENTE'  WHEN [pedidos].Status=2 THEN 'LIBERADO' WHEN [pedidos].Status=3 THEN 'SURTIDO' WHEN [pedidos].Status=4 THEN 'EN TRANSITO' WHEN [pedidos].Status=5 THEN 'ENTREGADO' END AS StatusDetalle, DetallePedidos.Pza AS Pza")
            .joins("inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave inner join V_CantidadxPedido ON detallepedidos.SKU = V_CantidadxPedido.SKU AND detallepedidos.Docto = V_CantidadxPedido.Docto AND V_CantidadxPedido.IdEmpresa=detallepedidos.IdEmpresa and V_CantidadxPedido.RutaId = detallepedidos.RutaId")
    query = query.where('pedidos.IdPedido = ?', params[:IdPedido]) if params[:IdPedido].present?
    query = query.where('pedidos.Pedido = ?', params[:Pedido]) if params[:Pedido].present?
    query = query.where('pedidos.Ruta = ?', params[:Ruta]) if params[:Ruta].present?
    query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
    query = query.where('pedidos.IdEmpresa = ?', params[:IdEmpresa]) if params[:IdEmpresa].present?
    query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
    query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
    query = query.where("(productos.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (productos.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
    #query = query.where('pedidos.Fecha between ? and ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query
  end



  def self.total_existencias(params)
    if params[:commit].present?
      query = select("isnull(SUM(V_CantidadxPedidoxGrupo.piezas)/V_CantidadxPedidoxGrupo.PzaXCja, 0) AS Cajas_Total, isnull(SUM(V_CantidadxPedidoxGrupo.piezas)%V_CantidadxPedidoxGrupo.PzaXCja, 0) AS Piezas_Total")
              .joins("LEFT JOIN Clientes ON Clientes.IdCli = Pedidos.CodCliente AND Clientes.IdEmpresa = Pedidos.IdEmpresa inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave
                      inner join V_CantidadxPedidoxGrupo ON detallepedidos.SKU = V_CantidadxPedidoxGrupo.SKU AND detallepedidos.Docto = V_CantidadxPedidoxGrupo.Docto AND V_CantidadxPedidoxGrupo.IdEmpresa=detallepedidos.IdEmpresa and V_CantidadxPedidoxGrupo.RutaId = detallepedidos.RutaId")
              .where("(pedidos.Cancelado='0')")
      query = query.where("pedidos.IdEmpresa = ?", params[:search6]) if params[:search6].present?
      query = query.where("pedidos.Ruta = ?", params[:rutaIdB]) if params[:rutaIdB].present?
      query = query.where("pedidos.Status = ? AND pedidos.Cancelado = 0", params[:Status]) if params[:Status].present? and params[:Status].to_i < 6
      query = query.where("[Pedidos].Cancelado = 1") if params[:Status].present? and params[:Status].to_i.eql?(6)
      query = query.where("pedidos.Fecha BETWEEN ? AND ?", (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
      query = query.where("pedidos.Pedido like :pedido", {:pedido => "%#{params[:Pedido_param]}%"}) if params[:Pedido_param].present?
      query = query.where("pedidos.Tipo like :tipo", {:tipo => "%#{params[:Tipo_param]}%"}) if params[:Tipo_param].present?
      query = query.where("Clientes.IdCli like :cliente", {:cliente => "%#{params[:Cliente_IdCli_param]}%"}) if params[:Cliente_IdCli_param].present?
      query = query.where("Clientes.NombreCorto like :nombre", {:nombre => "%#{params[:Cliente_NombreComercial_param]}%"}) if params[:Cliente_NombreComercial_param].present?
      query = query.where('pedidos.Fecha between ? and ?', (params[:Fecha_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:Fecha_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:Fecha_param].present?
      query = query.where('pedidos.FechaEntrega between ? and ?', (params[:FechaEntrega_param].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:FechaEntrega_param].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:FechaEntrega_param].present?
      query = query.where("(productos.Clave = :Clave or :Clave = '' or :Clave IS NULL) AND (productos.Producto = :Producto or :Producto = '' or :Producto IS NULL)",{Clave: params[:Clave_param], Producto: params[:Producto_param]})
      query = query.group('V_CantidadxPedidoxGrupo.PzaXCja')


    else
      query = select("0 AS Cajas_Total, 0 AS Piezas_Total").limit(1)
    end
    query
  end



  def self.detalle_promocionvisor_de_pedidos(params)
    query = select("[rutas].Ruta AS Ruta, [pedidos].Pedido, [pedidos].Fecha, [pedidos].FechaEntrega AS Fecha_Entrega, [productos].Clave AS Producto_Clave, [productos].Producto AS Producto_Producto, V_CantidadxPedido.Cajas AS Cajas, V_CantidadxPedido.Pzas AS Piezas, [detallepedidos].Precio AS DetallePedidos_Precio, [detallepedidos].Importe AS DetallePedidos_Importe, [detallepedidos].IVA AS DetallePedidos_IVA, [detallepedidos].DesctoV AS DetallePedidos_Descuento, [detallepedidos].Total AS DetallePedidos_Total, ISNULL([detallepedidos].Importe, 0) + ISNULL([detallepedidos].IVA, 0) - ISNULL([detallepedidos].DesctoV, 0) AS Total, CASE  WHEN  [pedidos].Status=1 THEN 'PENDIENTE'  WHEN [pedidos].Status=2 THEN 'LIBERADO' WHEN [pedidos].Status=3 THEN 'SURTIDO' WHEN [pedidos].Status=4 THEN 'EN TRANSITO' WHEN [pedidos].Status=5 THEN 'ENTREGADO' END AS StatusDetalle")
            .joins('inner join rutas ON pedidos.Ruta = rutas.IdRutas inner join detallepedidos ON pedidos.IdEmpresa = detallepedidos.IdEmpresa AND pedidos.Pedido = detallepedidos.Docto And pedidos.Ruta = detallepedidos.RutaId inner join productos on detallepedidos.SKU = productos.Clave inner join V_CantidadxPedido ON detallepedidos.SKU = V_CantidadxPedido.SKU AND detallepedidos.Docto = V_CantidadxPedido.Docto AND
            V_CantidadxPedido.IdEmpresa=detallepedidos.IdEmpresa  and  V_CantidadxPedido.RutaId = detallepedidos.RutaId')
            .where('pedidos.Pedido = ? AND pedidos.Ruta = ? AND pedidos.IdEmpresa = ?', (params[:Pedido]), (params[:Ruta]), (params[:IdEmpresa]))
    query
  end



  def self.cantidad_pedidos(params)
    query = select("CAST(Fecha AS DATE) as Fechax,  COUNT(CAST(Fecha AS DATE)) AS cantidad")
            .where('pedidos.IdEmpresa = ? and pedidos.Cancelado = ?', params[:search0], 0)
            .group('CAST(Fecha AS DATE)')
    query = query.where('pedidos.Fecha between ? and ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
  end

  def self.pedidosxtipo_venta(params)
    query = select("Tipo as Tipo_venta,  COUNT(Tipo) AS cantidad")
            .where('pedidos.IdEmpresa = ? and pedidos.Cancelado = ?', params[:search0], 0)
            .group('Tipo')
    query = query.where('pedidos.Fecha between ? and ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
  end

  def self.pedidosxdia(params)
    query = select("CASE WHEN DATENAME(dw, Fecha)='Monday' THEN 'Lunes' WHEN DATENAME(dw, Fecha)='Tuesday' THEN 'Martes' WHEN DATENAME(dw, Fecha)='Wednesday' THEN 'Miércoles' WHEN DATENAME(dw, Fecha)='Thursday' THEN 'Jueves'
                    WHEN DATENAME(dw, Fecha)='Friday' THEN 'Viernes' WHEN DATENAME(dw, Fecha)='Saturday' THEN 'Sábado' WHEN DATENAME(dw, Fecha)='Sunday' THEN 'Domingo' END as Fechax,  COUNT(DATENAME(dw, Fecha)) AS cantidad")
            .where('pedidos.IdEmpresa = ? and pedidos.Cancelado = ?', params[:search0], 0)
            .group('DATENAME(dw, Fecha)')
    query = query.where('pedidos.Fecha between ? and ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
  end

  def self.pedidosxclasif_cliente(params)
    query = select("relcliclas.Clas1 as Clase,  COUNT(relcliclas.Clas1) AS cantidad")
            .joins('inner join relcliclas ON pedidos.CodCliente = relcliclas.IdCliente')
            .where('pedidos.IdEmpresa = ? and pedidos.Cancelado = ?', params[:search0], 0)
            .group('relcliclas.Clas1')
    query = query.where('pedidos.Fecha between ? and ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
  end

  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |pedido|
        csv << pedido.attributes.values_at(*column_names)
      end
    end
  end

  def self.total_pedidos_grutas(params, empresa)
    query = select("DISTINCT Pedido")
            .where("([pedidos].Status = 3 OR [pedidos].Status = 4) AND [pedidos].Cancelado = 0 AND [pedidos].IdEmpresa = ? AND [pedidos].Ruta = ?", empresa, params[:search])
    query
  end

  def self.rep_bitiempos_busqueda_general_para_totales(params)
    query = where("(pedidos.Tipo != 'Obsequio') AND (pedidos.Cancelado = 0)")
    query = query.where("Ruta = ?", params[:search])
    query = query.where("IdEmpresa = ?", params[:search6])
    query = query.where("DiaO = ?", params[:diaO])
    query = query.where('IdVendedor = ?',params[:vendedor_id])
  end


end
