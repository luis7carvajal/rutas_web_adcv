class Detallebo < ActiveRecord::Base
  self.primary_key = "ID"


#este no se usa porque lo cambie a backorder
  def self.proceso_pedidos_liberados_backorder
    query = select("[clientes].IdCli AS Cliente, [clientes].Nombre AS Responsable, [clientes].NombreCorto AS NombreComercial, [productos].Clave AS ClaveProducto, [productos].Producto AS Producto, [backorder].PEDIDO AS Pedido,
                  [backorder].FOLIO_BO AS Folio_BO, [detalle_bo].PZA AS Pza, SUM([detalle_bo].IMPORTE) AS Importe, SUM([detalle_bo].IVA) AS IVA, SUM([detalle_bo].IMPORTE) + SUM([detalle_bo].IVA) AS Total, [backorder].Motivo AS Motivo")
           .joins('inner join productos ON detalle_bo.ARTICULO = Productos.Clave inner join backorder ON detalle_bo.PEDIDO = backorder.PEDIDO AND
                 detalle_bo.RUTA = backorder.RUTA AND detalle_bo.IDEMPRESA = backorder.IDEMPRESA inner join
                 clientes ON backorder.CODCLIENTE = clientes.IdCli')
           .group("clientes.IdCli, clientes.Nombre, clientes.NombreCorto, productos.Clave, productos.Producto, backorder.Motivo, backorder.PEDIDO, backorder.FOLIO_BO, detalle_bo.PZA, detalle_bo.ID")
    query
  end

#Este si
  def self.losDetallesBackorder_de_un_Backorder(params)
    query = select("[detalle_bo].PEDIDO, [detalle_bo].FOLIO_BO, [detalle_bo].RUTA, [detalle_bo].ARTICULO, [detalle_bo].PZA, [detalle_bo].TIPO, [detalle_bo].KG, [detalle_bo].PRECIO, [detalle_bo].IMPORTE, [detalle_bo].IMPORTE, [detalle_bo].IVA, [detalle_bo].IEPS, [detalle_bo].DESCUENTO, [detalle_bo].BAN_PROMO, [detalle_bo].STATUS, [detalle_bo].IDEMPRESA")
           .joins('inner join backorder ON detalle_bo.PEDIDO = backorder.PEDIDO AND
                 detalle_bo.RUTA = backorder.RUTA AND detalle_bo.IDEMPRESA = backorder.IDEMPRESA')
           .where('backorder.ID = ?', (params[:id_backorder]))

    query
  end


end
