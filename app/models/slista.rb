class Slista < ActiveRecord::Base
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  has_many :detalleld, class_name: "Detalle", foreign_key: "ListaId"
  accepts_nested_attributes_for :detalleld

  validates :Lista, presence: true
  validates :FechaIni, presence: true
  validates :FechaFin, presence: true
  validates :Tipo, presence: true

  def self.to_csv(options = {})#exportar
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |slista|
        csv << slista.attributes.values_at(*column_names)
      end
    end
  end

  def self.import(file,empresa)#importar
    @errors = []
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
#colocar condicion de que busque el producto, si no lo consigue no lo guarde
      row = Hash[[header, spreadsheet.row(i)].transpose]
      slista = find_by_id(row["id"]) || new
      slista.attributes = {id: row["id"], Lista: row["Nombre"], Tipo: row["Tipo"], Caduca: row["Caduca"], FechaIni: row["Fecha inicial"], FechaFin: row["Fecha final"], IdEmpresa: empresa}

      slista.detalleld_attributes = [{Articulo: row["Articulo"], Tipo: row["Tipo"], Factor: row["Factor"], IdEmpresa: empresa}]

      if slista.save
        # stuff to do on successful save
       else
         slista.errors.full_messages.each do |message|
           @errors << "Error fila #{i}, columna #{message}"
         end
       end


    end
    @errors #  <- need to return the @errors array
  end

  def self.open_spreadsheet(file)#importar
    case File.extname(file.original_filename)
     when '.csv' then Roo::Csv.new(file.path, packed: false, file_warning: :ignore)
     #when '.xls' then Roo::Excel.new(file.path, packed: false, file_warning: :ignore)
     when '.xlsx' then Roo::Excelx.new(file.path, packed: false, file_warning: :ignore)
     #else raise "Unknown file type: #{file.original_filename}"
     else raise "El formato debe ser .xlsx ó .csv"


     end
  end

end
