class Incidencia < ActiveRecord::Base
  self.primary_key = "Clave"
  self.table_name = 'Incidencias' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
end
