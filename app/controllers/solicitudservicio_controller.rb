class SolicitudservicioController < ApplicationController
  before_action :set_solicitudserv, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /solicitudservicio
  # GET /solicitudservicio.json
  def index
    params[:search6] = current_usuario.empresa_id
    @hoy = 25-04-2019

    @search = Solicitudserv.solicitud_servicio(params).search(search_params)
    @search.sorts = 'FolServicio' if @search.sorts.empty?
    @solicitudservicio = @search.result().page(params[:page]).per(15)

    @cantidad_solicitudservicio_activos = Solicitudserv.solicitud_servicio2(params).count

    @searchClienteB = Cliente.clientes_en_lista_de_precios(params).search(search_params)
    @searchClienteB.sorts = 'NombreCorto' if @searchClienteB.sorts.empty?
    @clientes_Selector = @searchClienteB.result().page(params[:clientes])

    @solicitudserv = Solicitudserv.new
    @cantidad_sabores_activos = Solicitudserv.all.count

    respond_to do |format|
      format.html
      format.js
    end
  end

  def formulario_crear_servicio
    params[:search6] = current_usuario.empresa_id

    @solicitudserv = Solicitudserv.new

    @DiaC = Folio.find_by(idempresa:params[:search6])
    @dia_juliano = "#{Date.today.strftime("%y").to_i}"+ "#{((Date.today - Date.today.beginning_of_year).to_i) +1}"
    if @dia_juliano.to_i == @DiaC.FolServicio.to_s.first(5).to_i
      @DiaC.update(FolServicio: @DiaC.FolServicio+1)
    else
      @DiaC.update(FolServicio: "#{@dia_juliano}001".to_i)
    end


    respond_to do |format|
      format.js
    end
  end

  # POST /solicitudservicio
  # POST /solicitudservicio.json
  def create
    params[:solicitudserv][:IdEmpresa] = current_usuario.empresa_id
    params[:solicitudserv][:Fecha] = DateTime.now.in_time_zone("Mexico City")
    params[:solicitudserv][:Cancelado] = 0
    params[:search6] = current_usuario.empresa_id
    @pieza = Catunidadmed.busqueda_pieza.first
    @solicitudserv = Solicitudserv.new(solicitudserv_params)

    respond_to do |format|
      if @solicitudserv.save
        Producto.create(Clave:@solicitudserv.FolServicio, Producto:"Servicio", CodBarras:@solicitudserv.FolServicio, Granel:0, IVA:0, IEPS:0, UniMed:@pieza.Clave, VBase:1, Equivalente:1, Ban_Envase: false, IdEmpresa:current_usuario.empresa_id, Status:"A")
        Productosxpza.create(Producto:@solicitudserv.FolServicio, PzaXCja:1, IdEmpresa:current_usuario.empresa_id)
        @lista_precio_cliente = Relclili.find_by_CodCliente(@solicitudserv.CodCliente).ListaP

        if params[:Tipo_Precio] == "precio_fijo"
          Deta.create(ListaId:@lista_precio_cliente, Articulo:@solicitudserv.FolServicio, PrecioMin: params[:precio_fijo], PrecioMax:params[:precio_fijo], IdEmpresa:current_usuario.empresa_id)
        elsif params[:Tipo_Precio] == "precio_por_rango"
          Deta.create(ListaId:@lista_precio_cliente, Articulo:@solicitudserv.FolServicio, PrecioMin: params[:precio_minimo], PrecioMax:params[:precio_maximo], IdEmpresa:current_usuario.empresa_id)
        end 
        @solicitudserv = Solicitudserv.solicitud_servicio(params).find_by_id(@solicitudserv.id)
        format.html { redirect_to @solicitudserv, notice: 'Solicitudserv was successfully created.' }
        format.json { render :show, status: :created, location: @solicitudserv }
        format.js {flash.now[:notice] = 'El servicio se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @solicitudserv.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el servicio.'} #ajax
      end
    end
  end

  # PATCH/PUT /solicitudservicio/1
  # PATCH/PUT /solicitudservicio/1.json
  def update
    params[:search6] = current_usuario.empresa_id
    respond_to do |format|
      if @solicitudserv.update(solicitudserv_params)
        @Deta= Deta.find_by(Articulo:@solicitudserv.FolServicio)

        if params[:Tipo_Precio] == "precio_fijo"
          @Deta.update(PrecioMin: params[:precio_fijo], PrecioMax:params[:precio_fijo])
        elsif params[:Tipo_Precio] == "precio_por_rango"
          @Deta.update(PrecioMin: params[:precio_minimo], PrecioMax:params[:precio_maximo])
        end 
        @solicitudserv = Solicitudserv.solicitud_servicio(params).find_by_id(@solicitudserv.id)

        format.html { redirect_to @solicitudserv, notice: 'Solicitudserv was successfully updated.' }
        format.json { render :show, status: :ok, location: @solicitudserv }
        format.js {flash.now[:notice] = 'El servicio se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @solicitudserv.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el servicio.'} #ajax
      end
    end
  end

  # DELETE /solicitudservicio/1
  # DELETE /solicitudservicio/1.json
  def destroy
    @solicitudserv.destroy
    respond_to do |format|
      format.html { redirect_to solicitudservicio_url, notice: 'Solicitudserv was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_solicitudserv
      @solicitudserv = Solicitudserv.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def solicitudserv_params
      params.require(:solicitudserv).permit(:FolServicio, :CodCliente, :DescripcionServicio, :Prioridad, :Status, :Fecha, :FechaServicio, :Cancelado, :Observaciones, :IdEmpresa)
    end
end
