class ProductosController < ApplicationController
  before_action :set_producto, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de productos"
  end



  # GET /productos
  # GET /productos.json
  def index
    params[:sucursal] = current_usuario.empresa_id
    @pathDeBusqueda = productos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Producto.activos.catalogo_productos(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'Producto' if @search.sorts.empty?
    @productosV = @search.result().page(params[:page])
    @cantidad_productos_activos = Producto.activos.count


    @productos = Producto.activos
    @producto = Producto.new
    @productogen = Productogen.new
    @producto.build_productosxpza
    #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    #pero en el caso de ser un has_many se debe usar @producto.productosxpza.build
    @productoenvase = Productoenvas.all
    @productoenvas = Productoenvas.new

    elementos_para_seleccionar

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @productos.to_csv}
      format.xls
    end

  end

  def datos_producto
    params[:IdEmpresa] = current_usuario.empresa_id
    elementos_para_seleccionar
    @producto = Producto.find_by_Clave(params[:producto])

    respond_to do |format|
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
    end
  end

  def elementos_para_seleccionar
    @unidadmedidas = Catunidadmed.activos
    @catmarcas = Catmarca.producto_marca.activos
  end



  def import
    empresa = current_usuario.empresa_id
    @errors = Producto.import(params[:file], empresa)
    if @errors.present?
      render :errorimportation; #Redirije a dicha vista para mostrar los errores
      return;
    else
      redirect_to productos_path, notice: "Productos importados."
    end
  end

  def productos_check
    @productox = Producto.comprobar_existencia(params[:producto][:Clave]).first
    respond_to do |format|
    format.json { render :json => !@productox }
    end
  end

  def productos_codbarras_check
    @productoc = Producto.find_by_CodBarras(params[:producto][:CodBarras])
    respond_to do |format|
    format.json { render :json => !@productoc }
    end
  end

  # GET /productos/1
  # GET /productos/1.json
  def show

  end

  # GET /productos/new
  def new
    @producto = Producto.new
    @producto.build_productosxpza
  end

  # GET /productos/1/edit
  def edit

  end

  # POST /productos
  # POST /productos.json

  def create
    params[:sucursal] = current_usuario.empresa_id
    if @puede_crear != true
      return
    end
    elementos_para_seleccionar
    @producto = Producto.new(producto_params)
    respond_to do |format|
      if @producto.save
        if @producto.Ban_Envase == false
          @producto_generado = Productogen.find_by_Clave(@producto.Clave.first(5)).try(:Clave)
        end
         @producto = Producto.activos.catalogo_productos(params).find_by(Clave: @producto.Clave)
        format.html { redirect_to @producto, notice: 'Producto was successfully created.' }
        format.json { render :show, status: :created, location: @producto }
        if params[:envase]
          format.js {render :js => "window.location.href='"+producto_envase_path(@producto)+"'"}
        else
          format.js {flash.now[:notice] = 'El producto se ha creado de forma exitosa.'} #ajax
        end
      else
        format.html { render :new }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el producto.'} #ajax
      end
    end
  end


  def create_producto_generico
    elementos_para_seleccionar
    @productogen = Productogen.new(productogen_params)

    respond_to do |format|
      if @productogen.save
        format.js {flash.now[:notice] = 'El producto genérico se ha creado de forma exitosa.'}
      else
        format.js {flash.now[:alert] = 'Error al crear el producto genérico.'}
      end
    end
  end

  # PATCH/PUT /productos/1
  # PATCH/PUT /productos/1.json
  def update
    params[:sucursal] = current_usuario.empresa_id
    if @puede_editar != true
      return
    end
    elementos_para_seleccionar
    respond_to do |format|
      if @producto.update(producto_params)
         @producto = Producto.activos.catalogo_productos(params).find_by(Clave: @producto.Clave)
        format.html { redirect_to @producto, notice: 'Producto was successfully updated.' }
        format.json { render :show, status: :ok, location: @producto }
        if params[:envase]
          format.js {render :js => "window.location.href='"+producto_envase_path(@producto)+"'"}
        else
          @producto.productoenvase.delete_all
          format.js {flash.now[:notice] = 'El producto se ha actualizado de forma exitosa.'} #ajax
        end
      else
        format.html { render :edit }
        format.json { render json: @producto.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar el producto.'} #ajax
      end
    end
  end

  # DELETE /productos/1
  # DELETE /productos/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to productos_url, notice: 'Producto was successfully destroyed.' }
      format.json { head :no_content }
      if @producto.Status == "A"
        @producto.update(Status:"I")
        format.js {flash.now[:notice] = 'El producto se ha borrado de forma exitosa.'} #ajax
      else
        @producto.update(Status:"A")
        format.js {flash.now[:notice] = 'El producto se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  def inactivos

    @pathDeBusqueda = productos_inactivos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    @search = Producto.inactivos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @productosV = @search.result().page(params[:page])
    @cantidad_productos_inactivos = Producto.inactivos.count
    @productos = Producto.inactivos
    #debido a que es una relacion has_one se debe hacer de esta forma: @producto.build_productosxpza
    #pero en el caso de ser un has_many se debe usar @producto.productosxpza.build
    @productoenvase = Productoenvas.all
    @productoenvas = Productoenvas.new

    elementos_para_seleccionar

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @productos.to_csv}
      format.xls
    end

  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producto
      @producto = Producto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def producto_params
      params.require(:producto).permit(:Clave, :ClaveMarca, :UniMedEq, :Producto, :CodBarras, :Granel, :IVA, :IEPS, :UniMed, :VBase, :Equivalente, :Sector, :Ban_Envase, :IdClasp, :IdEmpresa, :Status, :Producto_id, :cover, productosxpza_attributes: [:id, :PzaXCja, :IdEmpresa ])
    end

    def productogen_params
      params.require(:productogen).permit(:Clave, :Descripcion, :Status)
    end
end
