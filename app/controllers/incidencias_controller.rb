class IncidenciasController < ApplicationController
  before_action :set_incidencia, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]
  before_action :permiso_create, only: [:create]
  before_action :permiso_update, only: [:update]
  before_action :permiso_destroy, only: [:destroy]

  def modulo
    @Descripcion_Modulo = "Catálogo de incidencias"
  end

  # GET /incidencias
  # GET /incidencias.json
  def index
    @search = Incidencia.activos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @incidencias = @search.result().page(params[:page])
    @incidencia = Incidencia.new
    @cantidad_incidencias_activos = Incidencia.activos.count

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @incidencias.to_csv}
      format.xls
    end
  end

  def inactivos

    @search = Incidencia.inactivos.search(search_params)
    # make name the default sort column
    @search.sorts = 'Clave' if @search.sorts.empty?
    @incidencias = @search.result().page(params[:page])
    @cantidad_incidencias_inactivos = Incidencia.inactivos.count

    respond_to do |format|
      format.html
      format.js #para que se puedan enviar los datos de la busqueda del @search de la gema
      format.csv { send_data @incidencias.to_csv}
      format.xls
    end
  end

  def incidencia_check
    @incidenciax = Incidencia.find_by_Clave(params[:incidencia][:Clave])
    respond_to do |format|
    format.json { render :json => !@incidenciax }
    end
  end


  # GET /incidencias/1
  # GET /incidencias/1.json
  def show
  end

  # GET /incidencias/new
  def new
    @incidencia = Incidencia.new
  end

  # GET /incidencias/1/edit
  def edit
  end

  # POST /incidencias
  # POST /incidencias.json
  def create
    if @puede_crear != true
      return
    end
    @incidencia = Incidencia.new(incidencia_params)
    respond_to do |format|
      if @incidencia.save
        format.html { redirect_to @incidencia, notice: 'Incidencia was successfully created.' }
        format.json { render :show, status: :created, location: @incidencia }
        format.js {flash.now[:notice] = 'La incidencia se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @incidencia.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear la incidencia.'} #ajax
      end
    end
  end

  # PATCH/PUT /incidencias/1
  # PATCH/PUT /incidencias/1.json
  def update
    if @puede_editar != true
      return
    end
    respond_to do |format|
      if @incidencia.update(incidencia_params)
        format.html { redirect_to @incidencia, notice: 'Incidencia was successfully updated.' }
        format.json { render :show, status: :ok, location: @incidencia }
        format.js {flash.now[:notice] = 'La incidencia se ha actualizado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @incidencia.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al actualizar la incidencia.'} #ajax
      end
    end
  end

  # DELETE /incidencias/1
  # DELETE /incidencias/1.json
  def destroy
    if @puede_destroy != true
      return
    end
    respond_to do |format|
      format.html { redirect_to incidencia_url, notice: 'incidencia was successfully destroyed.' }
      format.json { head :no_content }
      if @incidencia.Status == true
        @incidencia.update(Status:false)
        format.js {flash.now[:notice] = 'La incidencia se ha borrado de forma exitosa.'} #ajax
      else
        @incidencia.update(Status:true)
        format.js {flash.now[:notice] = 'La incidencia se ha habilitado de forma exitosa.'} #ajax
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_incidencia
      @incidencia = Incidencia.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def incidencia_params
      params.require(:incidencia).permit(:Clave, :Descripcion, :Status, :IdEmpresa)
    end
end
