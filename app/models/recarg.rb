class Recarg < ActiveRecord::Base
  self.primary_key = "ID"
  belongs_to :producto, class_name:"Producto", foreign_key: "Articulo"
  belongs_to :ruta, class_name:"Ruta", foreign_key: "IdRuta"

#  def self.busqueda_general(params)
#  query = select('[recarga].ID,[recarga].Hora,[recarga].IdRuta,[recarga].Articulo,[recarga].Fecha,[recarga].Hora,[recarga].Cantidad,[productosxpzas].PzaXCja, ISNULL(recarga.Cantidad / productosxpzas.PzaXCja, 0) as Cajas ')
#       .joins('INNER JOIN productos ON recarga.Articulo=productos.Clave LEFT OUTER JOIN productosxpzas ON Productos.Clave = productosxpzas.Producto')
#       .group(:ID,:IdRuta,:Articulo,:Fecha,:Hora,:Cantidad,:Hora,:PzaXCja)
#       .where("(recarga.IdRuta = :rutaId or :rutaId = '') AND (recarga.IdEmpresa = :idempresa) AND (recarga.Cantidad > 0) AND (productos.Ban_Envase = 0)",{rutaId: params[:search],idempresa: params[:search6]})
#       .distinct
#  query = query.where('recarga.Fecha >= ? AND recarga.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
#  query
#   end

  def self.busqueda_general(params) #consulta corregida por la ultima que me enviaron el 8/12
   query = select('Rutas.Ruta, Recarga.Articulo, Productos.Producto, FORMAT(Recarga.Fecha, \'dd-MM-yyyy\') AS Fecha_Recarga,FORMAT(Recarga.Hora, \'HH:mm\') as Hora_Recarga, ISNULL(Recarga.Cantidad / ProductosXPzas.PzaXCja, 0) as Cajas , ISNULL(Recarga.Cantidad % ProductosXPzas.PzaXCja, 0) as Piezas')
        .joins('INNER JOIN Rutas ON Recarga.IdRuta = Rutas.IdRutas INNER JOIN Productos ON Recarga.Articulo = Productos.Clave LEFT OUTER JOIN ProductosXPzas ON Productos.Clave = ProductosXPzas.Producto')
        .where("(recarga.Cantidad > 0) AND (productos.Ban_Envase = 0) AND (recarga.IdRuta = :rutaId or :rutaId = '') AND (recarga.IdEmpresa = :idempresa)",{rutaId: params[:search],idempresa: params[:search6]})
        .distinct
   query = query.where('recarga.Fecha BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
   query = query.where('recarga.Diao = ?', params[:diaO]) if params[:diaO].present?
   query
  end

end
