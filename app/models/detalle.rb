class Detalle < ActiveRecord::Base
  self.table_name = 'DetalleLD' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  self.primary_key = "id"
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  belongs_to :slista, class_name:"Slista", foreign_key: "ListaId"
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model

  #traeme todos los que tengan listaid igual al parametro
  scope :productosquetiene, -> (id_param) { where(ListaId: id_param) }


  belongs_to :productos, class_name:"Producto", foreign_key: "Articulo"

  before_save :valores_por_default

  def valores_por_default
    self.Factor ||= 0
  end
end
