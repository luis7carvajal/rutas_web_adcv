class ReportesRutaController < ApplicationController
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:recargas, :envases, :index_tripulacion, :consumos]

  def modulo
    if action_name == "recargas"
      @Descripcion_Modulo = "Reporte de recargas"
    elsif action_name == "envases"
      @Descripcion_Modulo = "Reporte de envases"
    elsif action_name == "index_tripulacion"
      @Descripcion_Modulo = "Reporte de tripulación"
    elsif action_name == "consumos"
      @Descripcion_Modulo = "Reporte de consumos"
    end
  end

  def recargas
    @controlador = 'reportes_ruta'
    @accion = 'recargas'
    @pathDeBusqueda = reportes_ruta_recargas_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    params[:search6] = current_usuario.empresa_id
    ruta
    diao
  end

  def envases
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'reportes_ruta'
    @accion = 'envases'
    params[:search6] = current_usuario.empresa_id

    @pathDeBusqueda = reportes_ruta_envases_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    ruta
    diao
    @envases = Devenvase.busqueda_rep_envases(params).page(params[:envases]).per(10)
    @envases_devueltos = Devenvase.busqueda_rep_envases_devueltos(params).page(params[:envases_devueltos]).per(10)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def index_tripulacion
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'reportes_ruta'
    @accion = 'index_tripulacion'
    @pathDeBusqueda = tripulacion_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    params[:search6] = current_usuario.empresa_id
    ruta
    diao

    #@search = Ruta.tripulacion(params).search(search_params)
    # make name the default sort column
    #@search.sorts = 'IdRow' if @search.sorts.empty?
    #@tripulaciones = @search.result().page(params[:tripulacion])
    @tripulaciones = Ruta.tripulacion(params).page(params[:tripulacion]).per(30)

    respond_to do |format|
      format.html
      format.js
    end
  end


  def ruta
    elementos_de_busqueda_ruta_autocompletar
    @searchRutaB = Ruta.por_empresa(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @searchRutaB.sorts = 'IdRutas' if @searchRutaB.sorts.empty?
    @rutas_Selector = @searchRutaB.result().page(params[:rutas])
  end

  def diao
    params[:search6] = current_usuario.empresa_id
    @searchDiaOpB = Diaop.selector_dia_operativo(params).search(search_params)
    @searchDiaOpB.sorts = 'Fecha desc' if @searchDiaOpB.sorts.empty?
    @DiasOp_Selector = @searchDiaOpB.result().page(params[:DiasOp])
  end

  def consumos
    #para el parcial de ruta_diaop que pedira el nombre del controlador y la accion
    @controlador = 'reportes_ruta'
    @accion = 'consumos'
    @pathDeBusqueda = consumos_path # esto es usando para colocar el path dentro de la tabla de rutas, porque de esta forma puedo colocar el parcial rutas en la carpeta parciales y compartirlo en todas las vistas, solo cambia esta variable
    ruta
    diao
    elementros_consumo

    respond_to do |format|
      format.html
      format.js
    end
  end

  def elementros_consumo
    params[:search6] = current_usuario.empresa_id
    @search = Medidor.datos_consumo(params).search(search_params)
    # make name the default sort column
    @search.sorts = 'OdometroInicial' if @search.sorts.empty?
    @medidores = @search.result().page(params[:medidores]).per(15)
  end

  def update_dia
    @diao = Diaop.where("RutaId = ?", params[:search])
    respond_to do |format|
      format.js
    end
  end

  def busqueda_recarga
    params[:search6] = current_usuario.empresa_id
    elementos_de_busqueda_ruta_autocompletar
   @recarga = Recarg.busqueda_general(params)

    respond_to do |format|
    format.js
    end
  end





end
