class Usuario < ActiveRecord::Base
  self.primary_key = 'IdUsuario'
  self.table_name = 'Usuarios' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  has_many :perfiles, class_name: "Perfil", foreign_key: "IdUser"
  accepts_nested_attributes_for :perfiles

  has_many :usuario_sucursales, foreign_key: "IdUsuario"
  has_many :empresas, through: :usuario_sucursales
  accepts_nested_attributes_for :usuario_sucursales, allow_destroy: true

  has_many :perfiles_por_Catalogo, -> {where("Descripcion LIKE ?", "Catálogo%") }, class_name: "Perfil", foreign_key: "IdUser"
  accepts_nested_attributes_for :perfiles_por_Catalogo

  has_many :perfiles_por_Proceso, -> {where("Descripcion LIKE ?", "Proceso%") }, class_name: "Perfil", foreign_key: "IdUser"
  accepts_nested_attributes_for :perfiles_por_Proceso

  has_many :perfiles_por_Reporte, -> {where("Descripcion LIKE ?", "Reporte%") }, class_name: "Perfil", foreign_key: "IdUser"
  accepts_nested_attributes_for :perfiles_por_Reporte



  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  scope :activos, -> { where(Status: true) }
  scope :inactivos, -> { where(Status: false) }
  scope :por_empresa, ->(user_id) { where(empresa_id: user_id) } # model

  scope :comprobar_existencia, ->(paramc) { where(usuario: paramc)} # model
  scope :comprobar_existencia_Email, ->(param_e) { where(email: param_e)} # model

  scope :datos, ->(user_id) { find_by(IdUsuario: user_id)} # para que no devuelva una coleccion, sino un solo registro

  belongs_to :empresa
  belongs_to :empresamadre


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def login=(login)
    @login = login
  end

  def login
    @login || self.usuario || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
   conditions = warden_conditions.dup
   if login = conditions.delete(:login)
     where(conditions.to_hash).where(["lower(usuario) = :value OR lower(email) = :value", { :value => login.downcase }]).first
   elsif conditions.has_key?(:usuario) || conditions.has_key?(:email)
     where(conditions.to_hash).first
   end
 end

 def self.to_csv(options = {})#exportar
   CSV.generate(options) do |csv|
     csv << column_names
     all.each do |usuario|
       csv << usuario.attributes.values_at(*column_names)
     end
   end
 end


 def send_password_reset
   generate_token(:password_reset_token)
   self.password_reset_sent_at = Time.zone.now
   save!
   UsuarioMailer.forgot_password(self).deliver# This sends an e-mail with a link for the user to reset the password
 end
 # This generates a random password reset token for the user
 def generate_token(column)
   begin
     self[column] = SecureRandom.urlsafe_base64
   end while Usuario.exists?(column => self[column])
 end

end
