class Noventasingular < ActiveRecord::Base
  self.primary_key = "Id"
  belongs_to :ruta, class_name: "Ruta", foreign_key: "RutaId"
  belongs_to :cliente, class_name: "Cliente", foreign_key: "Cliente"
  belongs_to :motivonoventa, class_name: "Motivonoventa", foreign_key: "MotivoId"


  def self.busqueda_general(params)
    query = select('CL.IdCli ClaveCliente, CL.NombreCorto NombreCliente, MNV.Motivo, (SELECT MIN(HI) FROM BitacoraTiempos WHERE IdEmpresa = Noventas.IdEmpresa AND RutaId = Noventas.RutaId AND DiaO = Noventas.DiaO AND Codigo = Noventas.Cliente) FechaInicio, (SELECT MAX(HF) FROM BitacoraTiempos WHERE IdEmpresa = Noventas.IdEmpresa AND RutaId = Noventas.RutaId AND DiaO = Noventas.DiaO AND Codigo = Noventas.Cliente) FechaFinal')
    query = query.joins('INNER JOIN Clientes CL ON CL.IdCli = Noventas.Cliente')
    query = query.joins('INNER JOIN MotivosNoVenta MNV ON MNV.IdMot = Noventas.MotivoId')
    query = query.where("(Noventas.RutaId = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (Noventas.DiaO = :diao or :diao = '')  AND (Noventas.VendedorId = :vendedor_id or :vendedor_id = '') AND (Noventas.IdEmpresa = :sucursal)",{rutaId: params[:search], diao: params[:diaO], vendedor_id: params[:vendedor_id], sucursal: params[:search6]})
    query
  end

  def self.rep_bitiempos_SinVenta(params)
  	query = select("R.Ruta")
  					.distinct
  					.joins('LEFT JOIN MotivosNoVenta ON MotivosNoVenta.IdMot= Noventas.MotivoId INNER JOIN Vendedores ON Noventas.VendedorId = Vendedores.IdVendedor
  					left outer join Clientes C on Noventas.Cliente=C.IdCli JOIN RUTAS R ON R.IdRutas=Noventas.RutaId')
  					.where("(Noventas.RutaId = ?)AND (Noventas.Fecha Between ? AND ?) AND (Noventas.IdEmpresa= ?)",(params[:search]), (params[:fechaDiaO].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), (params[:fechaDiaO].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"),(params[:search6]))
  		query = query.where("(Noventas.VendedorId = :VendedorId or :VendedorId = '') ",{ VendedorId: params[:vendedor_id]})
  		query = query.where("(Noventas.VendedorId = :vendedor_id or :vendedor_id = '') ",{ vendedor_id: params[:vendedor_id]})
  		query
  end




end
