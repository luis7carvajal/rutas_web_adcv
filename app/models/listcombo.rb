class Listcombo < ActiveRecord::Base
  self.table_name = 'ListaCombo' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) }
  scope :activos, -> { where(Activa: true) }
  scope :inactivos, -> { where(Activa: false) }
  scope :comprobar_existencia, ->(paramc) { where(Clave: paramc)} # model

  has_many :detallecombo, class_name: "Detacombo", foreign_key: "ComboId"
  accepts_nested_attributes_for :detallecombo, reject_if: proc { |attributes| attributes['Cantidad'].blank? and attributes['Articulo'].blank? }, allow_destroy: true


end
