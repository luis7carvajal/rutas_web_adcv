class BackorderController < ApplicationController
  before_action :set_backord, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :clear_search_index, :only => [:index]
  before_action :permiso_listar, only: [:index]

  def modulo
    @Descripcion_Modulo = "Proceso de backorder"
  end
  # GET /backorder
  # GET /backorder.json
  def index
    @search_backorder = Backord.proceso_pedidos_liberados_backorder(current_usuario.empresa_id).search(search_params)
    # make name the default sort column
    @search_backorder.sorts = 'Folio_BO desc' if @search_backorder.sorts.empty?
    @backorder = @search_backorder.result().page(params[:backorder]).per(15)


  end

  # GET /backorder/1
  # GET /backorder/1.json
  def show
  end

  # GET /backorder/new
  def new
    @backord = Backord.new
  end

  # GET /backorder/1/edit
  def edit
  end

  # POST /backorder
  # POST /backorder.json
  def create
    @backord = Backord.new(backord_params)

    respond_to do |format|
      if @backord.save
        format.html { redirect_to @backord, notice: 'Backord was successfully created.' }
        format.json { render :show, status: :created, location: @backord }
        format.js {flash.now[:notice] = 'El Backorder se ha creado de forma exitosa.'} #ajax
      else
        format.html { render :new }
        format.json { render json: @backord.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al crear el Backorder.'} #ajax
      end
    end
  end

  # PATCH/PUT /backorder/1
  # PATCH/PUT /backorder/1.json
  def update
    respond_to do |format|
      if @backord.update(backord_params)

        @opcion = params[:opcion]

        if @backord.CANCELADA == TRUE and @opcion == "liberar"
          if Pedidoliberado.exists?(PEDIDO: @backord.PEDIDO, RUTA: @backord.RUTA, IDEMPRESA: @backord.IDEMPRESA)
            Pedidoliberado.find_by(PEDIDO: @backord.PEDIDO, RUTA: @backord.RUTA, IDEMPRESA: @backord.IDEMPRESA).update(CANCELADA: FALSE)

          else
           Pedidoliberado.create(PEDIDO: @backord.PEDIDO, RUTA: @backord.RUTA, COD_CLIENTE: @backord.CODCLIENTE, FOLIO_BO: @backord.FOLIO_BO, FECHA_PEDIDO: @backord.FECHA_PEDIDO, FECHA_LIBERACION: @backord.FECHA_LIBERACION, FECHA_ENTREGA: @backord.FECHA_ENTREGA, TIPO: @backord.TIPO, STATUS: 1, SUBTOTAL: @backord.SUBTOTAL, IVA: @backord.IVA,
                          IEPS: @backord.IEPS, TOTAL: @backord.TOTAL, KG: @backord.KG, CANCELADA: 0, IDEMPRESA: @backord.IDEMPRESA)

           params[:id_backorder] = @backord.ID

           @detallesbackorder = Detallebo.losDetallesBackorder_de_un_Backorder(params)

           @detallesbackorder.each do |detallebo|
             Detallepedidolib.create(PEDIDO: detallebo.PEDIDO, FOLIO_BO: detallebo.FOLIO_BO, RUTA: detallebo.RUTA, ARTICULO: detallebo.ARTICULO, PZA: detallebo.PZA, TIPO: detallebo.TIPO, KG: detallebo.KG, PRECIO: detallebo.PRECIO, IMPORTE: detallebo.IMPORTE, IVA: detallebo.IVA, IEPS: detallebo.IEPS, DESCUENTO: detallebo.DESCUENTO, BAN_PROMO: detallebo.BAN_PROMO, STATUS: detallebo.STATUS, IDEMPRESA: detallebo.IDEMPRESA)
            end
          end

          @pedidoliberado = Pedidoliberado.proceso_pedidos_lib.where(:PEDIDO => @backord.PEDIDO, :RUTA => @backord.RUTA, :IDEMPRESA => @backord.IDEMPRESA).first

        end
        format.html { redirect_to @backord, notice: 'Backord was successfully updated.' }
        format.json { render :show, status: :ok, location: @backord }
        format.js {flash.now[:notice] = 'El Backorder se ha modificado de forma exitosa.'} #ajax
      else
        format.html { render :edit }
        format.json { render json: @backord.errors, status: :unprocessable_entity }
        format.js {flash.now[:alert] = 'Error al modificar el Backorder.'} #ajax
      end
    end
  end

  # DELETE /backorder/1
  # DELETE /backorder/1.json
  def destroy
    @backord.destroy
    respond_to do |format|
      format.html { redirect_to backorder_url, notice: 'Backord was successfully destroyed.' }
      format.json { head :no_content }
      format.js {flash.now[:notice] = 'El envase se ha creado de forma exitosa.'} #ajax
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_backord
      @backord = Backord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def backord_params
      params.require(:backord).permit(:ID, :PEDIDO, :RUTA, :CODCLIENTE, :FOLIO_BO, :FECHA_PEDIDO, :FECHA_BO, :FECHA_ENTREGA, :FECHA_LIBERACION, :TIPO, :STATUS, :CANCELADA, :SUBTOTAL, :IVA, :IEPS, :TOTAL, :KG, :IDEMPRESA, :Motivo)
    end
end
