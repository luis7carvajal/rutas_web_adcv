class Vent < ActiveRecord::Base
  self.primary_key = 'Id'
  self.table_name = 'Venta' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  scope :por_ruta, -> (param) { where(RutaId: param) }
  scope :por_empresa, ->(user_id) { where(IdEmpresa: user_id) } # model
  scope :tipo_contado, -> { where(TipoVta: "Contado") }
  scope :tipo_credito, -> { where(TipoVta: "Credito") }
  scope :tipo_todas, -> { where("TipoVta != 'Obsequio'") }
  scope :pago_en_Efectivo, -> { joins(:formapag).where(formaspag: {Forma: "Efectivo"})}
  scope :pago_no_sea_Efectivo, -> { joins(:formapag).where.not(formaspag: {Forma: "Efectivo"}) }
  scope :sin_obsequios, -> { where.not(TipoVta: "Obsequio") }

  scope :descx, ->  {order("venta.Fecha ASC")}

  belongs_to :ruta, class_name:"Ruta", foreign_key: "RutaId"
  belongs_to :cliente, class_name:"Cliente", foreign_key: "CodCliente"
  belongs_to :vendedores, class_name:"Vendedor", foreign_key: "VendedorId"
  belongs_to :formapag, class_name: "Formapag", foreign_key: "FormaPag"



  has_many :detallevet, -> (vent) { where(Docto: vent.Documento, RutaId: vent.RutaId) },
           class_name: 'Detalleve'

  def self.estadisticas_liquidacion_concentrado(params)
    #Venta Contado
    query_contado = joins("INNER JOIN DetalleVet DV ON DV.RutaId = [venta].RutaID AND DV.IdEmpresa = [venta].IdEmpresa AND DV.Docto = [venta].Documento")
    query_contado = query_contado.where('[venta].RutaId = ?', params[:search])
    query_contado = query_contado.where('[venta].DiaO = ?', params[:diaO])
    query_contado = query_contado.where("[venta].TipoVta = 'Contado'")
    query_contado = query_contado.where("[venta].Cancelada = 0")
    contado = query_contado.sum('DV.Importe + DV.iva').to_f.round(2)


    #VentaCredito
    query_credito = joins("INNER JOIN DetalleVet DV ON DV.RutaId = [venta].RutaID AND DV.IdEmpresa = [venta].IdEmpresa AND DV.Docto = [venta].Documento")
    query_credito = query_credito.where('[venta].RutaId = ?', params[:search])
    query_credito = query_credito.where('[venta].DiaO = ?', params[:diaO])
    query_credito = query_credito.where("[venta].TipoVta = 'Credito'")
    query_contado = query_contado.where("[venta].Cancelada = 0")
    credito = query_credito.sum('DV.Importe + DV.iva').to_f.round(2)




    total = (contado + credito).to_f.round(2)

    #Promocion Cajas
    query_promocion_cajas = joins('INNER JOIN PRegalado P ON P.RutaId = [venta].RutaId AND P.IdEmpresa = [venta].IdEmpresa AND P.Docto = [venta].Documento AND P.DiaO = [venta].DiaO')
    query_promocion_cajas = query_promocion_cajas.where('[venta].RutaId = ?', params[:search])
    query_promocion_cajas = query_promocion_cajas.where("P.Tipo = 'V'")
    query_promocion_cajas = query_promocion_cajas.where("P.Tipmed = 'Caja'")
    query_promocion_cajas = query_promocion_cajas.where('[venta].DiaO = ?', params[:diaO])
    query_promocion_cajas = query_promocion_cajas.where('[venta].Cancelada = 0')
    query_promocion_cajas = query_promocion_cajas.where('[venta].IdEmpresa = ?', params[:search6])
    query_promocion_cajas = query_promocion_cajas.where('[venta].VendedorId = ?', params[:vendedor]) if params[:vendedor].present?
    query_promocion_cajas = query_promocion_cajas.where("[venta].TipoVta != 'Obsequio'")
    producto_promocion_cajas = query_promocion_cajas.select('P.Cant Cantidad, P.SKU Articulo,P.Tipo')
    promocion_cajas = 0;



 # FROM VENTA INNER JOIN PRegalado P ON P.RutaId = [venta].RutaId AND P.IdEmpresa = [venta].IdEmpresa AND P.Docto = [venta].Documento AND P.DiaO = [venta].DiaO
 # and  Venta.Rutaid=8 and P.Tipo='V' and P.Tipmed = 'Caja' and venta.DiaO=611


 # UNION ALL
#  select P.Cant Cantidad,P.SKU Articulo,P.Tipo
#  FROM pedidosliberados PL inner join PRegalado P on PL.PEDIDO=P.Docto and PL.RUTA=P.RutaId and PL.IDEMPRESA=P.idempresa
 # where PL.cancelada=0 and P.Tipo='P' and P.Tipmed = 'Caja' and PL.status=5 and PL.Ruta=8 and PL.DiaO_entrega=611


    producto_promocion_cajas.each do |producto|
      sql = "SELECT MAX(Precio) Precio, Articulo FROM
            (SELECT CASE WHEN PrecioMax > PrecioMin THEN PrecioMax ELSE PrecioMin END AS Precio, Articulo FROM DetalleLP WHERE ListaId IN (
              SELECT RelCliLis.ListaP FROM RelClirutas INNER JOIN RelCliLis ON RelClirutas.IdCliente = RelCliLis.CodCliente INNER JOIN ListaP ON RelCliLis.ListaP = ListaP.id GROUP BY RelClirutas.IdRuta, RelCliLis.ListaP, ListaP.Tipo HAVING RelClirutas.IdRuta = #{params[:search]}
            ) AND IdEmpresa = #{params[:search6]}) B
            WHERE Articulo = '#{producto.try(:Articulo)}'
            GROUP BY Articulo"
      producto_precio = ActiveRecord::Base.connection.select_value(sql)
      promocion_cajas += producto_precio.to_f * producto.try(:Cantidad).to_i
    end

    #Promocion Piezas
    query_promocion_piezas = joins('INNER JOIN PRegalado P ON P.RutaId = [venta].RutaId AND P.IdEmpresa = [venta].IdEmpresa AND P.Docto = [venta].Documento AND P.DiaO = [venta].DiaO')
    query_promocion_piezas = query_promocion_piezas.where("P.Tipmed = 'Pzas'")
    query_promocion_piezas = query_promocion_piezas.where("P.Tipo = 'V'")
    query_promocion_piezas = query_promocion_piezas.where('[venta].Cancelada = 0')
    query_promocion_piezas = query_promocion_piezas.where('[venta].DiaO = ?', params[:diaO])
    query_promocion_piezas = query_promocion_piezas.where('[venta].IdEmpresa = ?', params[:search6])
    query_promocion_piezas = query_promocion_piezas.where('[venta].RutaId = ?', params[:search])
    query_promocion_piezas = query_promocion_piezas.where('[venta].VendedorId = ?', params[:vendedor]) if params[:vendedor].present?
    query_promocion_piezas = query_promocion_piezas.where("[venta].TipoVta != 'Obsequio'")
    producto_promocion_piezas = query_promocion_piezas.select('P.Cant Cantidad, P.SKU Articulo')
    promocion_piezas = 0;

    producto_promocion_piezas.each do |producto|
      sql = "SELECT MAX(Precio) Precio, Articulo FROM
            (SELECT CASE WHEN PrecioMax > PrecioMin THEN PrecioMax ELSE PrecioMin END AS Precio, Articulo FROM DetalleLP WHERE ListaId IN (
              SELECT RelCliLis.ListaP FROM RelClirutas INNER JOIN RelCliLis ON RelClirutas.IdCliente = RelCliLis.CodCliente INNER JOIN ListaP ON RelCliLis.ListaP = ListaP.id GROUP BY RelClirutas.IdRuta, RelCliLis.ListaP, ListaP.Tipo HAVING RelClirutas.IdRuta = #{params[:search]}
            ) AND IdEmpresa = #{params[:search6]}) B
            WHERE Articulo = '#{producto.try(:Articulo)}'
            GROUP BY Articulo"
      producto_precio = ActiveRecord::Base.connection.select_value(sql)
      promocion_piezas += producto_precio.to_f * producto.try(:Cantidad).to_i
    end

    importe_promocion = (promocion_cajas + promocion_piezas).to_f.round(2)

    #Obsequio Cajas
    query_obsequio_cajas = joins('INNER JOIN DetalleVet DV ON DV.RutaId = [venta].RutaId AND DV.Docto = [venta].Documento AND DV.IdEmpresa = [venta].IdEmpresa')
    query_obsequio_cajas = query_obsequio_cajas.joins('INNER JOIN ProductosXPzas PxP ON PxP.IdEmpresa = [venta].IdEmpresa AND PxP.Producto = DV.Articulo INNER JOIN Productos P ON P.Clave = PxP.Producto')
    query_obsequio_cajas = query_obsequio_cajas.where('DV.Tipo = 0')
    query_obsequio_cajas = query_obsequio_cajas.where('[venta].Cancelada = 0')
    query_obsequio_cajas = query_obsequio_cajas.where('[venta].DiaO = ?', params[:diaO])
    query_obsequio_cajas = query_obsequio_cajas.where('[venta].IdEmpresa = ?', params[:search6])
    query_obsequio_cajas = query_obsequio_cajas.where('[venta].RutaId = ?', params[:search])
    query_obsequio_cajas = query_obsequio_cajas.where('[venta].VendedorId = ?', params[:vendedor]) if params[:vendedor].present?
    query_obsequio_cajas = query_obsequio_cajas.where("[venta].TipoVta = 'Obsequio'")
    obsequio_cajas = query_obsequio_cajas.sum('DV.Importe + DV.iva').to_f.round(2)


    #Obsequio Piezas
    query_obsequio_piezas = joins('INNER JOIN DetalleVet DV ON DV.RutaId = [venta].RutaId AND DV.Docto = [venta].Documento AND DV.IdEmpresa = [venta].IdEmpresa')
    query_obsequio_piezas = query_obsequio_piezas.joins('INNER JOIN ProductosXPzas PxP ON PxP.IdEmpresa = [venta].IdEmpresa AND PxP.Producto = DV.Articulo INNER JOIN Productos P ON P.Clave = PxP.Producto')
    query_obsequio_piezas = query_obsequio_piezas.where('DV.Tipo = 1')
    query_obsequio_piezas = query_obsequio_piezas.where('[venta].Cancelada = 0')
    query_obsequio_piezas = query_obsequio_piezas.where('[venta].DiaO = ?', params[:diaO])
    query_obsequio_piezas = query_obsequio_piezas.where('[venta].IdEmpresa = ?', params[:search6])
    query_obsequio_piezas = query_obsequio_piezas.where('[venta].RutaId = ?', params[:search])
    query_obsequio_piezas = query_obsequio_piezas.where('[venta].VendedorId = ?', params[:vendedor]) if params[:vendedor].present?
    query_obsequio_piezas = query_obsequio_piezas.where("[venta].TipoVta = 'Obsequio'")
    obsequio_piezas = query_obsequio_piezas.sum('(DV.Importe + DV.iva)/PxP.PzaXCja').to_f.round(2)



    #Descuentos
    query_descuentos = joins("INNER JOIN DetalleVet DV ON DV.RutaId = [venta].RutaID AND DV.IdEmpresa = [venta].IdEmpresa AND DV.Docto = [venta].Documento INNER JOIN Productos P ON P.Clave = DV.Articulo")
    query_descuentos = query_descuentos.where('[venta].Cancelada = 0')
    query_descuentos = query_descuentos.where('[venta].DiaO = ?', params[:diaO])
    query_descuentos = query_descuentos.where('[venta].IdEmpresa = ?', params[:search6])
    query_descuentos = query_descuentos.where('[venta].RutaId = ?', params[:search])
    query_descuentos = query_descuentos.where('[venta].VendedorId = ?', params[:vendedor]) if params[:vendedor].present?
    descuentos = query_descuentos.sum('DV.DescMon * ((CONVERT(FLOAT,(P.IVA))/100)+1)').round(2)




    importe_obsequio = (obsequio_cajas + obsequio_piezas).to_f.round(2)

    sql_inv_i = "SELECT SUM(Precio) FROM (
                  SELECT Stock * CONVERT(float, (
                    SELECT MAX(Precio) Precio FROM
                              (SELECT CASE WHEN PrecioMax > PrecioMin THEN PrecioMax ELSE PrecioMin END AS Precio, Articulo FROM DetalleLP WHERE ListaId IN (
                                SELECT RelCliLis.ListaP FROM RelClirutas INNER JOIN RelCliLis ON RelClirutas.IdCliente = RelCliLis.CodCliente INNER JOIN ListaP ON RelCliLis.ListaP = ListaP.id GROUP BY RelClirutas.IdRuta, RelCliLis.ListaP, ListaP.Tipo HAVING RelClirutas.IdRuta = StockHistorico.RutaId
                              ) AND IdEmpresa = StockHistorico.IdEmpresa) B
                              WHERE Articulo = StockHistorico.Articulo
                              GROUP BY Articulo
                  )) Precio From StockHistorico
                  WHERE IdEmpresa = #{params[:search6]}
                  AND DiaO = #{params[:diaO]}
                  AND RutaId = #{params[:search]}
                  AND Stock > 0
                ) A"
    inventario_inicial = ActiveRecord::Base.connection.select_value(sql_inv_i).to_f

    sql_inv_i = "SELECT SUM(Precio) FROM (
                  SELECT Stock * CONVERT(float, (
                    SELECT MAX(Precio) Precio FROM
                              (SELECT CASE WHEN PrecioMax > PrecioMin THEN PrecioMax ELSE PrecioMin END AS Precio, Articulo FROM DetalleLP WHERE ListaId IN (
                                SELECT RelCliLis.ListaP FROM RelClirutas INNER JOIN RelCliLis ON RelClirutas.IdCliente = RelCliLis.CodCliente INNER JOIN ListaP ON RelCliLis.ListaP = ListaP.id GROUP BY RelClirutas.IdRuta, RelCliLis.ListaP, ListaP.Tipo HAVING RelClirutas.IdRuta = Stock.Ruta
                              ) AND IdEmpresa = Stock.IdEmpresa) B
                              WHERE Articulo = Stock.Articulo
                              GROUP BY Articulo
                  )) Precio From Stock
                  WHERE IdEmpresa = #{params[:search6]}
                  AND Ruta = #{params[:search]}
                  AND Stock > 0
                ) A"
    inventario_final = ActiveRecord::Base.connection.select_value(sql_inv_i).to_f

    result = {
      :contado => contado,
      :credito => credito,
      :total => total,
      :promocion_cajas => promocion_cajas,
      :promocion_piezas => promocion_piezas,
      :importe_promocion => importe_promocion,
      :obsequio_cajas => obsequio_cajas,
      :obsequio_piezas => obsequio_piezas,
      :importe_obsequio => importe_obsequio,
      :descuentos => descuentos,
      :inventario_inicial => inventario_inicial,
      :inventario_final => inventario_final
    }
  end



    def self.busqueda_general(params)
      query = select('[venta].Id,[venta].FormaPag,[venta].RutaId,[venta].IdEmpresa,[venta].VendedorId,[venta].CodCliente,[venta].Documento,[venta].Fecha AS Fecha_Venta,[venta].TipoVta,[venta].DiasCred,[venta].CreditoDispo,[venta].Saldo,[venta].Fvence,[venta].SubTotal,[venta].IVA,[venta].IEPS,[venta].TOTAL,
                [venta].Cancelada, COUNT([detallevet].DescMon) AS venta_count,
                  SUM(detallevet.Precio) AS sum_Precio,
                  SUM(detallevet.DescMon) AS sum_DescMon,
                  ISNULL((SELECT DISTINCT SUM(detallevet.Pza) FROM (SELECT DISTINCT Articulo, Descripcion, Precio, Pza, Kg, DescPorc, DescMon, Tipo, Docto, Importe, IVA, IEPS, RutaId FROM DetalleVet) detallevet WHERE detallevet.RutaId=venta.RutaId AND detallevet.Docto=venta.Documento AND Tipo=0),0) AS sum_Cja,
                    ISNULL((SELECT DISTINCT SUM(detallevet.Pza) FROM (SELECT DISTINCT Articulo, Descripcion, Precio, Pza, Kg, DescPorc, DescMon, Tipo, Docto, Importe, IVA, IEPS, RutaId FROM DetalleVet) detallevet WHERE detallevet.RutaId=venta.RutaId AND detallevet.Docto=venta.Documento AND Tipo= 1),0) AS sum_Pza,
                   [rutas].Ruta, [clientes].Nombre as Responsable, [clientes].NombreCorto as NombreComercial, [formapago].Forma, COALESCE([Ve].Nombre, \'--\') AS Vendedor, COALESCE([Ayudante1].Nombre, \'--\') AS Ayudante1, COALESCE([Ayudante2].Nombre, \'--\') AS Ayudante2, Empresas.Sucursal AS Sucursal')
           .joins("left outer join detallevet ON venta.Documento=detallevet.Docto and venta.RutaId=detallevet.RutaId LEFT JOIN rutas ON venta.RutaId = rutas.IdRutas LEFT JOIN clientes ON venta.CodCliente = clientes.IdCli LEFT JOIN formaspag formapago ON venta.formapag = formapago.IdFpag LEFT JOIN Vendedores Ayudante1 ON Ayudante1.IdVendedor = Venta.ID_Ayudante1 AND Ayudante1.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ayudante2 ON Ayudante2.IdVendedor = Venta.ID_Ayudante2 AND Ayudante2.Tipo LIKE '%AYUDANTE%' LEFT JOIN Vendedores Ve ON Ve.IdVendedor = [venta].VendedorId INNER JOIN Empresas ON Empresas.IdEmpresa = [venta].IdEmpresa")
           .group("venta.Id, venta.RutaId, venta.IVA, venta.Saldo, venta.IEPS, rutas.Ruta, clientes.Nombre, clientes.NombreCorto, formapago.Forma, Empresas.Sucursal, venta.IdEmpresa")
           .group(:FormaPag, :VendedorId, :CodCliente, :Documento, :Fecha, :TipoVta, :DiasCred, :CreditoDispo, :Fvence, :SubTotal, :TOTAL, :Cancelada, 'Ayudante1.Nombre', 'Ayudante2.Nombre', 'Ve.Nombre')
           .where("(venta.RutaId = :rutaId or :rutaId = '' or :rutaId IS NULL) AND (detallevet.Articulo = :articulo or :articulo = '')  AND (venta.CodCliente = :codcliente or :codcliente = '') AND (venta.TipoVta != 'Obsequio') AND (venta.IdEmpresa = :sucursal or :sucursal = '')",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], sucursal: params[:search6]})
           .distinct
      query = query.where('venta.DiaO = ?  AND venta.VendedorId = ?', params[:diaO], params[:vendedor_id]) if params[:diaO].present?
      query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?

      query = query.where("(venta.CodCliente LIKE :Cliente or :Cliente = '') AND (clientes.Nombre LIKE :Responsable or :Responsable = '') AND (clientes.NombreCorto LIKE :NombreComercial or :NombreComercial = '') AND (venta.Documento LIKE :Documento or :Documento = '') AND (venta.TipoVta LIKE :TipoVta or :TipoVta = '') AND (formapago.Forma LIKE :Forma or :Forma = '')",
      {Cliente: "%#{params[:Cliente_param]}%", Responsable: "%#{params[:Responsable_param]}%", NombreComercial: "%#{params[:Nombre_Comercial_param]}%", Documento: "%#{params[:Folio_param]}%", TipoVta: "%#{params[:Tipo_param]}%", Forma: "%#{params[:Metodo_de_pago_param]}%"})
      query

    end

  def self.rep_bitiempos_busqueda_general_para_totales(params)
    query = where('Cancelada = 0')
    query = query.where("RutaId = ?", params[:search])
    query = query.where("IdEmpresa = ?", params[:search6])
    query = query.where("DiaO = ?", params[:diaO])
    query = query.where('VendedorId = ?',params[:vendedor_id])
  end

  def self.exportar_excel_reporte_venta(params)
    query = select("Venta.Fecha AS Fecha_Venta, Venta.RutaId, Venta.CodCliente,Clientes.Nombre as Responsable, Clientes.NombreCorto as Nombre_Comercial,  Venta.Documento as Folio, Venta.TipoVta, FormasPag.Forma as Metodo_Pago, Venta.Cancelada, Venta.ID_Ayudante1, DetalleVet.Articulo, DetalleVet.Descripcion, DetalleVet.Pza as Cantidad,case  when DetalleVet.Tipo = 0  then 'Cajas'  else  'Piezas' end as Unidad_Medida, DetalleVet.Precio,DetalleVet.IVA,
    ISNULL(DetalleVet.Importe, 0) + ISNULL(DetalleVet.IVA, 0) AS Sub_Total, DetalleVet.DescMon, ISNULL(DetalleVet.Precio, 0) + ISNULL(DetalleVet.IVA, 0) - ISNULL(DetalleVet.DescMon, 0) AS Total")
         .joins('INNER JOIN Clientes ON Venta.CodCliente = Clientes.IdCli AND Venta.IdEmpresa = Clientes.IdEmpresa INNER JOIN FormasPag ON Venta.FormaPag = FormasPag.IdFpag LEFT OUTER JOIN DetalleVet ON Venta.Documento = DetalleVet.Docto AND Venta.RutaId = DetalleVet.RutaId')
         .where("(Venta.RutaId = :rutaId or  :rutaId = '' or :rutaId IS NULL) AND (DetalleVet.Articulo = :articulo or :articulo = '')  AND (Venta.CodCliente = :codcliente or :codcliente = '') AND (Venta.IdEmpresa = :idempresa)",{rutaId: params[:search], articulo: params[:search3], codcliente: params[:search2], idempresa: params[:search6]})
    query = query.where('Venta.Fecha BETWEEN ? AND ?', (params[:search4].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search5].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search4].present? and params[:search5].present?
    query = query.where('Venta.DiaO = ?', params[:diaO]) if params[:diaO].present?
    query = query.order('Venta.CodCliente')
    query
  end


#   def
#     SELECT Venta.Fecha AS Fecha_Venta, Venta.RutaId, Venta.CodCliente,Clientes.Nombre as Responsable, Clientes.NombreCorto as 'Nombre Comercial',  Venta.Documento as Folio, Venta.TipoVta,FormasPag.Forma as  'Metodo de Pago',DetalleVet.Articulo, DetalleVet.Descripcion, DetalleVet.Pza as Cantidad,case  when DetalleVet.Tipo = 0  then 'Cajas'  else  'Piezas' end as  'Unidad de Medida',DetalleVet.Precio,DetalleVet.IVA,ISNULL(DetalleVet.Importe, 0) + ISNULL(DetalleVet.IVA, 0) AS 'Sub-Total'
#     FROM Venta
#     INNER JOIN Clientes ON Venta.CodCliente = Clientes.IdCli AND Venta.IdEmpresa = Clientes.IdEmpresa INNER JOIN    FormasPag ON Venta.FormaPag = FormasPag.IdFpag LEFT OUTER JOIN  DetalleVet ON Venta.Documento = DetalleVet.Docto AND Venta.RutaId = DetalleVet.RutaId
#     WHERE (Venta.RutaId = '21') AND (Venta.Fecha BETWEEN '20171012' and '20171013')
#     ORDER BY Venta.CodCliente
#   end


   def self.total_ventas_contado(params)
     query = select('SUM(detallevet.Importe+detallevet.IVA) AS suma_TOTAL')
          .joins('left outer join detallevet ON venta.Documento=detallevet.Docto and venta.RutaId=detallevet.RutaId')
          .group(:Id,:RutaId,:Documento,:Fecha,:TipoVta)
          .where("(venta.RutaId = :rutaId or  :rutaId = '' or :rutaId IS NULL) AND (venta.TipoVta = 'Contado') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
          .distinct
     query = query.where('venta.Fecha >= ? AND venta.Fecha <= ?', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?
     query
    end



    def self.rep_bitiempos_ventaTotal(params)
      query = select("ISNULL(SUM(ISNULL(Ventas.Importe, 0) + ISNULL(Ventas.IVA, 0)), 0) AS VentaTotal")
              .joins('INNER JOIN Ventas ON Venta.Documento = Ventas.Documento AND Ventas.CodCliente = Venta.CodCliente AND Ventas.RutaId = Venta.RutaId AND Ventas.DiaO = Venta.DiaO')
              .where("(Ventas.Cancelada = 0) AND (Ventas.RutaId = :rutaId) AND (Ventas.Fecha BETWEEN :fechainicial AND :fechafinal) AND (Venta.IdEmpresa = :idempresa)",{rutaId: params[:search], fechainicial: (params[:Fecha].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:Fecha].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
    end


    def self.rep_bitiempos_ventaCredito(params)
      query = select("ISNULL(SUM(ISNULL(Ventas.Importe, 0) + ISNULL(Ventas.IVA, 0)), 0) AS VentaCredito")
              .joins('INNER JOIN Ventas ON Venta.Documento = Ventas.Documento AND Ventas.CodCliente = Venta.CodCliente AND Ventas.RutaId = Venta.RutaId AND Ventas.DiaO = Venta.DiaO')
              .where("(Ventas.TipoVta = 'Credito') AND (Ventas.Cancelada = 0) AND (Ventas.RutaId = :rutaId) AND (Ventas.Fecha BETWEEN :fechainicial AND :fechafinal) AND (Venta.IdEmpresa = :idempresa)",{rutaId: params[:search], fechainicial: (params[:Fecha].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:Fecha].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
    end

    def self.rep_bitiempos_ventaContado(params)
      query = select("ISNULL(SUM(ISNULL(Ventas.Importe, 0) + ISNULL(Ventas.IVA, 0)), 0) AS VentaContado")
              .joins('INNER JOIN Ventas ON Venta.Documento = Ventas.Documento AND Ventas.CodCliente = Venta.CodCliente AND Ventas.RutaId = Venta.RutaId AND Ventas.DiaO = Venta.DiaO')
              .where("(Ventas.TipoVta = 'Contado') AND (Ventas.Cancelada = 0) AND (Ventas.RutaId = :rutaId) AND (Ventas.Fecha BETWEEN :fechainicial AND :fechafinal) AND (Venta.IdEmpresa = :idempresa)",{rutaId: params[:search], fechainicial: (params[:Fecha].try(:to_date).try(:beginning_of_day)).try(:strftime,"%Y-%m-%d %T"), fechafinal: (params[:Fecha].try(:to_date).try(:end_of_day)).try(:strftime,"%Y-%m-%d %T"), idempresa: params[:search6]})
    end


    def self.dashboard_VentasContado_VS_VentasCredito(params)
      query = select("Venta.TipoVta,CAST(fecha AS DATE) as Fecha, ISNULL(SUM(DetalleVet.Importe+dbo.DetalleVet.iva),0) as MontoVenta")
              .joins('INNER JOIN DetalleVet ON Venta.Documento = DetalleVet.Docto AND Venta.RutaId = DetalleVet.RutaId AND Venta.IdEmpresa = DetalleVet.IdEmpresa INNER JOIN Rutas ON Venta.RutaId = Rutas.IdRutas')
              .where("Venta.Cancelada=0 AND (Venta.RutaId = :rutaId or  :rutaId = '' or :rutaId IS NULL) AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search0]})
              .group('CAST(fecha AS DATE),Venta.TipoVta')
              .order('CAST(fecha AS DATE)')

      query = query.where('(venta.Fecha BETWEEN ? AND ?)', (params[:search1].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:search2].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:search1].present? and params[:search2].present?

    end

    def self.rep_liquidaciones_tabla_credito(params)
      query = select("Venta.Fecha, de.Articulo, de.Descripcion, de.Precio, de.Pza AS Cantidad, Venta.Kg AS Litros, (CASE ISNULL(de.Tipo,0) WHEN 0 THEN 'Cajas' ELSE 'Piezas' END) AS Unidad,de.descMon AS Descuento, DE.Importe+DE.IVA AS Total, Productos.Ban_Envase")
              .distinct
              .joins('INNER JOIN  DetalleVet AS de ON Venta.Documento = de.Docto AND Venta.RutaId = de.RutaId INNER JOIN  Rutas AS ru ON Venta.RutaId = ru.IdRutas INNER JOIN  Productos ON de.Articulo = Productos.Clave LEFT OUTER JOIN ProductosXPzas AS P ON de.Articulo = P.Producto')
              .where("Venta.Cancelada <> '1' AND (ru.IdRutas = :rutaId or  :rutaId = '' or :rutaId IS NULL) AND (Venta.TipoVta='Credito') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search6]})

      query = query.where('(Venta.Fecha BETWEEN ? AND ?)', (params[:fechaDiaO].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:fechaDiaO].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:fechaDiaO].present?
    end

    def self.rep_liquidaciones_tabla_contado(params)
      query = select("Venta.Fecha, de.Articulo, de.Descripcion, de.Precio, de.Pza AS Cantidad, Venta.Kg AS Litros, (CASE ISNULL(de.Tipo,0) WHEN 0 THEN 'Cajas' ELSE 'Piezas' END) AS Unidad,de.descMon AS Descuento, DE.Importe+DE.IVA AS Total, Productos.Ban_Envase")
              .distinct
              .joins('INNER JOIN  DetalleVet AS de ON Venta.Documento = de.Docto AND Venta.RutaId = de.RutaId INNER JOIN  Rutas AS ru ON Venta.RutaId = ru.IdRutas INNER JOIN  Productos ON de.Articulo = Productos.Clave LEFT OUTER JOIN ProductosXPzas AS P ON de.Articulo = P.Producto')
              .where("Venta.Cancelada <> '1' AND (ru.IdRutas = :rutaId or  :rutaId = '' or :rutaId IS NULL) AND (Venta.TipoVta='Contado') AND (venta.IdEmpresa = :idempresa)",{rutaId: params[:search], idempresa: params[:search6]})

      query = query.where('(Venta.Fecha BETWEEN ? AND ?)', (params[:fechaDiaO].to_date.beginning_of_day).strftime('%Y-%m-%d %T'), (params[:fechaDiaO].to_date.end_of_day).strftime('%Y-%m-%d %T')) if params[:fechaDiaO].present?
    end



end
