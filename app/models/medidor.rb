class Medidor < ActiveRecord::Base
  self.table_name = 'Medidores' #para la gema ransack pueda encontrar la tabla(esto parece que solo pasa en sqlserver)
  self.primary_key = 'IdRow'

  scope :ruta, ->(params) { where(IdRuta: params[:search])} # model
  scope :dia, ->(params) { where(DiaO: params[:diaO])} # model
  scope :por_empresa, ->(params) { where(IdEmpresa: params[:search6]) } # model


  def self.datos_consumo(params)
    query = select("[medidores].IdRow, [medidores].IdRuta, [medidores].OdometroInicial, [medidores].OdometroFinal, [medidores].KmR, [medidores].TanqueInicial, [medidores].TanqueFinal, ISNULL([medidores].LitrosCargados, 0) as LitrosCargados") #LOWER para colocar el valor en minusculas
            .where("(medidores.IdRuta = :rutaId or :rutaId = '') AND (medidores.DiaO = :diaO or :diaO = '') AND (medidores.IdEmpresa = :idempresa)",{rutaId: params[:search], diaO: params[:diaO], idempresa: params[:search6]})
    query = query.where('medidores.IdRow LIKE ?', "%#{params[:IdRow_param]}%") if params[:IdRow_param].present?
    query = query.where('medidores.OdometroInicial LIKE ?', "%#{params[:OdometroInicial_p]}%") if params[:OdometroInicial_p].present?
    query = query.where('medidores.OdometroFinal LIKE ?', "%#{params[:OdometroFinal_param]}%") if params[:OdometroFinal_param].present?
    query = query.where('medidores.KmR LIKE ?', "%#{params[:KmR_param]}%") if params[:KmR_param].present?
    query = query.where('medidores.TanqueInicial LIKE ?', "%#{params[:TanqueInicial_param]}%") if params[:TanqueInicial_param].present?
    query = query.where('medidores.TanqueFinal LIKE ?', "%#{params[:TanqueFinal_param]}%") if params[:TanqueFinal_param].present?
    query = query.where('medidores.LitrosCargados LIKE ?', "%#{params[:LitrosCargados_param]}%") if params[:LitrosCargados_param].present?
    query
  end

end
